<?php

namespace App\Listeners;

use Illuminate\Mail\Events\MessageSent;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LogSentMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Illuminate\Mail\Events\MessageSent  $event
     * @return void
     */
    public function handle(MessageSent $event)
    {
        DB::table('_maillog')->insert(
            [
                'to' => (isset($event->data['user']->email)) ? $event->data['user']->email : '-',
                'subject' => $event->message->getSubject(),
                'message' => $event->message->getBody(),
                'files' => "",
                'order_id' => 0,
                'created_at' => date("Y-m-d H:i:s")
            ]
        );
    }
}