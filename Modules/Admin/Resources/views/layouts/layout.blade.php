<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>{{config('app.name')}} | @if(isset($title)) {{$title}} @endif</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @foreach(config('assets.stylesheet') as $css)
        <link rel="stylesheet" href="{{$css}}">
    @endforeach

<!--end::Base Styles -->
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel="stylesheet" href="/assets/css/jquery.nestable.css">
</head>
<!-- end::Head -->
<?php
$cm = \Illuminate\Support\Facades\Cache::tags(['user-settings'])->get(Auth::id() . '-compact-mode');
if ($cm == 1) {
    $body_class = "m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas
m-footer--push m-aside--offcanvas-default m-brand--minimize m-aside-left--minimize";
} else {
    $body_class = "m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas
m-footer--push m-aside--offcanvas-default";
}
?>
<style>
    #loading-wrapper {
        position: fixed;
        width: 100%;
        height: 100%;
        left: 0;
        top: 0;
        background-color: #000000db;
        opacity: 0.8;
        z-index: 1051;
        /* display: none;
         */
    }

    #loading-text {
        display: block;
        position: absolute;
        top: 50%;
        left: 50%;
        color: white;
        width: 100px;
        height: 30px;
        margin: -7px 0 0 -45px;
        text-align: center;
        font-size: 20px;
        font-weight: 600;
    }

    #loading-content {
        display: block;
        position: relative;
        left: 50%;
        top: 50%;
        width: 170px;
        height: 170px;
        margin: -85px 0 0 -85px;
        border: 3px solid #f00;
    }

    #loading-content:after {
        content: "";
        position: absolute;
        border: 3px solid #0f0;
        left: 15px;
        right: 15px;
        top: 15px;
        bottom: 15px;
    }

    #loading-content:before {
        content: "";
        position: absolute;
        border: 3px solid #00f;
        left: 5px;
        right: 5px;
        top: 5px;
        bottom: 5px;
    }

    #loading-content {
        border: 3px solid transparent;
        border-top-color: #ef5350;
        border-bottom-color: #ef5350;
        border-radius: 50%;
        -webkit-animation: loader 2s linear infinite;
        -moz-animation: loader 2s linear infinite;
        -o-animation: loader 2s linear infinite;
        animation: loader 2s linear infinite;
    }

    #loading-content:before {
        border: 3px solid transparent;
        border-top-color: #0b9654;
        border-bottom-color: #0b9654;
        border-radius: 50%;
        -webkit-animation: loader 3s linear infinite;
        -moz-animation: loader 2s linear infinite;
        -o-animation: loader 2s linear infinite;
        animation: loader 3s linear infinite;
    }

    #loading-content:after {
        border: 3px solid transparent;
        border-top-color: black;
        border-bottom-color: black;
        border-radius: 50%;
        -webkit-animation: loader 1.5s linear infinite;
        animation: loader 1.5s linear infinite;
        -moz-animation: loader 2s linear infinite;
        -o-animation: loader 2s linear infinite;
    }
    .cke_toolbar_break {
        display: block;
        clear: none !important;
    }
    .has-danger .form-control, .has-danger .input-group-btn, .has-danger .cke {
        border-color: red !important;
    }
    .has-danger .form-control, .has-danger label  {
        color: red;
    } .has-danger .form-control, .has-danger label:after  {
              content: "\f133";
              font-family: Flaticon;
              font-style: normal;
              font-weight: 400;
              font-variant: normal;
              line-height: 1;

              text-decoration: inherit;
              text-rendering: optimizeLegibility;
              text-transform: none;
              -moz-osx-font-smoothing: grayscale;
              -webkit-font-smoothing: antialiased;
              font-smoothing: antialiased;
              font-size: 18px;
              padding-left: 12px;
    }
    @-webkit-keyframes loaders {
        0% {
            -webkit-transform: rotate(0deg);
            -ms-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    @keyframes loader {
        0% {
            /* -webkit-transform: rotate(0deg);
             */
            -ms-transform: rotate(0deg);
            /* transform: rotate(0deg);
             */
        }
        100% {
            -webkit-transform: rotate(360deg);
            -ms-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    #content-wrapper {
        color: #1cff39;
        position: fixed;
        left: 0;
        top: 20px;
        width: 100%;
        height: 100%
    }

    /* end loader*/
    .sorting{
        cursor:pointer;
    }
</style>

<body class="{{$body_class}}">

<div id="loading-wrapper" style=" display: none; ">
    <div id="loading-text">{{__('Mentés..')}}</div>
    <div id="loading-content"></div>
</div>
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
@include('admin::partials.header')
<!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
            <i class="la la-close"></i>
        </button>
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
            <!-- BEGIN: Aside Menu -->
            <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
                 m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
                <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                    <?php
                   $menu = config('menu');

                    $first = 1;
                    $routeName = optional(Route::getFacadeRoot()->current())->getName();
                    ?>
                    @foreach($menu as $item)
                        @if (is_array($item['route']) && is_array($item['route'][0]))
                            <?php $class = ''; ?>
                            <?php $permission = false; ?>
                            @foreach($item['route'] as $rt)

                                @if($rt['route'] == $routeName)
                                    <?php $class = 'm-menu__item--open m-menu__item--active'; ?>
                                @endif
                                <?php if (\App\Http\Controllers\Controller::checkAccessForUri($rt['route'])) $permission = true; ?>
                            @endforeach
                            @if ($permission)
                                <li class="m-menu__item  m-menu__item--submenu {{$class}}" aria-haspopup="true"
                                    m-menu-submenu-toggle="hover">
                                    <a href="javascript:;" class="m-menu__link m-menu__toggle">
                                        <i class="m-menu__link-icon {{$item['icon']}}"></i>
                                        <span class="m-menu__link-text">
										{{$item['title']}}
									</span>
                                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                                    </a>
                                    <div class="m-menu__submenu ">
                                        <span class="m-menu__arrow"></span>
                                        <ul class="m-menu__subnav">
                                            <li class="m-menu__item  m-menu__item--parent" aria-haspopup="true">
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													{{$item['title']}}
												</span>
											</span>
                                            </li>
                                            @foreach($item['route'] as $rt)
                                                @if(is_array($rt['route']))
                                                    <?php
                                                    $route = $rt['route'][0];
                                                    $params = $rt['route'][1];
                                                    ?>
                                                @else
                                                    <?php
                                                    $route = $rt['route'];
                                                    $params = [];
                                                    ?>
                                                @endif
                                                @if (\App\Http\Controllers\Controller::checkAccessForUri($rt['route']))
                                                    <li class="m-menu__item @if($route == $routeName) {{$class}} @endif"
                                                        aria-haspopup="true">
                                                        <a href="{{\App\Http\Controllers\Controller::getSafeRoute($route, $params)}}"
                                                           class="m-menu__link ">
                                                            <i class="m-menu__link-icon {{$rt['icon']}}"></i>
                                                            <span class="m-menu__link-text">
													{{__(''.$rt['title'])}}
												</span>
                                                        </a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>
                                <?php
                                $first++;
                                ?>
                            @endif
                        @else
                            @if(is_array($item['route']))
                                <?php
                                $route = $item['route'][0];
                                $params = $item['route'][1];
                                ?>
                            @else
                                <?php
                                $route = $item['route'];
                                $params = [];
                                ?>
                            @endif
                            @if (\App\Http\Controllers\Controller::checkAccessForUri($route))
                                <li class="m-menu__item  @if($route == $routeName) m-menu__item--active @endif"
                                    aria-haspopup="true">
                                    <a href="{{\App\Http\Controllers\Controller::getSafeRoute($route, $params)}}"
                                       class="m-menu__link ">
                                        <i class="m-menu__link-icon {{$item['icon']}}"></i>
                                        <span class="m-menu__link-title">
                                            <span class="m-menu__link-wrap">
                                                <span class="m-menu__link-text">
                                                    {{__(''.$item['title'])}}
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </li>
                                <?php
                                $first++;
                                ?>
                            @endif
                        @endif
                        @if (isset($item['divider']) && \Illuminate\Support\Facades\Auth::user()->type == 0)
                            <li class="m-menu__section ">
                                <h4 class="m-menu__section-text">
                                    {{__($item['divider'])}}
                                </h4>
                                <i class="m-menu__section-icon flaticon-more-v3"></i>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
            <!-- END: Aside Menu -->
        </div>
        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title ">
                            @if(isset($title)) {{$title}} @endif
                        </h3>
                    </div>
                </div>
            </div>
            <!-- END: Subheader -->
            <div class="m-content">
                @include('admin::modals.notifications')
                @yield('content')

                @include('admin::modals.support_modal')
                @include('admin::modals.cropper_modal')
            </div>
        </div>
    </div>
    <!-- end:: Body -->
    <!-- begin::Footer
    <footer class="m-grid__item		m-footer ">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								{{date("Y")}} &copy; {{config('app.name')}} system by
								<a href="{{config('app.company_url')}}" class="m-link">
									® {{config('app.company')}}
								</a>
							</span>
                </div>
            </div>
        </div>
    </footer>
     end::Footer -->
</div>

<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>
<!-- end::Scroll Top -->
@include('admin::partials.sidebar')
@foreach(config('assets.javascript') as $js)
	<script src="{{$js}}" type="text/javascript"></script>
@endforeach
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script src="/assets/js/jquery.doubleScroll.js"></script>
<script>
    var baseUrl = '<?php echo '/';?>';
    var siteUrl = '<?php echo '/';?>';
</script>
<?php
if(isset($controller)){
$load = str_replace("Controller", "", $controller);

$load = strtolower($load);
}else{
    $load="";
}
?>
@include('admin::js.base')
<?php
$modules = Module::allEnabled();
?>
@foreach($modules as $module)
    @if(file_exists(base_path().'/Modules/'.$module->name.'/Resources/views/js/'.$load.'.blade.php'))
        @include(strtolower($module->name).'::js.'.$load)
    @endif
@endforeach
@yield('modals')
@include('admin::modals.widget_modal')

@stack('scripts')

</body>
</html>
