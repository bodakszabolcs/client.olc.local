<?php

namespace Tests;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Modules\Acl\Entities\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\User\Entities\User;
use Modules\User\Entities\UserBilling;
use Modules\User\Entities\UserShipping;
use Tests\TestCase as BaseTestCase;

class UserUnitTest extends BaseTestCase
{

    public function testCreateUserSuccess()
    {
        $usr = new User();
        $usr->lastname = 'Lastname';
        $usr->firstname = 'Firstname';
        $usr->email = Str::random(10).'@example.com';
        $usr->password = Hash::make(Str::random(10));
        $usr->save();

        $us = UserShipping::firstOrCreate(['user_id' => $usr->id]);
        $ub = UserBilling::firstOrCreate(['user_id' => $usr->id]);

        $this->assertTrue(!is_null($usr->id));
        $this->assertTrue(!is_null($us->id));
        $this->assertTrue(!is_null($ub->id));
    }

}
