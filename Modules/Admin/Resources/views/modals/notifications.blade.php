<span id="notification-wrapper"></span>
@if (Session::has('error_message'))
    <?php
    $msg_class = 'danger';
    $msg_text = Session::get('error_message');
    ?>
    <div class=" noty m-alert m-alert--icon alert alert-{{$msg_class}}" role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-danger"></i>
        </div>
        <div class="m-alert__text">
            <strong>
                {{__('Hiba történt!')}}
            </strong>
            {!! $msg_text !!}
        </div>
        <div class="m-alert__actions" >
            <button type="button" class="btn btn-outline-light btn-sm m-btn m-btn--hover-brand"
                    data-dismiss="alert" aria-label="Close">
                {{__('Bezárás')}}
            </button>
        </div>
    </div>
@endif
@if (Session::has('info_message'))
    <?php
    $msg_class = 'info';
    $msg_text = Session::get('info_message');
    ?>
    <div class="noty m-alert  m-alert--icon alert alert-{{$msg_class}}" role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-danger"></i>
        </div>
        <div class="m-alert__text">
            <strong>
                {{__('Figyelem!')}}
            </strong>
            {!! $msg_text !!}
        </div>
        <div class="m-alert__actions" >
            <button type="button" class="btn btn-outline-light btn-sm m-btn m-btn--hover-brand"
                    data-dismiss="alert" aria-label="Close">
                {{__('Bezárás')}}
            </button>
        </div>
    </div>
@endif
@if (Session::has('warning_message'))
    <?php
    $msg_class = 'warning';
    $msg_text = Session::get('warning_message');
    ?>
    <div class="noty m-alert  m-alert--icon alert alert-{{$msg_class}}" role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-danger"></i>
        </div>
        <div class="m-alert__text">
            <strong>
                {{__('Vigyázat!')}}
            </strong>
            {!! $msg_text !!}
        </div>
        <div class="m-alert__actions" >
            <button type="button" class="btn btn-outline-light btn-sm m-btn m-btn--hover-brand"
                    data-dismiss="alert" aria-label="Close">
                {{__('Bezárás')}}
            </button>
        </div>
    </div>
@endif
@if (Session::has('success_message'))
    <?php
    $msg_class = 'success';
    $msg_text = Session::get('success_message');
    ?>
    <div class="noty m-alert m-alert--icon alert alert-{{$msg_class}}" role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-danger"></i>
        </div>
        <div class="m-alert__text">
            <strong>
                {{__('Sikeres!')}}
            </strong>
            {!! $msg_text !!}
        </div>
        <div class="m-alert__actions" >
            <button type="button" class="btn btn-outline-light btn-sm m-btn m-btn--hover-brand"
                    data-dismiss="alert" aria-label="Close">
                {{__('Bezárás')}}
            </button>
        </div>
    </div>
@endif
<style>
    .noty {
        position: fixed;
        top: 73px;
        max-width: 400px;
        right: 32px;
        z-index: 999;
    }
</style>