<?php

	namespace Modules\Company\Entities;

	use App\Http\Helper;
	use GuzzleHttp\Client;
	use GuzzleHttp\RequestOptions;
	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Database\Eloquent\SoftDeletes;
	use Illuminate\Support\Facades\Log;
	use Mockery\Exception;
	use App\LiquidModel;
	use Sunra\PhpSimple\HtmlDomParser;
	use GeneaLabs\LaravelModelCaching\Traits\Cachable;

	class Company extends LiquidModel
	{
		use SoftDeletes, Cachable;

		protected $table = 'companies';

		protected $dates = ['deleted_at', 'created_at', 'updated_at'];
		/**
		 * The attributes that are mass assignable.
	 	*
		 * @var array
	 	*/
		protected $fillable = [
			'name', //Név 
			'tax_number', //Adószám 
			'email', //Email
			'phone', //Telefonszám
			'shipping_city',
			'shipping_address',
			'shipping_country',
			'shipping_zip'

		];

		public $searchColumns = [
			'id' => 'text',
			'name'	=>	'text', 
			'tax_number'	=>	'text', 
			'email'	=>	'text',
			'phone'	=>	'text', 

			];

		public $searchColumnLabels = [
			'id' => 'ID',
			'name'	=>	'Cég neve', 
			'tax_number'	=>	'Adószám', 
			'email'	=>	'Email',
			'phone'	=>	'Telefonszám', 

			];
		public function getName()
		{
			return $this->name;
		}

		public function getTax_number()
		{
			return $this->tax_number;
		}


		public function getShipping_country()
		{
			return $this->shipping_country;
		}


		public static function updateCompanyData($data){
			$client = new Client();
			try{
				$postData = Helper::encodeData($data,'company');
				$response = $client->post( env('SERVER_URL') . '/api/update-company',
					[
						RequestOptions::JSON =>
							['data' => $postData]
					],
					['Content-Type' => 'application/json']);
			}catch (\Exception $e){
				Log::info($e->getMessage());
			}
		}
	}
?>
