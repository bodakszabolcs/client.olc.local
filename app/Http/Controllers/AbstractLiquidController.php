<?php

namespace App\Http\Controllers;

use App\Contracts\BaseControllerInterface;
use App\Http\Requests\BaseExportRequest;
use App\Role;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Routing\Controller;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Modules\Admin\Entities\VirtualUrl;
use App\User;

class AbstractLiquidController extends Controller implements BaseControllerInterface
{
    protected $model;
    protected $resource;
    protected $table;
    protected $pagination = true;
    protected $useResourceAsCollection = false;

    protected $successStatus = 200;
    protected $successMessage = 'OK';
    protected $errorStatus = 403;
    protected $errorMessage = 'ERROR';

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->resource = JsonResource::class;
        $this->collection = ResourceCollection::class;
    }

    /**
     * @param Request $request
     * @param array $extraSearch
     * @return mixed
     */
    public function index(Request $request, $extraSearch = [])
    {
        foreach ($extraSearch as $key => $val) {
            $this->model->searchColumns[$key] = array('select' => $val);
        }

        $list = $this->model->searchInModel($request->input());

        if ($this->pagination) {
            $pagination = (int)array_get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }

        if (!$this->useResourceAsCollection) {
            return new $this->collection($list);
        }

        return $this->resource::collection($list);
    }

    /**
     * @param $request
     * @return mixed
     */
    public function __create(FormRequest $request)
    {
        $this->model->fillAndSave($request->all());

        return new $this->resource($this->model);
    }

    /**
     * @param Request $request
     * @param int $id
     * @param null $auth
     * @return mixed
     */
    public function show(Request $request, $id = 0, $auth = null)
    {
        try {
            if ($auth == null) {
                $this->model = $this->model->where('id', '=', $id)->firstOrFail();
            } else {
                $this->model = $this->model->where([
                    ['id', '=', $id],
                    ['user_id', '=', $auth]
                ])->firstOrFail();
            }
        } catch (ModelNotFoundException $e) {
            $this->model = new $this->model();
        }

        return new $this->resource($this->model);
    }

    /**
     * @param $request
     * @param $id
     * @param null $auth
     * @return \Illuminate\Http\JsonResponse
     */
    public function __update(FormRequest $request, $id, $auth = null)
    {
        try {
            if ($auth == null) {
                $this->model = $this->model->where('id', '=', $id)->firstOrFail();
            } else {
                $this->model = $this->model->where([
                    ['id', '=', $id],
                    ['user_id', '=', $auth]
                ])->firstOrFail();
            }
        } catch (ModelNotFoundException $e) {
            return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
        }
        $this->model->fillAndSave($request->all());

        return new $this->resource($this->model);
    }

    /**
     * @param $id
     * @param null $auth
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id, $auth = null)
    {
        try {
            if ($auth == null) {
                $this->model = $this->model->where('id', '=', $id)->firstOrFail();
            } else {
                $this->model = $this->model->where([
                    ['id', '=', $id],
                    ['user_id', '=', $auth]
                ])->firstOrfail();
            }
        } catch (ModelNotFoundException $e) {
            return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
        }
        $this->model->delete();

        return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
    }

    public function export(BaseExportRequest $request)
    {
        return $this->model->export(array_get($request, 'headers', []), array_get($request, 'filter', []), array_get($request, 'ids', []), array_get($request, 'exportType', 'view'));
    }

    public static function checkAccessForUri($uri, $userId = null)
    {
        if (Cache::has('isSuperAdmin' . $userId)) {
            return true;
        }

        if (is_array($uri)) {
            $uri = $uri[0];
        }

        $aclCache = Cache::get('aclCache' . $userId, array());

        if (in_array($uri, $aclCache)) {
            return true;
        }

        $user = User::whereemail($userId)->first();

        $can = false;
        foreach ($user->roles as $role) {
            if ($role->name == "superadmin") {
                Cache::put('isSuperAdmin' . $userId, true, now()->addMinutes(15));
                return true;
            }
            foreach ($role->acls as $acls) {
                if ($acls->path == $uri) {
                    $aclCache[$acls->path] = $acls->path;
                    $can = true;
                }
            }
        }

        Cache::put('aclCache' . $userId, $aclCache, now()->addMinutes(15));
        return $can;
    }

    public function getStringBetween($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) {
            return '';
        }
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
}
