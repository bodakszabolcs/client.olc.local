<?php

namespace Modules\Admin\Entities;

use App\LiquidModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VirtualUrl extends LiquidModel
{
    use SoftDeletes, Cachable;

    protected $table = 'virtual_url';

    protected $dates =['deleted_at','created_at','updated_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_url',
        'to_url',
        'headers',
        'code'
    ];

    public $searchColumns = [
        'id' => 'text',
        'from_url' => 'text',
        'to_url' => 'text',
        'code' => 'text',

    ];

    public $searchColumnLabels = [
        'id' => 'ID',
        'from_url' => 'Honnan',
        'to_url' => 'Hova',
        'code' => 'Kód',

    ];

    public function __construct($attributes = [])
    {
        config(['laravel-model-caching.cache-prefix' => str_slug(config('app.name'))]);

        parent::__construct($attributes);
    }
}
