<?php

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Admin\Entities\VirtualUrl;
use Tests\TestCase as BaseTestCase;

class VirtualUrlUnitTest extends BaseTestCase
{

    public function testCreateVirtualUrlSuccess()
    {
        $vu = new VirtualUrl();
        $vu->from_url = "/from";
        $vu->to_url = "/to";
        $vu->headers = "";
        $vu->code = "";
        $vu->save();

        $this->assertTrue(!is_null($vu->id));
    }

}
