<?php

namespace App\Http;

use App\Http\Controllers\Controller;
use Faker\Provider\DateTime;
use Illuminate\Support\Facades\Session;

class Helper
{

    private static function attrBuilder($attributes = array())
    {
        $str = '';
        foreach ($attributes as $key => $value) {
            $str .= $key . '="' . $value . '" ';
        }
        return $str;
    }
    public static function decodeData($data){
        $data = substr($data,3).'=';
        return json_decode(base64_decode($data),true);
    }
    public static function encodeData($data,$key){
	    return rand(100, 999) . str_replace('=', '', base64_encode(json_encode([
			    $key => $data,'url'=>env('APP_URL'), 'date' => date('YmdHis')
		    ])));
	}
    public static  function modalFooter($close = 'Bezár',$save='',$attributes =[],$submit=false )
    {
    	$html='';
	    $attributes["class"] = 'btn  btn-outline-success m-btn m-btn--icon '.array_get($attributes, 'class', '');
    	if(!empty($save)) {
    		if($submit) {
			    $html .= '<button type="submit" ' . self::attrBuilder($attributes) . '>' . $save . '<i class="fa fa-check animated rotateIn"></i></button>';
		    }else{
			    $html .= '<a  ' . self::attrBuilder($attributes) . '>' . $save . '<i class="fa fa-check animated rotateIn"></i></a>';
		    }
	    }
	    $html .='<a  class="btn btn-outline-danger 	m-btn m-btn--icon" data-dismiss="modal">'.$close.' <i class="fa fa-close animated rotateIn"></i></a>';

    	return $html;
    }
    public static function dateDiff($first,$sec)
    {
	    $date1=date_create($first);
	    $date2=date_create($sec);
	    return date_diff($date1,$date2)->format('%a');
    }
	public static function bool($bool)
	{
		if($bool)
		{
			return __("Igen");
		}
		return __("Nem");
	}
    public static function formButtons($redirect = 'admin_dashboard', $sac = true, $close = true,$arg=[],$param='')
    {

        $buttons = '<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-sm-8 order-sm-2 m--align-right">
							<button class="btn m-btn--square btn-success" name="save_and_exit" type="submit">' . __('Mentés') . '</button>';

        if ($sac) {
            $buttons .= "<button style='margin-left: 5px' class='btn m-btn--square btn-warning' type='submit'>" . __('Mentés és folytatás') . "</button>";
        }

        $buttons .= "</div>
						<div class='col-sm-4 order-sm-1'>";


        if ($close) {
            $buttons .= "<a href='" . Session::get('redirect_url', route($redirect,$arg).$param) . "' class='btn m-btn--square btn-danger'>" . __('Mégse') . "</a>";
        }

        $buttons .= "</div>
					</div>
				</div>
			</div>";

        return $buttons;
    }

    public static function formOpen($name, $method, $action = '', $attributes = array())
    {
        $attributes["id"] = array_get($attributes, 'id', 'form-' . $name);
        $attributes["class"] = array_get($attributes, 'class', 'm-form');
        $attributes["enctype"] = array_get($attributes, 'enctype', 'application/x-www-form-urlencoded');
        $attributes["role"] = array_get($attributes, 'role', 'form');

        if (strtoupper($method) == 'GET' || strtoupper($method) == 'POST') {

	        $html = '<form  autocomplete="off" name="' . $name . '" ' . self::attrBuilder($attributes) . ' method="' . $method . '"';
            if ($action != '') $html .= ' action="' . $action . '"';
            $html .= '>';
        } else {
            $html = '<form name="' . $name . '" ' . self::attrBuilder($attributes) . ' method="POST"';
            if ($action != '') $html .= ' action="' . $action . '"';
            $html .= '>';
            $html .= '<input autocomplete="off" name="_method" type="hidden" value="' . $method . '">';
        }
        $html .= csrf_field();

        return $html;
    }

    public static function formClose()
    {
        return '</form>';
    }

	public static function date($date)
	{
		try {
			if($date == '0000-00-00' || $date =='0000-00-00 00:00:00'){
				return "";
			}
			if (\DateTime::createFromFormat('Y-m-d', $date) !== FALSE) {
				return date('Y.m.d', strtotime($date));
			}
		}
		catch(\Exception $e)
		{
		    return "";
		}
		return "";
	}

    public static function input($label, $field, $value = NULL, $attributes = array(), $errors = array(), $help = '', $prefix = NULL, $suffix = NULL)
    {


        $attributes["id"] = array_get($attributes, 'id', 'form-' . $field);
        $attributes["type"] = array_get($attributes, 'type', 'text');
        $attributes["class"] = array_get($attributes, 'class', 'form-control');
        $attributes["maxlength"] = array_get($attributes, 'maxlength', '255');
        $attributes["placeholder"] = array_get($attributes, 'placeholder');
        $attributes["div_class"] = array_get($attributes, 'div_class');
        $attributes["label_class"] = array_get($attributes, 'label_class');
        $attributes["has_label"] = array_get($attributes, 'has_label', true);
        if (is_null($errors)) {
            $has_error = false;
        } else {
            if (is_array($errors)) {
                $has_error = array_get($errors, $field, FALSE);
            } else {
                $has_error = $errors->has($field);
            }
        }

        $html = '<div class="form-group ' . $attributes['div_class'] . ' ' . ($has_error != FALSE ? 'has-danger' : '') . '">';


        if ($attributes['has_label'] && !empty($label)) {
            $html .= '<label for="' . $attributes["id"] . '" class="' . $attributes['label_class'] . '">' . $label . '</label>';
        } else {
            if (empty($attributes["placeholder"])) $attributes['placeholder'] = $label;
        }

        if (!empty($help)) {
            $html .= '<span class="m-form__help">' . $help . '</span>';
        }

        if ($prefix || $suffix) {
            $html .= '<div class="input-group">';
        }
        if ($prefix) {
            $html .= '<span class="input-group-addon">' . $prefix . '</span>';
        }
        if (!empty($attributes['label_class'])) {
            $html .= '<div class="col-7">';
        }
        $html .= '<input autocomplete="off" name="' . $field . '" value="' . $value . '" ' . self::attrBuilder($attributes) . ' />';

        if (!empty($has_error) && is_array($has_error)) {
            $attributes["class"] .= " is-invalid";
            foreach ($has_error as $s) {
                $html .= '<small class="text-danger">' . $s . '</small>';
            }
        } else if ($has_error == true) {
            $attributes["class"] .= " is-invalid";
            foreach ($errors->get($field) as $s) {
                $html .= '<small class="text-danger">' . $s . '</small>';
            }
        }

        if ($suffix) {
            $html .= '<span class="input-group-addon">' . $suffix . '</span>';
        }
        if ($prefix || $suffix) {
            $html .= '</div>';
        }
        if (!empty($attributes['label_class'])) {
            $html .= '</div>';
        }

        $html .= '</div>';
        return $html;
    }

    public static function text($label, $field, $value = NULL, $attributes = array(), $errors = array(), $prefix = null, $suffix = NULL)
    {

        $attributes["id"] = array_get($attributes, 'id', 'form-' . $field);
        $attributes["type"] = array_get($attributes, 'type', 'text');
        $attributes["class"] = array_get($attributes, 'class', 'form-control');
        $attributes["maxlength"] = array_get($attributes, 'maxlength', '255');
        $attributes["placeholder"] = array_get($attributes, 'placeholder');
        $has_error = array_get($errors, $field, FALSE);

        $html = '<div class="form-group ' . ($has_error != FALSE ? 'has-danger' : '') . '">
								<label>' . $label . '</label>';

        if (!empty($help)) {
            $html .= '<span class="m-form__help">' . $help . '</span>';
        }

        if ($prefix || $suffix) {
            $html .= '<div class="input-group">';
        }
        if ($prefix) {
            $html .= '<span class="input-group-addon">' . $prefix . '</span>';
        }
        $html .= '<span ' . self::attrBuilder($attributes) . '>' . $value . '</span>';
        if ($suffix) {
            $html .= '<span class="input-group-addon">' . $suffix . '</span>';
        }
        if ($prefix || $suffix) {
            $html .= '</div>';
        }
        if (!empty($has_error)) {
            foreach ($has_error as $s) {
                $html .= '<small class="text-danger">' . $s . '</small>';
            }

        }
        $html .= '</div>';
        return $html;
    }

    public static function fileSelector($label, $field, $type = 'browse-file', $value = NULL, $attributes = array(), $errors = array())
    {
        $attributes["id"] = array_get($attributes, 'id', 'unisharp-' . $field);
        $attributes["class"] = array_get($attributes, 'class', 'form-control');
        $attributes["data-preview"] = array_get($attributes, 'data-preview', 'holder-' . $field);

        $has_error = array_get($errors, $field, FALSE);

        $html = '<div class="form-group ' . ($has_error != FALSE ? 'has-danger' : '') . '">
								<label>' . $label . '</label>';

	    $html .= '<div class="custom-file">
									<input accept="image/*" type="file" '.self::attrBuilder($attributes).' class="custom-file-input '.$attributes['class'].'" id="customFile" name="' . $field . '">
									<label class="custom-file-label" for="'. $attributes["id"].'" style="font-weight: 400">
										Fájl kiválasztása
									</label>
								</div>
                           
                        </div>';

        if (!empty($has_error)) {
            foreach ($has_error as $s) {
                $html .= '<small class="text-danger">' . $s . '</small>';
            }

        }
        $html .= '';
        return $html;
    }
	public static function fileSelectorBrowser($label, $field, $type = 'browse-file', $value = NULL, $attributes = array(), $errors = array())
	{
		$attributes["id"] = array_get($attributes, 'id', 'unisharp-' . $field);
		$attributes["class"] = array_get($attributes, 'class', 'form-control');
		$attributes["data-preview"] = array_get($attributes, 'data-preview', 'holder-' . $field);

		$has_error = array_get($errors, $field, FALSE);

		$html = '<div class="form-group ' . ($has_error != FALSE ? 'has-danger' : '') . '">
								<label>' . $label . '</label>';

		$html .= '<div class="input-group">
                           <span class="input-group-btn">
                             <a data-input="' . $attributes['id'] . '" data-preview="' . $attributes['data-preview'] . '" class="btn btn-warning ' . $type . '">
                               <i class="fa fas fa-picture-o"></i> ' . __('Választás') . '
                             </a>
                           </span>
                            <input value="' . $value . '" ' . self::attrBuilder($attributes) . ' type="text" name="' . $field . '">
                        </div>';

		if (!empty($has_error)) {
			foreach ($has_error as $s) {
				$html .= '<small class="text-danger">' . $s . '</small>';
			}

		}
		$html .= '</div>';
		return $html;
	}
    public static function inputInlineLabel($label, $field, $value = NULL, $attributes = array(), $errors = array(), $help = '', $label_col = 2, $input_col = 10, $prefix = NULL, $suffix = NULL)
    {
        $attributes["id"] = array_get($attributes, 'id', 'form-' . $field);
        $attributes["type"] = array_get($attributes, 'type', 'text');
        $attributes["class"] = array_get($attributes, 'class', 'form-control');
        $attributes["maxlength"] = array_get($attributes, 'maxlength', '255');
        $attributes["placeholder"] = array_get($attributes, 'placeholder');
        $has_error = array_get($errors, $field, FALSE);
        $html = '<div class="form-group m-form__group">
								<label>' . $label . '</label>';

        if (!empty($help)) {
            $html .= '<span class="m-form__help">' . $help . '</span>';
        }

        if ($prefix || $suffix) {
            $html .= '<div class="input-group">';
        }
        if ($prefix) {
            $html .= '<span class="input-group-addon">' . $prefix . '</span>';
        }
        $html .= '<input autocomplete="off" type="text" name="' . $field . '" value="' . $value . '" ' . self::attrBuilder($attributes) . ' />';
        if ($suffix) {
            $html .= '<span class="input-group-addon">' . $suffix . '</span>';
        }
        if ($prefix || $suffix) {
            $html .= '</div>';
        }
        $html .= '</div>';
        return $html;
    }

    public static function price($number)
    {
        if (empty($number) || !is_numeric($number)) {
            return 0;
        } else {
            return number_format($number, 0, ' ', ' ');
        }
    }

    public static function file($label, $field, $attributes = array(), $errors = array(), $help = '')
    {
        $attributes["id"] = array_get($attributes, 'id', 'form-' . $field);
        $attributes["class"] = array_get($attributes, 'class', 'custom-file-input');
        $attributes["placeholder"] = array_get($attributes, 'placeholder', $label);
        $has_error = array_get($errors, $field, FALSE);
        //var_dump($has_error);
        $help_block = '';
        if (!empty($help)) {
            $help_block = '<span class="m-form__help">' . $help . '</span>';
        }

        $html = '<div class="form-group m-form__group form-group-file ' . ($has_error != FALSE ? 'has-danger' : '') . '">
								<label class="btn btn-primary" for="form-' . $field . '">' . $label . '</label>

								<div class="custom-file" >
								<input autocomplete="off" name="' . $field . '" type="file" ' . self::attrBuilder($attributes) . ' />
									' . $help_block . '
								';
        if (!empty($has_error)) {
            foreach ($has_error as $s) {
                $html .= '<small class="text-danger">' . $s . '</small>';
            }

        }
        $html .= '</div>
							</div>';
        return $html;
    }

    public static function password($label, $field, $value = NULL, $attributes = array(), $help = '', $errors = [])
    {
        $attributes["id"] = array_get($attributes, 'id', 'form-' . $field);
        $attributes["type"] = array_get($attributes, 'type', 'text');
        $attributes["class"] = array_get($attributes, 'class', 'form-control');
        $attributes["maxlength"] = array_get($attributes, 'maxlength', '255');
        $attributes["autocomplete"] = array_get($attributes, 'autocomplete', 'off');
        $attributes["div_class"] = array_get($attributes, 'div_class');
        $attributes["label_class"] = array_get($attributes, 'label_class');
        $attributes["has_label"] = array_get($attributes, 'has_label', true);

        if (is_null($errors)) {
            $has_error = false;
        } else {
            if (is_array($errors)) {
                $has_error = array_get($errors, $field, FALSE);
            } else {
                $has_error = $errors->has($field);
            }
        }

        $html = '<div class="form-group ' . $attributes['div_class'] . ' ' . ($has_error != FALSE ? 'has-danger' : '') . '">';


        if ($attributes['has_label']) {
            $html .= '<label for="' . $attributes["id"] . '" class="' . $attributes['label_class'] . '">' . $label . '</label>';
        } else {
            if (empty($attributes["placeholder"])) $attributes['placeholder'] = $label;
        }

        if (!empty($help)) {
            $html .= '<span class="m-form__help">' . $help . '</span>';
        }

        if (!empty($attributes['label_class'])) {
            $html .= '<div class="col-7">';
        }

        $html .= '<input autocomplete="off" type="password" name="' . $field . '" ' . self::attrBuilder($attributes) . ' />';

        if (!empty($has_error) && is_array($has_error)) {
            $attributes["class"] .= " is-invalid";
            foreach ($has_error as $s) {
                $html .= '<small class="text-danger">' . $s . '</small>';
            }
        } else if ($has_error == true) {
            $attributes["class"] .= " is-invalid";
            foreach ($errors->get($field) as $s) {
                $html .= '<small class="text-danger">' . $s . '</small>';
            }
        }

        if (!empty($attributes['label_class'])) {
            $html .= '</div>';
        }

        $html .= '</div>';
        return $html;
    }

    public static function calendar($label, $field, $value = NULL, $attributes = array(), $label_col = 2, $input_col = 10)
    {
        $attributes["id"] = array_get($attributes, 'id', 'form-' . $field);
        $attributes["type"] = array_get($attributes, 'type', 'text');
        $attributes["class"] = array_get($attributes, 'class', 'form-control date-picker datepicker');
        $attributes["maxlength"] = array_get($attributes, 'maxlength', '255');
        $attributes["readonly"] = array_get($attributes, 'readonly', true);
        $attributes["placeholder"] = array_get($attributes, 'placeholder', '1970.01.01');

        $html = '<div class="form-group">
								<label>' . $label . '</label>
								<input autocomplete="off" type="text" name="' . $field . '" value="' . $value . '" ' . self::attrBuilder($attributes) . ' />
							</div>';

        return $html;
    }

    public static function calendar_time($label, $field, $value = NULL, $attributes = array(), $label_col = 2, $input_col = 10)
    {
        $attributes["id"] = array_get($attributes, 'id', 'form-' . $field);
        $attributes["type"] = array_get($attributes, 'type', 'text');
        $attributes["class"] = array_get($attributes, 'class', 'form-control datetime-picker datetimepicker');
        $attributes["maxlength"] = array_get($attributes, 'maxlength', '255');
        //$attributes["readonly"] = array_get($attributes, 'readonly', true);


        $html = '<div class="form-group">
								<label>' . $label . '</label>
								<input autocomplete="off" type="text" name="' . $field . '" value="' . $value . '" ' . self::attrBuilder($attributes) . ' />
							</div>';

        return $html;
    }

    public static function save($name, $label = 'Mentés', $attributes = array(), $theme = 'blue')
    {
        $attributes["id"] = array_get($attributes, 'id', 'validate');
        $attributes["class"] = array_get($attributes, 'class', 'btn ' . $theme);

        return '<input autocomplete="off" type="submit" value="' . $label . '" ' . self::attrBuilder($attributes) . ' />';
    }

    public static function cancel($name, $label = 'Mégsem', $attributes = array(), $theme = 'yellow')
    {
        $attributes["id"] = array_get($attributes, 'id', 'cancel');
        $attributes["class"] = array_get($attributes, 'class', 'btn ' . $theme);
        $attributes["href"] = array_get($attributes, 'href', 'javascript:void(0);');
        $attributes["name"] = $name;

        return '<a ' . self::attrBuilder($attributes) . '>' . $label . '</a>';
    }

    public static function radio($label, $name, $options = array("1" => "Igen", "0" => "Nem"), $value = NULL)
    {
        $radio_html = '';
        foreach ($options as $val => $option) {
            $radio_html .= '<input autocomplete="off" type="radio" id="' . $name . '_' . $val . '" name="' . $name . '" value="' . $val . '" ' . (($value == $val) ? 'checked' : '') . ' />';
            $radio_html .= '<label for="' . $name . '_' . $val . '">' . $option . '</label>';
        }

        $html = '<div class="form-group">
						<div class="radio radio-success">' . $radio_html . '</div>
						<h5>' . $label . '</h5>
					</div>';

        return $html;
    }

    public static function onlySelect($name, $options, $value, $attributes = array(), $errors = array())
    {
        $has_error = array_get($errors, $name, FALSE);
        $attributes["id"] = array_get($attributes, 'id', 'form-' . $name);
        $attributes["class"] = array_get($attributes, 'class', 'custom-select form-control');
        $attributes["data-generated"] = array_get($attributes, 'data-generated', 'true');

        $options_gen = '';
        foreach ($options as $key => $val) {
            $sel = false;
            if (is_array($value)) {
                foreach ($value as $vk) {
                    if ($key == $vk) {
                        $sel = true;
                    }
                }
            } else {
                if ($key == $value) {
                    $sel = true;
                }
            }
            $options_gen .= '<option value="' . $key . '" ' . (($sel) ? 'selected' : '') . '>' . $val . '</option>';
        }

        $html = '<select name="' . $name . '" ' . self::attrBuilder($attributes) . '>' . $options_gen . '</select>';
        if (!empty($has_error)) {
            foreach ($has_error as $s) {
                $html .= '<small class="text-danger">' . $s . '</small>';
            }

        }


        return $html;
    }

    public static function select($label, $name, $options, $value, $attributes = array(), $errors = array())
    {
        $has_error = array_get($errors, $name, FALSE);
        $attributes["id"] = array_get($attributes, 'id', 'form-' . $name);
        $attributes["class"] = array_get($attributes, 'class', 'custom-select form-control');
        $attributes["div_class"] = array_get($attributes, 'div_class');
        $attributes["label_class"] = array_get($attributes, 'label_class');
        $attributes["data-generated"] = array_get($attributes, 'data-generated', 'true');

        if (is_null($errors)) {
            $has_error = false;
        } else {
            if (is_array($errors)) {
                $has_error = array_get($errors, $name, FALSE);
            } else {
                $has_error = $errors->has($name);
            }
        }

        $options_gen = '';
        foreach ($options as $key => $val) {
            $sel = false;
            if (is_array($value)) {
                foreach ($value as $vk) {
                    if ($key.'' === $vk.'') {
                        $sel = true;
                    }
                }
            } else {
                if ($key.'' === $value.'') {
                    $sel = true;
                }
            }
            $options_gen .= '<option value="' . $key . '" ' . (($sel) ? 'selected' : '') . '>' . $val . '</option>';
        }

        $html = '<div class="form-group ' . $attributes['div_class'] . ($has_error != FALSE ? 'has-danger' : '') . ((strrpos($attributes["class"], 'select2') === false) ? '' : ' form-group-default-select2') . '">
                        <label class="' . $attributes['label_class'] . '">' . $label . '</label>';

        if (!empty($attributes['label_class'])) {
            $html .= '<div class="col-7">';
        }

        $html .= '<select name="' . $name . '" ' . self::attrBuilder($attributes) . '>' . $options_gen . '</select>';

        if (!empty($has_error) && is_array($has_error)) {
            $attributes["class"] .= " is-invalid";
            foreach ($has_error as $s) {
                $html .= '<small class="text-danger">' . $s . '</small>';
            }
        } else if ($has_error == true) {
            $attributes["class"] .= " is-invalid";
            foreach ($errors->get($name) as $s) {
                $html .= '<small class="text-danger">' . $s . '</small>';
            }
        }

        if (!empty($attributes['label_class'])) {
            $html .= '</div>';
        }

        $html .= '</div>';

        return $html;
    }

    public static function tags($label, $name, $value, $attributes = array(), $errors = array())
    {
        $has_error = array_get($errors, $name, FALSE);
        $attributes["id"] = array_get($attributes, 'id', 'tags');
        $attributes["class"] = array_get($attributes, 'class', 'form-control');
        $attributes["data-generated"] = array_get($attributes, 'data-generated', 'true');

        $options = [];
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $options[$v] = $v;
            }
        }
        $options_gen = '';
        foreach ($options as $key => $val) {
            $sel = false;
            if (is_array($value)) {
                foreach ($value as $vk) {
                    if ($key == $vk) {
                        $sel = true;
                    }
                }
            } else {
                if ($key == $value) {
                    $sel = true;
                }
            }
            $options_gen .= '<option value="' . $key . '" ' . (($sel) ? 'selected' : '') . '>' . $val . '</option>';
        }

        $html = '<div class="form-group ' . ($has_error != FALSE ? 'has-danger' : '') . ((strrpos($attributes["class"], 'select2') === false) ? '' : ' form-group-default-select2') . ' " style=" display: table-caption;">
                        <label class="">' . $label . '</label>
		        <select name="' . $name . '" ' . self::attrBuilder($attributes) . '>' . $options_gen . '</select>';
        if (!empty($has_error)) {
            foreach ($has_error as $s) {
                $html .= '<small class="text-danger">' . $s . '</small>';
            }

        }
        $html .= '</div>';

        return $html;
    }

    public static function selectInlineLabel($label, $name, $options, $value, $attributes = array())
    {
        $attributes["id"] = array_get($attributes, 'id', 'form-' . $name);
        $attributes["class"] = array_get($attributes, 'class', 'custom-select form-control form-control');
        $attributes["data-generated"] = array_get($attributes, 'data-generated', 'true');

        $options_gen = '';
        foreach ($options as $key => $val) {
            $sel = false;
            if (is_array($value)) {
                foreach ($value as $vk) {
                    if ($key.'' === $vk.'') {
                        $sel = true;
                    }
                }
            } else {
                if ($key.'' === $value.'') {
                    $sel = true;
                }
            }
            $options_gen .= '<option value="' . $key . '" ' . (($sel) ? 'selected' : '') . '>' . $val . '</option>';
        }

        $html = '<div class="form-group form-group-default form-group-default-select2x" style=" display: table-caption;">
                        <label class="" >' . $label . '</label>
		        <select name="' . $name . '" ' . self::attrBuilder($attributes) . '>' . $options_gen . '</select>
		</div>';

        return $html;
    }

    public static function checkbox($label, $field, $value, $attributes = array(), $errors = '')
    {
        unset($attributes['id']);
        $has_error = array_get($errors, $field, FALSE);
        $html = '<div class="m-form__group form-group ' . ($has_error != FALSE ? 'has-danger' : '') . '">';
        $html .= '<div class="m-checkbox-list">';
        $html .= '<label class="m-checkbox m-checkbox--square" for="' . $field . '">';
        $html .= '<input autocomplete="off" type="checkbox" id="' . $field . '" ' . self::attrBuilder($attributes) . ' name="' . $field . '" value="' . ((!empty($value)) ? $value : 1) . '" ' . ((!empty($value)) ? 'checked' : '') . ' />';
        $html .= $label . '<span></span></label>';

        if (!empty($has_error)) {
            foreach ($has_error as $s) {
                $html .= '<small class="text-danger">' . $s . '</small>';
            }

        }
        $html .= '</div></div>';

        return $html;
    }


    public static function checkboxCategory($label, $field, $value, $attributes = array(), $errors = '')
    {

        unset($attributes['id']);
        $has_error = array_get($errors, $field, FALSE);
        $html = '<div class="m-form__group form-group ' . ($has_error != FALSE ? 'has-danger' : '') . '">';
        $html .= '<div class="m-checkbox-list">';
        $html .= '<label class="m-checkbox m-checkbox--square" for="' . $field . '">';
        $html .= '<input autocomplete="off" type="checkbox" id="' . $field . '" ' . self::attrBuilder($attributes) . ' name="' . $field . '" value="' . ((!empty($value)) ? $value : 1) . '" ' . ((!empty($value)) ? 'checked' : '') . ' />';
        $html .= $label . '<span></span></label>';

        if (!empty($has_error)) {
            foreach ($has_error as $s) {
                $html .= '<small class="text-danger">' . $s . '</small>';
            }

        }
        $html .= '</div></div>';

        return $html;
    }

    public static function textarea($label, $field, $value = NULL, $attributes = array(), $errors = null, $double_encode = TRUE, $label_col = 4, $input_col = 10)
    {
        $attributes["id"] = array_get($attributes, 'id', 'form' . $field);
        $has_error = array_get($errors, $field, FALSE);
        $attributes["class"] = array_get($attributes, 'class', 'form-control');
        $attributes["cols"] = array_get($attributes, 'cols', '');
       //$attributes["rows"] = array_get($attributes, 'rows', 3);
	    $attributes["div_class"] = array_get($attributes, 'div_class');
	    $attributes["label_class"] = array_get($attributes, 'label_class');
	    $attributes["has_label"] = array_get($attributes, 'has_label', true);
        $html = '';
        if (is_null($errors)) {
            $has_error = false;
        } else {
            if (is_array($errors)) {
                $has_error = array_get($errors, $field, FALSE);
            } else {
                $has_error = $errors->has($field);
            }
        }

	    $html = '<div class="form-group ' . $attributes['div_class'] . ' ' . ($has_error != FALSE ? 'has-danger' : '') . '">';


	    if ($attributes['has_label'] && !empty($label)) {
		    $html .= '<label for="' . $attributes["id"] . '" class="' . $attributes['label_class'] . '">' . $label . '</label>';
	    } else {
		    if (empty($attributes["placeholder"])) $attributes['placeholder'] = $label;
	    }
        //$html .= '<label for="' . $attributes["id"] . '">' . $label . '</label>';
	    if (!empty($help)) {
		    $html .= '<span class="m-form__help">' . $help . '</span>';
	    }



	    if (!empty($attributes['label_class'])) {
		    $html .= '<div class="col-10">';
	    }
        $html .= '<textarea name="' . $field . '" ' . self::attrBuilder($attributes) . ' >' . $value . '</textarea>';

        if (!empty($has_error) && is_array($has_error)) {
            $attributes["class"] .= " is-invalid";
            foreach ($has_error as $s) {
                $html .= '<small class="text-danger">' . $s . '</small>';
            }
        } else if ($has_error == true) {
            $attributes["class"] .= " is-invalid";
            foreach ($errors->get($field) as $s) {
                $html .= '<small class="text-danger">' . $s . '</small>';
            }
        }

        $html .= '</div>';

        return $html;
    }

    public static function text_button($label, $url_array, $icon, $attributes = array())
    {
        $routeName = array_get($url_array, 'route_name');
        $params = array_get($url_array, 'params');
	    $attributes["class"] = array_get($attributes, 'class', 'btn btn-primary');
        $url = route($routeName, $params);
        //        $url = Controller::returnUri($routeName, $params);

        if (Controller::checkAccessForURI($routeName) == false) {
            return false;
        }

        $html = '<i class="fa fa ' . $icon . '"></i> ';

        if (!empty($label)) {

	        $attributes['data-content']=$label;
	        $attributes['data-toggle']='m-popover';
        }
        $default ="btn-default";
        if($icon == 'fa-edit')
        {
        	$attributes['class'] = "btn btn-success";
        }
	    else if($icon == 'fa-trash')
	    {
		    $attributes['class'] = "btn btn-danger confirm";
	    }
	    else{
		    $attributes['class'] = (str_replace('dropdown-item','',$attributes['class']))?str_replace('dropdown-item','',$attributes['class']):'btn btn-primary';
	    }


        $attributes["class"] = array_get($attributes, 'class', $default);
        return '<a href="' . $url . '" ' . self::attrBuilder($attributes) . '>' . $html . '</a>';
    }

    public static function button($label, $url_array, $icon, $attributes = array())
    {
        $attributes["title"] = __($label);
        $attributes["class"] = array_get($attributes, 'class', 'btn default');
        return self::text_button(NULL, $url_array, $icon, $attributes);
    }

}
