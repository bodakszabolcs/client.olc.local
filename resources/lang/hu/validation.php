<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| such as the size rules. Feel free to tweak each of these messages.
	|
	*/

	'accepted'             => ':attribute el kell legyen fogadva!',
	'active_url'           => ':attribute nem érvényes url!',
	'after'                => ':attribute legalább :date utáni dátum kell, hogy legyen!',
	'alpha'                => ':attribute kizárólag betűket tartalmazhat!',
	'alpha_dash'           => ':attribute kizárólag betűket, számokat és kötőjeleket tartalmazhat!',
	'alpha_num'            => ':attribute kizárólag betűket és számokat tartalmazhat!',
	'array'                => ':attribute egy tömb kell, hogy legyen!',
	'before'               => ':attribute legalább :date előtti dátum kell, hogy legyen!',
	'between'              => [
		'numeric' => ':attribute :min és :max közötti szám kell, hogy legyen!',
		'file'    => ':attribute mérete :min és :max kilobájt között kell, hogy legyen!',
		'string'  => ':attribute hossza :min és :max karakter között kell, hogy legyen!',
		'array'   => ':attribute :min - :max közötti elemet kell, hogy tartalmazzon!',
	],
	'boolean'              => ':attribute mező csak true vagy false értéket kaphat!',
	'confirmed'            => ':attribute nem egyezik a megerősítéssel.',
	'date'                 => ':attribute nem érvényes dátum.',
	'date_format'          => ':attribute nem egyezik az alábbi dátum formátummal :format!',
	'different'            => ':attribute és :other értékei különbözőek kell, hogy legyenek!',
	'digits'               => ':attribute :digits számjegyű kell, hogy legyen!',
	'digits_between'       => ':attribute értéke :min és :max közötti számjegy lehet!',
	'dimensions'           => ':attribute felbontása nem megfelelő.',
	'distinct'             => ':attribute értékének egyedinek kell lennie!',
	'email'                => ':attribute nem érvényes email formátum.',
	'exists'               => ':attribute már létezik.',
	'file'                 => ':attribute fájl kell, hogy legyen!',
	'filled'               => ':attribute megadása kötelező!',
	'image'                => ':attribute képfájl kell, hogy legyen!',
	'in'                   => 'A kiválasztott :attribute érvénytelen.',
	'in_array'             => ':attribute értéke nem található a(z) :other értékek között.',
	'integer'              => ':attribute értéke szám kell, hogy legyen!',
	'ip'                   => ':attribute érvényes IP cím kell, hogy legyen!',
	'json'                 => ':attribute érvényes JSON szöveg kell, hogy legyen!',
	'max'                  => [
		'numeric' => ':attribute értéke nem lehet nagyobb, mint :max!',
		'file'    => ':attribute mérete nem lehet több, mint :max kilobájt.',
		'string'  => ':attribute hossza nem lehet több, mint :max karakter.',
		'array'   => ':attribute legfeljebb :max elemet kell, hogy tartalmazzon.',
	],
	'mimes'                => ':attribute kizárólag az alábbi fájlformátumok egyike lehet: :values.',
	'mimetypes'            => ':attribute kizárólag az alábbi fájlformátumok egyike lehet: :values.',
	'min'                  => [
		'numeric' => ':attribute értéke nem lehet kisebb, mint :min!',
		'file'    => ':attribute mérete nem lehet kevesebb, mint :min kilobájt.',
		'string'  => ':attribute hossza nem lehet kevesebb, mint :min karakter.',
		'array'   => ':attribute legalább :min elemet kell, hogy tartalmazzon.',
	],
	'not_in'               => ':attribute értéke érvénytelen.',
	'numeric'              => ':attribute szám kell, hogy legyen!',
	'present'              => ':attribute mező nem található!',
	'regex'                => ':attribute formátuma érvénytelen.',
	'required'             => ':attribute megadása kötelező!',
	'required_if'          => ':attribute megadása kötelező, ha a(z) :other értéke :value!',
	'required_unless'      => ':attribute megadása kötelező, ha a(z) :other értéke nem :values!',
	'required_with'        => ':attribute megadása kötelező, ha a(z) :values érték létezik.',
	'required_with_all'    => ':attribute megadása kötelező, ha a(z) :values értékek léteznek.',
	'required_without'     => ':attribute megadása kötelező, ha a(z) :values érték nem létezik.',
	'required_without_all' => ':attribute megadása kötelező, ha egyik :values érték sem létezik.',
	'same'                 => ':attribute és :other mezőknek egyezniük kell!',
	'size'                 => [
		'numeric' => ':attribute értéke :size kell, hogy legyen!',
		'file'    => ':attribute mérete :size kilobájt kell, hogy legyen!',
		'string'  => ':attribute hossza :size karakter kell, hogy legyen!',
		'array'   => ':attribute :size elemet kell tartalmazzon!',
	],
	'string'               => ':attribute szövegnek kell legyen.',
	'timezone'             => ':attribute nem létező időzona.',
	'unique'               => ':attribute már foglalt.',
	'uploaded'             => ':attribute feltöltése sikertelen.',
	'url'                  => ':attribute érvénytelen link.',

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom'               => [
		'attribute-name' => [
			'rule-name' => 'custom-message',
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes'           => [
        'vezeteknev' => 'Vezetéknév',
        'keresztnev' => 'Keresztnév',
        'password' => 'Jelszó',
        'email' => 'Email',
        'email_again' => 'Email újra',
        'telefon' => 'Telefonszám',
        'varos' => 'Város',
        'utca' => 'Utca',
        'iranyitoszam' => 'Irányítószám',
        'megye' => 'Ország'
	],

];
