<?php

	namespace Modules\Invoice\Entities;

	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Database\Eloquent\SoftDeletes;
	use Mockery\Exception;
	use App\LiquidModel;
	use Sunra\PhpSimple\HtmlDomParser;
	use GeneaLabs\LaravelModelCaching\Traits\Cachable;

	class Invoice extends LiquidModel
	{
		use SoftDeletes, Cachable;

		protected $table = 'invoices';

		protected $dates = ['deleted_at', 'created_at', 'updated_at'];
		/**
		 * The attributes that are mass assignable.
	 	*
		 * @var array
	 	*/
		protected $fillable = [
			'transaction_id', //Tranzakció azonosító 
			'deadline', //Fizetési határidő 
			'final_gross', //Bruttó összeg 
			'items', //Tételek 
			'status', //Állapot 
			'payment_method', //Fizetési mód
            'invoice'
		];

		public $searchColumns = [
			'id' => 'text',
			'transaction_id'	=>	'text', 
			'status'	=>	'text', 
			'payment_method'	=>	'text', 
			];

		public $searchColumnLabels = [
			'id' => 'ID',
			'transaction_id'	=>	'Tranzakció azonosító', 
			'status'	=>	'Állapot', 
			'payment_method'	=>	'Fizetési mód', 
			];
		public function getPayment_method()
		{
			return $this->payment_method;
		}

		protected $casts = [
			'payment_method'	=>	'array', 
			];
	}
?>
