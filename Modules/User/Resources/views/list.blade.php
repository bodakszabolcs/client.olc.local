@extends('admin::layouts.layout')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="head-btn">
                        {!! \App\Http\Helper::text_button(__('Új Hozzáadás'),array('route_name' => 'admin_user_edit', 'params' => array()),'fa-plus', array('class'=>'btn m-btn--square btn-primary')) !!}
                        {!! \App\Http\Helper::text_button(__('Lista exportálása'),array('route_name' => 'admin_user_export', 'params' => $params),'fa-save', array('class'=>'btn btn-warning')) !!}
                    </div>
                </div>
                <div class="m-portlet__body">
                    {!! $filterHeader !!}
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover mb-150">
                            <tr>

                                <th class="sorting" data-column="name">{{__('Név')}}</th>
                                <th class="sorting" data-column="email">{{__('Email')}}</th>
                                <th class="sorting" data-column="email">{{__('Üzlet')}}</th>

                                <th class="text-right" >{{__('Műveletek')}}</th>
                            </tr>
                            @foreach($model as $m)
                                <tr>

                                    <td>
                                        <b> {{$m->lastname}} {{$m->firstname}}</b><br>
                                        @foreach($m->roles as $role)
                                            @if (!$loop->first)
                                                <br/>
                                            @endif
                                            {{$role->description}}
                                        @endforeach
                                    </td>
                                    <td>
                                        {{$m->email}}

                                    </td>
                                    <td>
                                        @php
                                            $shop =\Modules\Company\Entities\Shop::where('shop_id','=',$m->shop_id)->first();

                                        @endphp
                                        {{ $shop?$shop->name:'Összes' }}

                                    </td>
                                    <td  class="text-right">

                                                {!! \App\Http\Helper::text_button(__('Szerkesztés'),array('route_name'=>'admin_user_edit', 'params' => array($m->id)),'fa-edit', array('class'=>'dropdown-item')) !!}
                                                {!! \App\Http\Helper::text_button(__('Belépés mint'),array('route_name'=>'admin_user_force_login', 'params' => array($m->id)),'fa-sign-in', array('class'=>'dropdown-item btn btn-info confirm')) !!}
                                                {!! \App\Http\Helper::text_button(__('Felhasználó feloldása'),array('route_name'=>'admin_unban_user', 'params' => array($m->id)),'fa-unlock', array('class'=>'dropdown-item btn btn-warning confirm')) !!}
                                                {!! \App\Http\Helper::text_button(__('Törlés'),array('route_name'=>'admin_user_delete', 'params' => array($m->id)),'fa-trash', array('class'=>'dropdown-item confirm')) !!}

                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    {!! $model->appends($_GET)->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
