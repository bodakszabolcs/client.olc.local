<script type="text/javascript">
    $(document).on('keyup', '#form-search_query', function () {
        var value = $(this).val();

        $('table tr td:nth-child(2)').each(function () {

            if ($(this).text().search(value) > -1) {
                $(this).closest('tr').show();
            }
            else {
                $(this).closest('tr').hide();
            }
        });
    });
    $(document).on('keyup', '#form-search_request', function () {
        var value = $(this).val();

        $('table tr td:nth-child(3)').each(function () {

            if ($(this).text().search(value) > -1) {
                $(this).closest('tr').show();
            }
            else {
                $(this).closest('tr').hide();
            }
        });
    });
</script>
