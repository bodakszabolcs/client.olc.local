<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\User\Entities\User;

class CreateApp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:app';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create base application setup';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $database;
    private $user;
    private $password;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $exist = Storage::disk('base')->exists('.env');

        if ($this->confirm('Did you run: composer:install previously?')) {
            $env_file = Storage::disk('base')->get('.env');

            $url = "";

            do {
                $this->database = $this->ask('Please enter the database name.');
                $this->user = $this->ask('Please enter the database user.');
                $this->password = $this->secret('Please enter the database password.');
                $url = $this->ask('Please enter the application url!');
            } while (empty($this->database) || empty($this->user) || empty($url));

            $cache_driver = $this->choice('Choose cache driver', ['file', 'memcached', 'database', 'redis'], 0);
            $session_driver = $this->choice('Choose session driver', ['file', 'memcached', 'database', 'redis'], 0);

            $env_file = str_replace("[database]", $this->database, $env_file);
            $env_file = str_replace("[user]", $this->user, $env_file);
            $env_file = str_replace("[pass]", $this->password, $env_file);
            $env_file = str_replace("[cache_driver]", $cache_driver, $env_file);
            $env_file = str_replace("[session_driver]", $session_driver, $env_file);

            $env_file = str_replace("[url]", $url, $env_file);

            $htacc_file = Storage::disk('base')->get('.htaccess');

            $url_trunc = explode("//", $url)[1];

            $htacc_file = str_replace("creativecrm.local", $url_trunc, $env_file);

            $this->line('Please wait, while creating .env file!');
            Storage::disk('base')->put('.env', $env_file);
            $this->line('.env file created.');

            Artisan::call('key:generate');
            $this->line('Generating key.');

            try {
                Artisan::call('migrate:reset');
                Artisan::call('module:migrate-reset');
                $this->line("Reset migration.");

                Artisan::call('migrate');
                Artisan::call('module:migrate');
                $this->line("Migrating blueprints.");


            } catch (QueryException $e) {
                Artisan::call('migrate:refresh');
                Artisan::call('module:migrate-refresh');
                $this->line("Refresh migration.");
            } catch (\PDOException $e) {
                Artisan::call('migrate:refresh');
                Artisan::call('module:migrate-refresh');
                $this->line("Refresh migration.");
            }

            $lastname = $firstname = $email = $password = "";

            do {
                $lastname = $this->ask('Please enter your lastname!');
                $firstname = $this->ask('Please enter your firstname!');
                $email = $this->ask('Please enter your email!');
                $password = $this->secret('Please enter your password!');
            } while (empty($lastname) || empty($firstname) || empty($email));

            $user = new User();
            $user->lastname = $lastname;
            $user->firstname = $firstname;
            $user->email = $email;
            $user->password = bcrypt($password);
            $user->save();

            DB::table('role_user')->insert([
                'user_id' => $user->id,
                'role_id' => 1
            ]);

            DB::table('role_user')->insert([
                'user_id' => $user->id,
                'role_id' => 2
            ]);

            Artisan::call('db:seed');
            Artisan::call('module:seed');
            $this->line("Seeding database.");

            Artisan::call('vendor:publish', ['--provider' => 'Recze\SimplePay\SimplepayServiceProvider']);
            Artisan::call('vendor:publish', ['--provider' => 'Recze\SimplePay\SzamlazzServiceProvider']);
            $this->line('Publishing vendors');


            $this->line('Application successfully installed');
        } else {
            $this->line('Please run: composer install command from terminal before installing the application!');
        }
    }
}
