<?php

namespace Modules\Acl\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AclDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('roles')->insert([
            'name' => 'superadmin',
            'description' => 'Super Admin'
        ]);

        DB::table('roles')->insert([
            'name' => 'login',
            'description' => 'Login'
        ]);
    }
}
