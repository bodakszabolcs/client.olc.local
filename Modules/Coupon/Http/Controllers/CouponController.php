<?php

	namespace Modules\Coupon\Http\Controllers;

	use App\Http\Helper;
    use GuzzleHttp\Client;
    use GuzzleHttp\RequestOptions;

    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Log;
    use Mockery\Exception;
    use Modules\Company\Entities\Shop;
    use Modules\Coupon\Entities\AutomaticCoupon;
    use Modules\Coupon\Entities\Coupon;

	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Session;
	use Validator;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\View;
	use Modules\Admin\Http\Controllers\AdminController;

	class CouponController extends AdminController
	{
		public $niceNames = array(
			'user_id'	=>	'Felhasználó',
			'shop_id'	=>	'Bolt',
			'code'	=>	'Kód',
			'type'	=>	'Típus',
			'amount'	=>	'Mennyiség',
			'expire'	=>	'Érvényesség',
			);
		public function __construct()
		{
			View::share('sub_title', __('Kuponok'));
			View::share('subtitle_link', route('admin_coupon_list'));
		}

		public function CouponList(Request $request)
		{
			$model = new Coupon();
			$list = $model->searchInModel($request->input());
			$list = $list->leftJoin(DB::raw('(select user_id, user_name from purchases group by user_id) as us'),'us.user_id','=','coupons.user_id')
				->select(DB::raw('coupons.*,user_name'))->orderBy('used', 'asc')->orderBy('id', 'desc')->paginate(50)->withPath($request->path());
			View::share('title', __('Kuponok lista'));

			return view('coupon::list', [
				'model' => $list, 'filterHeader' => $model->drawSearchTableHeader($request)
			]);

		}

		public function Edit(Request $request, $id = 0,$tab='data')
		{
			View::share('title', __('Kuponok szerkesztés'));

			$model = Coupon::find($id);
            if ($id > 0 && $model->approved ==1) {
                $request->session()->flash('error_message', __('Mivel a felhasználó azonnali értesítést kap a kuponokról így a tartalma nem szerkeszthető'));
                return redirect(route('admin_coupon_list'));
            }
			$errors = [];
			if (!is_null($model)) {

			} else {

				$model = new Coupon();
			}
			if ($request->isMethod('post')) {
				$tab = $request->input('tab', 'data');
				switch ($tab) {
					case 'data': {
						$response = $this->validateData();
						break;
					}
					default:
						$response = $this->validateData();
						break;
				}
				if ($response['status'] == 'success') {
					$model->fill($request->all());
					if(empty($model->code)){
					    $model->code = Coupon::randomCoupon();
                    }
					$model->save();
						$request->session()->flash('success_message', __('A mentés sikeres'));
				} else {
					$request->session()->flash('error_message', __('Hiba a mentés során'));


					Session::put('field_errors', $response['cust_errors']);

					return redirect(route('admin_coupon_edit', [$id, $tab]))->withErrors($response['validator'])->withInput();
				}
				if ($request->has('save_and_exit')) {
					return redirect()->intended(session()->get('redirect_url', route('admin_coupon_list')));
				}
				if ($response['status'] == 'success') {

					return redirect()->intended(route('admin_coupon_edit', ['id' => $model->id,'tab'=>$tab]));
				}
			}

			return view('coupon::edit', ['model' => $model, 'id' => $id, 'tab' => $tab, 'errors' => $errors]);
		}
		public function validateData()
		{

			$rules = [
			'user_id'	=>	'required',  
			'shop_id'	=>	'required',
			'type'	=>	'required',  
			'amount'	=>	'required',  
			'expire'	=>	'required',  

			];
			$validator = Validator::make(Input::all(), $rules);
			$validator->setAttributeNames($this->niceNames);
			$not_valid = $validator->fails();
			if ($not_valid) {
				return ['status' => 'error', 'cust_errors' => $validator->messages()->toArray(), 'validator' => $validator];
			} else {
				return ['status' => 'success', 'message' => 'A mentés sikeres'];
			}
		}
        public function automaticCoupon(Request $request){
            View::share('title', __('Automatikus kupongenerálási szabályok'));
		    $model =  AutomaticCoupon::all();
            if ($request->isMethod('post')) {

                $data = $request->input('rules',[]);

                foreach ($data as $k =>$v){
                    if((empty($v['value'])  && !empty($v['amount'])) || (!empty($v['value'])  && empty($v['amount'])) ){
                        $request->session()->flash('error_message', __('Hiba a mentés során. Minden adat megadása kötelező'));
                        return redirect()->back()->withInput();
                    }

                }
                foreach ($data as $k =>$v){
                    $m = new AutomaticCoupon();
                    if(!empty($v['id'])){
                        $m = AutomaticCoupon::find($v['id']);
                    }
                    $m->fill($v);
                    $m->save();

                }
                $request->session()->flash('success_message', __('A mentés sikeres'));
                return redirect()->back()->withInput();


            }

            return view('coupon::automatic-list', [
                'model' => $model->toArray()
            ]);
        }
		public function Delete(Request $request, $id)
		{
			try {
				if ($id == 0) {
					throw new ModelNotFoundException();
				}
				$model = Coupon::findOrFail($id);
                if($model->approved==1){
                    throw new ModelNotFoundException();
                }
				$model->delete();
				$request->session()->flash('success_message', __('success_delete'));
			} catch (ModelNotFoundException $e) {
				$request->session()->flash('error_message', __('Az elem nem törölhető'));
			}
			return redirect()->intended(route('admin_coupon_list'));
		}
        public function Approve(Request $request, $id)
        {
            try {
                if ($id == 0) {
                    throw new ModelNotFoundException();
                }
                $model = Coupon::findOrFail($id);
                $model->approved = 1;
                $model->save();
                $client = new Client();
                try {

                    $postData = Helper::encodeData($model->toArray(), 'coupon');
                    $client->post(env('SERVER_URL') . '/api/create-coupon', [
                            RequestOptions::JSON =>
                                ['data' => $postData]
                        ],
                        ['Content-Type' => 'application/json']);
                }
                catch(Exception $e){
                    Log::info($e->getMessage());
                }
                $request->session()->flash('success_message', __('A jóvahagyás sikeres'));
            } catch (ModelNotFoundException $e) {
                $request->session()->flash('error_message', __('Az elem nem található'));
            }
            return redirect()->intended(route('admin_coupon_list'));
        }


	}