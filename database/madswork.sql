-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2018. Sze 06. 19:07
-- Kiszolgáló verziója: 10.1.34-MariaDB
-- PHP verzió: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `madswork`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `accesslog`
--

CREATE TABLE `accesslog` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'Felhasználó',
  `route` varchar(255) NOT NULL COMMENT 'Route',
  `action` varchar(255) NOT NULL COMMENT 'Művelet',
  `extra` text NOT NULL COMMENT 'Extra info',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `lead_image` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Lead kép',
  `end` datetime NOT NULL COMMENT 'Megjenítés vége',
  `publication_date` datetime NOT NULL COMMENT 'Publikálás dátuma',
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Típus',
  `lead` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Lead',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tartalom',
  `tags` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Címkék',
  `facebook` int(11) NOT NULL COMMENT 'Facebook share',
  `twitter` int(11) NOT NULL COMMENT 'Twitter share',
  `google` int(11) NOT NULL COMMENT 'Google share',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Cím',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'URL',
  `twitter_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter url',
  `twitter_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter cím',
  `twitter_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter leírás',
  `twitter_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter kép',
  `twitter_creator` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter létrehozó',
  `og_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Facebook url',
  `og_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Facebook cím',
  `og_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Facebook leírás',
  `og_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Facebook kép',
  `og_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Facebook típus',
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Meta cím',
  `meta_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Meta leírás',
  `meta_keys` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Meta kulcsok',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Aktív',
  `related_content` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Kapcsolódó tartalom',
  `recommended_content` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Ajánlott tartalom',
  `recommended_content_select` int(11) NOT NULL COMMENT 'Ajánlott tartalom forrás',
  `recommended_tags` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Ajánlott címkék',
  `recommended_category` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Ajánlott kategóriák',
  `watch_end` int(11) NOT NULL DEFAULT '0' COMMENT 'Vég figyelembe vétele',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `article_category`
--

CREATE TABLE `article_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(11) NOT NULL COMMENT 'Cikk',
  `category_id` int(11) NOT NULL COMMENT 'Kategória'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `article_item_settings`
--

CREATE TABLE `article_item_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(11) NOT NULL COMMENT 'Cikk',
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Típus',
  `show_article_recommendation` int(11) NOT NULL DEFAULT '0' COMMENT 'Ajánlott tartalom mutatása',
  `show_facebook` int(11) NOT NULL DEFAULT '0' COMMENT 'Facebook oldalsáv',
  `show_related_content` int(11) NOT NULL DEFAULT '0' COMMENT 'Kapcsolódó tartalom',
  `show_recommended_content` int(11) NOT NULL DEFAULT '0' COMMENT 'Ajánlott tartalom',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `article_settings`
--

CREATE TABLE `article_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Típus',
  `show_article_recommendation` int(11) NOT NULL DEFAULT '0' COMMENT 'Ajánlott tartalom mutatása',
  `show_facebook` int(11) NOT NULL DEFAULT '0' COMMENT 'Facebook oldalsáv',
  `show_related_content` int(11) NOT NULL DEFAULT '0' COMMENT 'Kapcsolódó tartalom',
  `show_recommended_content` int(11) NOT NULL DEFAULT '0' COMMENT 'Ajánlott tartalom',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_mobile` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `place` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL COMMENT 'Város',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `cities`
--

INSERT INTO `cities` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Budapest', NULL, '2018-08-14 17:43:56', '2018-08-14 17:43:56'),
(2, 'budapest', '2018-08-14 19:16:51', '2018-08-14 19:16:40', '2018-08-14 19:16:51');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `companies`
--

CREATE TABLE `companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL COMMENT 'Cég neve',
  `tax` text NOT NULL COMMENT 'Adószám',
  `eu_tax` text NOT NULL COMMENT 'Eu adószám',
  `company_number` text NOT NULL COMMENT 'Cégjegyzékszám',
  `delegate` text NOT NULL COMMENT 'Képviselő',
  `delegete_position` text NOT NULL COMMENT 'Képviselő beosztása',
  `comment` text NOT NULL COMMENT 'Megjegyzés',
  `zip` text NOT NULL COMMENT 'Irányítószám',
  `country` text NOT NULL COMMENT 'Ország',
  `city` text NOT NULL COMMENT 'Város',
  `address` text NOT NULL COMMENT 'Utca, házszám',
  `mail_address` text NOT NULL COMMENT 'Postai cím',
  `mailbox` text NOT NULL COMMENT 'Postafiók',
  `contact` text NOT NULL COMMENT 'Kapcsolattartó',
  `business_description` text NOT NULL COMMENT 'Milyen ügyben keressem?',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `companies`
--

INSERT INTO `companies` (`id`, `name`, `tax`, `eu_tax`, `company_number`, `delegate`, `delegete_position`, `comment`, `zip`, `country`, `city`, `address`, `mail_address`, `mailbox`, `contact`, `business_description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Beverly Hills', 'Mbdex', 'Oklol', 'Beverly Corp', 'Kifib', 'Kuzzb', 'Ozqlu', '90210', 'USA', 'Beverly Hills', '7481 Beverly Dr', '7481 Beverly Dr', 'Uzhhp', '1', 'Ascdo', NULL, '2018-07-14 09:03:28', '2018-07-14 09:03:28');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `company_contacts`
--

CREATE TABLE `company_contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL COMMENT 'Név',
  `position` text NOT NULL COMMENT 'Munkakör',
  `email` text NOT NULL COMMENT 'Email',
  `phone` text NOT NULL COMMENT 'Telefon',
  `fax` text NOT NULL COMMENT 'Fax',
  `company_id` text NOT NULL COMMENT 'Cég',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `company_contacts`
--

INSERT INTO `company_contacts` (`id`, `name`, `position`, `email`, `phone`, `fax`, `company_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Beverly Hills', 'Mvffl', 'bhills_5534@mailinator.com', '3105555534', '3105555534', '1', '2018-07-14 13:53:42', '2018-07-14 13:53:32', '2018-07-14 13:53:42'),
(2, 'Beverly Hills', 'Zfcpw', 'bhills_3878@mailinator.com', '3105553878', '3105553878', '1', NULL, '2018-07-14 13:54:01', '2018-07-14 13:54:01'),
(3, 'Beverly Hills', 'Ikkmv', 'bhills_0286@mailinator.com', '3105550286', '3105550286', '1', NULL, '2018-07-14 13:54:15', '2018-07-14 13:54:15'),
(4, 'Beverly Hills', 'Jgxrd', 'bhills_0373@mailinator.com', '3105550373', '3105550373', '1', NULL, '2018-07-14 14:07:57', '2018-07-14 14:07:57');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `company_contract`
--

CREATE TABLE `company_contract` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `type` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Típus',
  `contract_date` date NOT NULL COMMENT 'Szerződéskötés dátuma',
  `contract_type` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Határozott/határozatlan',
  `deadline` text NOT NULL COMMENT 'Fizetési határidő',
  `paper_contract` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Papír alapú szerződés',
  `contact` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'MADS kapcsolattartó (Felelős)',
  `payment_type` text NOT NULL COMMENT 'Elszámolás típusa',
  `summation` text NOT NULL COMMENT 'Átvétel dátumai, összegek',
  `warrantee` text NOT NULL COMMENT 'Garancia',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `company_contract`
--

INSERT INTO `company_contract` (`id`, `company_id`, `type`, `contract_date`, `contract_type`, `deadline`, `paper_contract`, `contact`, `payment_type`, `summation`, `warrantee`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '1900-11-14', 0, 'Yuepw', 0, 1, '0', 'Nysgei lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Afvnr', NULL, '2018-07-14 15:00:24', '2018-08-15 20:12:05'),
(2, 1, 0, '0000-00-00', 0, 'Gyfsm', 0, 1, '0', 'Hjxrdc lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Vbrtl', '2018-07-14 15:01:07', '2018-07-14 15:00:59', '2018-07-14 15:01:07');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `company_documents`
--

CREATE TABLE `company_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `document_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Dokumentum típusa',
  `date` date NOT NULL COMMENT 'Dátum',
  `comment` text COMMENT 'megjegyzés',
  `file` text NOT NULL COMMENT 'Fájl',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `company_documents`
--

INSERT INTO `company_documents` (`id`, `company_id`, `document_id`, `date`, `comment`, `file`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-08-21', 'dsafdasdfdas', 'uploads/1/xlsUu7kLYl1kWjsW81b2ketzwze7Z2Z5bVtDTon7.xlsx', NULL, '2018-08-17 09:01:28', '2018-08-17 09:01:28');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `company_industries`
--

CREATE TABLE `company_industries` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `company_sites_id` int(11) NOT NULL,
  `feor_id` int(11) NOT NULL,
  `teaor_id` int(11) NOT NULL,
  `name` text NOT NULL COMMENT 'Tevékenység neve',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Felelős',
  `work_category` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Kategória',
  `person` text NOT NULL COMMENT 'Utasítást adó személy',
  `aszf` text NOT NULL COMMENT 'A szolgáltatás fogadója adatvédelmi tájékoztatójának és adatkezelési szabályzatának elérhetősége',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `company_industries`
--

INSERT INTO `company_industries` (`id`, `company_id`, `company_sites_id`, `feor_id`, `teaor_id`, `name`, `user_id`, `work_category`, `person`, `aszf`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 1, 1, 'Beverly Hills', 1, 1, 'Skjjn', 'Sivgv', NULL, '2018-08-17 08:08:05', '2018-08-17 08:35:24'),
(2, 1, 0, 1, 1, 'dsafdas', 1, 1, 'dsafdasfasd', 'sdfdsafds', NULL, '2018-08-17 08:19:22', '2018-08-17 08:19:22'),
(3, 1, 1, 1, 1, 'xcyvcyxvcxy', 1, 1, 'sdfdsa', 'dsafdas', NULL, '2018-08-17 08:26:11', '2018-08-17 08:26:11');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `company_sites`
--

CREATE TABLE `company_sites` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` text NOT NULL COMMENT 'Telephely neve',
  `address` text NOT NULL COMMENT 'Munkavégzés helye',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `company_sites`
--

INSERT INTO `company_sites` (`id`, `company_id`, `name`, `address`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 0, 'Beverly Hills', '7003 Beverly Dr', NULL, '2018-07-14 18:24:26', '2018-07-14 18:24:26'),
(2, 1, 'dfsgsdf', 'dfgsdf', NULL, '2018-08-15 19:49:27', '2018-08-15 19:49:27');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `content_duplication`
--

CREATE TABLE `content_duplication` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'URL',
  `env` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'környezet',
  `article_id` int(11) NOT NULL COMMENT 'Cikk',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `documents`
--

CREATE TABLE `documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL COMMENT 'Megnevezés',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `documents`
--

INSERT INTO `documents` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Belépési', NULL, '2018-08-14 19:27:06', '2018-08-14 19:27:06');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `driver_license`
--

CREATE TABLE `driver_license` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL COMMENT 'Jogosítvány',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `driver_license`
--

INSERT INTO `driver_license` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'B- kategória', NULL, '2018-08-14 19:12:31', '2018-08-14 19:12:31');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `educations`
--

CREATE TABLE `educations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL COMMENT 'Végzettség',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `educations`
--

INSERT INTO `educations` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Általános iskola', NULL, '2018-08-14 19:00:23', '2018-08-14 19:00:23');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `email`
--

CREATE TABLE `email` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Egyedi azonosító',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Sablon neve',
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tárgy',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tartalom',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `emails`
--

CREATE TABLE `emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text NOT NULL COMMENT 'Megnevezés',
  `subject` text NOT NULL COMMENT 'Tárgy',
  `content` text NOT NULL COMMENT 'Tartalom',
  `created_at` datetime NOT NULL COMMENT 'Létrehozás dátuma',
  `updated_at` datetime NOT NULL COMMENT 'Módosítás dátuma',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- A tábla adatainak kiíratása `emails`
--

INSERT INTO `emails` (`id`, `title`, `subject`, `content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Megerősítés', 'Regisztráció megerősítés', '<p>[URL]</p>\r\n', '2018-02-24 14:32:46', '2018-02-24 14:32:46', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `feor`
--

CREATE TABLE `feor` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) NOT NULL COMMENT 'Azonosító',
  `name` text NOT NULL COMMENT 'Megnevezés',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `feor`
--

INSERT INTO `feor` (`id`, `key`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '001', 'FEOR1', NULL, '2018-08-14 19:22:03', '2018-08-14 19:22:03');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL COMMENT 'Nyelv',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `languages`
--

INSERT INTO `languages` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Angol (alap)', NULL, '2018-08-14 18:55:44', '2018-08-14 18:55:44');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `layouts`
--

CREATE TABLE `layouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view_path` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `col` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `items_per_row` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `lq_options`
--

CREATE TABLE `lq_options` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Egyedi azonosító',
  `lq_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Kulcs',
  `lq_value` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Érték'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `lq_options`
--

INSERT INTO `lq_options` (`id`, `lq_key`, `lq_value`) VALUES
(1, 'logo', ''),
(2, 'analytics', '<script type=\"text/javascript\"></script>'),
(3, 'front_page', '1'),
(4, 'webshop', '1'),
(5, 'socials_facebook', '#'),
(6, 'socials_google', '#'),
(7, 'socials_instagram', '#'),
(8, 'socials_youtube', '#');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Egyedi azonosító',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Menü megnevezése',
  `shortcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Menü shortcode',
  `active` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Aktív',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Egyedi azonosító',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Szülő elem',
  `menu_id` int(11) NOT NULL COMMENT 'Menü',
  `page_id` int(11) DEFAULT NULL COMMENT 'Oldal',
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Egyedi url',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Menü felirat',
  `page_order` int(11) NOT NULL COMMENT 'Sorrend'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `nationality`
--

CREATE TABLE `nationality` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL COMMENT 'Állampolgársá',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `nationality`
--

INSERT INTO `nationality` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Magyar', NULL, '2018-08-14 19:31:11', '2018-08-14 19:31:11'),
(2, 'Magyar', NULL, '2018-08-15 17:44:32', '2018-08-15 17:44:32');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(10) UNSIGNED NOT NULL,
  `email_group` text NOT NULL COMMENT 'Hírlevél neve',
  `csv` longtext NOT NULL COMMENT 'csv file',
  `subject` text NOT NULL COMMENT 'Tárgy',
  `from` text NOT NULL COMMENT 'Feladó',
  `content` longtext NOT NULL COMMENT 'Tartalom',
  `created_at` datetime NOT NULL COMMENT 'Létrehozás dátuma',
  `updated_at` datetime NOT NULL COMMENT 'Módosítás dátuma',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `sent` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `newsletter_list`
--

CREATE TABLE `newsletter_list` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Egyedi azonosító',
  `newsletter_id` int(11) NOT NULL,
  `to` varchar(255) NOT NULL COMMENT 'Címzett',
  `to_name` varchar(255) NOT NULL COMMENT 'Megrendelés azonosító',
  `unsubscribe_link` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('4f2db1f4-7d5b-4d42-9e22-1ee11201afec', 'App\\Notifications\\ApplicationNotification', 'Modules\\User\\Entities\\User', 1, '{\"title\":\"Successfull Login\",\"user\":1,\"important\":false,\"action\":\"admin_login\",\"url\":\"http:\\/\\/madswork.admin.local\\/admin\\/login\"}', NULL, '2018-08-17 08:52:25', '2018-08-17 08:52:25'),
('556e55e4-d864-42b8-a20f-cc50c330c78f', 'App\\Notifications\\ApplicationNotification', 'Modules\\User\\Entities\\User', 1, '{\"title\":\"Successfull Login\",\"user\":1,\"important\":false,\"action\":\"admin_login\",\"url\":\"http:\\/\\/liquid.local\\/admin\\/login\"}', NULL, '2018-07-10 17:34:44', '2018-07-10 17:34:44'),
('8f3b6881-a56f-4002-98ad-08684853cf16', 'App\\Notifications\\ApplicationNotification', 'Modules\\User\\Entities\\User', 1, '{\"title\":\"Successfull Login\",\"user\":1,\"important\":false,\"action\":\"admin_login\",\"url\":\"http:\\/\\/127.0.0.1:8000\\/admin\\/login\"}', NULL, '2018-08-17 05:38:07', '2018-08-17 05:38:07'),
('9790ae04-f727-47e3-a5c3-ebac30390224', 'App\\Notifications\\ApplicationNotification', 'Modules\\User\\Entities\\User', 1, '{\"title\":\"Successfull Login\",\"user\":1,\"important\":false,\"action\":\"admin_login\",\"url\":\"http:\\/\\/madswork.admin.local:8000\\/admin\\/login\"}', NULL, '2018-08-16 19:43:34', '2018-08-16 19:43:34'),
('a7be3d72-d916-48b3-8d24-c366181a2da3', 'App\\Notifications\\ApplicationNotification', 'Modules\\User\\Entities\\User', 1, '{\"title\":\"Successfull Login\",\"user\":1,\"important\":false,\"action\":\"admin_login\",\"url\":\"http:\\/\\/localhost:8000\\/admin\\/login\"}', NULL, '2018-08-16 19:38:59', '2018-08-16 19:38:59');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Egyedi azonosító',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Megnevezés',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tartalom',
  `picture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Oldal kép',
  `have_sitebuilder` int(11) NOT NULL COMMENT 'Sitebuilder vane',
  `facebook` int(11) NOT NULL COMMENT 'Facebook share',
  `twitter` int(11) NOT NULL COMMENT 'Twitter share',
  `google` int(11) NOT NULL COMMENT 'Google share',
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'URL',
  `twitter_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter url',
  `twitter_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter cím',
  `twitter_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter leírás',
  `twitter_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter kép',
  `twitter_creator` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter létrehozó',
  `og_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Facebook url',
  `og_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Facebook cím',
  `og_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Facebook leírás',
  `og_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Facebook kép',
  `og_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Facebook típus',
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Meta cím',
  `meta_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Meta leírás',
  `meta_keys` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Meta kulcsok',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Aktív',
  `lead_image` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Lead kép',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `positions`
--

CREATE TABLE `positions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL COMMENT 'Munkakör',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `positions`
--

INSERT INTO `positions` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Könyvelő', NULL, '2018-08-14 19:24:00', '2018-08-14 19:24:00');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `project_status`
--

CREATE TABLE `project_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL COMMENT 'Megnevezés',
  `o` int(10) UNSIGNED NOT NULL COMMENT 'Rendezés',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `project_status`
--

INSERT INTO `project_status` (`id`, `name`, `o`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'sdafas', 0, NULL, '2018-08-14 04:56:51', '2018-08-14 04:56:51'),
(2, 'Önéletrajz bekérve', 0, NULL, '2018-08-14 19:29:19', '2018-08-14 19:29:19');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Egyedi azonosító',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Slug megnevezés',
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Megnevezés',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'superadmin', 'Super Admin', NULL, NULL, NULL),
(2, 'login', 'Login', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `roles_acls`
--

CREATE TABLE `roles_acls` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Egyedi azonosító',
  `role_id` int(11) NOT NULL COMMENT 'Szabály',
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Útvonal'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(11) NOT NULL COMMENT 'Felhasználó',
  `role_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Szabály'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, '1'),
(1, '2');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `sitebuilder`
--

CREATE TABLE `sitebuilder` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'URL',
  `header` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Fejléc',
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tartalom',
  `footer` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Lábléc',
  `header_tmp` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Fejléc tmp',
  `content_tmp` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tartalom tmp',
  `footer_tmp` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Lábléc tmp',
  `template_tmp` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Sablon tmp',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `sitebuilder_history`
--

CREATE TABLE `sitebuilder_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `sitebuilder_id` int(11) NOT NULL COMMENT 'Sitebuilder ID',
  `header` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Fejléc',
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tartalom',
  `footer` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Lábléc',
  `template` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Sablon',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `specialty_educations`
--

CREATE TABLE `specialty_educations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL COMMENT 'Speciális végzettség',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `specialty_educations`
--

INSERT INTO `specialty_educations` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Valami speciális', NULL, '2018-08-14 19:10:29', '2018-08-14 19:10:29');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `teaor`
--

CREATE TABLE `teaor` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) NOT NULL,
  `name` text NOT NULL COMMENT 'Név',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `teaor`
--

INSERT INTO `teaor` (`id`, `key`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '0111', 'Első', NULL, '2018-08-14 19:20:27', '2018-08-14 19:20:27');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tickets`
--

CREATE TABLE `tickets` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `page` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `topsliders`
--

CREATE TABLE `topsliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` text NOT NULL COMMENT 'Kép',
  `title` text NOT NULL COMMENT 'Cím',
  `link` text NOT NULL COMMENT 'Hivatkozás',
  `description` text NOT NULL COMMENT 'Leírás',
  `o` int(10) UNSIGNED NOT NULL COMMENT 'Rendezés',
  `created_at` datetime NOT NULL COMMENT 'Létrehozás dátuma',
  `updated_at` datetime NOT NULL COMMENT 'Módosítás dátuma',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `topsliders`
--

INSERT INTO `topsliders` (`id`, `image`, `title`, `link`, `description`, `o`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '/uploads/banners/banner_7.jpg', 'Cukrászda kereső', '/', 'Találd meg a hozzád legközelebbi cukrászdát', 0, '2018-04-01 10:04:56', '2018-04-11 14:06:25', NULL),
(2, '/uploads/banners/banner_8.jpg', 'Kemenes Cukrászda', '/kemenes-cukraszda-11', 'A legtöbben ezt ajánlják', 0, '2018-04-01 10:05:40', '2018-04-11 14:16:45', NULL),
(3, '/uploads/banners/banner_4.jpg', 'Remek ajánlatok, korrekt ár', '/kereses', 'Nézdd meg partnereinket', 0, '2018-04-01 10:06:21', '2018-04-11 13:33:05', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `training_areas`
--

CREATE TABLE `training_areas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL COMMENT 'Képzési terület',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `training_areas`
--

INSERT INTO `training_areas` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Informatika', NULL, '2018-08-14 19:07:04', '2018-08-14 19:07:04');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Egyedi azonosító',
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Vezetéknév',
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Keresztnév',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Email cím',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Jelszó',
  `hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Regisztráció megerősítése link',
  `hash_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Új jelszó generálása link',
  `facebook_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Facebook id',
  `google_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Google id',
  `hash_2_date` datetime DEFAULT NULL COMMENT 'Új jelszó generálás érvényesség',
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Token',
  `active` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Aktív',
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `lastname`, `firstname`, `email`, `password`, `hash`, `hash_2`, `facebook_id`, `google_id`, `hash_2_date`, `remember_token`, `active`, `avatar`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'bodák', 'szabolcs', 'bodak.szabolcs@gmail.com', '$2y$10$I/DBjKkOn/wY1SEo1kyJrO.IOUkzCS3ZqaUJnksbgMXfvErBSvyOi', NULL, NULL, NULL, NULL, NULL, 'mXQiAnOANo47xjtALiCKnJygBFGA3IjH12TMlyRRwPUA6FYGgpeF4lTLTlDy', 1, NULL, NULL, '2018-07-10 17:24:20', '2018-07-14 08:29:04');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `user_billing`
--

CREATE TABLE `user_billing` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Egyedi azonosító',
  `user_id` int(11) NOT NULL COMMENT 'Felhasználó',
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ország',
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Irányítószám',
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Város',
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Utca, hsz',
  `note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Megjegyzés',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Számlázási név',
  `tax_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Adószám'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `user_billing`
--

INSERT INTO `user_billing` (`id`, `user_id`, `country`, `zip`, `city`, `address`, `note`, `name`, `tax_number`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `user_shipping`
--

CREATE TABLE `user_shipping` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Egyedi azonosító',
  `user_id` int(11) NOT NULL COMMENT 'Felhasználó',
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ország',
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Irányítószám',
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Város',
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Utca, hsz',
  `note` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Megjegyzés',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Szállítási név',
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Telefonszám'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `user_shipping`
--

INSERT INTO `user_shipping` (`id`, `user_id`, `country`, `zip`, `city`, `address`, `note`, `name`, `phone`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `virtual_url`
--

CREATE TABLE `virtual_url` (
  `id` int(10) UNSIGNED NOT NULL,
  `from_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Kezdő url',
  `to_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Cél url',
  `headers` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Headerek',
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Kód',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `widgets`
--

CREATE TABLE `widgets` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Widget neve',
  `size` int(11) NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `layout` int(11) NOT NULL,
  `show_title` int(11) NOT NULL,
  `background_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `opacity_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pagination` int(11) NOT NULL,
  `autoslide` int(11) NOT NULL,
  `pagination_speed` int(11) NOT NULL,
  `pagination_count` int(11) NOT NULL,
  `item_limit` int(11) NOT NULL,
  `render_direction` int(11) NOT NULL,
  `manual` int(11) NOT NULL,
  `content_types` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_duplication` int(11) NOT NULL,
  `category_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_by` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_by_direction` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `where_not_in` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `workcategories`
--

CREATE TABLE `workcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL COMMENT 'Milyen munka érdekli?',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `workcategories`
--

INSERT INTO `workcategories` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Irodai', NULL, '2018-08-14 19:14:43', '2018-08-14 19:14:43');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `workers`
--

CREATE TABLE `workers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL COMMENT 'név',
  `phone` text NOT NULL COMMENT 'Telefon',
  `email` text NOT NULL COMMENT 'Email',
  `coutry` text NOT NULL COMMENT 'Ország',
  `zip` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'irányítószám',
  `city` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Város',
  `street` text NOT NULL COMMENT 'utca',
  `house_number` text NOT NULL COMMENT 'Házszám',
  `building` text NOT NULL COMMENT 'Épület',
  `stairway` text NOT NULL COMMENT 'Lépcsőház',
  `floor` text NOT NULL COMMENT 'Emelet',
  `door` text NOT NULL COMMENT 'Ajtó',
  `residence_address` text NOT NULL COMMENT 'Tartózkodási cím',
  `id_number` text NOT NULL COMMENT 'Személyi Igazolvány szám',
  `birthplace` text NOT NULL COMMENT 'Születési hely',
  `birthdate` date NOT NULL COMMENT 'Születési dátum',
  `mother_name` text NOT NULL COMMENT 'Anyja neve',
  `nationality` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Állampolgárság',
  `passport` text NOT NULL COMMENT 'Útlevélszám',
  `gender` text NOT NULL COMMENT 'Neme',
  `tax_number` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Adószám',
  `taj` text NOT NULL COMMENT 'Tajszám',
  `bank_account` text NOT NULL COMMENT 'Bankszámlaszám',
  `munkahey` text NOT NULL COMMENT 'Munkahely',
  `work_start` date NOT NULL COMMENT 'Bejelentés dátuma',
  `contract_date` date NOT NULL COMMENT 'Szerződés aláírás dátuma',
  `job_date` date NOT NULL COMMENT 'Munkaköri aláírás dátuma',
  `eu_date` date NOT NULL COMMENT 'EÜ kiskönyv lejárati dátuma',
  `lungfilter_date` date NOT NULL COMMENT 'Tüdőszűrő lejárati dátuma',
  `logout_date` date NOT NULL COMMENT 'Kilépés',
  `education` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Legmagasabb iskolai végzettség',
  `training_area` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Képzési terület',
  `specialtyeducation` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Speciális végzettségek',
  `driving_license` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Jogosítvány',
  `language` text NOT NULL COMMENT 'Nyelv',
  `newsletter` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Hírlevelt kér?',
  `work_category` text NOT NULL COMMENT 'Milyen munka érdekli?',
  `work_place` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Hol szeretne dolgozni?',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `workers`
--

INSERT INTO `workers` (`id`, `name`, `phone`, `email`, `coutry`, `zip`, `city`, `street`, `house_number`, `building`, `stairway`, `floor`, `door`, `residence_address`, `id_number`, `birthplace`, `birthdate`, `mother_name`, `nationality`, `passport`, `gender`, `tax_number`, `taj`, `bank_account`, `munkahey`, `work_start`, `contract_date`, `job_date`, `eu_date`, `lungfilter_date`, `logout_date`, `education`, `training_area`, `specialtyeducation`, `driving_license`, `language`, `newsletter`, `work_category`, `work_place`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Beverly Hills', '3105556709', 'bhills_6709@mailinator.com', 'Zkenw', 90210, 1, 'Ypszk', 'Zpqnj', 'Qysor', 'Wdpmq', 'Loeqv', 'Dmzyi', '6709 Beverly Dr', 'Eqyqt', 'Axnik', '2018-08-29', 'Beverly Hills', 2, 'Hgdaa', '0', 0, 'Crqok', 'Dmpei', 'sdfsd', '2018-08-15', '2018-08-30', '1900-10-31', '2018-08-15', '1900-11-13', '2018-08-30', 1, 1, 1, 1, '[\"1\"]', 0, '\"1\"', 1, NULL, '2018-08-15 17:48:29', '2018-08-16 19:44:26');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `worker_documents`
--

CREATE TABLE `worker_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `worker_id` int(11) NOT NULL,
  `document_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Dokumentum típusa',
  `date` date NOT NULL COMMENT 'Dátum',
  `comment` text COMMENT 'megjegyzés',
  `file` text NOT NULL COMMENT 'Fájl',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `worker_documents`
--

INSERT INTO `worker_documents` (`id`, `worker_id`, `document_id`, `date`, `comment`, `file`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 0, 1, '2018-08-08', 'asdfdasfdsa', '1', NULL, '2018-08-15 19:14:15', '2018-08-15 19:14:15'),
(2, 1, 1, '2018-08-22', NULL, 'uploads/1/Ss0jblos4ethUqs5oZza5NZFM0ymhnMEOVuXf8Zz.jpeg', '2018-08-15 19:32:27', '2018-08-15 19:16:55', '2018-08-15 19:32:27'),
(3, 1, 1, '2018-08-22', 'dsfgsdf', 'uploads/1/7hbBq9LHQxXzRtkfLxdT6cxIDxdA5Gr3p8rqVEDT.jpeg', NULL, '2018-08-15 19:24:05', '2018-08-15 19:24:05');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `_accesslog`
--

CREATE TABLE `_accesslog` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'Felhasználó',
  `route` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Route',
  `action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Művelet',
  `extra` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Extra info',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- A tábla adatainak kiíratása `_accesslog`
--

INSERT INTO `_accesslog` (`id`, `user_id`, `route`, `action`, `extra`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-10 17:35:00', '2018-07-10 17:35:00', NULL),
(2, 1, 'admin/layout/list', 'GET', '[]', '2018-07-10 17:35:18', '2018-07-10 17:35:18', NULL),
(3, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-10 17:35:24', '2018-07-10 17:35:24', NULL),
(4, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-10 17:39:49', '2018-07-10 17:39:49', NULL),
(5, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-10 17:40:35', '2018-07-10 17:40:35', NULL),
(6, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-10 17:41:47', '2018-07-10 17:41:47', NULL),
(7, 1, 'admin/banner/edit', 'GET', '[]', '2018-07-10 17:42:04', '2018-07-10 17:42:04', NULL),
(8, 1, 'admin/banner/list', 'GET', '[]', '2018-07-10 17:42:09', '2018-07-10 17:42:09', NULL),
(9, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-10 18:13:08', '2018-07-10 18:13:08', NULL),
(10, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"8cZqx0NlR06xlktPXQmiFV0KVXGDut1cTuyH4ttd\",\"model_name\":\"teszt\",\"table_name\":\"teszt\",\"menu_name\":\"teszt\",\"menu_icon\":\"teszt\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"a\"],\"column_comment\":[\"a\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"a\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-07-10 18:13:36', '2018-07-10 18:13:36', NULL),
(11, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-10 18:13:45', '2018-07-10 18:13:45', NULL),
(12, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"8cZqx0NlR06xlktPXQmiFV0KVXGDut1cTuyH4ttd\",\"model_name\":\"teszt\",\"table_name\":\"teszt\",\"menu_name\":\"teszt\",\"menu_icon\":\"teszt\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"a\"],\"column_comment\":[\"a\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"a\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-07-10 18:14:30', '2018-07-10 18:14:30', NULL),
(13, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-10 18:14:31', '2018-07-10 18:14:31', NULL),
(14, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"8cZqx0NlR06xlktPXQmiFV0KVXGDut1cTuyH4ttd\",\"model_name\":\"teszt0\",\"table_name\":\"teszt0\",\"menu_name\":\"teszt0\",\"menu_icon\":\"teszt\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"a\"],\"column_comment\":[\"a\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"a\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-07-10 18:14:46', '2018-07-10 18:14:46', NULL),
(15, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-10 18:14:48', '2018-07-10 18:14:48', NULL),
(16, 1, 'admin/teszt0/list', 'GET', '[]', '2018-07-10 18:14:54', '2018-07-10 18:14:54', NULL),
(17, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"8cZqx0NlR06xlktPXQmiFV0KVXGDut1cTuyH4ttd\",\"model_name\":\"ss\",\"table_name\":\"ss\",\"menu_name\":\"ss\",\"menu_icon\":\"ss\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"ss\"],\"column_comment\":[\"ss\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"ss\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-07-10 18:17:47', '2018-07-10 18:17:47', NULL),
(18, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-10 18:17:49', '2018-07-10 18:17:49', NULL),
(19, 1, 'admin/ss/list', 'GET', '[]', '2018-07-10 18:17:55', '2018-07-10 18:17:55', NULL),
(20, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"8cZqx0NlR06xlktPXQmiFV0KVXGDut1cTuyH4ttd\",\"model_name\":\"sss\",\"table_name\":\"sss\",\"menu_name\":\"ss\",\"menu_icon\":null,\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"ss\"],\"column_comment\":[\"ss\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"sss\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-07-10 18:18:39', '2018-07-10 18:18:39', NULL),
(21, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-10 18:18:41', '2018-07-10 18:18:41', NULL),
(22, 1, 'admin/sss/list', 'GET', '[]', '2018-07-10 18:18:50', '2018-07-10 18:18:50', NULL),
(23, 1, 'admin/sss/list', 'GET', '{\"direction\":null,\"orderBy\":\"0\",\"search-fillable\":null,\"id\":null}', '2018-07-10 18:18:55', '2018-07-10 18:18:55', NULL),
(24, 1, 'admin/sss/edit', 'GET', '[]', '2018-07-10 18:18:58', '2018-07-10 18:18:58', NULL),
(25, 1, 'admin/page/list', 'GET', '[]', '2018-07-10 18:19:23', '2018-07-10 18:19:23', NULL),
(26, 1, 'admin/page/list', 'GET', '[]', '2018-07-10 18:19:27', '2018-07-10 18:19:27', NULL),
(27, 1, 'admin/page/edit', 'GET', '[]', '2018-07-10 18:19:29', '2018-07-10 18:19:29', NULL),
(28, 1, 'admin/sss/list', 'GET', '[]', '2018-07-10 18:19:36', '2018-07-10 18:19:36', NULL),
(29, 1, 'admin/sss/edit', 'GET', '[]', '2018-07-10 18:19:40', '2018-07-10 18:19:40', NULL),
(30, 1, 'admin/sss/edit', 'POST', '{\"_token\":\"8cZqx0NlR06xlktPXQmiFV0KVXGDut1cTuyH4ttd\",\"ss\":\"sadfasd\",\"save_and_exit\":null}', '2018-07-10 18:19:45', '2018-07-10 18:19:45', NULL),
(31, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-10 18:20:22', '2018-07-10 18:20:22', NULL),
(32, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-10 18:24:25', '2018-07-10 18:24:25', NULL),
(33, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"8cZqx0NlR06xlktPXQmiFV0KVXGDut1cTuyH4ttd\",\"model_name\":\"hhh\",\"table_name\":\"hh\",\"menu_name\":\"hh\",\"menu_icon\":\"hh\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"hh\"],\"column_comment\":[\"hh\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"hh\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-07-10 18:24:38', '2018-07-10 18:24:38', NULL),
(34, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-10 18:24:40', '2018-07-10 18:24:40', NULL),
(35, 1, 'admin/hhh/list', 'GET', '[]', '2018-07-10 18:24:48', '2018-07-10 18:24:48', NULL),
(36, 1, 'admin/hhh/edit', 'GET', '[]', '2018-07-10 18:24:51', '2018-07-10 18:24:51', NULL),
(37, 1, 'admin/hhh/edit', 'POST', '{\"_token\":\"8cZqx0NlR06xlktPXQmiFV0KVXGDut1cTuyH4ttd\",\"hh\":\"sadfsdafdsa\"}', '2018-07-10 18:24:56', '2018-07-10 18:24:56', NULL),
(38, 1, 'admin/hhh/edit/1/data', 'GET', '[]', '2018-07-10 18:24:57', '2018-07-10 18:24:57', NULL),
(39, 1, 'admin/hhh/edit/1/data', 'POST', '{\"_token\":\"8cZqx0NlR06xlktPXQmiFV0KVXGDut1cTuyH4ttd\",\"hh\":\"sadfsdafdsa\",\"save_and_exit\":null}', '2018-07-10 18:25:03', '2018-07-10 18:25:03', NULL),
(40, 1, 'admin/hhh/list', 'GET', '[]', '2018-07-10 18:25:04', '2018-07-10 18:25:04', NULL),
(41, 1, 'admin/hhh/edit/1', 'GET', '[]', '2018-07-10 18:25:09', '2018-07-10 18:25:09', NULL),
(42, 1, 'admin/hhh/edit/1', 'POST', '{\"_token\":\"8cZqx0NlR06xlktPXQmiFV0KVXGDut1cTuyH4ttd\",\"hh\":\"sadfsdafdsa\"}', '2018-07-10 18:25:13', '2018-07-10 18:25:13', NULL),
(43, 1, 'admin/hhh/edit/1/data', 'GET', '[]', '2018-07-10 18:25:14', '2018-07-10 18:25:14', NULL),
(44, 1, 'admin/hhh/edit/1/data', 'POST', '{\"_token\":\"8cZqx0NlR06xlktPXQmiFV0KVXGDut1cTuyH4ttd\",\"hh\":\"sadfsdafdsa\",\"save_and_exit\":null}', '2018-07-10 18:26:40', '2018-07-10 18:26:40', NULL),
(45, 1, 'admin/hhh/list', 'GET', '[]', '2018-07-10 18:26:41', '2018-07-10 18:26:41', NULL),
(46, 1, 'admin/acl/list', 'GET', '[]', '2018-07-14 08:28:06', '2018-07-14 08:28:06', NULL),
(47, 1, 'admin/user/list', 'GET', '[]', '2018-07-14 08:28:11', '2018-07-14 08:28:11', NULL),
(48, 1, 'admin/user/edit/1', 'GET', '[]', '2018-07-14 08:28:17', '2018-07-14 08:28:17', NULL),
(49, 1, 'admin/user/edit/1', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"tab\":\"general\",\"lastname\":\"bod\\u00e1k\",\"firstname\":\"szabolcs\",\"email\":\"bodak.szabolcs@gmail.com\",\"password\":null,\"password_again\":null,\"shipping\":{\"name\":null,\"country\":null,\"zip\":null,\"city\":null,\"address\":null,\"phone\":null},\"billing\":{\"name\":null,\"country\":null,\"zip\":null,\"city\":null,\"address\":null,\"tax_number\":null},\"save_and_exit\":null}', '2018-07-14 08:29:04', '2018-07-14 08:29:04', NULL),
(50, 1, 'admin/user/list', 'GET', '[]', '2018-07-14 08:29:05', '2018-07-14 08:29:05', NULL),
(51, 1, 'admin/page/list', 'GET', '[]', '2018-07-14 08:30:25', '2018-07-14 08:30:25', NULL),
(52, 1, 'admin/article/list', 'GET', '[]', '2018-07-14 08:30:34', '2018-07-14 08:30:34', NULL),
(53, 1, 'admin/modules', 'GET', '[]', '2018-07-14 08:31:05', '2018-07-14 08:31:05', NULL),
(54, 1, 'admin/modules/Article', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"turn_off\":\"Kikapcsol\\u00e1s\"}', '2018-07-14 08:31:12', '2018-07-14 08:31:12', NULL),
(55, 1, 'admin/modules', 'GET', '[]', '2018-07-14 08:31:13', '2018-07-14 08:31:13', NULL),
(56, 1, 'admin/modules', 'GET', '[]', '2018-07-14 08:31:24', '2018-07-14 08:31:24', NULL),
(57, 1, 'admin/email/list', 'GET', '[]', '2018-07-14 08:35:43', '2018-07-14 08:35:43', NULL),
(58, 1, 'admin/email/edit', 'GET', '[]', '2018-07-14 08:35:48', '2018-07-14 08:35:48', NULL),
(59, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-14 08:36:30', '2018-07-14 08:36:30', NULL),
(60, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"model_name\":\"Company\",\"table_name\":\"companies\",\"menu_name\":\"C\\u00e9gek\",\"menu_icon\":\"fa fa-buildings\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\",\"tax\",\"eu_tax\",\"company_number\",\"delegate\",\"delegete_position\",\"comment\",\"zip\",\"country\",\"city\",\"address\",\"mail_address\",\"mailbox\",\"contact\",\"business_description\"],\"column_comment\":[\"C\\u00e9g neve\",\"Ad\\u00f3sz\\u00e1m\",\"Eu ad\\u00f3sz\\u00e1m\",\"C\\u00e9gjegyz\\u00e9ksz\\u00e1m\",\"K\\u00e9pvisel\\u0151\",\"K\\u00e9pvisel\\u0151 beoszt\\u00e1sa\",\"Megjegyz\\u00e9s\",\"Ir\\u00e1ny\\u00edt\\u00f3sz\\u00e1m\",\"Orsz\\u00e1g\",\"V\\u00e1ros\",\"Utca, h\\u00e1zsz\\u00e1m\",\"Postai c\\u00edm\",\"Postafi\\u00f3k\",\"Kapcsolattart\\u00f3\",\"Milyen \\u00fcgyben keressem?\"],\"column_type\":[\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\"],\"column_fillable\":[\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\"],\"column_cast\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_getter\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_label\":[\"C\\u00e9g neve\",\"Ad\\u00f3sz\\u00e1m\",\"EU ad\\u00f3sz\\u00e1m\",\"C\\u00e9gjegyz\\u00e9ksz\\u00e1m\",\"K\\u00e9pvisel\\u0151\",\"K\\u00e9pvisel\\u0151 beoszt\\u00e1sa\",\"Megjegyz\\u00e9s\",\"Ir\\u00e1ny\\u00edt\\u00f3sz\\u00e1m\",\"Orsz\\u00e1g\",\"V\\u00e1ros\",\"Utca, h\\u00e1zsz\\u00e1m\",\"Postai c\\u00edm\",\"Postafi\\u00f3k\",\"Kapcsolattart\\u00f3\",\"Milyen \\u00fcgyben keressem?\"],\"column_input\":[\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"select\",\"input\"],\"column_value\":[null,null,null,null,null,null,null,null,null,null,null,null,null,\"[]\",null],\"column_search\":[\"1\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_search_input\":[\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"select\",\"text\"],\"column_search_value\":[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],\"required\":[\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\"],\"save_and_exit\":null}', '2018-07-14 08:50:33', '2018-07-14 08:50:33', NULL),
(61, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-14 08:50:39', '2018-07-14 08:50:39', NULL),
(62, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 08:50:47', '2018-07-14 08:50:47', NULL),
(63, 1, 'admin/company/edit', 'GET', '[]', '2018-07-14 08:50:52', '2018-07-14 08:50:52', NULL),
(64, 1, 'admin/company/edit', 'GET', '[]', '2018-07-14 08:55:59', '2018-07-14 08:55:59', NULL),
(65, 1, 'admin/company/edit', 'GET', '[]', '2018-07-14 08:58:49', '2018-07-14 08:58:49', NULL),
(66, 1, 'admin/company/edit', 'GET', '[]', '2018-07-14 08:59:08', '2018-07-14 08:59:08', NULL),
(67, 1, 'admin/company/edit', 'GET', '[]', '2018-07-14 08:59:24', '2018-07-14 08:59:24', NULL),
(68, 1, 'admin/company/edit', 'GET', '[]', '2018-07-14 08:59:39', '2018-07-14 08:59:39', NULL),
(69, 1, 'admin/company/edit', 'GET', '[]', '2018-07-14 08:59:56', '2018-07-14 08:59:56', NULL),
(70, 1, 'admin/company/edit', 'GET', '[]', '2018-07-14 09:00:37', '2018-07-14 09:00:37', NULL),
(71, 1, 'admin/company/edit', 'GET', '[]', '2018-07-14 09:02:58', '2018-07-14 09:02:58', NULL),
(72, 1, 'admin/company/edit', 'GET', '[]', '2018-07-14 09:03:11', '2018-07-14 09:03:11', NULL),
(73, 1, 'admin/company/edit', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"name\":\"Beverly Hills\",\"tax\":\"Mbdex\",\"eu_tax\":\"Oklol\",\"company_number\":\"Beverly Corp\",\"delegate\":\"Kifib\",\"delegete_position\":\"Kuzzb\",\"comment\":\"Ozqlu\",\"zip\":\"90210\",\"country\":\"USA\",\"city\":\"Beverly Hills\",\"address\":\"7481 Beverly Dr\",\"mail_address\":\"7481 Beverly Dr\",\"mailbox\":\"Uzhhp\",\"contact\":\"1\",\"business_description\":\"Ascdo\"}', '2018-07-14 09:03:28', '2018-07-14 09:03:28', NULL),
(74, 1, 'admin/company/edit/1/data', 'GET', '[]', '2018-07-14 09:03:29', '2018-07-14 09:03:29', NULL),
(75, 1, 'admin/company/edit/1/data', 'GET', '[]', '2018-07-14 09:07:43', '2018-07-14 09:07:43', NULL),
(76, 1, 'admin/company/edit/1/data', 'GET', '[]', '2018-07-14 09:10:48', '2018-07-14 09:10:48', NULL),
(77, 1, 'admin/company/edit/1/data', 'GET', '[]', '2018-07-14 09:18:36', '2018-07-14 09:18:36', NULL),
(78, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 09:24:40', '2018-07-14 09:24:40', NULL),
(79, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 09:27:42', '2018-07-14 09:27:42', NULL),
(80, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 09:27:49', '2018-07-14 09:27:49', NULL),
(81, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 09:28:21', '2018-07-14 09:28:21', NULL),
(82, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 09:28:24', '2018-07-14 09:28:24', NULL),
(83, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 09:28:29', '2018-07-14 09:28:29', NULL),
(84, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-07-14 09:28:32', '2018-07-14 09:28:32', NULL),
(85, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 09:28:34', '2018-07-14 09:28:34', NULL),
(86, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 09:29:40', '2018-07-14 09:29:40', NULL),
(87, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 09:29:50', '2018-07-14 09:29:50', NULL),
(88, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"basedata\"}', '2018-07-14 09:29:53', '2018-07-14 09:29:53', NULL),
(89, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"basedata\"}', '2018-07-14 09:30:03', '2018-07-14 09:30:03', NULL),
(90, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-07-14 09:30:10', '2018-07-14 09:30:10', NULL),
(91, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 09:30:13', '2018-07-14 09:30:13', NULL),
(92, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 09:30:17', '2018-07-14 09:30:17', NULL),
(93, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 09:30:37', '2018-07-14 09:30:37', NULL),
(94, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-07-14 09:30:43', '2018-07-14 09:30:43', NULL),
(95, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 09:30:46', '2018-07-14 09:30:46', NULL),
(96, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"candidates\"}', '2018-07-14 09:30:49', '2018-07-14 09:30:49', NULL),
(97, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 09:30:53', '2018-07-14 09:30:53', NULL),
(98, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-07-14 09:30:56', '2018-07-14 09:30:56', NULL),
(99, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 09:31:23', '2018-07-14 09:31:23', NULL),
(100, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 09:31:29', '2018-07-14 09:31:29', NULL),
(101, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 09:36:43', '2018-07-14 09:36:43', NULL),
(102, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-14 09:36:59', '2018-07-14 09:36:59', NULL),
(103, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"model_name\":\"CompanySites\",\"table_name\":\"company_sites\",\"menu_name\":\"Telephelyek\",\"menu_icon\":\"fa-globe\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\",\"address\"],\"column_comment\":[\"Telephely neve\",\"Munkav\\u00e9gz\\u00e9s helye\"],\"column_type\":[\"TEXT\",\"TEXT\"],\"column_fillable\":[\"1\",\"1\"],\"column_cast\":[\"0\",\"0\"],\"column_getter\":[\"0\",\"0\"],\"column_label\":[\"Telephely neve\",\"Munkav\\u00e9gz\\u00e9s helye\"],\"column_input\":[\"input\",\"input\"],\"column_value\":[null,null],\"column_search\":[\"0\",\"0\"],\"column_search_input\":[\"text\",\"text\"],\"column_search_value\":[null,null],\"required\":[\"1\",\"1\"],\"save_and_exit\":null}', '2018-07-14 09:38:41', '2018-07-14 09:38:41', NULL),
(104, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-14 09:38:43', '2018-07-14 09:38:43', NULL),
(105, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"model_name\":\"CompanyContact\",\"table_name\":\"company_contacts\",\"menu_name\":\"Kapcsolattart\\u00f3k\",\"menu_icon\":\"fa-globe\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\",\"position\",\"email\",\"phone\",\"fax\",\"company_id\"],\"column_comment\":[\"N\\u00e9v\",\"Beoszt\\u00e1s\",\"Email\",\"Telefon\",\"Fax\",\"C\\u00e9g\"],\"column_type\":[\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\"],\"column_fillable\":[\"1\",\"1\",\"1\",\"1\",\"1\",\"0\"],\"column_cast\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_getter\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_label\":[\"N\\u00e9v\",\"Beoszt\\u00e1s\",\"Email\",\"Telefon\",\"Fax\",\"C\\u00e9g\"],\"column_input\":[\"input\",\"input\",\"input\",\"input\",\"input\",\"input\"],\"column_value\":[null,null,null,null,null,null],\"column_search\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_search_input\":[\"text\",\"text\",\"text\",\"text\",\"text\",\"text\"],\"column_search_value\":[null,null,null,null,null,null],\"required\":[\"1\",\"1\",\"1\",\"1\",\"1\",\"0\"],\"save_and_exit\":null}', '2018-07-14 09:42:13', '2018-07-14 09:42:13', NULL),
(106, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-14 09:42:16', '2018-07-14 09:42:16', NULL),
(107, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"model_name\":\"CompanyIndustry\",\"table_name\":\"company_industries\",\"menu_name\":\"Tev\\u00e9kenys\\u00e9g\",\"menu_icon\":\"fa-globe\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\",\"user_id\",\"work_category\",\"person\",\"aszf\"],\"column_comment\":[\"Tev\\u00e9kenys\\u00e9g neve\",\"Felel\\u0151s\",\"Kateg\\u00f3ria\",\"Utas\\u00edt\\u00e1st ad\\u00f3 szem\\u00e9ly\",\"A szolg\\u00e1ltat\\u00e1s fogad\\u00f3ja adatv\\u00e9delmi t\\u00e1j\\u00e9koztat\\u00f3j\\u00e1nak \\u00e9s adatkezel\\u00e9si szab\\u00e1lyzat\\u00e1nak el\\u00e9rhet\\u0151s\\u00e9ge\"],\"column_type\":[\"TEXT\",\"INT\",\"INT\",\"TEXT\",\"TEXT\"],\"column_fillable\":[\"1\",\"1\",\"1\",\"1\",\"1\"],\"column_cast\":[\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_getter\":[\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_label\":[\"Tev\\u00e9kenys\\u00e9g neve\",\"Felel\\u0151s\",\"Felel\\u0151s\",\"Utas\\u00edt\\u00e1st ad\\u00f3 szem\\u00e9ly\",\"A szolg\\u00e1ltat\\u00e1s fogad\\u00f3ja adatv\\u00e9delmi t\\u00e1j\\u00e9koztat\\u00f3j\\u00e1nak \\u00e9s adatkezel\\u00e9si szab\\u00e1lyzat\\u00e1nak el\\u00e9rhet\\u0151s\\u00e9ge\"],\"column_input\":[\"input\",\"select\",\"select\",\"input\",\"input\"],\"column_value\":[null,null,null,null,null],\"column_search\":[\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_search_input\":[\"text\",\"text\",\"text\",\"text\",\"text\"],\"column_search_value\":[null,null,null,null,null],\"required\":[\"1\",\"1\",\"1\",\"1\",\"1\"],\"save_and_exit\":null}', '2018-07-14 09:45:51', '2018-07-14 09:45:51', NULL),
(108, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-14 09:45:53', '2018-07-14 09:45:53', NULL),
(109, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 09:46:12', '2018-07-14 09:46:12', NULL),
(110, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 09:46:18', '2018-07-14 09:46:18', NULL),
(111, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"model_name\":\"CompanyContract\",\"table_name\":\"company_contract\",\"menu_name\":\"Szerz\\u0151d\\u00e9sek\",\"menu_icon\":\"fa-globe\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"company_id\",\"type\",\"contract_date\",\"contract_type\",\"deadline\",\"paper_contract\",\"contact\",\"payment_type\",\"summation\",\"warrantee\"],\"column_comment\":[null,\"T\\u00edpus\",\"Szerz\\u0151d\\u00e9sk\\u00f6t\\u00e9s d\\u00e1tuma\",\"Hat\\u00e1rozott\\/hat\\u00e1rozatlan\",\"Fizet\\u00e9si hat\\u00e1rid\\u0151\",\"Pap\\u00edr alap\\u00fa szerz\\u0151d\\u00e9s\",\"MADS kapcsolattart\\u00f3 (Felel\\u0151s)\",\"Elsz\\u00e1mol\\u00e1s t\\u00edpusa\",\"\\u00c1tv\\u00e9tel d\\u00e1tumai, \\u00f6sszegek\",\"Garancia\"],\"column_type\":[\"INT\",\"INT\",\"DATE\",\"INT\",\"TEXT\",\"INT\",\"INT\",\"TEXT\",\"TEXT\",\"TEXT\"],\"column_fillable\":[\"0\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\"],\"column_cast\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_getter\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_label\":[\"C\\u00e9g\",\"T\\u00edpus\",\"Szerz\\u0151d\\u00e9sk\\u00f6t\\u00e9s d\\u00e1tuma\",\"Hat\\u00e1rozott\\/hat\\u00e1rozatlan\",\"Fizet\\u00e9si hat\\u00e1rid\\u0151\",\"Pap\\u00edr alap\\u00fa szerz\\u0151d\\u00e9s\",\"MADS kapcsolattart\\u00f3 (Felel\\u0151s)\",\"Elsz\\u00e1mol\\u00e1s t\\u00edpusa\",\"\\u00c1tv\\u00e9tel d\\u00e1tumai, \\u00f6sszegek\",\"Garancia\"],\"column_input\":[\"input\",\"select\",\"date\",\"select\",\"input\",\"select\",\"select\",\"select\",\"textarea\",\"input\"],\"column_value\":[null,null,null,null,null,\"[0=>__(\'Nincs\'),1=>__(\'Van\')]\",\"[]\",\"[0=>\'Szorz\\u00f3\',1=>\'Fix\']\",null,null],\"column_search\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_search_input\":[\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\"],\"column_search_value\":[null,null,null,null,null,null,null,null,null,null],\"required\":[\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\"],\"save_and_exit\":null}', '2018-07-14 09:54:05', '2018-07-14 09:54:05', NULL),
(112, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-14 09:54:07', '2018-07-14 09:54:07', NULL),
(113, 1, 'admin/builder/settings', 'GET', '[]', '2018-07-14 09:57:10', '2018-07-14 09:57:10', NULL),
(114, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 10:03:14', '2018-07-14 10:03:14', NULL),
(115, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 10:03:19', '2018-07-14 10:03:19', NULL),
(116, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 10:03:25', '2018-07-14 10:03:25', NULL),
(117, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 10:03:47', '2018-07-14 10:03:47', NULL),
(118, 1, 'admin/company/edit/1', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"name\":\"Beverly Hills\",\"position\":null,\"email\":null,\"phone\":null,\"fax\":null,\"tab\":\"contact\"}', '2018-07-14 10:04:07', '2018-07-14 10:04:07', NULL),
(119, 1, 'admin/company/edit/1/contact', 'GET', '[]', '2018-07-14 10:04:09', '2018-07-14 10:04:09', NULL),
(120, 1, 'admin/company/edit/1/contact', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 10:05:56', '2018-07-14 10:05:56', NULL),
(121, 1, 'admin/company/edit/1/contact', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"name\":\"Beverly Hills\",\"position\":null,\"email\":null,\"phone\":null,\"fax\":null,\"save_and_exit\":null,\"tab\":\"contact\"}', '2018-07-14 10:06:02', '2018-07-14 10:06:02', NULL),
(122, 1, 'admin/company/edit/1/contact', 'GET', '[]', '2018-07-14 10:06:03', '2018-07-14 10:06:03', NULL),
(123, 1, 'admin/company/edit/1/contact', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 10:07:00', '2018-07-14 10:07:00', NULL),
(124, 1, 'admin/company/edit/1/contact', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"name\":\"Beverly Hills\",\"position\":null,\"email\":null,\"phone\":null,\"fax\":null,\"save_and_exit\":null,\"tab\":\"contact\"}', '2018-07-14 10:07:04', '2018-07-14 10:07:04', NULL),
(125, 1, 'admin/company/edit/1/contact', 'GET', '[]', '2018-07-14 10:07:06', '2018-07-14 10:07:06', NULL),
(126, 1, 'admin/company/edit/1/contact', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 10:08:11', '2018-07-14 10:08:11', NULL),
(127, 1, 'admin/company/edit/1/contact', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 10:08:22', '2018-07-14 10:08:22', NULL),
(128, 1, 'admin/companycontact/edit/1', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"name\":\"Beverly Hills\",\"position\":null,\"email\":null,\"phone\":null,\"fax\":null,\"save_and_exit\":null}', '2018-07-14 10:08:39', '2018-07-14 10:08:39', NULL),
(129, 1, 'admin/companycontact/list', 'GET', '[]', '2018-07-14 10:08:40', '2018-07-14 10:08:40', NULL),
(130, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 10:09:34', '2018-07-14 10:09:34', NULL),
(131, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 10:09:40', '2018-07-14 10:09:40', NULL),
(132, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 10:09:44', '2018-07-14 10:09:44', NULL),
(133, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 10:09:46', '2018-07-14 10:09:46', NULL),
(134, 1, 'admin/companycontact/edit/1', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"name\":\"Beverly Hills\",\"position\":null,\"email\":null,\"phone\":null,\"fax\":null,\"save_and_exit\":null}', '2018-07-14 10:09:51', '2018-07-14 10:09:51', NULL),
(135, 1, 'admin/companycontact/list', 'GET', '[]', '2018-07-14 10:09:52', '2018-07-14 10:09:52', NULL),
(136, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 10:10:39', '2018-07-14 10:10:39', NULL),
(137, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 10:10:45', '2018-07-14 10:10:45', NULL),
(138, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 10:10:50', '2018-07-14 10:10:50', NULL),
(139, 1, 'admin/companycontact/edit/1', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"name\":\"Beverly Hills\",\"position\":null,\"email\":null,\"phone\":null,\"fax\":null,\"save_and_exit\":null}', '2018-07-14 10:10:57', '2018-07-14 10:10:57', NULL),
(140, 1, 'admin/companycontact/list', 'GET', '[]', '2018-07-14 10:10:58', '2018-07-14 10:10:58', NULL),
(141, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 10:11:41', '2018-07-14 10:11:41', NULL),
(142, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 10:11:48', '2018-07-14 10:11:48', NULL),
(143, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 10:11:53', '2018-07-14 10:11:53', NULL),
(144, 1, 'admin/companycontact/edit/1', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"name\":\"Beverly Hills\",\"position\":null,\"email\":null,\"phone\":null,\"fax\":null}', '2018-07-14 10:12:05', '2018-07-14 10:12:05', NULL),
(145, 1, 'admin/companycontact/list', 'GET', '[]', '2018-07-14 10:12:06', '2018-07-14 10:12:06', NULL),
(146, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 10:12:43', '2018-07-14 10:12:43', NULL),
(147, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 10:12:49', '2018-07-14 10:12:49', NULL),
(148, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 10:12:54', '2018-07-14 10:12:54', NULL),
(149, 1, 'admin/companycontact/edit/1', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"name\":\"Beverly Hills\",\"position\":null,\"email\":null,\"phone\":null,\"fax\":null,\"save_and_exit\":null}', '2018-07-14 10:13:00', '2018-07-14 10:13:00', NULL),
(150, 1, 'admin/companycontact/list', 'GET', '[]', '2018-07-14 10:13:01', '2018-07-14 10:13:01', NULL),
(151, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 10:13:18', '2018-07-14 10:13:18', NULL),
(152, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 10:13:23', '2018-07-14 10:13:23', NULL),
(153, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 10:13:27', '2018-07-14 10:13:27', NULL),
(154, 1, 'admin/companycontact/edit/1', 'POST', '{\"_token\":\"c5jP96FI3jETG1WEAlBcB2Yep6bv11ki9JyIGWXV\",\"name\":\"Beverly Hills\",\"position\":null,\"email\":null,\"phone\":null,\"fax\":null}', '2018-07-14 10:13:34', '2018-07-14 10:13:34', NULL),
(155, 1, 'admin/companycontact/list', 'GET', '[]', '2018-07-14 10:13:36', '2018-07-14 10:13:36', NULL),
(156, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 10:14:15', '2018-07-14 10:14:15', NULL),
(157, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 10:14:22', '2018-07-14 10:14:22', NULL),
(158, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 10:14:26', '2018-07-14 10:14:26', NULL),
(159, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 10:14:36', '2018-07-14 10:14:36', NULL),
(160, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 10:16:26', '2018-07-14 10:16:26', NULL),
(161, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 10:16:32', '2018-07-14 10:16:32', NULL),
(162, 1, 'admin/companycontact/list', 'GET', '{\"company_id\":\"1?tab=contact\"}', '2018-07-14 10:16:38', '2018-07-14 10:16:38', NULL),
(163, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 10:23:40', '2018-07-14 10:23:40', NULL),
(164, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 10:23:47', '2018-07-14 10:23:47', NULL),
(165, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 13:23:28', '2018-07-14 13:23:28', NULL),
(166, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 13:23:28', '2018-07-14 13:23:28', NULL),
(167, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 13:24:49', '2018-07-14 13:24:49', NULL),
(168, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 13:24:54', '2018-07-14 13:24:54', NULL),
(169, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 13:24:54', '2018-07-14 13:24:54', NULL),
(170, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-07-14 13:27:39', '2018-07-14 13:27:39', NULL),
(171, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 13:27:42', '2018-07-14 13:27:42', NULL),
(172, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 13:29:47', '2018-07-14 13:29:47', NULL),
(173, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:29:51', '2018-07-14 13:29:51', NULL),
(174, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:31:10', '2018-07-14 13:31:10', NULL),
(175, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:31:47', '2018-07-14 13:31:47', NULL),
(176, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:32:08', '2018-07-14 13:32:08', NULL),
(177, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:33:23', '2018-07-14 13:33:23', NULL),
(178, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:33:40', '2018-07-14 13:33:40', NULL),
(179, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:33:58', '2018-07-14 13:33:58', NULL),
(180, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:34:20', '2018-07-14 13:34:20', NULL),
(181, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:34:51', '2018-07-14 13:34:51', NULL),
(182, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:35:08', '2018-07-14 13:35:08', NULL),
(183, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:35:30', '2018-07-14 13:35:30', NULL),
(184, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:36:32', '2018-07-14 13:36:32', NULL),
(185, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:37:51', '2018-07-14 13:37:51', NULL),
(186, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:38:30', '2018-07-14 13:38:30', NULL),
(187, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:40:13', '2018-07-14 13:40:13', NULL),
(188, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 13:49:54', '2018-07-14 13:49:54', NULL),
(189, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 13:50:49', '2018-07-14 13:50:49', NULL),
(190, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 13:50:56', '2018-07-14 13:50:56', NULL),
(191, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 13:52:33', '2018-07-14 13:52:33', NULL),
(192, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 13:52:42', '2018-07-14 13:52:42', NULL),
(193, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 13:53:04', '2018-07-14 13:53:04', NULL),
(194, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 13:53:23', '2018-07-14 13:53:23', NULL),
(195, 1, 'admin/company/contact/edit/1', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"name\":\"Beverly Hills\",\"position\":\"Mvffl\",\"email\":\"bhills_5534@mailinator.com\",\"phone\":\"3105555534\",\"fax\":\"3105555534\",\"save_and_exit\":null}', '2018-07-14 13:53:32', '2018-07-14 13:53:32', NULL),
(196, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:53:33', '2018-07-14 13:53:33', NULL),
(197, 1, 'admin/company/contact/delete/1/1', 'GET', '[]', '2018-07-14 13:53:42', '2018-07-14 13:53:42', NULL),
(198, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:53:43', '2018-07-14 13:53:43', NULL),
(199, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 13:53:47', '2018-07-14 13:53:47', NULL),
(200, 1, 'admin/company/contact/edit/1', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"name\":null,\"position\":null,\"email\":null,\"phone\":null,\"fax\":null,\"save_and_exit\":null}', '2018-07-14 13:53:50', '2018-07-14 13:53:50', NULL),
(201, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 13:53:51', '2018-07-14 13:53:51', NULL),
(202, 1, 'admin/company/contact/edit/1', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"name\":\"Beverly Hills\",\"position\":\"Zfcpw\",\"email\":\"bhills_3878@mailinator.com\",\"phone\":\"3105553878\",\"fax\":\"3105553878\",\"save_and_exit\":null}', '2018-07-14 13:54:01', '2018-07-14 13:54:01', NULL),
(203, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:54:02', '2018-07-14 13:54:02', NULL),
(204, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 13:54:10', '2018-07-14 13:54:10', NULL),
(205, 1, 'admin/company/contact/edit/1', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"name\":\"Beverly Hills\",\"position\":\"Ikkmv\",\"email\":\"bhills_0286@mailinator.com\",\"phone\":\"3105550286\",\"fax\":\"3105550286\"}', '2018-07-14 13:54:15', '2018-07-14 13:54:15', NULL),
(206, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:54:16', '2018-07-14 13:54:16', NULL),
(207, 1, 'admin/company/edit/1', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":\"bb\",\"id\":null}', '2018-07-14 13:54:35', '2018-07-14 13:54:35', NULL),
(208, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:54:58', '2018-07-14 13:54:58', NULL),
(209, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 13:55:19', '2018-07-14 13:55:19', NULL),
(210, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 13:57:50', '2018-07-14 13:57:50', NULL),
(211, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 13:57:56', '2018-07-14 13:57:56', NULL),
(212, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 13:58:01', '2018-07-14 13:58:01', NULL),
(213, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 13:58:05', '2018-07-14 13:58:05', NULL),
(214, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:58:09', '2018-07-14 13:58:09', NULL),
(215, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 13:59:46', '2018-07-14 13:59:46', NULL),
(216, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 14:00:14', '2018-07-14 14:00:14', NULL),
(217, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:00:18', '2018-07-14 14:00:18', NULL),
(218, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:01:42', '2018-07-14 14:01:42', NULL),
(219, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:02:23', '2018-07-14 14:02:23', NULL),
(220, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:03:07', '2018-07-14 14:03:07', NULL),
(221, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:04:08', '2018-07-14 14:04:08', NULL),
(222, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:04:14', '2018-07-14 14:04:14', NULL),
(223, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:04:57', '2018-07-14 14:04:57', NULL),
(224, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:05:20', '2018-07-14 14:05:20', NULL),
(225, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:06:00', '2018-07-14 14:06:00', NULL),
(226, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:06:10', '2018-07-14 14:06:10', NULL),
(227, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:06:28', '2018-07-14 14:06:28', NULL),
(228, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:07:02', '2018-07-14 14:07:02', NULL),
(229, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 14:07:13', '2018-07-14 14:07:13', NULL),
(230, 1, 'admin/company/contact/edit/1', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"name\":null,\"position\":null,\"email\":null,\"phone\":null,\"fax\":null,\"save_and_exit\":null}', '2018-07-14 14:07:25', '2018-07-14 14:07:25', NULL),
(231, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:07:27', '2018-07-14 14:07:27', NULL),
(232, 1, 'admin/company/contact/edit/1', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"name\":null,\"position\":null,\"email\":null,\"phone\":null,\"fax\":null,\"save_and_exit\":null}', '2018-07-14 14:07:38', '2018-07-14 14:07:38', NULL),
(233, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:07:39', '2018-07-14 14:07:39', NULL),
(234, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:07:51', '2018-07-14 14:07:51', NULL),
(235, 1, 'admin/company/contact/edit/1', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"name\":\"Beverly Hills\",\"position\":\"Jgxrd\",\"email\":\"bhills_0373@mailinator.com\",\"phone\":\"3105550373\",\"fax\":\"3105550373\",\"save_and_exit\":null}', '2018-07-14 14:07:57', '2018-07-14 14:07:57', NULL),
(236, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 14:07:58', '2018-07-14 14:07:58', NULL),
(237, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 14:08:36', '2018-07-14 14:08:36', NULL),
(238, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:08:49', '2018-07-14 14:08:49', NULL),
(239, 1, 'admin/company/contact/edit/1', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"name\":null,\"position\":null,\"email\":null,\"phone\":null,\"fax\":null,\"save_and_exit\":null}', '2018-07-14 14:08:56', '2018-07-14 14:08:56', NULL),
(240, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-07-14 14:08:58', '2018-07-14 14:08:58', NULL),
(241, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 14:09:03', '2018-07-14 14:09:03', NULL),
(242, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 14:09:08', '2018-07-14 14:09:08', NULL),
(243, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 14:09:12', '2018-07-14 14:09:12', NULL),
(244, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-07-14 14:09:15', '2018-07-14 14:09:15', NULL),
(245, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 14:09:21', '2018-07-14 14:09:21', NULL),
(246, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"candidates\"}', '2018-07-14 14:09:24', '2018-07-14 14:09:24', NULL),
(247, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 14:09:27', '2018-07-14 14:09:27', NULL),
(248, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 14:12:32', '2018-07-14 14:12:32', NULL),
(249, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:12:44', '2018-07-14 14:12:44', NULL),
(250, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:20:57', '2018-07-14 14:20:57', NULL),
(251, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:21:28', '2018-07-14 14:21:28', NULL),
(252, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:21:45', '2018-07-14 14:21:45', NULL),
(253, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:22:05', '2018-07-14 14:22:05', NULL),
(254, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:23:31', '2018-07-14 14:23:31', NULL),
(255, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:23:55', '2018-07-14 14:23:55', NULL),
(256, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:24:45', '2018-07-14 14:24:45', NULL),
(257, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:25:25', '2018-07-14 14:25:25', NULL),
(258, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:25:54', '2018-07-14 14:25:54', NULL),
(259, 1, 'admin/companycontract/edit/1', 'GET', '[]', '2018-07-14 14:26:00', '2018-07-14 14:26:00', NULL),
(260, 1, 'admin/companycontract/list', 'GET', '[]', '2018-07-14 14:26:01', '2018-07-14 14:26:01', NULL),
(261, 1, 'admin/companycontract/list', 'GET', '[]', '2018-07-14 14:29:26', '2018-07-14 14:29:26', NULL),
(262, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:29:54', '2018-07-14 14:29:54', NULL),
(263, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:30:16', '2018-07-14 14:30:16', NULL),
(264, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:30:35', '2018-07-14 14:30:35', NULL),
(265, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 14:31:03', '2018-07-14 14:31:03', NULL),
(266, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:31:40', '2018-07-14 14:31:40', NULL),
(267, 1, 'admin/company/edit/1', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null}', '2018-07-14 14:32:34', '2018-07-14 14:32:34', NULL),
(268, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-07-14 14:32:40', '2018-07-14 14:32:40', NULL),
(269, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:32:45', '2018-07-14 14:32:45', NULL),
(270, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:35:23', '2018-07-14 14:35:23', NULL),
(271, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:36:28', '2018-07-14 14:36:28', NULL),
(272, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 14:36:37', '2018-07-14 14:36:37', NULL),
(273, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 14:38:01', '2018-07-14 14:38:01', NULL),
(274, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 14:38:07', '2018-07-14 14:38:07', NULL),
(275, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:38:13', '2018-07-14 14:38:13', NULL),
(276, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:38:43', '2018-07-14 14:38:43', NULL),
(277, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:39:52', '2018-07-14 14:39:52', NULL),
(278, 1, 'admin/company/contract/edit/1', 'GET', '[]', '2018-07-14 14:39:59', '2018-07-14 14:39:59', NULL),
(279, 1, 'admin/company/contract/edit/1', 'GET', '[]', '2018-07-14 14:47:45', '2018-07-14 14:47:45', NULL),
(280, 1, 'admin/company/contract/edit/1', 'GET', '[]', '2018-07-14 14:48:29', '2018-07-14 14:48:29', NULL),
(281, 1, 'admin/company/contract/edit/1', 'GET', '[]', '2018-07-14 14:49:16', '2018-07-14 14:49:16', NULL),
(282, 1, 'admin/company/contract/edit/1', 'GET', '[]', '2018-07-14 14:49:24', '2018-07-14 14:49:24', NULL),
(283, 1, 'admin/company/contract/edit/1', 'GET', '[]', '2018-07-14 14:49:39', '2018-07-14 14:49:39', NULL),
(284, 1, 'admin/company/contract/edit/1', 'GET', '[]', '2018-07-14 14:49:57', '2018-07-14 14:49:57', NULL),
(285, 1, 'admin/company/contract/edit/1', 'GET', '[]', '2018-07-14 14:50:08', '2018-07-14 14:50:08', NULL),
(286, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 14:50:13', '2018-07-14 14:50:13', NULL),
(287, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 14:50:20', '2018-07-14 14:50:20', NULL),
(288, 1, 'admin/company/contract/edit/1', 'GET', '[]', '2018-07-14 14:50:56', '2018-07-14 14:50:56', NULL),
(289, 1, 'admin/company/contract/edit/1', 'GET', '[]', '2018-07-14 14:51:47', '2018-07-14 14:51:47', NULL),
(290, 1, 'admin/company/contract/edit/1', 'GET', '[]', '2018-07-14 14:52:43', '2018-07-14 14:52:43', NULL),
(291, 1, 'admin/company/contract/edit/1', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"type\":\"0\",\"contract_date\":\"Zsylf\",\"contract_type\":\"0\",\"deadline\":\"Yuepw\",\"paper_contract\":\"0\",\"contact\":\"1\",\"payment_type\":\"0\",\"summation\":\"Nysgei lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\",\"warrantee\":\"Afvnr\",\"save_and_exit\":null}', '2018-07-14 14:53:21', '2018-07-14 14:53:21', NULL),
(292, 1, 'admin/company/contract/edit/0/data', 'GET', '[]', '2018-07-14 14:53:22', '2018-07-14 14:53:22', NULL),
(293, 1, 'admin/company/contract/edit/0/data', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"type\":\"0\",\"contract_date\":\"Zsylf\",\"contract_type\":\"0\",\"deadline\":\"Yuepw\",\"paper_contract\":\"0\",\"contact\":\"1\",\"payment_type\":\"0\",\"summation\":\"Nysgei lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\",\"warrantee\":\"Afvnr\",\"save_and_exit\":null}', '2018-07-14 15:00:24', '2018-07-14 15:00:24', NULL),
(294, 1, 'admin/company/edit/0', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 15:00:25', '2018-07-14 15:00:25', NULL),
(295, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 15:00:38', '2018-07-14 15:00:38', NULL),
(296, 1, 'admin/company/edit/1', 'GET', '[]', '2018-07-14 15:00:43', '2018-07-14 15:00:43', NULL),
(297, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 15:00:47', '2018-07-14 15:00:47', NULL),
(298, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 15:00:49', '2018-07-14 15:00:49', NULL),
(299, 1, 'admin/company/contract/edit/1', 'GET', '[]', '2018-07-14 15:00:53', '2018-07-14 15:00:53', NULL),
(300, 1, 'admin/company/contract/edit/1', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"type\":\"0\",\"contract_date\":\"Uaned\",\"contract_type\":\"0\",\"deadline\":\"Gyfsm\",\"paper_contract\":\"0\",\"contact\":\"1\",\"payment_type\":\"0\",\"summation\":\"Hjxrdc lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\",\"warrantee\":\"Vbrtl\",\"save_and_exit\":null}', '2018-07-14 15:00:59', '2018-07-14 15:00:59', NULL),
(301, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 15:01:00', '2018-07-14 15:01:00', NULL),
(302, 1, 'admin/company/contract/delete/1/2', 'GET', '[]', '2018-07-14 15:01:07', '2018-07-14 15:01:07', NULL),
(303, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 15:01:07', '2018-07-14 15:01:07', NULL),
(304, 1, 'admin/company/contract/edit/1/1', 'GET', '[]', '2018-07-14 15:01:18', '2018-07-14 15:01:18', NULL);
INSERT INTO `_accesslog` (`id`, `user_id`, `route`, `action`, `extra`, `created_at`, `updated_at`, `deleted_at`) VALUES
(305, 1, 'admin/company/contract/edit/1/1', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"type\":\"0\",\"contract_date\":\"0000-00-00\",\"contract_type\":\"0\",\"deadline\":\"Yuepw\",\"paper_contract\":\"0\",\"contact\":\"1\",\"payment_type\":\"0\",\"summation\":\"Nysgei lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\",\"warrantee\":\"Afvnr\",\"save_and_exit\":null}', '2018-07-14 15:01:21', '2018-07-14 15:01:21', NULL),
(306, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-07-14 15:01:22', '2018-07-14 15:01:22', NULL),
(307, 1, 'admin/company/contract/edit/1', 'GET', '[]', '2018-07-14 15:01:25', '2018-07-14 15:01:25', NULL),
(308, 1, 'admin/company/contract/edit/1', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"type\":\"0\",\"contract_date\":null,\"contract_type\":\"0\",\"deadline\":null,\"paper_contract\":\"0\",\"contact\":null,\"payment_type\":\"0\",\"summation\":null,\"warrantee\":null,\"save_and_exit\":null}', '2018-07-14 15:01:29', '2018-07-14 15:01:29', NULL),
(309, 1, 'admin/company/contract/edit/0/data', 'GET', '[]', '2018-07-14 15:01:30', '2018-07-14 15:01:30', NULL),
(310, 1, 'admin/company/contract/edit/0/data', 'GET', '[]', '2018-07-14 15:02:09', '2018-07-14 15:02:09', NULL),
(311, 1, 'admin/company/contract/edit/1', 'GET', '[]', '2018-07-14 15:02:15', '2018-07-14 15:02:15', NULL),
(312, 1, 'admin/company/contract/edit/1', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"type\":\"0\",\"contract_date\":null,\"contract_type\":\"0\",\"deadline\":null,\"paper_contract\":\"0\",\"contact\":null,\"payment_type\":\"0\",\"summation\":null,\"warrantee\":null,\"save_and_exit\":null}', '2018-07-14 15:02:21', '2018-07-14 15:02:21', NULL),
(313, 1, 'admin/company/contract/edit/0/data', 'GET', '[]', '2018-07-14 15:02:22', '2018-07-14 15:02:22', NULL),
(314, 1, 'admin/company/contract/edit/1', 'GET', '[]', '2018-07-14 15:02:43', '2018-07-14 15:02:43', NULL),
(315, 1, 'admin/company/contract/edit/1', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"type\":\"0\",\"contract_date\":null,\"contract_type\":\"0\",\"deadline\":null,\"paper_contract\":\"0\",\"contact\":null,\"payment_type\":\"0\",\"summation\":null,\"warrantee\":null,\"save_and_exit\":null}', '2018-07-14 15:03:04', '2018-07-14 15:03:04', NULL),
(316, 1, 'admin/company/contract/edit/1/0', 'GET', '[]', '2018-07-14 15:03:05', '2018-07-14 15:03:05', NULL),
(317, 1, 'admin/company/contract/edit/1/0', 'GET', '[]', '2018-07-14 15:07:30', '2018-07-14 15:07:30', NULL),
(318, 1, 'admin/company/contract/edit/1/0', 'GET', '[]', '2018-07-14 15:07:57', '2018-07-14 15:07:57', NULL),
(319, 1, 'admin/company/contract/edit/1', 'POST', '{\"_token\":\"aAygSALvOtUXcGKMlAoWyA082NU5lopXFyX7AcWF\",\"type\":\"0\",\"contract_date\":\"2018-07-12\",\"contract_type\":\"0\",\"deadline\":null,\"paper_contract\":\"0\",\"contact\":\"1\",\"payment_type\":\"0\",\"summation\":null,\"warrantee\":null,\"save_and_exit\":null}', '2018-07-14 15:08:13', '2018-07-14 15:08:13', NULL),
(320, 1, 'admin/company/contract/edit/1/0', 'GET', '[]', '2018-07-14 15:08:13', '2018-07-14 15:08:13', NULL),
(321, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-07-14 15:08:32', '2018-07-14 15:08:32', NULL),
(322, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 15:08:35', '2018-07-14 15:08:35', NULL),
(323, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-07-14 17:51:30', '2018-07-14 17:51:30', NULL),
(324, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 18:12:57', '2018-07-14 18:12:57', NULL),
(325, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 18:16:38', '2018-07-14 18:16:38', NULL),
(326, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 18:16:49', '2018-07-14 18:16:49', NULL),
(327, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 18:17:09', '2018-07-14 18:17:09', NULL),
(328, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 18:17:20', '2018-07-14 18:17:20', NULL),
(329, 1, 'admin/company/companysites/edit', 'GET', '[]', '2018-07-14 18:18:30', '2018-07-14 18:18:30', NULL),
(330, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 18:18:37', '2018-07-14 18:18:37', NULL),
(331, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 18:19:08', '2018-07-14 18:19:08', NULL),
(332, 1, 'admin/company/companysites/edit/1', 'GET', '[]', '2018-07-14 18:19:12', '2018-07-14 18:19:12', NULL),
(333, 1, 'admin/company/companysites/edit/1', 'GET', '[]', '2018-07-14 18:19:32', '2018-07-14 18:19:32', NULL),
(334, 1, 'admin/company/companysites/edit/1', 'GET', '[]', '2018-07-14 18:19:45', '2018-07-14 18:19:45', NULL),
(335, 1, 'admin/company/companysites/edit/1', 'GET', '[]', '2018-07-14 18:21:39', '2018-07-14 18:21:39', NULL),
(336, 1, 'admin/company/companysites/edit/1', 'GET', '[]', '2018-07-14 18:23:19', '2018-07-14 18:23:19', NULL),
(337, 1, 'admin/company/companysites/edit/1', 'GET', '[]', '2018-07-14 18:24:10', '2018-07-14 18:24:10', NULL),
(338, 1, 'admin/company/companysites/edit/1', 'POST', '{\"_token\":\"nXw5pAOTjrEv5cPavq7k6A90det2gn4o5DuAMQxX\",\"name\":\"Beverly Hills\",\"address\":\"7003 Beverly Dr\",\"save_and_exit\":null}', '2018-07-14 18:24:26', '2018-07-14 18:24:26', NULL),
(339, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 18:24:28', '2018-07-14 18:24:28', NULL),
(340, 1, 'admin/company/companysites/edit/1', 'GET', '[]', '2018-07-14 18:24:32', '2018-07-14 18:24:32', NULL),
(341, 1, 'admin/company/companysites/edit/1', 'POST', '{\"_token\":\"nXw5pAOTjrEv5cPavq7k6A90det2gn4o5DuAMQxX\",\"name\":null,\"address\":null,\"save_and_exit\":null}', '2018-07-14 18:24:36', '2018-07-14 18:24:36', NULL),
(342, 1, 'admin/company/companysites/edit/1/0', 'GET', '[]', '2018-07-14 18:24:37', '2018-07-14 18:24:37', NULL),
(343, 1, 'admin/company/companysites/edit/1/0', 'GET', '[]', '2018-07-14 18:26:12', '2018-07-14 18:26:12', NULL),
(344, 1, 'admin/company/companysites/edit/1/0', 'GET', '[]', '2018-07-14 18:26:36', '2018-07-14 18:26:36', NULL),
(345, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 18:29:05', '2018-07-14 18:29:05', NULL),
(346, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 18:29:09', '2018-07-14 18:29:09', NULL),
(347, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-07-14 18:29:14', '2018-07-14 18:29:14', NULL),
(348, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-07-14 18:29:18', '2018-07-14 18:29:18', NULL),
(349, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 18:29:22', '2018-07-14 18:29:22', NULL),
(350, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 18:30:59', '2018-07-14 18:30:59', NULL),
(351, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 18:38:31', '2018-07-14 18:38:31', NULL),
(352, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 18:38:44', '2018-07-14 18:38:44', NULL),
(353, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 18:39:05', '2018-07-14 18:39:05', NULL),
(354, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-07-14 18:39:14', '2018-07-14 18:39:14', NULL),
(355, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 18:39:19', '2018-07-14 18:39:19', NULL),
(356, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 18:39:38', '2018-07-14 18:39:38', NULL),
(357, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 18:39:52', '2018-07-14 18:39:52', NULL),
(358, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 18:45:31', '2018-07-14 18:45:31', NULL),
(359, 1, 'admin/company/list', 'GET', '[]', '2018-07-14 18:51:30', '2018-07-14 18:51:30', NULL),
(360, 1, 'admin/user/list', 'GET', '[]', '2018-07-18 11:03:34', '2018-07-18 11:03:34', NULL),
(361, 1, 'admin/user/edit', 'GET', '[]', '2018-07-18 11:17:00', '2018-07-18 11:17:00', NULL),
(362, 1, 'admin/user/list', 'GET', '[]', '2018-08-02 17:23:39', '2018-08-02 17:23:39', NULL),
(363, 1, 'admin/company/list', 'GET', '[]', '2018-08-02 17:23:44', '2018-08-02 17:23:44', NULL),
(364, 1, 'admin/company/edit/1', 'GET', '[]', '2018-08-02 17:23:48', '2018-08-02 17:23:48', NULL),
(365, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-02 17:23:53', '2018-08-02 17:23:53', NULL),
(366, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-08-02 17:23:56', '2018-08-02 17:23:56', NULL),
(367, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-02 17:24:00', '2018-08-02 17:24:00', NULL),
(368, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-08-02 17:24:05', '2018-08-02 17:24:05', NULL),
(369, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"candidates\"}', '2018-08-02 17:24:11', '2018-08-02 17:24:11', NULL),
(370, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-02 17:24:14', '2018-08-02 17:24:14', NULL),
(371, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-02 17:33:12', '2018-08-02 17:33:12', NULL),
(372, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"qRueco1tnxfNO0T28aQsctwreXnPTHwleIKQZ3rt\",\"model_name\":\"Language\",\"table_name\":\"languages\",\"menu_name\":\"Nyelvek\",\"menu_icon\":\"fa-font\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\"],\"column_comment\":[\"Nyelv\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"Nyelv (fok)\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-08-02 17:35:13', '2018-08-02 17:35:13', NULL),
(373, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"qRueco1tnxfNO0T28aQsctwreXnPTHwleIKQZ3rt\",\"model_name\":\"Language\",\"table_name\":\"languages\",\"menu_name\":\"Nyelvek\",\"menu_icon\":\"fa-font\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\"],\"column_comment\":[\"Nyelv\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"Nyelv (fok)\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-08-02 17:35:27', '2018-08-02 17:35:27', NULL),
(374, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-02 17:35:28', '2018-08-02 17:35:28', NULL),
(375, 1, 'admin/language/list', 'GET', '[]', '2018-08-02 17:35:39', '2018-08-02 17:35:39', NULL),
(376, 1, 'admin/language/edit', 'GET', '[]', '2018-08-02 17:35:43', '2018-08-02 17:35:43', NULL),
(377, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-02 17:36:04', '2018-08-02 17:36:04', NULL),
(378, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"qRueco1tnxfNO0T28aQsctwreXnPTHwleIKQZ3rt\",\"model_name\":\"Education\",\"table_name\":\"educations\",\"menu_name\":\"V\\u00e9gzetts\\u00e9g\",\"menu_icon\":\"fa-file-word-o\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\"],\"column_comment\":[\"V\\u00e9gzetts\\u00e9g\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"V\\u00e9gzetts\\u00e9g\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-08-02 17:37:20', '2018-08-02 17:37:20', NULL),
(379, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-02 17:37:23', '2018-08-02 17:37:23', NULL),
(380, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"qRueco1tnxfNO0T28aQsctwreXnPTHwleIKQZ3rt\",\"model_name\":\"Trainingarea\",\"table_name\":\"training_areas\",\"menu_name\":\"K\\u00e9pz\\u00e9si ter\\u00fclet\",\"menu_icon\":\"fa-code-fork\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\"],\"column_comment\":[\"K\\u00e9pz\\u00e9si ter\\u00fclet\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"K\\u00e9pz\\u00e9si ter\\u00fclet\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-08-02 17:39:32', '2018-08-02 17:39:32', NULL),
(381, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-02 17:39:34', '2018-08-02 17:39:34', NULL),
(382, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"qRueco1tnxfNO0T28aQsctwreXnPTHwleIKQZ3rt\",\"model_name\":\"Specialtyeducation\",\"table_name\":\"specialty_educations\",\"menu_name\":\"Speci\\u00e1lis v\\u00e9gzetts\\u00e9g\",\"menu_icon\":\"fa-user-secret\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\"],\"column_comment\":[\"Speci\\u00e1lis v\\u00e9gzetts\\u00e9g\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"Speci\\u00e1lis v\\u00e9gzetts\\u00e9g\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-08-02 17:42:37', '2018-08-02 17:42:37', NULL),
(383, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-02 17:42:39', '2018-08-02 17:42:39', NULL),
(384, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"qRueco1tnxfNO0T28aQsctwreXnPTHwleIKQZ3rt\",\"model_name\":\"Driverlicense\",\"table_name\":\"driver_license\",\"menu_name\":\"Jogos\\u00edtv\\u00e1ny\",\"menu_icon\":\"fa-car\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\"],\"column_comment\":[\"Jogos\\u00edtv\\u00e1ny\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"Jogos\\u00edtv\\u00e1ny\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-08-02 17:44:14', '2018-08-02 17:44:14', NULL),
(385, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-02 17:44:16', '2018-08-02 17:44:16', NULL),
(386, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"qRueco1tnxfNO0T28aQsctwreXnPTHwleIKQZ3rt\",\"model_name\":\"Workcategory\",\"table_name\":\"workcategories\",\"menu_name\":\"Munka kateg\\u00f3ri\\u00e1k\",\"menu_icon\":\"fa-briefcase\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\"],\"column_comment\":[\"Milyen munka \\u00e9rdekli?\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"Munka kateg\\u00f3ria\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-08-02 17:46:04', '2018-08-02 17:46:04', NULL),
(387, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-02 17:46:06', '2018-08-02 17:46:06', NULL),
(388, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"qRueco1tnxfNO0T28aQsctwreXnPTHwleIKQZ3rt\",\"model_name\":\"City\",\"table_name\":\"cities\",\"menu_name\":\"V\\u00e1ros\",\"menu_icon\":\"fa-building\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\"],\"column_comment\":[\"V\\u00e1ros\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"V\\u00e1ros\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-08-02 17:47:11', '2018-08-02 17:47:11', NULL),
(389, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-02 17:47:14', '2018-08-02 17:47:14', NULL),
(390, 1, 'admin/city/list', 'GET', '[]', '2018-08-02 18:08:39', '2018-08-02 18:08:39', NULL),
(391, 1, 'admin/user/list', 'GET', '[]', '2018-08-03 08:14:41', '2018-08-03 08:14:41', NULL),
(392, 1, 'admin/user/edit', 'GET', '[]', '2018-08-03 08:14:47', '2018-08-03 08:14:47', NULL),
(393, 1, 'admin/workcategory/list', 'GET', '[]', '2018-08-14 03:42:19', '2018-08-14 03:42:19', NULL),
(394, 1, 'admin/driverlicense/list', 'GET', '[]', '2018-08-14 03:42:30', '2018-08-14 03:42:30', NULL),
(395, 1, 'admin/driverlicense/list', 'GET', '[]', '2018-08-14 03:42:30', '2018-08-14 03:42:30', NULL),
(396, 1, 'admin/driverlicense/list', 'GET', '[]', '2018-08-14 03:42:33', '2018-08-14 03:42:33', NULL),
(397, 1, 'admin/driverlicense/list', 'GET', '[]', '2018-08-14 03:42:41', '2018-08-14 03:42:41', NULL),
(398, 1, 'admin/driverlicense/list', 'GET', '[]', '2018-08-14 03:42:48', '2018-08-14 03:42:48', NULL),
(399, 1, 'admin/driverlicense/list', 'GET', '[]', '2018-08-14 03:42:52', '2018-08-14 03:42:52', NULL),
(400, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-14 03:43:15', '2018-08-14 03:43:15', NULL),
(401, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"c5t068DZLUmI7f3odtRM3Jde263eaXBUGlJcT9Co\",\"model_name\":\"Teaor\",\"table_name\":\"teaor\",\"menu_name\":\"Nemzetgazdas\\u00e1gi \\u00e1gazat(TE\\u00c1OR)\",\"menu_icon\":\"fa fa-flag\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"key\",\"name\"],\"column_comment\":[null,\"N\\u00e9v\"],\"column_type\":[\"VARCHAR(255)\",\"TEXT\"],\"column_fillable\":[\"1\",\"1\"],\"column_cast\":[\"0\",\"0\"],\"column_getter\":[\"0\",\"0\"],\"column_label\":[\"Azonos\\u00edt\\u00f3\",\"Megnevez\\u00e9s\"],\"column_input\":[\"input\",\"input\"],\"column_value\":[null,null],\"column_search\":[\"0\",\"0\"],\"column_search_input\":[\"text\",\"text\"],\"column_search_value\":[null,null],\"required\":[\"1\",\"1\"],\"save_and_exit\":null}', '2018-08-14 03:45:37', '2018-08-14 03:45:37', NULL),
(402, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-14 03:45:51', '2018-08-14 03:45:51', NULL),
(403, 1, 'admin/education/list', 'GET', '[]', '2018-08-14 03:46:19', '2018-08-14 03:46:19', NULL),
(404, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-14 03:46:50', '2018-08-14 03:46:50', NULL),
(405, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"c5t068DZLUmI7f3odtRM3Jde263eaXBUGlJcT9Co\",\"model_name\":\"Position\",\"table_name\":\"positions\",\"menu_name\":\"Beoszt\\u00e1s\",\"menu_icon\":\"fa fa-id-badge\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\"],\"column_comment\":[\"Beoszt\\u00e1s\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"Beoszt\\u00e1s\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-08-14 03:48:05', '2018-08-14 03:48:05', NULL),
(406, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-14 03:48:08', '2018-08-14 03:48:08', NULL),
(407, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"c5t068DZLUmI7f3odtRM3Jde263eaXBUGlJcT9Co\",\"model_name\":\"Feor\",\"table_name\":\"feor\",\"menu_name\":\"Foglalkoz\\u00e1sok\",\"menu_icon\":\"fa fa-legal\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"key\",\"name\"],\"column_comment\":[\"Azonos\\u00edt\\u00f3\",\"Megnevez\\u00e9s\"],\"column_type\":[\"VARCHAR(255)\",\"TEXT\"],\"column_fillable\":[\"1\",\"1\"],\"column_cast\":[\"0\",\"0\"],\"column_getter\":[\"0\",\"0\"],\"column_label\":[\"Azonos\\u00edt\\u00f3\",\"Megnevez\\u00e9s\"],\"column_input\":[\"input\",\"input\"],\"column_value\":[null,null],\"column_search\":[\"0\",\"0\"],\"column_search_input\":[\"text\",\"text\"],\"column_search_value\":[null,null],\"required\":[\"1\",\"1\"],\"save_and_exit\":null}', '2018-08-14 03:54:56', '2018-08-14 03:54:56', NULL),
(408, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-14 03:54:59', '2018-08-14 03:54:59', NULL),
(409, 1, 'admin/specialtyeducation/list', 'GET', '[]', '2018-08-14 03:59:49', '2018-08-14 03:59:49', NULL),
(410, 1, 'admin/specialtyeducation/list', 'GET', '[]', '2018-08-14 04:00:56', '2018-08-14 04:00:56', NULL),
(411, 1, 'admin/specialtyeducation/list', 'GET', '[]', '2018-08-14 04:01:23', '2018-08-14 04:01:23', NULL),
(412, 1, 'admin/specialtyeducation/list', 'GET', '[]', '2018-08-14 04:01:30', '2018-08-14 04:01:30', NULL),
(413, 1, 'admin/specialtyeducation/list', 'GET', '[]', '2018-08-14 04:01:54', '2018-08-14 04:01:54', NULL),
(414, 1, 'admin/specialtyeducation/list', 'GET', '[]', '2018-08-14 04:02:20', '2018-08-14 04:02:20', NULL),
(415, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-14 04:03:29', '2018-08-14 04:03:29', NULL),
(416, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"c5t068DZLUmI7f3odtRM3Jde263eaXBUGlJcT9Co\",\"model_name\":\"Worker\",\"table_name\":\"workers\",\"menu_name\":\"Feln\\u0151ttek\",\"menu_icon\":\"fa-briefcase\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\",\"phone\",\"email\",\"coutry\",\"zip\",\"city\",\"street\",\"house_number\",\"building\",\"stairway\",\"floor\",\"door\",\"residence_address\",\"id_number\",\"birthplace\",\"birthdate\",\"mother_name\",\"nationality\",\"passport\",\"gender\",\"tax_number\",\"taj\",\"bank_account\",\"Munkahey\",\"work_start\",\"contract_date\",\"job_date\",\"eu_date\",\"lungfilter_date\",\"logout_date\",\"education\",\"training_area\",\"specialtyeducation\",\"driving_license\",\"language\",\"newsletter\",\"work_category\",\"work_place\"],\"column_comment\":[\"n\\u00e9v\",\"Telefon\",\"Email\",\"Orsz\\u00e1g\",\"ir\\u00e1ny\\u00edt\\u00f3sz\\u00e1m\",\"V\\u00e1ros\",\"utca\",\"H\\u00e1zsz\\u00e1m\",\"\\u00c9p\\u00fclet\",\"L\\u00e9pcs\\u0151h\\u00e1z\",\"Emelet\",\"Ajt\\u00f3\",\"Tart\\u00f3zkod\\u00e1si c\\u00edm\",\"Szem\\u00e9lyi Igazolv\\u00e1ny sz\\u00e1m\",\"Sz\\u00fclet\\u00e9si hely\",\"Sz\\u00fclet\\u00e9si d\\u00e1tum\",\"Anyja neve\",\"\\u00c1llampolg\\u00e1rs\\u00e1g\",\"\\u00datlev\\u00e9lsz\\u00e1m\",\"Neme\",\"Ad\\u00f3sz\\u00e1m\",\"Tajsz\\u00e1m\",\"Banksz\\u00e1mlasz\\u00e1m\",\"Munkahely\",\"Bejelent\\u00e9s d\\u00e1tuma\",\"Szerz\\u0151d\\u00e9s al\\u00e1\\u00edr\\u00e1s d\\u00e1tuma\",\"Munkak\\u00f6ri al\\u00e1\\u00edr\\u00e1s d\\u00e1tuma\",\"E\\u00dc kisk\\u00f6nyv lej\\u00e1rati d\\u00e1tuma\",\"T\\u00fcd\\u0151sz\\u0171r\\u0151 lej\\u00e1rati d\\u00e1tuma\",\"Kil\\u00e9p\\u00e9s\",\"Legmagasabb iskolai v\\u00e9gzetts\\u00e9g\",\"K\\u00e9pz\\u00e9si ter\\u00fclet\",\"Speci\\u00e1lis v\\u00e9gzetts\\u00e9gek\",\"Jogos\\u00edtv\\u00e1ny\",\"Nyelv\",\"H\\u00edrlevelt k\\u00e9r?\",\"Milyen munka \\u00e9rdekli?\",\"Hol szeretne dolgozni?\"],\"column_type\":[\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"INT\",\"INT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"DATE\",\"TEXT\",\"INT\",\"TEXT\",\"TEXT\",\"INT\",\"TEXT\",\"TEXT\",\"TEXT\",\"DATE\",\"DATE\",\"DATE\",\"DATE\",\"DATE\",\"DATE\",\"INT\",\"INT\",\"INT\",\"INT\",\"TEXT\",\"INT\",\"TEXT\",\"INT\"],\"column_fillable\":[\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\"],\"column_cast\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"1\",\"0\",\"1\",\"0\"],\"column_getter\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_label\":[\"N\\u00e9v\",\"Telefon\",\"Email\",\"Orsz\\u00e1g\",\"Ir\\u00e1ny\\u00edt\\u00f3sz\\u00e1m\",\"V\\u00e1ros\",\"Utca\",\"H\\u00e1zsz\\u00e1m\",\"\\u00c9p\\u00fclet\",\"L\\u00e9pcs\\u0151h\\u00e1z\",\"Emelet\",\"Ajt\\u00f3\",\"Tart\\u00f3zkod\\u00e1si c\\u00edm\",\"Szem\\u00e9lyi Igazolv\\u00e1ny sz\\u00e1m\",\"Sz\\u00fclet\\u00e9si hely\",\"Sz\\u00fclet\\u00e9si d\\u00e1tum\",\"Anyja neve\",\"\\u00c1llampolg\\u00e1rs\\u00e1g\",\"\\u00datlev\\u00e9lsz\\u00e1m\",\"Neme\",\"Ad\\u00f3sz\\u00e1m\",\"Tajsz\\u00e1m\",\"Banksz\\u00e1mlasz\\u00e1m\",\"Munkahely\",\"Bejelent\\u00e9s d\\u00e1tuma\",\"Szerz\\u0151d\\u00e9s al\\u00e1\\u00edr\\u00e1s d\\u00e1tuma\",\"Munkak\\u00f6ri al\\u00e1\\u00edr\\u00e1s d\\u00e1tuma\",\"E\\u00dc kisk\\u00f6nyv lej\\u00e1rati d\\u00e1tuma\",\"T\\u00fcd\\u0151sz\\u0171r\\u0151 lej\\u00e1rati d\\u00e1tuma\",\"Kil\\u00e9p\\u00e9s\",\"Legmagasabb iskolai v\\u00e9gzetts\\u00e9g\",\"K\\u00e9pz\\u00e9si ter\\u00fclet\",\"Speci\\u00e1lis v\\u00e9gzetts\\u00e9gek\",\"Jogos\\u00edtv\\u00e1ny\",\"Nyelv\",\"H\\u00edrlevelt k\\u00e9r?\",\"Milyen munka \\u00e9rdekli?\",\"Hol szeretne dolgozni?\"],\"column_input\":[\"input\",\"input\",\"input\",\"input\",\"input\",\"select\",\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"date\",\"input\",\"select\",\"input\",\"select\",\"input\",\"input\",\"input\",\"input\",\"date\",\"date\",\"date\",\"date\",\"date\",\"date\",\"select\",\"select\",\"select\",\"select\",\"select\",\"select\",\"select\",\"select\"],\"column_value\":[null,null,null,null,null,\"[]\",null,null,null,null,null,null,null,null,null,null,null,null,null,\"[0=>\'F\\u00e9rfi\',1=>\'N\\u0151\']\",null,null,null,null,null,null,null,null,null,null,\"[]\",\"[]\",\"[]\",\"[]\",null,\"[0=>\'Nem\',1=>\'Igen\']\",\"[]\",\"[]\"],\"column_search\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_search_input\":[\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\"],\"column_search_value\":[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],\"required\":[\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\"],\"save_and_exit\":null}', '2018-08-14 04:28:25', '2018-08-14 04:28:25', NULL),
(417, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"c5t068DZLUmI7f3odtRM3Jde263eaXBUGlJcT9Co\",\"model_name\":\"Worker\",\"table_name\":\"workers\",\"menu_name\":\"Feln\\u0151ttek\",\"menu_icon\":\"fa-briefcase\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\",\"phone\",\"email\",\"coutry\",\"zip\",\"city\",\"street\",\"house_number\",\"building\",\"stairway\",\"floor\",\"door\",\"residence_address\",\"id_number\",\"birthplace\",\"birthdate\",\"mother_name\",\"nationality\",\"passport\",\"gender\",\"tax_number\",\"taj\",\"bank_account\",\"Munkahey\",\"work_start\",\"contract_date\",\"job_date\",\"eu_date\",\"lungfilter_date\",\"logout_date\",\"education\",\"training_area\",\"specialtyeducation\",\"driving_license\",\"language\",\"newsletter\",\"work_category\",\"work_place\"],\"column_comment\":[\"n\\u00e9v\",\"Telefon\",\"Email\",\"Orsz\\u00e1g\",\"ir\\u00e1ny\\u00edt\\u00f3sz\\u00e1m\",\"V\\u00e1ros\",\"utca\",\"H\\u00e1zsz\\u00e1m\",\"\\u00c9p\\u00fclet\",\"L\\u00e9pcs\\u0151h\\u00e1z\",\"Emelet\",\"Ajt\\u00f3\",\"Tart\\u00f3zkod\\u00e1si c\\u00edm\",\"Szem\\u00e9lyi Igazolv\\u00e1ny sz\\u00e1m\",\"Sz\\u00fclet\\u00e9si hely\",\"Sz\\u00fclet\\u00e9si d\\u00e1tum\",\"Anyja neve\",\"\\u00c1llampolg\\u00e1rs\\u00e1g\",\"\\u00datlev\\u00e9lsz\\u00e1m\",\"Neme\",\"Ad\\u00f3sz\\u00e1m\",\"Tajsz\\u00e1m\",\"Banksz\\u00e1mlasz\\u00e1m\",\"Munkahely\",\"Bejelent\\u00e9s d\\u00e1tuma\",\"Szerz\\u0151d\\u00e9s al\\u00e1\\u00edr\\u00e1s d\\u00e1tuma\",\"Munkak\\u00f6ri al\\u00e1\\u00edr\\u00e1s d\\u00e1tuma\",\"E\\u00dc kisk\\u00f6nyv lej\\u00e1rati d\\u00e1tuma\",\"T\\u00fcd\\u0151sz\\u0171r\\u0151 lej\\u00e1rati d\\u00e1tuma\",\"Kil\\u00e9p\\u00e9s\",\"Legmagasabb iskolai v\\u00e9gzetts\\u00e9g\",\"K\\u00e9pz\\u00e9si ter\\u00fclet\",\"Speci\\u00e1lis v\\u00e9gzetts\\u00e9gek\",\"Jogos\\u00edtv\\u00e1ny\",\"Nyelv\",\"H\\u00edrlevelt k\\u00e9r?\",\"Milyen munka \\u00e9rdekli?\",\"Hol szeretne dolgozni?\"],\"column_type\":[\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"INT\",\"INT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"TEXT\",\"DATE\",\"TEXT\",\"INT\",\"TEXT\",\"TEXT\",\"INT\",\"TEXT\",\"TEXT\",\"TEXT\",\"DATE\",\"DATE\",\"DATE\",\"DATE\",\"DATE\",\"DATE\",\"INT\",\"INT\",\"INT\",\"INT\",\"TEXT\",\"INT\",\"TEXT\",\"INT\"],\"column_fillable\":[\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\"],\"column_cast\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"1\",\"0\",\"1\",\"0\"],\"column_getter\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_label\":[\"N\\u00e9v\",\"Telefon\",\"Email\",\"Orsz\\u00e1g\",\"Ir\\u00e1ny\\u00edt\\u00f3sz\\u00e1m\",\"V\\u00e1ros\",\"Utca\",\"H\\u00e1zsz\\u00e1m\",\"\\u00c9p\\u00fclet\",\"L\\u00e9pcs\\u0151h\\u00e1z\",\"Emelet\",\"Ajt\\u00f3\",\"Tart\\u00f3zkod\\u00e1si c\\u00edm\",\"Szem\\u00e9lyi Igazolv\\u00e1ny sz\\u00e1m\",\"Sz\\u00fclet\\u00e9si hely\",\"Sz\\u00fclet\\u00e9si d\\u00e1tum\",\"Anyja neve\",\"\\u00c1llampolg\\u00e1rs\\u00e1g\",\"\\u00datlev\\u00e9lsz\\u00e1m\",\"Neme\",\"Ad\\u00f3sz\\u00e1m\",\"Tajsz\\u00e1m\",\"Banksz\\u00e1mlasz\\u00e1m\",\"Munkahely\",\"Bejelent\\u00e9s d\\u00e1tuma\",\"Szerz\\u0151d\\u00e9s al\\u00e1\\u00edr\\u00e1s d\\u00e1tuma\",\"Munkak\\u00f6ri al\\u00e1\\u00edr\\u00e1s d\\u00e1tuma\",\"E\\u00dc kisk\\u00f6nyv lej\\u00e1rati d\\u00e1tuma\",\"T\\u00fcd\\u0151sz\\u0171r\\u0151 lej\\u00e1rati d\\u00e1tuma\",\"Kil\\u00e9p\\u00e9s\",\"Legmagasabb iskolai v\\u00e9gzetts\\u00e9g\",\"K\\u00e9pz\\u00e9si ter\\u00fclet\",\"Speci\\u00e1lis v\\u00e9gzetts\\u00e9gek\",\"Jogos\\u00edtv\\u00e1ny\",\"Nyelv\",\"H\\u00edrlevelt k\\u00e9r?\",\"Milyen munka \\u00e9rdekli?\",\"Hol szeretne dolgozni?\"],\"column_input\":[\"input\",\"input\",\"input\",\"input\",\"input\",\"select\",\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"input\",\"date\",\"input\",\"select\",\"input\",\"select\",\"input\",\"input\",\"input\",\"input\",\"date\",\"date\",\"date\",\"date\",\"date\",\"date\",\"select\",\"select\",\"select\",\"select\",\"select\",\"select\",\"select\",\"select\"],\"column_value\":[null,null,null,null,null,\"[]\",null,null,null,null,null,null,null,null,null,null,null,null,null,\"[0=>\'F\\u00e9rfi\',1=>\'N\\u0151\']\",null,null,null,null,null,null,null,null,null,null,\"[]\",\"[]\",\"[]\",\"[]\",null,\"[0=>\'Nem\',1=>\'Igen\']\",\"[]\",\"[]\"],\"column_search\":[\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\"],\"column_search_input\":[\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\",\"text\"],\"column_search_value\":[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null],\"required\":[\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\",\"1\"],\"save_and_exit\":null}', '2018-08-14 04:29:20', '2018-08-14 04:29:20', NULL),
(418, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-14 04:29:35', '2018-08-14 04:29:35', NULL),
(419, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 04:29:44', '2018-08-14 04:29:44', NULL),
(420, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 04:29:50', '2018-08-14 04:29:50', NULL),
(421, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 04:30:23', '2018-08-14 04:30:23', NULL),
(422, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 04:30:40', '2018-08-14 04:30:40', NULL),
(423, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 04:31:36', '2018-08-14 04:31:36', NULL),
(424, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 04:31:43', '2018-08-14 04:31:43', NULL),
(425, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 04:32:01', '2018-08-14 04:32:01', NULL),
(426, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 04:32:05', '2018-08-14 04:32:05', NULL),
(427, 1, 'admin/modules', 'GET', '[]', '2018-08-14 04:40:16', '2018-08-14 04:40:16', NULL),
(428, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-14 04:40:26', '2018-08-14 04:40:26', NULL),
(429, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"c5t068DZLUmI7f3odtRM3Jde263eaXBUGlJcT9Co\",\"model_name\":\"Document\",\"table_name\":\"documents\",\"menu_name\":\"Dokumentumok\",\"menu_icon\":\"fa fa-file\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\"],\"column_comment\":[\"Megnevez\\u00e9s\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"Megnevez\\u00e9s\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-08-14 04:41:16', '2018-08-14 04:41:16', NULL),
(430, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-14 04:41:20', '2018-08-14 04:41:20', NULL),
(431, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"c5t068DZLUmI7f3odtRM3Jde263eaXBUGlJcT9Co\",\"model_name\":\"WorkerDocument\",\"table_name\":\"worker_documents\",\"menu_name\":\"Docs\",\"menu_icon\":\"fa fa-file-o\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"document_id\",\"date\",\"comment\",\"file\"],\"column_comment\":[\"Dokumentum t\\u00edpusa\",\"D\\u00e1tum\",\"megjegyz\\u00e9s\",\"F\\u00e1jl\"],\"column_type\":[\"INT\",\"DATE\",\"TEXT\",\"TEXT\"],\"column_fillable\":[\"1\",\"1\",\"1\",\"1\"],\"column_cast\":[\"0\",\"0\",\"0\",\"0\"],\"column_getter\":[\"0\",\"0\",\"0\",\"0\"],\"column_label\":[\"Dokumentum t\\u00edpusa\",\"D\\u00e1tum\",\"Megjegyz\\u00e9s\",\"F\\u00e1jl\"],\"column_input\":[\"select\",\"date\",\"input\",\"file\"],\"column_value\":[\"[]\",null,null,null],\"column_search\":[\"0\",\"0\",\"0\",\"0\"],\"column_search_input\":[\"text\",\"text\",\"text\",\"text\"],\"column_search_value\":[null,null,null,null],\"required\":[\"1\",\"1\",\"1\",\"1\"],\"save_and_exit\":null}', '2018-08-14 04:44:14', '2018-08-14 04:44:14', NULL),
(432, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-14 04:44:16', '2018-08-14 04:44:16', NULL),
(433, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 04:47:10', '2018-08-14 04:47:10', NULL),
(434, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 04:47:45', '2018-08-14 04:47:45', NULL),
(435, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 04:48:48', '2018-08-14 04:48:48', NULL),
(436, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 04:49:05', '2018-08-14 04:49:05', NULL),
(437, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 04:54:16', '2018-08-14 04:54:16', NULL),
(438, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-14 04:54:30', '2018-08-14 04:54:30', NULL),
(439, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"c5t068DZLUmI7f3odtRM3Jde263eaXBUGlJcT9Co\",\"model_name\":\"ProjectStatus\",\"table_name\":\"project_status\",\"menu_name\":\"Projekt st\\u00e1tuszok\",\"menu_icon\":\"fa fa-star\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"1\",\"export\":\"1\",\"column_name\":[\"name\"],\"column_comment\":[\"Megnevez\\u00e9s\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"Megnevez\\u00e9s\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-08-14 04:56:06', '2018-08-14 04:56:06', NULL),
(440, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-14 04:56:09', '2018-08-14 04:56:09', NULL),
(441, 1, 'admin/projectstatus/edit', 'GET', '[]', '2018-08-14 04:56:43', '2018-08-14 04:56:43', NULL),
(442, 1, 'admin/projectstatus/edit', 'POST', '{\"_token\":\"c5t068DZLUmI7f3odtRM3Jde263eaXBUGlJcT9Co\",\"name\":\"sdafas\"}', '2018-08-14 04:56:51', '2018-08-14 04:56:51', NULL),
(443, 1, 'admin/projectstatus/edit/1/data', 'GET', '[]', '2018-08-14 04:56:53', '2018-08-14 04:56:53', NULL),
(444, 1, 'admin/projectstatus/edit/1/data', 'POST', '{\"_token\":\"c5t068DZLUmI7f3odtRM3Jde263eaXBUGlJcT9Co\",\"name\":\"sdafas\",\"save_and_exit\":null}', '2018-08-14 04:56:56', '2018-08-14 04:56:56', NULL),
(445, 1, 'admin/projectstatus/list', 'GET', '[]', '2018-08-14 04:56:58', '2018-08-14 04:56:58', NULL),
(446, 1, 'admin/projectstatus/list', 'GET', '[]', '2018-08-14 05:06:48', '2018-08-14 05:06:48', NULL),
(447, 1, 'admin/projectstatus/list', 'GET', '[]', '2018-08-14 05:09:23', '2018-08-14 05:09:23', NULL),
(448, 1, 'admin/acl/list', 'GET', '[]', '2018-08-14 05:10:24', '2018-08-14 05:10:24', NULL),
(449, 1, 'admin/acl/edit', 'GET', '[]', '2018-08-14 05:10:28', '2018-08-14 05:10:28', NULL),
(450, 1, 'admin/company/list', 'GET', '[]', '2018-08-14 05:15:52', '2018-08-14 05:15:52', NULL),
(451, 1, 'admin/company/list', 'GET', '[]', '2018-08-14 05:16:44', '2018-08-14 05:16:44', NULL),
(452, 1, 'admin/language/list', 'GET', '[]', '2018-08-14 05:16:53', '2018-08-14 05:16:53', NULL),
(453, 1, 'admin/language/list', 'GET', '[]', '2018-08-14 05:17:59', '2018-08-14 05:17:59', NULL),
(454, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 05:18:04', '2018-08-14 05:18:04', NULL),
(455, 1, 'admin/projectstatus/list', 'GET', '[]', '2018-08-14 05:18:19', '2018-08-14 05:18:19', NULL),
(456, 1, 'admin/projectstatus/list', 'GET', '[]', '2018-08-14 05:19:36', '2018-08-14 05:19:36', NULL),
(457, 1, 'admin/language/list', 'GET', '[]', '2018-08-14 16:50:00', '2018-08-14 16:50:00', NULL),
(458, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 17:05:23', '2018-08-14 17:05:23', NULL),
(459, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 17:16:21', '2018-08-14 17:16:21', NULL),
(460, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 17:20:32', '2018-08-14 17:20:32', NULL),
(461, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:20:40', '2018-08-14 17:20:40', NULL),
(462, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:21:56', '2018-08-14 17:21:56', NULL),
(463, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:23:30', '2018-08-14 17:23:30', NULL),
(464, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:39:53', '2018-08-14 17:39:53', NULL),
(465, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:41:01', '2018-08-14 17:41:01', NULL),
(466, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:42:28', '2018-08-14 17:42:28', NULL),
(467, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:43:00', '2018-08-14 17:43:00', NULL),
(468, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:43:32', '2018-08-14 17:43:32', NULL),
(469, 1, 'admin/city/list', 'GET', '[]', '2018-08-14 17:43:43', '2018-08-14 17:43:43', NULL),
(470, 1, 'admin/city/edit', 'GET', '[]', '2018-08-14 17:43:47', '2018-08-14 17:43:47', NULL),
(471, 1, 'admin/city/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"Budapest\",\"save_and_exit\":null}', '2018-08-14 17:43:55', '2018-08-14 17:43:55', NULL),
(472, 1, 'admin/city/list', 'GET', '[]', '2018-08-14 17:43:57', '2018-08-14 17:43:57', NULL),
(473, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 17:44:02', '2018-08-14 17:44:02', NULL),
(474, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:44:05', '2018-08-14 17:44:05', NULL),
(475, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:44:22', '2018-08-14 17:44:22', NULL),
(476, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:45:57', '2018-08-14 17:45:57', NULL),
(477, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:46:32', '2018-08-14 17:46:32', NULL),
(478, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:53:22', '2018-08-14 17:53:22', NULL),
(479, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:56:39', '2018-08-14 17:56:39', NULL),
(480, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:57:13', '2018-08-14 17:57:13', NULL),
(481, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:57:54', '2018-08-14 17:57:54', NULL),
(482, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 17:59:12', '2018-08-14 17:59:12', NULL),
(483, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:00:48', '2018-08-14 18:00:48', NULL),
(484, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:06:15', '2018-08-14 18:06:15', NULL),
(485, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:06:49', '2018-08-14 18:06:49', NULL),
(486, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:11:28', '2018-08-14 18:11:28', NULL),
(487, 1, 'admin/company/list', 'GET', '[]', '2018-08-14 18:11:55', '2018-08-14 18:11:55', NULL),
(488, 1, 'admin/company/edit', 'GET', '[]', '2018-08-14 18:11:59', '2018-08-14 18:11:59', NULL),
(489, 1, 'admin/company/list', 'GET', '[]', '2018-08-14 18:12:04', '2018-08-14 18:12:04', NULL),
(490, 1, 'admin/company/edit/1', 'GET', '[]', '2018-08-14 18:12:10', '2018-08-14 18:12:10', NULL),
(491, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-14 18:12:15', '2018-08-14 18:12:15', NULL),
(492, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-14 18:12:19', '2018-08-14 18:12:19', NULL),
(493, 1, 'admin/user/list', 'GET', '[]', '2018-08-14 18:17:20', '2018-08-14 18:17:20', NULL),
(494, 1, 'admin/user/list', 'GET', '[]', '2018-08-14 18:17:21', '2018-08-14 18:17:21', NULL),
(495, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 18:17:21', '2018-08-14 18:17:21', NULL),
(496, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:17:24', '2018-08-14 18:17:24', NULL),
(497, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:18:09', '2018-08-14 18:18:09', NULL),
(498, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:18:24', '2018-08-14 18:18:24', NULL),
(499, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:19:39', '2018-08-14 18:19:39', NULL),
(500, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:20:49', '2018-08-14 18:20:49', NULL),
(501, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:22:12', '2018-08-14 18:22:12', NULL),
(502, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:22:23', '2018-08-14 18:22:23', NULL),
(503, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:24:41', '2018-08-14 18:24:41', NULL),
(504, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:25:07', '2018-08-14 18:25:07', NULL),
(505, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:26:04', '2018-08-14 18:26:04', NULL),
(506, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:26:24', '2018-08-14 18:26:24', NULL),
(507, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:26:49', '2018-08-14 18:26:49', NULL),
(508, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:27:17', '2018-08-14 18:27:17', NULL),
(509, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:27:51', '2018-08-14 18:27:51', NULL),
(510, 1, 'admin/worker/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"Beverly Hills\",\"phone\":\"3105559520\",\"email\":\"bhills_9520@mailinator.com\",\"coutry\":\"Ywlij\",\"zip\":\"90210\",\"city\":\"1\",\"street\":\"Uysqz\",\"house_number\":\"Lwnpv\",\"building\":\"Hnbby\",\"stairway\":\"Qhmau\",\"floor\":\"Obhvf\",\"door\":\"Zwkug\",\"residence_address\":\"9520 Beverly Dr\",\"id_number\":\"Zrkdx\",\"birthplace\":\"Yukzn\",\"birthdate\":\"Vjtxh\",\"mother_name\":\"Beverly Hills\",\"nationality\":null,\"passport\":\"Hzcrh\",\"gender\":\"0\",\"tax_number\":\"Wajzs\",\"taj\":\"Jocmg\",\"bank_account\":\"Qstew\",\"newsletter\":\"0\",\"work_category\":null,\"work_place\":\"1\",\"education\":null,\"training_area\":null,\"specialtyeducation\":null,\"driving_license\":null,\"language\":[null],\"Munkahey\":\"Otsqt\",\"work_start\":\"Srgug\",\"contract_date\":\"Znwtk\",\"job_date\":\"Mfhkh\",\"eu_date\":\"Bihvg\",\"lungfilter_date\":\"Xxioa\",\"logout_date\":\"Ijgbl\"}', '2018-08-14 18:28:04', '2018-08-14 18:28:04', NULL),
(511, 1, 'admin/worker/edit/0/data', 'GET', '[]', '2018-08-14 18:28:05', '2018-08-14 18:28:05', NULL),
(512, 1, 'admin/worker/edit/0/data', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"Beverly Hills\",\"phone\":\"3105559520\",\"email\":\"bhills_9520@mailinator.com\",\"coutry\":\"Ywlij\",\"zip\":\"90210\",\"city\":\"1\",\"street\":\"Uysqz\",\"house_number\":\"Lwnpv\",\"building\":\"Hnbby\",\"stairway\":\"Qhmau\",\"floor\":\"Obhvf\",\"door\":\"Zwkug\",\"residence_address\":\"9520 Beverly Dr\",\"id_number\":\"Zrkdx\",\"birthplace\":\"Yukzn\",\"birthdate\":\"Vjtxh\",\"mother_name\":\"Beverly Hills\",\"nationality\":null,\"passport\":\"Hzcrh\",\"gender\":\"0\",\"tax_number\":\"Wajzs\",\"taj\":null,\"bank_account\":\"Qstew\",\"newsletter\":\"0\",\"work_category\":null,\"work_place\":\"1\",\"education\":null,\"training_area\":null,\"specialtyeducation\":null,\"driving_license\":null,\"Munkahey\":\"Otsqt\",\"work_start\":\"Srgug\",\"contract_date\":\"Znwtk\",\"job_date\":\"Mfhkh\",\"eu_date\":\"Bihvg\",\"lungfilter_date\":\"Xxioa\",\"logout_date\":\"Ijgbl\"}', '2018-08-14 18:29:32', '2018-08-14 18:29:32', NULL),
(513, 1, 'admin/worker/edit/0/data', 'GET', '[]', '2018-08-14 18:29:33', '2018-08-14 18:29:33', NULL),
(514, 1, 'admin/worker/edit/0/data', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"Beverly Hills\",\"phone\":\"3105559520\",\"email\":\"bhills_9520@mailinator.com\",\"coutry\":\"Ywlij\",\"zip\":\"90210\",\"city\":\"1\",\"street\":\"Uysqz\",\"house_number\":\"Lwnpv\",\"building\":\"Hnbby\",\"stairway\":\"Qhmau\",\"floor\":\"Obhvf\",\"door\":\"Zwkug\",\"residence_address\":\"9520 Beverly Dr\",\"id_number\":\"Zrkdx\",\"birthplace\":\"Yukzn\",\"birthdate\":\"Vjtxh\",\"mother_name\":\"Beverly Hills\",\"nationality\":null,\"passport\":\"Hzcrh\",\"gender\":\"0\",\"tax_number\":\"Wajzs\",\"taj\":null,\"bank_account\":\"Qstew\",\"newsletter\":\"0\",\"work_category\":null,\"work_place\":\"1\",\"education\":null,\"training_area\":null,\"specialtyeducation\":null,\"driving_license\":null,\"language\":[null],\"Munkahey\":\"Otsqt\",\"work_start\":\"Srgug\",\"contract_date\":\"Znwtk\",\"job_date\":\"Mfhkh\",\"eu_date\":\"Bihvg\",\"lungfilter_date\":\"Xxioa\",\"logout_date\":\"Ijgbl\"}', '2018-08-14 18:30:11', '2018-08-14 18:30:11', NULL),
(515, 1, 'admin/worker/edit/0/data', 'GET', '[]', '2018-08-14 18:30:12', '2018-08-14 18:30:12', NULL),
(516, 1, 'admin/worker/edit/0/data', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"Beverly Hills\",\"phone\":\"3105559520\",\"email\":\"bhills_9520@mailinator.com\",\"coutry\":\"Ywlij\",\"zip\":\"90210\",\"city\":\"1\",\"street\":\"Uysqz\",\"house_number\":\"Lwnpv\",\"building\":\"Hnbby\",\"stairway\":\"Qhmau\",\"floor\":\"Obhvf\",\"door\":\"Zwkug\",\"residence_address\":\"9520 Beverly Dr\",\"id_number\":\"Zrkdx\",\"birthplace\":\"Yukzn\",\"birthdate\":\"Vjtxh\",\"mother_name\":\"Beverly Hills\",\"nationality\":null,\"passport\":\"Hzcrh\",\"gender\":\"0\",\"tax_number\":\"Wajzs\",\"taj\":null,\"bank_account\":\"Qstew\",\"newsletter\":\"0\",\"work_category\":null,\"work_place\":\"1\",\"education\":null,\"training_area\":null,\"specialtyeducation\":null,\"driving_license\":null,\"language\":[null],\"Munkahey\":\"Otsqt\",\"work_start\":\"Srgug\",\"contract_date\":\"Znwtk\",\"job_date\":\"Mfhkh\",\"eu_date\":\"Bihvg\",\"lungfilter_date\":\"Xxioa\",\"logout_date\":\"Ijgbl\"}', '2018-08-14 18:33:32', '2018-08-14 18:33:32', NULL),
(517, 1, 'admin/worker/edit/0/data', 'GET', '[]', '2018-08-14 18:33:33', '2018-08-14 18:33:33', NULL);
INSERT INTO `_accesslog` (`id`, `user_id`, `route`, `action`, `extra`, `created_at`, `updated_at`, `deleted_at`) VALUES
(518, 1, 'admin/worker/edit/0/data', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"Beverly Hills\",\"phone\":\"3105559520\",\"email\":\"bhills_9520@mailinator.com\",\"coutry\":\"Ywlij\",\"zip\":\"90210\",\"city\":\"1\",\"street\":\"Uysqz\",\"house_number\":\"Lwnpv\",\"building\":\"Hnbby\",\"stairway\":\"Qhmau\",\"floor\":\"Obhvf\",\"door\":\"Zwkug\",\"residence_address\":\"9520 Beverly Dr\",\"id_number\":\"Zrkdx\",\"birthplace\":\"Yukzn\",\"birthdate\":\"Vjtxh\",\"mother_name\":\"Beverly Hills\",\"nationality\":null,\"passport\":\"Hzcrh\",\"gender\":\"0\",\"tax_number\":\"Wajzs\",\"taj\":null,\"bank_account\":\"Qstew\",\"newsletter\":\"0\",\"work_category\":null,\"work_place\":\"1\",\"education\":null,\"training_area\":null,\"specialtyeducation\":null,\"driving_license\":null,\"language\":[null],\"Munkahey\":\"Otsqt\",\"work_start\":\"Srgug\",\"contract_date\":\"Znwtk\",\"job_date\":\"Mfhkh\",\"eu_date\":\"Bihvg\",\"lungfilter_date\":\"Xxioa\",\"logout_date\":\"Ijgbl\"}', '2018-08-14 18:35:10', '2018-08-14 18:35:10', NULL),
(519, 1, 'admin/worker/edit/0/data', 'GET', '[]', '2018-08-14 18:35:11', '2018-08-14 18:35:11', NULL),
(520, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-14 18:35:51', '2018-08-14 18:35:51', NULL),
(521, 1, 'admin/builder/settings', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"model_name\":\"Nationality\",\"table_name\":\"nationality\",\"menu_name\":\"\\u00c1llampolg\\u00e1rs\\u00e1g\",\"menu_icon\":\"fa fa-user\",\"edit\":\"1\",\"delete\":\"1\",\"order\":\"0\",\"export\":\"1\",\"column_name\":[\"name\"],\"column_comment\":[\"\\u00c1llampolg\\u00e1rs\\u00e1\"],\"column_type\":[\"TEXT\"],\"column_fillable\":[\"1\"],\"column_cast\":[\"0\"],\"column_getter\":[\"0\"],\"column_label\":[\"\\u00c1llampolg\\u00e1r\\u00e1s\"],\"column_input\":[\"input\"],\"column_value\":[null],\"column_search\":[\"0\"],\"column_search_input\":[\"text\"],\"column_search_value\":[null],\"required\":[\"1\"],\"save_and_exit\":null}', '2018-08-14 18:36:44', '2018-08-14 18:36:44', NULL),
(522, 1, 'admin/builder/settings', 'GET', '[]', '2018-08-14 18:36:59', '2018-08-14 18:36:59', NULL),
(523, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 18:37:06', '2018-08-14 18:37:06', NULL),
(524, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 18:37:09', '2018-08-14 18:37:09', NULL),
(525, 1, 'admin/worker/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":null,\"phone\":null,\"email\":null,\"coutry\":null,\"zip\":null,\"city\":null,\"street\":null,\"house_number\":null,\"building\":null,\"stairway\":null,\"floor\":null,\"door\":null,\"residence_address\":null,\"id_number\":null,\"birthplace\":null,\"birthdate\":null,\"mother_name\":null,\"nationality\":null,\"passport\":null,\"gender\":\"0\",\"tax_number\":null,\"taj\":null,\"bank_account\":null,\"newsletter\":\"0\",\"work_category\":null,\"work_place\":null,\"education\":null,\"training_area\":null,\"specialtyeducation\":null,\"driving_license\":null,\"language\":[null],\"Munkahey\":null,\"work_start\":null,\"contract_date\":null,\"job_date\":null,\"eu_date\":null,\"lungfilter_date\":null,\"logout_date\":null}', '2018-08-14 18:37:14', '2018-08-14 18:37:14', NULL),
(526, 1, 'admin/worker/edit/0/data', 'GET', '[]', '2018-08-14 18:37:15', '2018-08-14 18:37:15', NULL),
(527, 1, 'admin/language/list', 'GET', '[]', '2018-08-14 18:43:14', '2018-08-14 18:43:14', NULL),
(528, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:43:18', '2018-08-14 18:43:18', NULL),
(529, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:45:39', '2018-08-14 18:45:39', NULL),
(530, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:47:17', '2018-08-14 18:47:17', NULL),
(531, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:47:28', '2018-08-14 18:47:28', NULL),
(532, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:47:46', '2018-08-14 18:47:46', NULL),
(533, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:48:00', '2018-08-14 18:48:00', NULL),
(534, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:48:09', '2018-08-14 18:48:09', NULL),
(535, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:48:41', '2018-08-14 18:48:41', NULL),
(536, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:49:02', '2018-08-14 18:49:02', NULL),
(537, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:49:31', '2018-08-14 18:49:31', NULL),
(538, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:49:44', '2018-08-14 18:49:44', NULL),
(539, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:50:19', '2018-08-14 18:50:19', NULL),
(540, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:52:30', '2018-08-14 18:52:30', NULL),
(541, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:52:50', '2018-08-14 18:52:50', NULL),
(542, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:53:27', '2018-08-14 18:53:27', NULL),
(543, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:54:21', '2018-08-14 18:54:21', NULL),
(544, 1, 'admin/language/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":null}', '2018-08-14 18:54:33', '2018-08-14 18:54:33', NULL),
(545, 1, 'admin/language/edit/0/data', 'GET', '[]', '2018-08-14 18:54:34', '2018-08-14 18:54:34', NULL),
(546, 1, 'admin/language/edit/0/data', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":null}', '2018-08-14 18:55:26', '2018-08-14 18:55:26', NULL),
(547, 1, 'admin/language/edit/0/data', 'GET', '[]', '2018-08-14 18:55:28', '2018-08-14 18:55:28', NULL),
(548, 1, 'admin/language/edit/0/data', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"Angol (alap)\",\"save_and_exit\":null}', '2018-08-14 18:55:44', '2018-08-14 18:55:44', NULL),
(549, 1, 'admin/language/list', 'GET', '[]', '2018-08-14 18:55:45', '2018-08-14 18:55:45', NULL),
(550, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:55:50', '2018-08-14 18:55:50', NULL),
(551, 1, 'admin/language/list', 'GET', '[]', '2018-08-14 18:56:03', '2018-08-14 18:56:03', NULL),
(552, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:56:35', '2018-08-14 18:56:35', NULL),
(553, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 18:57:06', '2018-08-14 18:57:06', NULL),
(554, 1, 'admin/language/list', 'GET', '[]', '2018-08-14 18:57:35', '2018-08-14 18:57:35', NULL),
(555, 1, 'admin/education/list', 'GET', '[]', '2018-08-14 18:57:40', '2018-08-14 18:57:40', NULL),
(556, 1, 'admin/education/list', 'GET', '[]', '2018-08-14 19:00:02', '2018-08-14 19:00:02', NULL),
(557, 1, 'admin/education/edit', 'GET', '[]', '2018-08-14 19:00:07', '2018-08-14 19:00:07', NULL),
(558, 1, 'admin/education/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"\\u00c1ltal\\u00e1nos iskola\",\"save_and_exit\":null}', '2018-08-14 19:00:23', '2018-08-14 19:00:23', NULL),
(559, 1, 'admin/education/list', 'GET', '[]', '2018-08-14 19:00:24', '2018-08-14 19:00:24', NULL),
(560, 1, 'admin/education/edit/1', 'GET', '[]', '2018-08-14 19:00:29', '2018-08-14 19:00:29', NULL),
(561, 1, 'admin/education/edit/1', 'GET', '[]', '2018-08-14 19:01:00', '2018-08-14 19:01:00', NULL),
(562, 1, 'admin/education/edit/1', 'GET', '[]', '2018-08-14 19:03:48', '2018-08-14 19:03:48', NULL),
(563, 1, 'admin/education/list', 'GET', '[]', '2018-08-14 19:04:19', '2018-08-14 19:04:19', NULL),
(564, 1, 'admin/trainingarea/list', 'GET', '[]', '2018-08-14 19:04:27', '2018-08-14 19:04:27', NULL),
(565, 1, 'admin/trainingarea/edit', 'GET', '[]', '2018-08-14 19:04:33', '2018-08-14 19:04:33', NULL),
(566, 1, 'admin/trainingarea/edit', 'GET', '[]', '2018-08-14 19:06:45', '2018-08-14 19:06:45', NULL),
(567, 1, 'admin/trainingarea/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"Informatika\",\"save_and_exit\":null}', '2018-08-14 19:07:04', '2018-08-14 19:07:04', NULL),
(568, 1, 'admin/trainingarea/list', 'GET', '[]', '2018-08-14 19:07:06', '2018-08-14 19:07:06', NULL),
(569, 1, 'admin/specialtyeducation/list', 'GET', '[]', '2018-08-14 19:07:15', '2018-08-14 19:07:15', NULL),
(570, 1, 'admin/specialtyeducation/list', 'GET', '[]', '2018-08-14 19:10:07', '2018-08-14 19:10:07', NULL),
(571, 1, 'admin/specialtyeducation/edit', 'GET', '[]', '2018-08-14 19:10:12', '2018-08-14 19:10:12', NULL),
(572, 1, 'admin/specialtyeducation/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"Valami speci\\u00e1lis\",\"save_and_exit\":null}', '2018-08-14 19:10:29', '2018-08-14 19:10:29', NULL),
(573, 1, 'admin/specialtyeducation/list', 'GET', '[]', '2018-08-14 19:10:30', '2018-08-14 19:10:30', NULL),
(574, 1, 'admin/driverlicense/list', 'GET', '[]', '2018-08-14 19:10:37', '2018-08-14 19:10:37', NULL),
(575, 1, 'admin/driverlicense/edit', 'GET', '[]', '2018-08-14 19:10:40', '2018-08-14 19:10:40', NULL),
(576, 1, 'admin/driverlicense/edit', 'GET', '[]', '2018-08-14 19:12:22', '2018-08-14 19:12:22', NULL),
(577, 1, 'admin/driverlicense/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"B- kateg\\u00f3ria\",\"save_and_exit\":null}', '2018-08-14 19:12:31', '2018-08-14 19:12:31', NULL),
(578, 1, 'admin/driverlicense/list', 'GET', '[]', '2018-08-14 19:12:32', '2018-08-14 19:12:32', NULL),
(579, 1, 'admin/workcategory/list', 'GET', '[]', '2018-08-14 19:12:37', '2018-08-14 19:12:37', NULL),
(580, 1, 'admin/workcategory/edit', 'GET', '[]', '2018-08-14 19:14:36', '2018-08-14 19:14:36', NULL),
(581, 1, 'admin/workcategory/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"Irodai\",\"save_and_exit\":null}', '2018-08-14 19:14:43', '2018-08-14 19:14:43', NULL),
(582, 1, 'admin/workcategory/list', 'GET', '[]', '2018-08-14 19:14:45', '2018-08-14 19:14:45', NULL),
(583, 1, 'admin/city/list', 'GET', '[]', '2018-08-14 19:14:50', '2018-08-14 19:14:50', NULL),
(584, 1, 'admin/city/edit', 'GET', '[]', '2018-08-14 19:14:56', '2018-08-14 19:14:56', NULL),
(585, 1, 'admin/city/edit', 'GET', '[]', '2018-08-14 19:16:31', '2018-08-14 19:16:31', NULL),
(586, 1, 'admin/city/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"budapest\",\"save_and_exit\":null}', '2018-08-14 19:16:40', '2018-08-14 19:16:40', NULL),
(587, 1, 'admin/city/list', 'GET', '[]', '2018-08-14 19:16:41', '2018-08-14 19:16:41', NULL),
(588, 1, 'admin/city/delete/2', 'GET', '[]', '2018-08-14 19:16:51', '2018-08-14 19:16:51', NULL),
(589, 1, 'admin/city/list', 'GET', '[]', '2018-08-14 19:16:53', '2018-08-14 19:16:53', NULL),
(590, 1, 'admin/teaor/list', 'GET', '[]', '2018-08-14 19:17:01', '2018-08-14 19:17:01', NULL),
(591, 1, 'admin/teaor/edit', 'GET', '[]', '2018-08-14 19:18:43', '2018-08-14 19:18:43', NULL),
(592, 1, 'admin/teaor/edit', 'GET', '[]', '2018-08-14 19:20:06', '2018-08-14 19:20:06', NULL),
(593, 1, 'admin/teaor/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"key\":\"0111\",\"name\":\"Els\\u0151\",\"save_and_exit\":null}', '2018-08-14 19:20:27', '2018-08-14 19:20:27', NULL),
(594, 1, 'admin/teaor/list', 'GET', '[]', '2018-08-14 19:20:28', '2018-08-14 19:20:28', NULL),
(595, 1, 'admin/feor/list', 'GET', '[]', '2018-08-14 19:20:35', '2018-08-14 19:20:35', NULL),
(596, 1, 'admin/feor/edit', 'GET', '[]', '2018-08-14 19:21:50', '2018-08-14 19:21:50', NULL),
(597, 1, 'admin/feor/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"key\":\"001\",\"name\":\"FEOR1\",\"save_and_exit\":null}', '2018-08-14 19:22:03', '2018-08-14 19:22:03', NULL),
(598, 1, 'admin/feor/list', 'GET', '[]', '2018-08-14 19:22:04', '2018-08-14 19:22:04', NULL),
(599, 1, 'admin/position/list', 'GET', '[]', '2018-08-14 19:22:11', '2018-08-14 19:22:11', NULL),
(600, 1, 'admin/position/edit', 'GET', '[]', '2018-08-14 19:23:47', '2018-08-14 19:23:47', NULL),
(601, 1, 'admin/position/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"K\\u00f6nyvel\\u0151\",\"save_and_exit\":null}', '2018-08-14 19:24:00', '2018-08-14 19:24:00', NULL),
(602, 1, 'admin/position/list', 'GET', '[]', '2018-08-14 19:24:01', '2018-08-14 19:24:01', NULL),
(603, 1, 'admin/document/list', 'GET', '[]', '2018-08-14 19:24:07', '2018-08-14 19:24:07', NULL),
(604, 1, 'admin/document/edit', 'GET', '[]', '2018-08-14 19:26:56', '2018-08-14 19:26:56', NULL),
(605, 1, 'admin/document/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"Bel\\u00e9p\\u00e9si\",\"save_and_exit\":null}', '2018-08-14 19:27:06', '2018-08-14 19:27:06', NULL),
(606, 1, 'admin/document/list', 'GET', '[]', '2018-08-14 19:27:07', '2018-08-14 19:27:07', NULL),
(607, 1, 'admin/projectstatus/list', 'GET', '[]', '2018-08-14 19:27:16', '2018-08-14 19:27:16', NULL),
(608, 1, 'admin/projectstatus/list', 'GET', '[]', '2018-08-14 19:28:55', '2018-08-14 19:28:55', NULL),
(609, 1, 'admin/projectstatus/edit', 'GET', '[]', '2018-08-14 19:29:04', '2018-08-14 19:29:04', NULL),
(610, 1, 'admin/projectstatus/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"\\u00d6n\\u00e9letrajz bek\\u00e9rve\",\"save_and_exit\":null}', '2018-08-14 19:29:19', '2018-08-14 19:29:19', NULL),
(611, 1, 'admin/projectstatus/list', 'GET', '[]', '2018-08-14 19:29:20', '2018-08-14 19:29:20', NULL),
(612, 1, 'admin/nationality/list', 'GET', '[]', '2018-08-14 19:29:27', '2018-08-14 19:29:27', NULL),
(613, 1, 'admin/nationality/edit', 'GET', '[]', '2018-08-14 19:31:02', '2018-08-14 19:31:02', NULL),
(614, 1, 'admin/nationality/edit', 'POST', '{\"_token\":\"kvXXKDIoeoRdyCdyMPk6hHFrBYAs8k51aaMDnZyh\",\"name\":\"Magyar\",\"save_and_exit\":null}', '2018-08-14 19:31:11', '2018-08-14 19:31:11', NULL),
(615, 1, 'admin/nationality/list', 'GET', '[]', '2018-08-14 19:31:12', '2018-08-14 19:31:12', NULL),
(616, 1, 'admin/nationality/list', 'GET', '[]', '2018-08-14 19:32:13', '2018-08-14 19:32:13', NULL),
(617, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 19:32:19', '2018-08-14 19:32:19', NULL),
(618, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 19:32:23', '2018-08-14 19:32:23', NULL),
(619, 1, 'admin/acl/list', 'GET', '[]', '2018-08-14 19:32:56', '2018-08-14 19:32:56', NULL),
(620, 1, 'admin/user/list', 'GET', '[]', '2018-08-14 19:33:00', '2018-08-14 19:33:00', NULL),
(621, 1, 'admin/company/list', 'GET', '[]', '2018-08-14 19:33:04', '2018-08-14 19:33:04', NULL),
(622, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 19:33:07', '2018-08-14 19:33:07', NULL),
(623, 1, 'admin/language/list', 'GET', '[]', '2018-08-14 19:33:12', '2018-08-14 19:33:12', NULL),
(624, 1, 'admin/education/list', 'GET', '[]', '2018-08-14 19:33:16', '2018-08-14 19:33:16', NULL),
(625, 1, 'admin/acl/list', 'GET', '[]', '2018-08-14 19:33:35', '2018-08-14 19:33:35', NULL),
(626, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 19:33:50', '2018-08-14 19:33:50', NULL),
(627, 1, 'admin/language/list', 'GET', '[]', '2018-08-14 19:34:17', '2018-08-14 19:34:17', NULL),
(628, 1, 'admin/language/list', 'GET', '[]', '2018-08-14 19:34:21', '2018-08-14 19:34:21', NULL),
(629, 1, 'admin/trainingarea/list', 'GET', '[]', '2018-08-14 19:34:43', '2018-08-14 19:34:43', NULL),
(630, 1, 'admin/specialtyeducation/list', 'GET', '[]', '2018-08-14 19:34:46', '2018-08-14 19:34:46', NULL),
(631, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 19:35:27', '2018-08-14 19:35:27', NULL),
(632, 1, 'admin/language/list', 'GET', '[]', '2018-08-14 19:35:47', '2018-08-14 19:35:47', NULL),
(633, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 19:35:51', '2018-08-14 19:35:51', NULL),
(634, 1, 'admin/company/list', 'GET', '[]', '2018-08-14 19:35:55', '2018-08-14 19:35:55', NULL),
(635, 1, 'admin/company/edit', 'GET', '[]', '2018-08-14 19:35:59', '2018-08-14 19:35:59', NULL),
(636, 1, 'admin/worker/list', 'GET', '[]', '2018-08-14 19:36:07', '2018-08-14 19:36:07', NULL),
(637, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-14 19:36:12', '2018-08-14 19:36:12', NULL),
(638, 1, 'admin/language/list', 'GET', '[]', '2018-08-14 19:36:38', '2018-08-14 19:36:38', NULL),
(639, 1, 'admin/language/edit', 'GET', '[]', '2018-08-14 19:36:42', '2018-08-14 19:36:42', NULL),
(640, 1, 'admin/translation/list', 'GET', '[]', '2018-08-14 19:37:09', '2018-08-14 19:37:09', NULL),
(641, 1, 'admin/translation/list', 'GET', '[]', '2018-08-14 19:37:16', '2018-08-14 19:37:16', NULL),
(642, 1, 'admin/projectstatus/list', 'GET', '[]', '2018-08-15 17:40:36', '2018-08-15 17:40:36', NULL),
(643, 1, 'admin/projectstatus/edit/1', 'GET', '[]', '2018-08-15 17:40:58', '2018-08-15 17:40:58', NULL),
(644, 1, 'admin/projectstatus/edit/1', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"name\":\"sdafas\",\"save_and_exit\":null}', '2018-08-15 17:41:02', '2018-08-15 17:41:02', NULL),
(645, 1, 'admin/projectstatus/list', 'GET', '[]', '2018-08-15 17:41:04', '2018-08-15 17:41:04', NULL),
(646, 1, 'admin/worker/list', 'GET', '[]', '2018-08-15 17:41:14', '2018-08-15 17:41:14', NULL),
(647, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-15 17:41:18', '2018-08-15 17:41:18', NULL),
(648, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-15 17:42:48', '2018-08-15 17:42:48', NULL),
(649, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-15 17:43:02', '2018-08-15 17:43:02', NULL),
(650, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-15 17:43:35', '2018-08-15 17:43:35', NULL),
(651, 1, 'admin/worker/edit', 'GET', '{\"tab\":\"data\"}', '2018-08-15 17:43:41', '2018-08-15 17:43:41', NULL),
(652, 1, 'admin/worker/edit', 'GET', '{\"tab\":\"data\"}', '2018-08-15 17:43:43', '2018-08-15 17:43:43', NULL),
(653, 1, 'admin/nationality/list', 'GET', '[]', '2018-08-15 17:44:05', '2018-08-15 17:44:05', NULL),
(654, 1, 'admin/nationality/edit', 'GET', '[]', '2018-08-15 17:44:09', '2018-08-15 17:44:09', NULL),
(655, 1, 'admin/nationality/edit', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"name\":\"Magyar\",\"save_and_exit\":null}', '2018-08-15 17:44:32', '2018-08-15 17:44:32', NULL),
(656, 1, 'admin/nationality/list', 'GET', '[]', '2018-08-15 17:44:34', '2018-08-15 17:44:34', NULL),
(657, 1, 'admin/worker/list', 'GET', '[]', '2018-08-15 17:44:55', '2018-08-15 17:44:55', NULL),
(658, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-15 17:44:59', '2018-08-15 17:44:59', NULL),
(659, 1, 'admin/worker/edit', 'GET', '[]', '2018-08-15 17:47:08', '2018-08-15 17:47:08', NULL),
(660, 1, 'admin/worker/edit', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"name\":\"Beverly Hills\",\"phone\":\"3105556709\",\"email\":\"bhills_6709@mailinator.com\",\"coutry\":\"Zkenw\",\"zip\":\"90210\",\"city\":\"1\",\"street\":\"Ypszk\",\"house_number\":\"Zpqnj\",\"building\":\"Qysor\",\"stairway\":\"Wdpmq\",\"floor\":\"Loeqv\",\"door\":\"Dmzyi\",\"residence_address\":\"6709 Beverly Dr\",\"id_number\":\"Eqyqt\",\"birthplace\":\"Axnik\",\"birthdate\":\"2018-08-29\",\"mother_name\":\"Beverly Hills\",\"nationality\":\"2\",\"passport\":\"Hgdaa\",\"gender\":\"0\",\"tax_number\":\"Wrtzn\",\"taj\":\"Crqok\",\"bank_account\":\"Dmpei\",\"newsletter\":\"0\",\"work_category\":\"1\",\"work_place\":\"1\",\"education\":\"1\",\"training_area\":\"1\",\"specialtyeducation\":\"1\",\"driving_license\":\"1\",\"language\":[\"1\"],\"Munkahey\":\"Yagbz\",\"work_start\":\"2018-08-15\",\"contract_date\":\"2018-08-30\",\"job_date\":\"Fuhud\",\"eu_date\":\"2018-08-15\",\"lungfilter_date\":\"Gzyro\",\"logout_date\":\"2018-08-30\",\"save_and_exit\":null}', '2018-08-15 17:48:29', '2018-08-15 17:48:29', NULL),
(661, 1, 'admin/worker/list', 'GET', '[]', '2018-08-15 17:48:31', '2018-08-15 17:48:31', NULL),
(662, 1, 'admin/worker/list', 'GET', '[]', '2018-08-15 17:50:54', '2018-08-15 17:50:54', NULL),
(663, 1, 'admin/worker/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":\"2018.08.15\",\"id\":null}', '2018-08-15 17:51:29', '2018-08-15 17:51:29', NULL),
(664, 1, 'admin/worker/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":\"2018.08.15\",\"id\":null}', '2018-08-15 17:52:01', '2018-08-15 17:52:01', NULL),
(665, 1, 'admin/worker/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null}', '2018-08-15 17:52:16', '2018-08-15 17:52:16', NULL),
(666, 1, 'admin/worker/edit/1', 'GET', '[]', '2018-08-15 17:52:23', '2018-08-15 17:52:23', NULL),
(667, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 17:52:27', '2018-08-15 17:52:27', NULL),
(668, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 17:54:53', '2018-08-15 17:54:53', NULL),
(669, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 17:55:17', '2018-08-15 17:55:17', NULL),
(670, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 18:06:57', '2018-08-15 18:06:57', NULL),
(671, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 18:07:07', '2018-08-15 18:07:07', NULL),
(672, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 18:08:05', '2018-08-15 18:08:05', NULL),
(673, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 18:09:08', '2018-08-15 18:09:08', NULL),
(674, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 18:10:00', '2018-08-15 18:10:00', NULL),
(675, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 18:13:06', '2018-08-15 18:13:06', NULL),
(676, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 18:15:41', '2018-08-15 18:15:41', NULL),
(677, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 18:16:01', '2018-08-15 18:16:01', NULL),
(678, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 18:16:17', '2018-08-15 18:16:17', NULL),
(679, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 18:23:06', '2018-08-15 18:23:06', NULL),
(680, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 18:23:23', '2018-08-15 18:23:23', NULL),
(681, 1, 'admin/worker/edit/1', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"name\":\"Beverly Hills\",\"phone\":\"3105556709\",\"email\":\"bhills_6709@mailinator.com\",\"coutry\":\"Zkenw\",\"zip\":\"90210\",\"city\":\"1\",\"street\":\"Ypszk\",\"house_number\":\"Zpqnj\",\"building\":\"Qysor\",\"stairway\":\"Wdpmq\",\"floor\":\"Loeqv\",\"door\":\"Dmzyi\",\"residence_address\":\"6709 Beverly Dr\",\"id_number\":\"Eqyqt\",\"birthplace\":\"Axnik\",\"birthdate\":\"2018-08-29\",\"mother_name\":\"Beverly Hills\",\"nationality\":\"2\",\"passport\":\"Hgdaa\",\"gender\":\"0\",\"tax_number\":\"0\",\"taj\":\"Crqok\",\"bank_account\":\"Dmpei\",\"newsletter\":\"0\",\"work_category\":\"1\",\"work_place\":\"1\",\"education\":\"1\",\"training_area\":\"1\",\"specialtyeducation\":\"1\",\"driving_license\":\"1\",\"language\":[\"1\"],\"Munkahey\":null,\"work_start\":\"2018-08-15\",\"contract_date\":\"2018-08-30\",\"job_date\":\"0000-00-00\",\"eu_date\":\"2018-08-15\",\"lungfilter_date\":\"0000-00-00\",\"logout_date\":\"2018-08-30\",\"save_and_exit\":null,\"tab\":\"documents\"}', '2018-08-15 18:24:55', '2018-08-15 18:24:55', NULL),
(682, 1, 'admin/worker/edit/1/documents', 'GET', '[]', '2018-08-15 18:24:57', '2018-08-15 18:24:57', NULL),
(683, 1, 'admin/worker/edit/1/documents', 'GET', '[]', '2018-08-15 18:29:21', '2018-08-15 18:29:21', NULL),
(684, 1, 'admin/worker/edit/1/documents', 'GET', '[]', '2018-08-15 18:29:38', '2018-08-15 18:29:38', NULL),
(685, 1, 'admin/worker/edit/1/documents', 'GET', '[]', '2018-08-15 18:32:16', '2018-08-15 18:32:16', NULL),
(686, 1, 'admin/worker/edit/1/documents', 'GET', '[]', '2018-08-15 18:32:47', '2018-08-15 18:32:47', NULL),
(687, 1, 'admin/worker/edit/1/documents', 'GET', '[]', '2018-08-15 18:33:47', '2018-08-15 18:33:47', NULL),
(688, 1, 'admin/worker/edit/1/documents', 'GET', '[]', '2018-08-15 18:34:48', '2018-08-15 18:34:48', NULL),
(689, 1, 'admin/worker/edit/1/documents', 'GET', '[]', '2018-08-15 18:35:47', '2018-08-15 18:35:47', NULL),
(690, 1, 'admin/worker/edit/1/documents', 'GET', '[]', '2018-08-15 18:35:57', '2018-08-15 18:35:57', NULL),
(691, 1, 'admin/worker/edit/1/documents', 'GET', '[]', '2018-08-15 18:36:10', '2018-08-15 18:36:10', NULL),
(692, 1, 'admin/worker/edit/1/documents', 'GET', '[]', '2018-08-15 18:36:21', '2018-08-15 18:36:21', NULL),
(693, 1, 'admin/worker/document/edit', 'GET', '[]', '2018-08-15 18:36:29', '2018-08-15 18:36:29', NULL),
(694, 1, 'admin/worker/edit/1/documents', 'GET', '[]', '2018-08-15 18:38:45', '2018-08-15 18:38:45', NULL),
(695, 1, 'admin/worker/document/edit/1', 'GET', '[]', '2018-08-15 18:38:55', '2018-08-15 18:38:55', NULL),
(696, 1, 'admin/worker/document/list', 'GET', '[]', '2018-08-15 18:38:57', '2018-08-15 18:38:57', NULL),
(697, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 18:39:13', '2018-08-15 18:39:13', NULL),
(698, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 18:39:19', '2018-08-15 18:39:19', NULL),
(699, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 18:39:25', '2018-08-15 18:39:25', NULL),
(700, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 18:40:30', '2018-08-15 18:40:30', NULL),
(701, 1, 'admin/worker/document/edit/1', 'GET', '[]', '2018-08-15 18:40:36', '2018-08-15 18:40:36', NULL),
(702, 1, 'admin/worker/document/list', 'GET', '[]', '2018-08-15 18:40:37', '2018-08-15 18:40:37', NULL),
(703, 1, 'admin/worker/document/list', 'GET', '[]', '2018-08-15 18:42:34', '2018-08-15 18:42:34', NULL),
(704, 1, 'admin/worker/document/edit/1', 'GET', '[]', '2018-08-15 18:42:43', '2018-08-15 18:42:43', NULL),
(705, 1, 'admin/worker/document/edit/1', 'GET', '[]', '2018-08-15 18:44:30', '2018-08-15 18:44:30', NULL),
(706, 1, 'admin/worker/document/edit/1', 'GET', '[]', '2018-08-15 18:45:07', '2018-08-15 18:45:07', NULL),
(707, 1, 'admin/worker/document/edit/1', 'GET', '[]', '2018-08-15 18:47:43', '2018-08-15 18:47:43', NULL),
(708, 1, 'admin/worker/document/edit/1', 'GET', '[]', '2018-08-15 18:59:23', '2018-08-15 18:59:23', NULL),
(709, 1, 'admin/worker/document/edit/1', 'GET', '[]', '2018-08-15 19:00:13', '2018-08-15 19:00:13', NULL),
(710, 1, 'admin/worker/document/edit/1', 'GET', '[]', '2018-08-15 19:01:54', '2018-08-15 19:01:54', NULL),
(711, 1, 'admin/worker/document/edit/1', 'GET', '[]', '2018-08-15 19:04:13', '2018-08-15 19:04:13', NULL),
(712, 1, 'admin/worker/document/edit/1', 'GET', '[]', '2018-08-15 19:05:00', '2018-08-15 19:05:00', NULL),
(713, 1, 'admin/worker/document/edit/1', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"document_id\":null,\"date\":null,\"comment\":null,\"save_and_exit\":null}', '2018-08-15 19:06:09', '2018-08-15 19:06:09', NULL),
(714, 1, 'admin/worker/document/edit/0/data', 'GET', '[]', '2018-08-15 19:06:10', '2018-08-15 19:06:10', NULL),
(715, 1, 'admin/worker/document/edit/0/data', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"document_id\":null,\"date\":null,\"comment\":null,\"save_and_exit\":null}', '2018-08-15 19:07:28', '2018-08-15 19:07:28', NULL),
(716, 1, 'admin/worker/document/edit/data/data', 'GET', '[]', '2018-08-15 19:07:29', '2018-08-15 19:07:29', NULL),
(717, 1, 'admin/worker/document/edit/data/data', 'GET', '[]', '2018-08-15 19:09:32', '2018-08-15 19:09:32', NULL),
(718, 1, 'admin/worker/document/edit/data/data', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"document_id\":null,\"date\":null,\"comment\":null,\"save_and_exit\":null}', '2018-08-15 19:09:37', '2018-08-15 19:09:37', NULL),
(719, 1, 'admin/worker/document/edit/data/data', 'GET', '[]', '2018-08-15 19:09:38', '2018-08-15 19:09:38', NULL),
(720, 1, 'admin/worker/document/edit/data/data', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"document_id\":null,\"date\":null,\"comment\":null,\"save_and_exit\":null}', '2018-08-15 19:10:34', '2018-08-15 19:10:34', NULL),
(721, 1, 'admin/worker/document/edit/data/data', 'GET', '[]', '2018-08-15 19:10:36', '2018-08-15 19:10:36', NULL),
(722, 1, 'admin/worker/document/edit/data/data', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"document_id\":null,\"date\":null,\"comment\":null,\"save_and_exit\":null}', '2018-08-15 19:11:07', '2018-08-15 19:11:07', NULL),
(723, 1, 'admin/worker/document/edit/data/data', 'GET', '[]', '2018-08-15 19:11:08', '2018-08-15 19:11:08', NULL),
(724, 1, 'admin/worker/document/edit/data/data', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"document_id\":\"1\",\"date\":\"2018-08-08\",\"comment\":\"asdfdasfdsa\",\"save_and_exit\":null,\"file\":{}}', '2018-08-15 19:14:14', '2018-08-15 19:14:14', NULL),
(725, 1, 'admin/worker/edit/data', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 19:14:16', '2018-08-15 19:14:16', NULL),
(726, 1, 'admin/worker/list', 'GET', '[]', '2018-08-15 19:15:24', '2018-08-15 19:15:24', NULL),
(727, 1, 'admin/worker/edit/1', 'GET', '[]', '2018-08-15 19:15:40', '2018-08-15 19:15:40', NULL),
(728, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 19:15:46', '2018-08-15 19:15:46', NULL),
(729, 1, 'admin/worker/document/edit/1', 'GET', '[]', '2018-08-15 19:15:52', '2018-08-15 19:15:52', NULL),
(730, 1, 'admin/worker/document/edit/1', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"document_id\":\"1\",\"date\":\"2018-08-22\",\"comment\":null,\"save_and_exit\":null,\"file\":{}}', '2018-08-15 19:16:17', '2018-08-15 19:16:17', NULL),
(731, 1, 'admin/worker/document/edit/1', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"document_id\":\"1\",\"date\":\"2018-08-22\",\"comment\":null,\"save_and_exit\":null,\"file\":{}}', '2018-08-15 19:16:55', '2018-08-15 19:16:55', NULL),
(732, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 19:16:56', '2018-08-15 19:16:56', NULL),
(733, 1, 'admin/worker/document/edit/1', 'GET', '[]', '2018-08-15 19:17:10', '2018-08-15 19:17:10', NULL),
(734, 1, 'admin/worker/document/edit/1', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"document_id\":\"1\",\"date\":\"2018-08-22\",\"comment\":\"dsfgsdf\",\"save_and_exit\":null,\"file\":{}}', '2018-08-15 19:24:05', '2018-08-15 19:24:05', NULL),
(735, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 19:24:06', '2018-08-15 19:24:06', NULL),
(736, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 19:27:07', '2018-08-15 19:27:07', NULL),
(737, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 19:30:01', '2018-08-15 19:30:01', NULL),
(738, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 19:31:17', '2018-08-15 19:31:17', NULL),
(739, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 19:31:31', '2018-08-15 19:31:31', NULL),
(740, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-15 19:31:58', '2018-08-15 19:31:58', NULL),
(741, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 19:32:02', '2018-08-15 19:32:02', NULL),
(742, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"freedays\"}', '2018-08-15 19:32:07', '2018-08-15 19:32:07', NULL),
(743, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 19:32:10', '2018-08-15 19:32:10', NULL),
(744, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-15 19:32:17', '2018-08-15 19:32:17', NULL),
(745, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 19:32:21', '2018-08-15 19:32:21', NULL),
(746, 1, 'admin/worker/document/delete/1/2', 'GET', '[]', '2018-08-15 19:32:26', '2018-08-15 19:32:26', NULL),
(747, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 19:32:28', '2018-08-15 19:32:28', NULL),
(748, 1, 'admin/company/list', 'GET', '[]', '2018-08-15 19:33:44', '2018-08-15 19:33:44', NULL),
(749, 1, 'admin/company/edit/1', 'GET', '[]', '2018-08-15 19:33:49', '2018-08-15 19:33:49', NULL),
(750, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 19:34:37', '2018-08-15 19:34:37', NULL),
(751, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-08-15 19:34:41', '2018-08-15 19:34:41', NULL),
(752, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-15 19:34:44', '2018-08-15 19:34:44', NULL),
(753, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-08-15 19:34:49', '2018-08-15 19:34:49', NULL),
(754, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"candidates\"}', '2018-08-15 19:35:01', '2018-08-15 19:35:01', NULL),
(755, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-15 19:35:41', '2018-08-15 19:35:41', NULL),
(756, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 19:35:48', '2018-08-15 19:35:48', NULL),
(757, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-15 19:35:52', '2018-08-15 19:35:52', NULL),
(758, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-15 19:40:08', '2018-08-15 19:40:08', NULL),
(759, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-15 19:41:17', '2018-08-15 19:41:17', NULL),
(760, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-15 19:42:00', '2018-08-15 19:42:00', NULL),
(761, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-15 19:42:17', '2018-08-15 19:42:17', NULL),
(762, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-15 19:42:54', '2018-08-15 19:42:54', NULL),
(763, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-15 19:43:00', '2018-08-15 19:43:00', NULL),
(764, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-15 19:43:58', '2018-08-15 19:43:58', NULL),
(765, 1, 'admin/company/edit/1', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"name\":\"Beverly Hills\",\"tax\":\"Mbdex\",\"eu_tax\":\"Oklol\",\"company_number\":\"Beverly Corp\",\"delegate\":\"Kifib\",\"delegete_position\":\"Kuzzb\",\"comment\":\"Ozqlu\",\"zip\":\"90210\",\"country\":\"USA\",\"city\":\"Beverly Hills\",\"address\":\"7481 Beverly Dr\",\"mail_address\":\"7481 Beverly Dr\",\"mailbox\":\"Uzhhp\",\"contact\":\"1\",\"business_description\":\"Ascdo\",\"save_and_exit\":null,\"tab\":\"data\"}', '2018-08-15 19:44:58', '2018-08-15 19:44:58', NULL),
(766, 1, 'admin/company/list', 'GET', '[]', '2018-08-15 19:45:00', '2018-08-15 19:45:00', NULL),
(767, 1, 'admin/company/edit/1', 'GET', '[]', '2018-08-15 19:45:04', '2018-08-15 19:45:04', NULL),
(768, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 19:45:12', '2018-08-15 19:45:12', NULL),
(769, 1, 'admin/company/companysites/edit/1', 'GET', '[]', '2018-08-15 19:45:16', '2018-08-15 19:45:16', NULL),
(770, 1, 'admin/company/companysites/edit/1', 'GET', '[]', '2018-08-15 19:47:11', '2018-08-15 19:47:11', NULL),
(771, 1, 'admin/company/companysites/edit/1', 'GET', '[]', '2018-08-15 19:47:37', '2018-08-15 19:47:37', NULL),
(772, 1, 'admin/company/companysites/edit/1', 'GET', '[]', '2018-08-15 19:48:17', '2018-08-15 19:48:17', NULL),
(773, 1, 'admin/company/companysites/edit/1', 'GET', '[]', '2018-08-15 19:49:10', '2018-08-15 19:49:10', NULL),
(774, 1, 'admin/company/companysites/edit/1', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"name\":\"dfsgsdf\",\"address\":\"dfgsdf\",\"save_and_exit\":null}', '2018-08-15 19:49:27', '2018-08-15 19:49:27', NULL),
(775, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 19:49:29', '2018-08-15 19:49:29', NULL),
(776, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-08-15 19:49:36', '2018-08-15 19:49:36', NULL),
(777, 1, 'admin/company/contact/edit/1/4', 'GET', '[]', '2018-08-15 19:49:42', '2018-08-15 19:49:42', NULL),
(778, 1, 'admin/company/contact/edit/1/4', 'GET', '[]', '2018-08-15 19:58:24', '2018-08-15 19:58:24', NULL),
(779, 1, 'admin/company/contact/edit/1/4', 'GET', '[]', '2018-08-15 20:01:14', '2018-08-15 20:01:14', NULL),
(780, 1, 'admin/company/contact/edit/1/4', 'GET', '[]', '2018-08-15 20:01:37', '2018-08-15 20:01:37', NULL),
(781, 1, 'admin/company/contact/edit/1/4', 'GET', '[]', '2018-08-15 20:01:52', '2018-08-15 20:01:52', NULL),
(782, 1, 'admin/company/list', 'GET', '[]', '2018-08-15 20:02:01', '2018-08-15 20:02:01', NULL),
(783, 1, 'admin/company/edit/1', 'GET', '[]', '2018-08-15 20:02:06', '2018-08-15 20:02:06', NULL),
(784, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 20:02:12', '2018-08-15 20:02:12', NULL),
(785, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 20:03:13', '2018-08-15 20:03:13', NULL),
(786, 1, 'admin/company/companysites/edit/1', 'GET', '[]', '2018-08-15 20:03:17', '2018-08-15 20:03:17', NULL),
(787, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 20:03:21', '2018-08-15 20:03:21', NULL),
(788, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-08-15 20:03:25', '2018-08-15 20:03:25', NULL),
(789, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-08-15 20:03:33', '2018-08-15 20:03:33', NULL),
(790, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-08-15 20:04:04', '2018-08-15 20:04:04', NULL),
(791, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-08-15 20:05:39', '2018-08-15 20:05:39', NULL),
(792, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-08-15 20:06:09', '2018-08-15 20:06:09', NULL),
(793, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-15 20:06:21', '2018-08-15 20:06:21', NULL),
(794, 1, 'admin/company/companysites/edit/1', 'GET', '[]', '2018-08-15 20:06:25', '2018-08-15 20:06:25', NULL),
(795, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-08-15 20:06:32', '2018-08-15 20:06:32', NULL),
(796, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-08-15 20:06:36', '2018-08-15 20:06:36', NULL),
(797, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-08-15 20:06:54', '2018-08-15 20:06:54', NULL),
(798, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-08-15 20:07:32', '2018-08-15 20:07:32', NULL),
(799, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-08-15 20:08:36', '2018-08-15 20:08:36', NULL),
(800, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-15 20:08:48', '2018-08-15 20:08:48', NULL),
(801, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-08-15 20:08:52', '2018-08-15 20:08:52', NULL),
(802, 1, 'admin/company/contract/edit/1/1', 'GET', '[]', '2018-08-15 20:09:04', '2018-08-15 20:09:04', NULL),
(803, 1, 'admin/company/contract/edit/1/1', 'GET', '[]', '2018-08-15 20:09:49', '2018-08-15 20:09:49', NULL),
(804, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-15 20:10:03', '2018-08-15 20:10:03', NULL),
(805, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-15 20:10:28', '2018-08-15 20:10:28', NULL),
(806, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-08-15 20:10:30', '2018-08-15 20:10:30', NULL),
(807, 1, 'admin/company/contract/edit/1/1', 'GET', '[]', '2018-08-15 20:11:57', '2018-08-15 20:11:57', NULL),
(808, 1, 'admin/company/contract/edit/1/1', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"type\":\"0\",\"contract_date\":\"1900-11-14\",\"contract_type\":\"0\",\"deadline\":\"Yuepw\",\"paper_contract\":\"0\",\"contact\":\"1\",\"payment_type\":\"0\",\"summation\":\"Nysgei lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\",\"warrantee\":\"Afvnr\",\"save_and_exit\":null}', '2018-08-15 20:12:05', '2018-08-15 20:12:05', NULL),
(809, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-08-15 20:12:07', '2018-08-15 20:12:07', NULL),
(810, 1, 'admin/company/contract/edit/1/1', 'GET', '[]', '2018-08-15 20:12:35', '2018-08-15 20:12:35', NULL),
(811, 1, 'admin/company/contract/edit/1/1', 'GET', '[]', '2018-08-15 20:20:44', '2018-08-15 20:20:44', NULL),
(812, 1, 'admin/company/contract/edit/1/1', 'GET', '[]', '2018-08-15 20:21:03', '2018-08-15 20:21:03', NULL),
(813, 1, 'admin/company/contract/edit/1/1', 'GET', '[]', '2018-08-15 20:21:33', '2018-08-15 20:21:33', NULL),
(814, 1, 'admin/company/list', 'GET', '[]', '2018-08-15 20:21:44', '2018-08-15 20:21:44', NULL),
(815, 1, 'admin/company/list', 'GET', '[]', '2018-08-15 20:22:17', '2018-08-15 20:22:17', NULL),
(816, 1, 'admin/company/list', 'GET', '[]', '2018-08-15 20:23:00', '2018-08-15 20:23:00', NULL),
(817, 1, 'admin/company/list', 'GET', '[]', '2018-08-15 20:26:00', '2018-08-15 20:26:00', NULL),
(818, 1, 'admin/company/list', 'GET', '[]', '2018-08-15 20:26:30', '2018-08-15 20:26:30', NULL),
(819, 1, 'admin/company/list', 'GET', '[]', '2018-08-15 20:26:41', '2018-08-15 20:26:41', NULL),
(820, 1, 'admin/company/list', 'GET', '[]', '2018-08-15 20:27:35', '2018-08-15 20:27:35', NULL),
(821, 1, 'admin/company/list', 'GET', '[]', '2018-08-15 20:29:11', '2018-08-15 20:29:11', NULL),
(822, 1, 'admin/company/list', 'GET', '[]', '2018-08-15 20:30:33', '2018-08-15 20:30:33', NULL),
(823, 1, 'admin/company/edit', 'GET', '[]', '2018-08-15 20:31:05', '2018-08-15 20:31:05', NULL),
(824, 1, 'admin/company/edit', 'GET', '{\"tab\":\"data\"}', '2018-08-15 20:32:01', '2018-08-15 20:32:01', NULL),
(825, 1, 'admin/user/list', 'GET', '[]', '2018-08-15 20:32:03', '2018-08-15 20:32:03', NULL),
(826, 1, 'admin/user/edit/1', 'GET', '[]', '2018-08-15 20:32:11', '2018-08-15 20:32:11', NULL),
(827, 1, 'admin/company/list', 'GET', '[]', '2018-08-15 20:32:19', '2018-08-15 20:32:19', NULL),
(828, 1, 'admin/worker/list', 'GET', '[]', '2018-08-15 20:32:23', '2018-08-15 20:32:23', NULL),
(829, 1, 'admin/worker/edit/1', 'GET', '[]', '2018-08-15 20:32:47', '2018-08-15 20:32:47', NULL),
(830, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 20:32:51', '2018-08-15 20:32:51', NULL),
(831, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"freedays\"}', '2018-08-15 20:32:55', '2018-08-15 20:32:55', NULL),
(832, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-15 20:32:59', '2018-08-15 20:32:59', NULL),
(833, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-15 20:33:13', '2018-08-15 20:33:13', NULL),
(834, 1, 'admin/language/list', 'GET', '[]', '2018-08-15 20:33:26', '2018-08-15 20:33:26', NULL),
(835, 1, 'admin/language/edit/1', 'GET', '[]', '2018-08-15 20:33:49', '2018-08-15 20:33:49', NULL),
(836, 1, 'admin/language/list', 'GET', '[]', '2018-08-15 20:33:57', '2018-08-15 20:33:57', NULL),
(837, 1, 'admin/language/edit/1', 'GET', '[]', '2018-08-15 20:34:01', '2018-08-15 20:34:01', NULL),
(838, 1, 'admin/language/edit/1', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"name\":\"Angol (alap)\",\"save_and_exit\":null}', '2018-08-15 20:34:05', '2018-08-15 20:34:05', NULL),
(839, 1, 'admin/language/list', 'GET', '[]', '2018-08-15 20:34:06', '2018-08-15 20:34:06', NULL),
(840, 1, 'admin/education/list', 'GET', '[]', '2018-08-15 20:34:15', '2018-08-15 20:34:15', NULL),
(841, 1, 'admin/education/list', 'GET', '[]', '2018-08-15 20:35:29', '2018-08-15 20:35:29', NULL),
(842, 1, 'admin/education/list', 'GET', '[]', '2018-08-15 20:36:30', '2018-08-15 20:36:30', NULL),
(843, 1, 'admin/education/list', 'GET', '[]', '2018-08-15 20:37:17', '2018-08-15 20:37:17', NULL),
(844, 1, 'admin/education/edit', 'GET', '[]', '2018-08-15 20:37:24', '2018-08-15 20:37:24', NULL),
(845, 1, 'admin/education/edit', 'GET', '[]', '2018-08-15 20:39:16', '2018-08-15 20:39:16', NULL),
(846, 1, 'admin/acl/list', 'GET', '[]', '2018-08-15 20:39:40', '2018-08-15 20:39:40', NULL),
(847, 1, 'admin/user/list', 'GET', '[]', '2018-08-15 20:39:45', '2018-08-15 20:39:45', NULL),
(848, 1, 'admin/company/list', 'GET', '[]', '2018-08-15 20:39:49', '2018-08-15 20:39:49', NULL),
(849, 1, 'admin/company/edit', 'GET', '[]', '2018-08-15 20:39:52', '2018-08-15 20:39:52', NULL),
(850, 1, 'admin/acl/list', 'GET', '[]', '2018-08-15 20:41:39', '2018-08-15 20:41:39', NULL),
(851, 1, 'admin/worker/list', 'GET', '[]', '2018-08-15 20:41:47', '2018-08-15 20:41:47', NULL),
(852, 1, 'admin/worker/edit/1', 'GET', '[]', '2018-08-15 20:41:52', '2018-08-15 20:41:52', NULL),
(853, 1, 'admin/worker/edit/1', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"name\":\"Beverly Hills\",\"phone\":\"3105556709\",\"email\":\"bhills_6709@mailinator.com\",\"coutry\":\"Zkenw\",\"zip\":\"90210\",\"city\":\"1\",\"street\":\"Ypszk\",\"house_number\":\"Zpqnj\",\"building\":\"Qysor\",\"stairway\":\"Wdpmq\",\"floor\":\"Loeqv\",\"door\":\"Dmzyi\",\"residence_address\":\"6709 Beverly Dr\",\"id_number\":\"Eqyqt\",\"birthplace\":\"Axnik\",\"birthdate\":\"2018-08-29\",\"mother_name\":\"Beverly Hills\",\"nationality\":\"2\",\"passport\":\"Hgdaa\",\"gender\":\"0\",\"tax_number\":\"0\",\"taj\":\"Crqok\",\"bank_account\":\"Dmpei\",\"newsletter\":\"0\",\"work_category\":\"1\",\"work_place\":\"1\",\"education\":\"1\",\"training_area\":\"1\",\"specialtyeducation\":\"1\",\"driving_license\":\"1\",\"language\":[\"1\"],\"Munkahey\":null,\"work_start\":\"2018-08-15\",\"contract_date\":\"2018-08-30\",\"job_date\":\"0000-00-00\",\"eu_date\":\"2018-08-15\",\"lungfilter_date\":\"1900-11-13\",\"logout_date\":\"2018-08-30\",\"save_and_exit\":null}', '2018-08-15 20:42:14', '2018-08-15 20:42:14', NULL),
(854, 1, 'admin/worker/edit/1/data', 'GET', '[]', '2018-08-15 20:42:16', '2018-08-15 20:42:16', NULL),
(855, 1, 'admin/worker/edit/1/data', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"name\":\"Beverly Hills\",\"phone\":\"3105556709\",\"email\":\"bhills_6709@mailinator.com\",\"coutry\":\"Zkenw\",\"zip\":\"90210\",\"city\":\"1\",\"street\":\"Ypszk\",\"house_number\":\"Zpqnj\",\"building\":\"Qysor\",\"stairway\":\"Wdpmq\",\"floor\":\"Loeqv\",\"door\":\"Dmzyi\",\"residence_address\":\"6709 Beverly Dr\",\"id_number\":\"Eqyqt\",\"birthplace\":\"Axnik\",\"birthdate\":\"2018-08-29\",\"mother_name\":\"Beverly Hills\",\"nationality\":\"2\",\"passport\":\"Hgdaa\",\"gender\":\"0\",\"tax_number\":\"0\",\"taj\":\"Crqok\",\"bank_account\":\"Dmpei\",\"newsletter\":\"0\",\"work_category\":\"1\",\"work_place\":\"1\",\"education\":\"1\",\"training_area\":\"1\",\"specialtyeducation\":\"1\",\"driving_license\":\"1\",\"language\":[\"1\"],\"Munkahey\":\"yxcvyx\",\"work_start\":\"2018-08-15\",\"contract_date\":\"2018-08-30\",\"job_date\":\"0000-00-00\",\"eu_date\":\"2018-08-15\",\"lungfilter_date\":\"1900-11-13\",\"logout_date\":\"2018-08-30\",\"save_and_exit\":null}', '2018-08-15 20:42:22', '2018-08-15 20:42:22', NULL),
(856, 1, 'admin/worker/list', 'GET', '[]', '2018-08-15 20:42:23', '2018-08-15 20:42:23', NULL),
(857, 1, 'admin/search', 'GET', '{\"query\":\"yxcvcyxvx\"}', '2018-08-15 20:42:46', '2018-08-15 20:42:46', NULL),
(858, 1, 'admin/profile', 'GET', '[]', '2018-08-15 20:42:58', '2018-08-15 20:42:58', NULL),
(859, 1, 'admin/acl/list', 'GET', '[]', '2018-08-15 20:43:28', '2018-08-15 20:43:28', NULL),
(860, 1, 'admin/user/list', 'GET', '[]', '2018-08-15 20:43:32', '2018-08-15 20:43:32', NULL),
(861, 1, 'admin/user/list', 'GET', '[]', '2018-08-15 20:43:38', '2018-08-15 20:43:38', NULL),
(862, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:43:43', '2018-08-15 20:43:43', NULL),
(863, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:45:43', '2018-08-15 20:45:43', NULL),
(864, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:45:52', '2018-08-15 20:45:52', NULL),
(865, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:45:57', '2018-08-15 20:45:57', NULL),
(866, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:46:46', '2018-08-15 20:46:46', NULL),
(867, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:47:04', '2018-08-15 20:47:04', NULL),
(868, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:47:36', '2018-08-15 20:47:36', NULL),
(869, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:47:40', '2018-08-15 20:47:40', NULL),
(870, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:48:18', '2018-08-15 20:48:18', NULL),
(871, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:48:24', '2018-08-15 20:48:24', NULL),
(872, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:48:54', '2018-08-15 20:48:54', NULL),
(873, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:49:20', '2018-08-15 20:49:20', NULL),
(874, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:49:33', '2018-08-15 20:49:33', NULL),
(875, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":\"name\",\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:49:58', '2018-08-15 20:49:58', NULL),
(876, 1, 'admin/user/edit/1', 'GET', '[]', '2018-08-15 20:50:04', '2018-08-15 20:50:04', NULL);
INSERT INTO `_accesslog` (`id`, `user_id`, `route`, `action`, `extra`, `created_at`, `updated_at`, `deleted_at`) VALUES
(877, 1, 'admin/user/edit/1', 'POST', '{\"_token\":\"7hmIqCugBWrmS43ih6FFyXnrO5thepSBBjNxEDz4\",\"tab\":\"general\",\"lastname\":\"bod\\u00e1k\",\"firstname\":\"szabolcs\",\"email\":\"bodak.szabolcs@gmail.com\",\"password\":null,\"password_again\":null,\"shipping\":{\"name\":null,\"country\":null,\"zip\":null,\"city\":null,\"address\":null,\"phone\":null},\"billing\":{\"name\":null,\"country\":null,\"zip\":null,\"city\":null,\"address\":null,\"tax_number\":null},\"save_and_exit\":null}', '2018-08-15 20:50:10', '2018-08-15 20:50:10', NULL),
(878, 1, 'admin/user/list', 'GET', '[]', '2018-08-15 20:50:11', '2018-08-15 20:50:11', NULL),
(879, 1, 'admin/user/list', 'GET', '[]', '2018-08-15 20:51:50', '2018-08-15 20:51:50', NULL),
(880, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:51:57', '2018-08-15 20:51:57', NULL),
(881, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:52:23', '2018-08-15 20:52:23', NULL),
(882, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:52:27', '2018-08-15 20:52:27', NULL),
(883, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:52:31', '2018-08-15 20:52:31', NULL),
(884, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:52:32', '2018-08-15 20:52:32', NULL),
(885, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:52:50', '2018-08-15 20:52:50', NULL),
(886, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:52:55', '2018-08-15 20:52:55', NULL),
(887, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:53:00', '2018-08-15 20:53:00', NULL),
(888, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:53:14', '2018-08-15 20:53:14', NULL),
(889, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:53:33', '2018-08-15 20:53:33', NULL),
(890, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:53:39', '2018-08-15 20:53:39', NULL),
(891, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":null,\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:53:44', '2018-08-15 20:53:44', NULL),
(892, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":\"email\",\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:53:49', '2018-08-15 20:53:49', NULL),
(893, 1, 'admin/user/list', 'GET', '{\"direction\":null,\"orderBy\":\"created_at\",\"search-fillable\":null,\"id\":null,\"created_at_from_to\":\"2018-08-15 - 2018-08-15\",\"name\":null,\"email\":null}', '2018-08-15 20:53:52', '2018-08-15 20:53:52', NULL),
(894, 1, 'admin/company/list', 'GET', '[]', '2018-08-16 05:45:39', '2018-08-16 05:45:39', NULL),
(895, 1, 'admin/company/list', 'GET', '[]', '2018-08-16 19:41:50', '2018-08-16 19:41:50', NULL),
(896, 1, 'admin/company/edit', 'GET', '[]', '2018-08-16 19:41:53', '2018-08-16 19:41:53', NULL),
(897, 1, 'admin/company/list', 'GET', '[]', '2018-08-16 19:41:57', '2018-08-16 19:41:57', NULL),
(898, 1, 'admin/company/edit/1', 'GET', '[]', '2018-08-16 19:42:06', '2018-08-16 19:42:06', NULL),
(899, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-16 19:42:08', '2018-08-16 19:42:08', NULL),
(900, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-08-16 19:42:11', '2018-08-16 19:42:11', NULL),
(901, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-16 19:42:13', '2018-08-16 19:42:13', NULL),
(902, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-08-16 19:42:15', '2018-08-16 19:42:15', NULL),
(903, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"candidates\"}', '2018-08-16 19:42:16', '2018-08-16 19:42:16', NULL),
(904, 1, 'admin/user/list', 'GET', '[]', '2018-08-16 19:43:41', '2018-08-16 19:43:41', NULL),
(905, 1, 'admin/user/edit/1', 'GET', '[]', '2018-08-16 19:43:46', '2018-08-16 19:43:46', NULL),
(906, 1, 'admin/worker/list', 'GET', '[]', '2018-08-16 19:43:51', '2018-08-16 19:43:51', NULL),
(907, 1, 'admin/worker/edit/1', 'GET', '[]', '2018-08-16 19:43:55', '2018-08-16 19:43:55', NULL),
(908, 1, 'admin/worker/edit/1', 'POST', '{\"_token\":\"DNk1VHvEJSJJqZPbTiJvVsHLVVK6gxnl0tHWFcFb\",\"name\":\"Beverly Hills\",\"phone\":\"3105556709\",\"email\":\"bhills_6709@mailinator.com\",\"coutry\":\"Zkenw\",\"zip\":\"90210\",\"city\":\"1\",\"street\":\"Ypszk\",\"house_number\":\"Zpqnj\",\"building\":\"Qysor\",\"stairway\":\"Wdpmq\",\"floor\":\"Loeqv\",\"door\":\"Dmzyi\",\"residence_address\":\"6709 Beverly Dr\",\"id_number\":\"Eqyqt\",\"birthplace\":\"Axnik\",\"birthdate\":\"2018-08-29\",\"mother_name\":\"Beverly Hills\",\"nationality\":\"2\",\"passport\":\"Hgdaa\",\"gender\":\"0\",\"tax_number\":\"0\",\"taj\":\"Crqok\",\"bank_account\":\"Dmpei\",\"newsletter\":\"0\",\"work_category\":\"1\",\"work_place\":\"1\",\"education\":\"1\",\"training_area\":\"1\",\"specialtyeducation\":\"1\",\"driving_license\":\"1\",\"language\":[\"1\"],\"Munkahey\":null,\"work_start\":\"2018-08-15\",\"contract_date\":\"2018-08-30\",\"job_date\":\"1900-10-31\",\"eu_date\":\"2018-08-15\",\"lungfilter_date\":\"1900-11-13\",\"logout_date\":\"2018-08-30\",\"save_and_exit\":null}', '2018-08-16 19:44:15', '2018-08-16 19:44:15', NULL),
(909, 1, 'admin/worker/edit/1/data', 'GET', '[]', '2018-08-16 19:44:15', '2018-08-16 19:44:15', NULL),
(910, 1, 'admin/worker/edit/1/data', 'POST', '{\"_token\":\"DNk1VHvEJSJJqZPbTiJvVsHLVVK6gxnl0tHWFcFb\",\"name\":\"Beverly Hills\",\"phone\":\"3105556709\",\"email\":\"bhills_6709@mailinator.com\",\"coutry\":\"Zkenw\",\"zip\":\"90210\",\"city\":\"1\",\"street\":\"Ypszk\",\"house_number\":\"Zpqnj\",\"building\":\"Qysor\",\"stairway\":\"Wdpmq\",\"floor\":\"Loeqv\",\"door\":\"Dmzyi\",\"residence_address\":\"6709 Beverly Dr\",\"id_number\":\"Eqyqt\",\"birthplace\":\"Axnik\",\"birthdate\":\"2018-08-29\",\"mother_name\":\"Beverly Hills\",\"nationality\":\"2\",\"passport\":\"Hgdaa\",\"gender\":\"0\",\"tax_number\":\"0\",\"taj\":\"Crqok\",\"bank_account\":\"Dmpei\",\"newsletter\":\"0\",\"work_category\":\"1\",\"work_place\":\"1\",\"education\":\"1\",\"training_area\":\"1\",\"specialtyeducation\":\"1\",\"driving_license\":\"1\",\"language\":[\"1\"],\"Munkahey\":\"sdfsd\",\"work_start\":\"2018-08-15\",\"contract_date\":\"2018-08-30\",\"job_date\":\"1900-10-31\",\"eu_date\":\"2018-08-15\",\"lungfilter_date\":\"1900-11-13\",\"logout_date\":\"2018-08-30\",\"save_and_exit\":null}', '2018-08-16 19:44:26', '2018-08-16 19:44:26', NULL),
(911, 1, 'admin/worker/list', 'GET', '[]', '2018-08-16 19:44:26', '2018-08-16 19:44:26', NULL),
(912, 1, 'admin/worker/list', 'GET', '{\"direction\":null,\"orderBy\":\"0\",\"search-fillable\":null,\"id\":null}', '2018-08-16 19:46:11', '2018-08-16 19:46:11', NULL),
(913, 1, 'admin/company/list', 'GET', '[]', '2018-08-16 19:46:34', '2018-08-16 19:46:34', NULL),
(914, 1, 'admin/company/list', 'GET', '[]', '2018-08-16 19:52:28', '2018-08-16 19:52:28', NULL),
(915, 1, 'admin/worker/list', 'GET', '[]', '2018-08-16 19:52:35', '2018-08-16 19:52:35', NULL),
(916, 1, 'admin/worker/list', 'GET', '[]', '2018-08-17 05:02:18', '2018-08-17 05:02:18', NULL),
(917, 1, 'admin/worker/edit/1', 'GET', '[]', '2018-08-17 05:02:22', '2018-08-17 05:02:22', NULL),
(918, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 05:02:26', '2018-08-17 05:02:26', NULL),
(919, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"freedays\"}', '2018-08-17 05:02:29', '2018-08-17 05:02:29', NULL),
(920, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 05:02:31', '2018-08-17 05:02:31', NULL),
(921, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 05:02:34', '2018-08-17 05:02:34', NULL),
(922, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 05:05:42', '2018-08-17 05:05:42', NULL),
(923, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 05:05:48', '2018-08-17 05:05:48', NULL),
(924, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 05:06:39', '2018-08-17 05:06:39', NULL),
(925, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 05:07:15', '2018-08-17 05:07:15', NULL),
(926, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 05:07:28', '2018-08-17 05:07:28', NULL),
(927, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 05:07:42', '2018-08-17 05:07:42', NULL),
(928, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 05:07:45', '2018-08-17 05:07:45', NULL),
(929, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 05:07:47', '2018-08-17 05:07:47', NULL),
(930, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 05:07:51', '2018-08-17 05:07:51', NULL),
(931, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 05:09:15', '2018-08-17 05:09:15', NULL),
(932, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 05:09:55', '2018-08-17 05:09:55', NULL),
(933, 1, 'admin/company/edit', 'GET', '[]', '2018-08-17 05:10:28', '2018-08-17 05:10:28', NULL),
(934, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 05:11:19', '2018-08-17 05:11:19', NULL),
(935, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 05:38:14', '2018-08-17 05:38:14', NULL),
(936, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 06:32:59', '2018-08-17 06:32:59', NULL),
(937, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 06:33:16', '2018-08-17 06:33:16', NULL),
(938, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 06:35:34', '2018-08-17 06:35:34', NULL),
(939, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 06:35:53', '2018-08-17 06:35:53', NULL),
(940, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 06:36:43', '2018-08-17 06:36:43', NULL),
(941, 1, 'admin/company/edit/1', 'GET', '[]', '2018-08-17 06:37:19', '2018-08-17 06:37:19', NULL),
(942, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-17 06:37:22', '2018-08-17 06:37:22', NULL),
(943, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-08-17 06:37:24', '2018-08-17 06:37:24', NULL),
(944, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 06:37:25', '2018-08-17 06:37:25', NULL),
(945, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-08-17 06:37:27', '2018-08-17 06:37:27', NULL),
(946, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"candidates\"}', '2018-08-17 06:37:29', '2018-08-17 06:37:29', NULL),
(947, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-17 06:37:32', '2018-08-17 06:37:32', NULL),
(948, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-17 06:37:36', '2018-08-17 06:37:36', NULL),
(949, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-17 06:38:41', '2018-08-17 06:38:41', NULL),
(950, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-17 06:38:43', '2018-08-17 06:38:43', NULL),
(951, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-17 06:38:43', '2018-08-17 06:38:43', NULL),
(952, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-17 06:38:44', '2018-08-17 06:38:44', NULL),
(953, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-17 06:38:44', '2018-08-17 06:38:44', NULL),
(954, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 06:39:19', '2018-08-17 06:39:19', NULL),
(955, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 06:40:24', '2018-08-17 06:40:24', NULL),
(956, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 06:40:52', '2018-08-17 06:40:52', NULL),
(957, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 06:44:05', '2018-08-17 06:44:05', NULL),
(958, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 06:44:20', '2018-08-17 06:44:20', NULL),
(959, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 06:44:40', '2018-08-17 06:44:40', NULL),
(960, 1, 'admin/company/edit/1', 'GET', '[]', '2018-08-17 06:47:57', '2018-08-17 06:47:57', NULL),
(961, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-17 06:48:01', '2018-08-17 06:48:01', NULL),
(962, 1, 'admin/company/companysites/edit/1', 'GET', '[]', '2018-08-17 06:48:04', '2018-08-17 06:48:04', NULL),
(963, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-17 06:48:07', '2018-08-17 06:48:07', NULL),
(964, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-08-17 06:48:10', '2018-08-17 06:48:10', NULL),
(965, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-08-17 06:48:13', '2018-08-17 06:48:13', NULL),
(966, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-08-17 06:51:24', '2018-08-17 06:51:24', NULL),
(967, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 06:51:29', '2018-08-17 06:51:29', NULL),
(968, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 07:01:27', '2018-08-17 07:01:27', NULL),
(969, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 07:01:49', '2018-08-17 07:01:49', NULL),
(970, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 07:02:03', '2018-08-17 07:02:03', NULL),
(971, 1, 'admin/company/edit/1', 'GET', '[]', '2018-08-17 07:07:17', '2018-08-17 07:07:17', NULL),
(972, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 07:07:21', '2018-08-17 07:07:21', NULL),
(973, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 07:09:42', '2018-08-17 07:09:42', NULL),
(974, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 07:09:46', '2018-08-17 07:09:46', NULL),
(975, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 07:10:58', '2018-08-17 07:10:58', NULL),
(976, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 07:11:17', '2018-08-17 07:11:17', NULL),
(977, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 07:14:31', '2018-08-17 07:14:31', NULL),
(978, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 07:15:43', '2018-08-17 07:15:43', NULL),
(979, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 07:16:14', '2018-08-17 07:16:14', NULL),
(980, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 07:16:47', '2018-08-17 07:16:47', NULL),
(981, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 07:19:07', '2018-08-17 07:19:07', NULL),
(982, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-08-17 07:19:14', '2018-08-17 07:19:14', NULL),
(983, 1, 'admin/company/contact/edit/1', 'GET', '[]', '2018-08-17 07:19:16', '2018-08-17 07:19:16', NULL),
(984, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 07:19:19', '2018-08-17 07:19:19', NULL),
(985, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 07:19:21', '2018-08-17 07:19:21', NULL),
(986, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 07:34:49', '2018-08-17 07:34:49', NULL),
(987, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 07:35:30', '2018-08-17 07:35:30', NULL),
(988, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 07:37:42', '2018-08-17 07:37:42', NULL),
(989, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 07:38:21', '2018-08-17 07:38:21', NULL),
(990, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 07:38:43', '2018-08-17 07:38:43', NULL),
(991, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"candidates\"}', '2018-08-17 08:07:05', '2018-08-17 08:07:05', NULL),
(992, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-08-17 08:07:09', '2018-08-17 08:07:09', NULL),
(993, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 08:07:28', '2018-08-17 08:07:28', NULL),
(994, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-08-17 08:07:30', '2018-08-17 08:07:30', NULL),
(995, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 08:07:31', '2018-08-17 08:07:31', NULL),
(996, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:07:40', '2018-08-17 08:07:40', NULL),
(997, 1, 'admin/company/industry/edit/1', 'POST', '{\"_token\":\"iZ9c1WRsDL7cpLF5tcFcrADOXpCwTtDmQWPFfiVc\",\"name\":\"Beverly Hills\",\"user_id\":\"1\",\"work_category\":\"1\",\"teaor_id\":\"1\",\"feor_id\":\"1\",\"person\":\"Skjjn\",\"aszf\":\"Sivgv\",\"save_and_exit\":null}', '2018-08-17 08:08:05', '2018-08-17 08:08:05', NULL),
(998, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 08:08:05', '2018-08-17 08:08:05', NULL),
(999, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 08:13:50', '2018-08-17 08:13:50', NULL),
(1000, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:13:53', '2018-08-17 08:13:53', NULL),
(1001, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:14:07', '2018-08-17 08:14:07', NULL),
(1002, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:15:08', '2018-08-17 08:15:08', NULL),
(1003, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:15:28', '2018-08-17 08:15:28', NULL),
(1004, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:15:40', '2018-08-17 08:15:40', NULL),
(1005, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:15:49', '2018-08-17 08:15:49', NULL),
(1006, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:16:00', '2018-08-17 08:16:00', NULL),
(1007, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:16:30', '2018-08-17 08:16:30', NULL),
(1008, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:16:42', '2018-08-17 08:16:42', NULL),
(1009, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:17:09', '2018-08-17 08:17:09', NULL),
(1010, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:17:11', '2018-08-17 08:17:11', NULL),
(1011, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:17:43', '2018-08-17 08:17:43', NULL),
(1012, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:18:06', '2018-08-17 08:18:06', NULL),
(1013, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:18:42', '2018-08-17 08:18:42', NULL),
(1014, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:18:53', '2018-08-17 08:18:53', NULL),
(1015, 1, 'admin/company/industry/edit/1', 'POST', '{\"_token\":\"iZ9c1WRsDL7cpLF5tcFcrADOXpCwTtDmQWPFfiVc\",\"name\":\"dsafdas\",\"user_id\":\"1\",\"company_sites_id\":\"1\",\"work_category\":\"1\",\"teaor_id\":\"1\",\"feor_id\":\"1\",\"person\":\"dsafdasfasd\",\"aszf\":\"sdfdsafds\",\"save_and_exit\":null}', '2018-08-17 08:19:22', '2018-08-17 08:19:22', NULL),
(1016, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 08:19:22', '2018-08-17 08:19:22', NULL),
(1017, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 08:22:25', '2018-08-17 08:22:25', NULL),
(1018, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 08:24:41', '2018-08-17 08:24:41', NULL),
(1019, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 08:25:30', '2018-08-17 08:25:30', NULL),
(1020, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 08:25:32', '2018-08-17 08:25:32', NULL),
(1021, 1, 'admin/company/industry/edit/1', 'GET', '[]', '2018-08-17 08:25:35', '2018-08-17 08:25:35', NULL),
(1022, 1, 'admin/company/industry/edit/1', 'POST', '{\"_token\":\"iZ9c1WRsDL7cpLF5tcFcrADOXpCwTtDmQWPFfiVc\",\"name\":\"xcyvcyxvcxy\",\"user_id\":\"1\",\"company_sites_id\":\"1\",\"work_category\":\"1\",\"teaor_id\":null,\"feor_id\":null,\"person\":\"sdfdsa\",\"aszf\":\"dsafdas\",\"save_and_exit\":null}', '2018-08-17 08:25:58', '2018-08-17 08:25:58', NULL),
(1023, 1, 'admin/company/industry/edit/1/0/data', 'GET', '[]', '2018-08-17 08:25:58', '2018-08-17 08:25:58', NULL),
(1024, 1, 'admin/company/industry/edit/1/0/data', 'POST', '{\"_token\":\"iZ9c1WRsDL7cpLF5tcFcrADOXpCwTtDmQWPFfiVc\",\"name\":\"xcyvcyxvcxy\",\"user_id\":\"1\",\"company_sites_id\":\"1\",\"work_category\":\"1\",\"teaor_id\":\"1\",\"feor_id\":\"1\",\"person\":\"sdfdsa\",\"aszf\":\"dsafdas\",\"save_and_exit\":null}', '2018-08-17 08:26:11', '2018-08-17 08:26:11', NULL),
(1025, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 08:26:11', '2018-08-17 08:26:11', NULL),
(1026, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 08:29:09', '2018-08-17 08:29:09', NULL),
(1027, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 08:29:53', '2018-08-17 08:29:53', NULL),
(1028, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-17 08:30:11', '2018-08-17 08:30:11', NULL),
(1029, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-08-17 08:30:49', '2018-08-17 08:30:49', NULL),
(1030, 1, 'admin/worker/list', 'GET', '[]', '2018-08-17 08:32:17', '2018-08-17 08:32:17', NULL),
(1031, 1, 'admin/worker/edit/1', 'GET', '[]', '2018-08-17 08:32:25', '2018-08-17 08:32:25', NULL),
(1032, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 08:32:27', '2018-08-17 08:32:27', NULL),
(1033, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-17 08:32:30', '2018-08-17 08:32:30', NULL),
(1034, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 08:32:31', '2018-08-17 08:32:31', NULL),
(1035, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-17 08:33:03', '2018-08-17 08:33:03', NULL),
(1036, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 08:33:08', '2018-08-17 08:33:08', NULL),
(1037, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-17 08:33:10', '2018-08-17 08:33:10', NULL),
(1038, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 08:33:11', '2018-08-17 08:33:11', NULL),
(1039, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-17 08:33:13', '2018-08-17 08:33:13', NULL),
(1040, 1, 'admin/worker/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 08:33:44', '2018-08-17 08:33:44', NULL),
(1041, 1, 'admin/worker/document/edit/1', 'GET', '[]', '2018-08-17 08:33:46', '2018-08-17 08:33:46', NULL),
(1042, 1, 'admin/user/list', 'GET', '[]', '2018-08-17 08:34:56', '2018-08-17 08:34:56', NULL),
(1043, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 08:34:57', '2018-08-17 08:34:57', NULL),
(1044, 1, 'admin/company/edit/1', 'GET', '[]', '2018-08-17 08:35:01', '2018-08-17 08:35:01', NULL),
(1045, 1, 'admin/company/edit/1', 'GET', '[]', '2018-08-17 08:47:31', '2018-08-17 08:47:31', NULL),
(1046, 1, 'admin/company/edit/1', 'GET', '[]', '2018-08-17 08:51:17', '2018-08-17 08:51:17', NULL),
(1047, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 08:51:22', '2018-08-17 08:51:22', NULL),
(1048, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 08:51:32', '2018-08-17 08:51:32', NULL),
(1049, 1, 'admin/company/list', 'GET', '[]', '2018-08-17 08:52:28', '2018-08-17 08:52:28', NULL),
(1050, 1, 'admin/company/edit/1', 'GET', '[]', '2018-08-17 08:52:32', '2018-08-17 08:52:32', NULL),
(1051, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 08:52:34', '2018-08-17 08:52:34', NULL),
(1052, 1, 'admin/company/document/edit/1', 'GET', '[]', '2018-08-17 08:56:44', '2018-08-17 08:56:44', NULL),
(1053, 1, 'admin/company/document/edit/1', 'GET', '[]', '2018-08-17 08:57:46', '2018-08-17 08:57:46', NULL),
(1054, 1, 'admin/company/document/edit/1', 'GET', '[]', '2018-08-17 08:59:40', '2018-08-17 08:59:40', NULL),
(1055, 1, 'admin/company/document/edit/1', 'GET', '[]', '2018-08-17 08:59:59', '2018-08-17 08:59:59', NULL),
(1056, 1, 'admin/company/document/edit/1', 'GET', '[]', '2018-08-17 09:00:45', '2018-08-17 09:00:45', NULL),
(1057, 1, 'admin/company/document/edit/1', 'GET', '[]', '2018-08-17 09:01:01', '2018-08-17 09:01:01', NULL),
(1058, 1, 'admin/company/document/edit/1', 'POST', '{\"_token\":\"nSo2FLXtNGDJRmL0tVgv6Rm1NkaQhidmPRHT4XtF\",\"document_id\":\"1\",\"date\":\"2018-08-21\",\"comment\":\"dsafdasdfdas\",\"save_and_exit\":null,\"file\":{}}', '2018-08-17 09:01:28', '2018-08-17 09:01:28', NULL),
(1059, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 09:01:28', '2018-08-17 09:01:28', NULL),
(1060, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 09:02:13', '2018-08-17 09:02:13', NULL),
(1061, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 09:04:17', '2018-08-17 09:04:17', NULL),
(1062, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 09:05:30', '2018-08-17 09:05:30', NULL),
(1063, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 09:05:37', '2018-08-17 09:05:37', NULL),
(1064, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-17 09:05:57', '2018-08-17 09:05:57', NULL),
(1065, 1, 'admin/acl/list', 'GET', '[]', '2018-08-18 18:08:48', '2018-08-18 18:08:48', NULL),
(1066, 1, 'admin/user/list', 'GET', '[]', '2018-08-18 18:08:50', '2018-08-18 18:08:50', NULL),
(1067, 1, 'admin/company/list', 'GET', '[]', '2018-08-18 18:08:53', '2018-08-18 18:08:53', NULL),
(1068, 1, 'admin/worker/list', 'GET', '[]', '2018-08-18 18:08:56', '2018-08-18 18:08:56', NULL),
(1069, 1, 'admin/language/list', 'GET', '[]', '2018-08-18 18:09:02', '2018-08-18 18:09:02', NULL),
(1070, 1, 'admin/education/list', 'GET', '[]', '2018-08-18 18:09:05', '2018-08-18 18:09:05', NULL),
(1071, 1, 'admin/trainingarea/list', 'GET', '[]', '2018-08-18 18:09:08', '2018-08-18 18:09:08', NULL),
(1072, 1, 'admin/worker/list', 'GET', '[]', '2018-08-18 18:09:30', '2018-08-18 18:09:30', NULL),
(1073, 1, 'admin/worker/edit/1', 'GET', '[]', '2018-08-18 18:09:34', '2018-08-18 18:09:34', NULL),
(1074, 1, 'admin/company/list', 'GET', '[]', '2018-08-18 18:10:18', '2018-08-18 18:10:18', NULL),
(1075, 1, 'admin/company/list', 'GET', '[]', '2018-08-18 18:10:20', '2018-08-18 18:10:20', NULL),
(1076, 1, 'admin/company/edit/1', 'GET', '[]', '2018-08-18 18:10:23', '2018-08-18 18:10:23', NULL),
(1077, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-18 18:10:27', '2018-08-18 18:10:27', NULL),
(1078, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-18 18:10:29', '2018-08-18 18:10:29', NULL),
(1079, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-08-18 18:10:31', '2018-08-18 18:10:31', NULL),
(1080, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-18 18:10:34', '2018-08-18 18:10:34', NULL),
(1081, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-08-18 18:10:36', '2018-08-18 18:10:36', NULL),
(1082, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"candidates\"}', '2018-08-18 18:10:38', '2018-08-18 18:10:38', NULL),
(1083, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-08-18 18:10:40', '2018-08-18 18:10:40', NULL),
(1084, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contract\"}', '2018-08-18 18:10:41', '2018-08-18 18:10:41', NULL),
(1085, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-18 18:10:42', '2018-08-18 18:10:42', NULL),
(1086, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"contact\"}', '2018-08-18 18:10:45', '2018-08-18 18:10:45', NULL),
(1087, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"documents\"}', '2018-08-18 18:10:52', '2018-08-18 18:10:52', NULL),
(1088, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-18 18:10:55', '2018-08-18 18:10:55', NULL),
(1089, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-18 18:10:57', '2018-08-18 18:10:57', NULL),
(1090, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"sites\"}', '2018-08-18 18:11:01', '2018-08-18 18:11:01', NULL),
(1091, 1, 'admin/company/companysites/edit/1/2', 'GET', '[]', '2018-08-18 18:11:05', '2018-08-18 18:11:05', NULL),
(1092, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"industry\"}', '2018-08-18 18:11:12', '2018-08-18 18:11:12', NULL),
(1093, 1, 'admin/company/industry/edit/1/3', 'GET', '[]', '2018-08-18 18:11:16', '2018-08-18 18:11:16', NULL),
(1094, 1, 'admin/company/edit/1', 'GET', '{\"tab\":\"data\"}', '2018-08-18 18:11:34', '2018-08-18 18:11:34', NULL),
(1095, 1, 'admin/company/list', 'GET', '[]', '2018-08-18 18:11:39', '2018-08-18 18:11:39', NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `_logins`
--

CREATE TABLE `_logins` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Egyedi azonosító',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Email cím',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Beírt jelszó',
  `visitor` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'IP cím',
  `disabled_until` datetime DEFAULT NULL COMMENT 'Kizárás ideje',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `_maillog`
--

CREATE TABLE `_maillog` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Egyedi azonosító',
  `to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Címzett',
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tárgy',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Üzenet',
  `files` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Fájlok',
  `order_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Megrendelés azonosító',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `accesslog`
--
ALTER TABLE `accesslog`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articles_id_unique` (`id`),
  ADD KEY `articles_deleted_at_slug_title_index` (`deleted_at`,`slug`,`title`);

--
-- A tábla indexei `article_category`
--
ALTER TABLE `article_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `article_category_id_unique` (`id`);

--
-- A tábla indexei `article_item_settings`
--
ALTER TABLE `article_item_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `article_item_settings_id_unique` (`id`);

--
-- A tábla indexei `article_settings`
--
ALTER TABLE `article_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `article_settings_id_unique` (`id`);

--
-- A tábla indexei `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banners_deleted_at_slug_index` (`deleted_at`,`slug`);

--
-- A tábla indexei `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_deleted_at_parent_id_index` (`deleted_at`,`parent_id`);

--
-- A tábla indexei `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `companies_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `company_contacts`
--
ALTER TABLE `company_contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_contacts_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `company_contract`
--
ALTER TABLE `company_contract`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_contract_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `company_documents`
--
ALTER TABLE `company_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `worker_documents_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `company_industries`
--
ALTER TABLE `company_industries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_industries_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `company_sites`
--
ALTER TABLE `company_sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_sites_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `content_duplication`
--
ALTER TABLE `content_duplication`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `content_duplication_id_unique` (`id`);

--
-- A tábla indexei `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `driver_license`
--
ALTER TABLE `driver_license`
  ADD PRIMARY KEY (`id`),
  ADD KEY `driver_license_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `educations`
--
ALTER TABLE `educations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `educations_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_id_unique` (`id`),
  ADD KEY `email_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `feor`
--
ALTER TABLE `feor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `feor_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `languages_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `layouts`
--
ALTER TABLE `layouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `layouts_deleted_at_slug_index` (`deleted_at`,`slug`);

--
-- A tábla indexei `lq_options`
--
ALTER TABLE `lq_options`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lq_options_id_unique` (`id`),
  ADD KEY `lq_options_lq_key_index` (`lq_key`);

--
-- A tábla indexei `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_id_unique` (`id`),
  ADD KEY `menus_deleted_at_shortcode_index` (`deleted_at`,`shortcode`);

--
-- A tábla indexei `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menu_items_id_unique` (`id`),
  ADD KEY `menu_items_menu_id_page_id_index` (`menu_id`,`page_id`);

--
-- A tábla indexei `nationality`
--
ALTER TABLE `nationality`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nationality_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `newsletter_list`
--
ALTER TABLE `newsletter_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `_maillog_id_unique` (`id`);

--
-- A tábla indexei `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- A tábla indexei `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_id_unique` (`id`),
  ADD KEY `pages_deleted_at_slug_index` (`deleted_at`,`slug`);

--
-- A tábla indexei `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `positions_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `project_status`
--
ALTER TABLE `project_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_status_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_id_unique` (`id`);

--
-- A tábla indexei `roles_acls`
--
ALTER TABLE `roles_acls`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_acls_id_unique` (`id`),
  ADD KEY `roles_acls_role_id_path_index` (`role_id`,`path`);

--
-- A tábla indexei `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `role_user_role_id_user_id_index` (`role_id`,`user_id`);

--
-- A tábla indexei `sitebuilder`
--
ALTER TABLE `sitebuilder`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sitebuilder_id_unique` (`id`),
  ADD KEY `sitebuilder_url_deleted_at_index` (`url`,`deleted_at`);

--
-- A tábla indexei `sitebuilder_history`
--
ALTER TABLE `sitebuilder_history`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sitebuilder_history_id_unique` (`id`),
  ADD KEY `sitebuilder_history_sitebuilder_id_deleted_at_index` (`sitebuilder_id`,`deleted_at`);

--
-- A tábla indexei `specialty_educations`
--
ALTER TABLE `specialty_educations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `specialty_educations_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `teaor`
--
ALTER TABLE `teaor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teaor_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `topsliders`
--
ALTER TABLE `topsliders`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `training_areas`
--
ALTER TABLE `training_areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_areas_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_id_unique` (`id`),
  ADD KEY `users_deleted_at_email_index` (`deleted_at`,`email`);

--
-- A tábla indexei `user_billing`
--
ALTER TABLE `user_billing`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_billing_id_unique` (`id`),
  ADD KEY `user_billing_user_id_index` (`user_id`);

--
-- A tábla indexei `user_shipping`
--
ALTER TABLE `user_shipping`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_shipping_id_unique` (`id`),
  ADD KEY `user_shipping_user_id_index` (`user_id`);

--
-- A tábla indexei `virtual_url`
--
ALTER TABLE `virtual_url`
  ADD PRIMARY KEY (`id`),
  ADD KEY `virtual_url_deleted_at_from_url_index` (`deleted_at`,`from_url`);

--
-- A tábla indexei `widgets`
--
ALTER TABLE `widgets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `widgets_deleted_at_slug_index` (`deleted_at`,`slug`);

--
-- A tábla indexei `workcategories`
--
ALTER TABLE `workcategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `workcategories_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `workers`
--
ALTER TABLE `workers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `workers_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `worker_documents`
--
ALTER TABLE `worker_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `worker_documents_deleted_at_index` (`deleted_at`);

--
-- A tábla indexei `_accesslog`
--
ALTER TABLE `_accesslog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `_accesslog_user_id_route_index` (`user_id`,`route`);

--
-- A tábla indexei `_logins`
--
ALTER TABLE `_logins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `_logins_id_unique` (`id`),
  ADD KEY `_logins_email_visitor_disabled_until_index` (`email`,`visitor`,`disabled_until`);

--
-- A tábla indexei `_maillog`
--
ALTER TABLE `_maillog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `_maillog_id_unique` (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `accesslog`
--
ALTER TABLE `accesslog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `article_category`
--
ALTER TABLE `article_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `article_item_settings`
--
ALTER TABLE `article_item_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `article_settings`
--
ALTER TABLE `article_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `company_contacts`
--
ALTER TABLE `company_contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT a táblához `company_contract`
--
ALTER TABLE `company_contract`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `company_documents`
--
ALTER TABLE `company_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `company_industries`
--
ALTER TABLE `company_industries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT a táblához `company_sites`
--
ALTER TABLE `company_sites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `content_duplication`
--
ALTER TABLE `content_duplication`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `driver_license`
--
ALTER TABLE `driver_license`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `educations`
--
ALTER TABLE `educations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `email`
--
ALTER TABLE `email`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Egyedi azonosító';

--
-- AUTO_INCREMENT a táblához `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `feor`
--
ALTER TABLE `feor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `layouts`
--
ALTER TABLE `layouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `lq_options`
--
ALTER TABLE `lq_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Egyedi azonosító', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT a táblához `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Egyedi azonosító';

--
-- AUTO_INCREMENT a táblához `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Egyedi azonosító';

--
-- AUTO_INCREMENT a táblához `nationality`
--
ALTER TABLE `nationality`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `newsletter_list`
--
ALTER TABLE `newsletter_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Egyedi azonosító';

--
-- AUTO_INCREMENT a táblához `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Egyedi azonosító';

--
-- AUTO_INCREMENT a táblához `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `project_status`
--
ALTER TABLE `project_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Egyedi azonosító', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT a táblához `roles_acls`
--
ALTER TABLE `roles_acls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Egyedi azonosító';

--
-- AUTO_INCREMENT a táblához `sitebuilder`
--
ALTER TABLE `sitebuilder`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `sitebuilder_history`
--
ALTER TABLE `sitebuilder_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `specialty_educations`
--
ALTER TABLE `specialty_educations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `teaor`
--
ALTER TABLE `teaor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `topsliders`
--
ALTER TABLE `topsliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT a táblához `training_areas`
--
ALTER TABLE `training_areas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Egyedi azonosító', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `user_billing`
--
ALTER TABLE `user_billing`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Egyedi azonosító', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `user_shipping`
--
ALTER TABLE `user_shipping`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Egyedi azonosító', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `virtual_url`
--
ALTER TABLE `virtual_url`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `widgets`
--
ALTER TABLE `widgets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `workcategories`
--
ALTER TABLE `workcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `workers`
--
ALTER TABLE `workers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT a táblához `worker_documents`
--
ALTER TABLE `worker_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT a táblához `_accesslog`
--
ALTER TABLE `_accesslog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1096;

--
-- AUTO_INCREMENT a táblához `_logins`
--
ALTER TABLE `_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Egyedi azonosító';

--
-- AUTO_INCREMENT a táblához `_maillog`
--
ALTER TABLE `_maillog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Egyedi azonosító';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
