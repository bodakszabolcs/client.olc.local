<?php

namespace App\Contracts;

interface BaseModelInterface
{
    public function searchInModel(array $filter);
    public function fillAndSave(array $request);
}
