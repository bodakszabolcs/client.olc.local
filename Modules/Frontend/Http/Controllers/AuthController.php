<?php

namespace Modules\Frontend\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use App\Mail\ForgotPassword;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Storage;
use Laravel\Socialite\Facades\Socialite;
use Modules\Frontend\Http\Request\ForgottenPasswordRequest;
use Modules\Frontend\Http\Request\UserPasswordRequest;
use Modules\Frontend\Http\Request\UserRegisterRequest;
use Modules\Frontend\Http\Request\ValidateHashRequest;
use Modules\User\Entities\User;
use Modules\User\Entities\UserBilling;
use Modules\User\Http\Requests\UserUpdateRequest;
use Modules\User\Transformers\UserResource;
use Illuminate\Http\Request;

class AuthController extends AbstractLiquidController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function loginSocial(Request $request)
    {
        try {
            $sc_result = Socialite::driver($request->input('type'))->fields([
                'first_name',
                'last_name',
                'email',
            ])->userFromToken($request->input('token'));
            if ($sc_result->id != $request->input('facebook_id')) {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            return response(json_encode($e->getMessage()), $this->errorStatus);
        }
        
        $user = User::firstOrCreate(['email' => $sc_result->email, 'lastname' => $sc_result->user['last_name'], 'firstname' => $sc_result->user['first_name']]);
        $pw = date("Y-m-d H:i:s");
       
        if (empty($user->password)) {
            $user->password = Hash::make($pw);
        }
        $user->facebook_id = $sc_result->id;
        if(empty($user->avatar)){
            $user->avatar = $sc_result->avatar;
        }
        if(empty($user->name)) {
	        $user->name = $user->last_name . " " . $user->firstname;
	        $user->save();
        }
        
        Auth::login($user);

       // return response()->json(['access_token' => $sc_result], $this->successStatus);
        return response()->json(['access_token' => $user->createToken('access_token')->accessToken], $this->successStatus);
    }

    public function register(UserRegisterRequest $request)
    {
        $user = new User();
        $user->fill($request->all());
        $user->password = Hash::make($request->input('password'));
        $user->save();
        Auth::login($user);
        return response()->json(['access_token' => $user->createToken('access_token')->accessToken], $this->successStatus);
    }

    public function checkUser(Request $request)
    {

        if ($request->has('facebook_id')) {
            $user = User::where('facebook_id', '=', $request->input('facebook_id'))->first();
            if (!is_null($user)) {
                return response()->json(['exist' => true], $this->successStatus);
            }
        }
        if ($request->has('google_id')) {
            $user = User::where('google_id', '=', $request->input('google_id'))->first();
            if (!is_null($user)) {
                return response()->json(['exist' => true], $this->successStatus);
            }
        }
        if ($request->has('email')) {
            $user = User::where('email', '=', $request->input('email'))->first();
            if (!is_null($user)) {
                return response()->json(['exist' => true], $this->successStatus);
            }
        }
        return response()->json(['exist' => false], $this->errorStatus);

    }

    public function forgot(ForgottenPasswordRequest $request)
    {
        try {
            $user = User::where('email', $request->get('email'))->firstOrFail();
            $user->hash_2 = md5($request->get('email').date("Y-m-d H:i:s"));
            $user->save();
        } catch (ModelNotFoundException $e) {
            return response()->json(['status' => 'ERROR'], $this->errorStatus);
        }

        $link = $user->hash_2;

        Mail::to($user)->send(new ForgotPassword($user, $link));

        return response()->json(['status' => 'OK'], $this->successStatus);
    }

    public function changePassword(UserPasswordRequest $request)
    {
        $user = Auth::user();
        $user->password = Hash::make($request->get('new_password'));
        $user->hash_2 = null;
        $user->hash_2_date = null;
        $user->save();

        return response()->json(['status' => 'OK'], $this->successStatus);
    }

    public function validateHash(Request $request, $hash)
    {
        try {
            $user = User::where('hash_2', $hash)->firstOrFail();
            return response()->json(['status' => 'OK', 'data' => ['email' => $user->email]], $this->successStatus);
        } catch (ModelNotFoundException $e)
        {
            return response()->json(['status' => 'ERROR', 'data' => ['email' => $hash]], $this->errorStatus);
        }
    }

    public function newPassword(ValidateHashRequest $request, $hash)
    {
        try {
            $user = User::where('hash_2', $hash)->firstOrFail();

            $user->password = Hash::make($request->input('password'));
            $user->hash_2 = null;
            $user->save();

            return response()->json(['status' => 'OK', 'data' => ['email' => $user->email]], $this->successStatus);
        } catch (ModelNotFoundException $e)
        {
            return response()->json(['status' => 'ERROR', 'data' => ['email' => $hash]], $this->errorStatus);
        }
    }

    public function profile(Request $request)
    {
        $user = \Modules\User\Entities\User::find(Auth::id());

        return new UserResource($user);
    }

    public function updateProfile(UserUpdateRequest $request)
    {
        $user = User::find(Auth::id());

        $user->fill($request->all());
        if ($request->has('password')) {
            $user->password = Hash::make($request->input('password'));
        }
        $user->save();

        return new UserResource($user);
    }

    public function fileUpload(Request $request)
    {
        $inPath = '/uploads/avatars';

        if ($request->has('path') && !empty($request->input('path'))) {
            $inPath = 'uploads/' . $request->input('path');
        }
        if ($request->has('file')) {
            $file = $request->file('file');

            $fileName = Storage::disk('uploads')->put($inPath, $file);

            $user = User::find(Auth::id());
            $user->avatar = $fileName;
            $user->save();


            return response()->json([
                'data' => [
                    'status' => 'ok',
                    'full_path' => url($fileName),
                ]
            ], $this->successStatus);
        }
    }
}
