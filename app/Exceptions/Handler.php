<?php

namespace App\Exceptions;

use App\Http\Papertrail;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Foundation\Http\Exceptions\MaintenanceModeException;
use Illuminate\Session\TokenMismatchException;
use App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
        \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException::class
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {

        if (
            !($exception instanceof MethodNotAllowedHttpException) &&
            !($exception instanceof AuthenticationException) &&
            !($exception instanceof AuthorizationException) &&
            !($exception instanceof HttpException) &&
            !($exception instanceof ModelNotFoundException) &&
            !($exception instanceof TokenMismatchException) &&
            !($exception instanceof ValidationException) &&
            !($exception instanceof MethodNotAllowedHttpException)
        )
        {
            if (!empty($exception->getMessage()))
            {
                Papertrail::send_error_log('ERROR', $exception->getMessage(), $exception->getFile(), $exception->getLine());
            }
        }
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {

        switch ($exception)
        {
            case ($exception instanceof UnauthorizedException):
                if (strpos($request->path(),'admin') !== false && Auth::check())
                {

                    return response()->view('errors.admin_401', [], 401);
                } else {
                    return response()->view('errors.401', [], 401);
                }
                break;
            case ($exception instanceof NotFoundHttpException):
                if (strpos($request->path(),'admin') !== false && Auth::check())
                {
                    return response()->view('errors.admin_404', [], 404);
                } else {
                    return response()->view('errors.404', [], 404);
                }
                break;
            case ($exception instanceof MaintenanceModeException):
                if (strpos($request->path(),'admin') !== false && Auth::check())
                {
                    return response()->view('errors.admin_503', [], 503);
                } else {
                    return response()->view('errors.503', [], 503);
                }
                break;
            case ($exception instanceof TokenMismatchException):
                return redirect($request->fullUrl())->with('error_message',__('Az űrlap lejárt. Kérem töltse ki újra!'));
                break;
            case ($exception instanceof UnauthorizedException):
            case ($exception instanceof AuthenticationException):
            case ($exception instanceof UnauthorizedHttpException):
                return parent::render($request, $exception);
                break;
            default:
                if (App::environment() != 'local')
                {
                    if (strpos($request->path(),'admin') !== false && Auth::check())
                    {
                        return response()->view('errors.admin_error', ['exception' => $exception], 500);
                    } else {
                        return response()->view('errors.error', ['exception' => $exception], 500);
                    }
                }
                return parent::render($request, $exception);
                break;
        }
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        $prefix = $request->route()->getPrefix();
        if (!empty($prefix) && $prefix != '/')
        {
            return redirect()->guest('admin/login');
        } else {
            return redirect()->guest(route('frontend_static',['/']).'#bejelentkezes');
        }
    }
}
