<?php

	namespace Modules\Coupon\Entities;

	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Database\Eloquent\SoftDeletes;
	use Mockery\Exception;
	use App\LiquidModel;
	use Sunra\PhpSimple\HtmlDomParser;
	use GeneaLabs\LaravelModelCaching\Traits\Cachable;

	class AutomaticCoupon extends LiquidModel
	{
		use SoftDeletes, Cachable;
        public static $rules =[
            1 => 'Ha a havi vásárlások összege eléri a megadott összeget',
            2 => 'Ha a havi vásárlások száma több mint',
            3 => 'Ha már X napja nem vásárolt',
            4 => 'Első várálás után',
        ];
		protected $table = 'coupon_rules';

		protected $dates = ['deleted_at', 'created_at', 'updated_at'];
		/**
		 * The attributes that are mass assignable.
	 	*
		 * @var array
	 	*/
		protected $fillable = [
			'rule', //user
			'value', //Bolt
			'type', //kód
			'amount', //

		];

		public $searchColumns = [
			'id' => 'text',
			'user_name'	=>	'text',
			'shop_id'	=>	'text', 
			'code'	=>	'text', 
			'type'	=>	'text', 
			'amount'	=>	'text', 
			'expire'	=>	'text', 
			];

		public $searchColumnLabels = [
			'id' => 'ID',
			'user_name'	=>	'Felhasználó',
			'shop_id'	=>	'Bolt', 
			'code'	=>	'Kód', 
			'type'	=>	'Típus', 
			'amount'	=>	'Mennyiség', 
			'expire'	=>	'Érvényesség', 
			];
		protected $casts = [
			];
		public function shop(){
			return $this->belongsTo('Modules\Company\Entities\Shop','shop_id','id');
		}
		public static function getCoupon($user_id,$code){
			return \Modules\Coupon\Entities\Coupon::where('code','=',$code)->first();
		}
		public static function useCoupon($user_id,$code){

		}
	}
?>