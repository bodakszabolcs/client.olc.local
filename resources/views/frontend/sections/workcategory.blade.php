<section id="munkakategoriak">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2>Munka kategóriák</h2>
				<br>

			</div>
		</div>
		<div class="row">
			<?php
			$avCategories = Modules\Work\Entities\Work::join('workcategories','workcategory_id','=','workcategories.id')->where('archive','=',0)->select(DB::raw('parent_id as id, parent_id'))->groupBy('parent_id')->get()->pluck('id','parent_id');
			$categories = Modules\Workcategory\Entities\Workcategory::whereIn('id',$avCategories)->get();

			?>
			@foreach($categories as $c)
			<div class="col-md-3 col-sm-6">



			<a href="/munkak/{{$c->slug}}">
				 <div style="background: #fff   url('{{$c->image}}')   center center no-repeat; background-size: cover;height:260px"></div>


			</a>
						<h4 class="text-center">{{$c->name}}</h4>
						<a class="btn btn-success form-control" href="/munkak/{{$c->slug}}">Részletek<i class="fa fa-arrow-right pull-right"></i></a>
						<br>
						<br>
						<br>

			</div>
				@if($loop->iteration%4 == 0)
						<div class="clearfix"></div>
				@endif
			@endforeach



			<div class="clearfix"></div>


			</div>
		</div>
	</section>