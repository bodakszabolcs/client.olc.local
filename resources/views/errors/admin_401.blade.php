@extends('admin::layouts.layout')
@section('content')
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid  m-error-3" style="background-image: url(/assets/app/media/img//error/bg3.jpg);">
            <div class="m-error_container">
					<span class="m-error_number">
						<h1>
							401
						</h1>
					</span>
                <p class="m-error_title m--font-light">
                    {{__('Hoppá!')}}
                </p>
                <p class="m-error_subtitle">
                    {{__('Az oldal megtekintéséhez nincs jogosultsága!')}}
                </p>
                <p class="m-error_description">
                    {{__('Kérem lépjen kapcsolatba az Adminisztrátorral, ha gyakran látja ezt az oldalt!')}}
                </p>
            </div>
        </div>
    </div>
@endsection
