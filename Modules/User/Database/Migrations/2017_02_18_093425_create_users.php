<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->comment('Egyedi azonosító')->unique();
            $table->string('lastname')->comment('Vezetéknév');
            $table->string('firstname')->comment('Keresztnév');
            $table->string('email')->comment('Email cím');
            $table->string('password')->comment('Jelszó');
            $table->string('hash')->comment('Regisztráció megerősítése link')->nullable();
            $table->string('hash_2')->comment('Új jelszó generálása link')->nullable();
            $table->string('facebook_id')->comment('Facebook id')->nullable();
            $table->string('google_id')->comment('Google id')->nullable();
            $table->dateTime('hash_2_date')->comment('Új jelszó generálás érvényesség')->nullable();
            $table->string('remember_token')->comment('Token')->nullable();
            $table->tinyInteger('active')->comment('Aktív')->default(1);
            $table->string('avatar')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->index(['deleted_at','email']);
        });

        Schema::create('user_billing', function (Blueprint $table) {
            $table->increments('id')->comment('Egyedi azonosító')->unique();
            $table->integer('user_id')->comment('Felhasználó');
            $table->string('country')->comment('Ország')->nullable();
            $table->string('zip')->comment('Irányítószám')->nullable();
            $table->string('city')->comment('Város')->nullable();
            $table->string('address')->comment('Utca, hsz')->nullable();
            $table->string('note')->comment('Megjegyzés')->nullable();
            $table->string('name')->comment('Számlázási név')->nullable();
            $table->string('tax_number')->comment('Adószám')->nullable();
            $table->index(['user_id']);
        });

        Schema::create('user_shipping', function (Blueprint $table) {
            $table->increments('id')->comment('Egyedi azonosító')->unique();
            $table->integer('user_id')->comment('Felhasználó');
            $table->string('country')->comment('Ország')->nullable();
            $table->string('zip')->comment('Irányítószám')->nullable();
            $table->string('city')->comment('Város')->nullable();
            $table->string('address')->comment('Utca, hsz')->nullable();
            $table->string('note')->comment('Megjegyzés')->nullable();
            $table->string('name')->comment('Szállítási név')->nullable();
            $table->string('phone')->comment('Telefonszám')->nullable();
            $table->index(['user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        Schema::drop('user_billing');
        Schema::drop('user_shipping');
    }
}
