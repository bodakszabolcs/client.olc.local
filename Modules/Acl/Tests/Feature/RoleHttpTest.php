<?php

namespace Tests;

use Modules\Acl\Entities\Role;
use Modules\User\Entities\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase as BaseTestCase;

class RoleHttpTest extends TestCase
{
    protected $user = "";

    public function setUp()
    {
        parent::setUp();
        $this->user = User::find(1);
    }

    public function testRoleHttp()
    {
        $response = $this->actingAs($this->user)
            ->get(route('admin_acl_list'))
            ->assertStatus(200);

        $response = $this->actingAs($this->user)
            ->get(route('admin_acl_edit'))
            ->assertStatus(200);

        $response = $this->actingAs($this->user)
            ->get(route('admin_acl_edit',[1]))
            ->assertStatus(200);

    }

    public function testRoleHttpEdit()
    {
        $response = $this->actingAs($this->user)
            ->post(route('admin_acl_edit'),['name' => 'Test', 'description' => 'Test Description'])
            ->assertStatus(302);

        $last_role = Role::orderBy('id','DESC')->first();

        $response = $this->actingAs($this->user)
            ->get(route('admin_acl_delete',[$last_role->id]))
            ->assertStatus(302);

        $response = $this->actingAs($this->user)
            ->get(route('admin_acl_edit',[$last_role->id]))
            ->assertRedirect(route('admin_acl_list'));
    }
}
