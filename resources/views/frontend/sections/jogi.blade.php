@php
	$jogi = \Modules\Content\Entities\Content::find(7);

@endphp
@if(!is_null($jogi))
<section class="hetedik" id="jogi">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<img src="/assets/frontend//img/jogi.png" alt="Jogi tanácsadás">

				<h3>{{$jogi->title}}</h3>

			</div>
		</div>
		<div class="row">
			{!! $jogi->content !!}
		</div>
		<div class="col-lg-8 col-lg-offset-2 text-center">
			<a href="http://www.drsli.hu/" class="btn btn-lg btn-outline">
				Ajánlatkérés
			</a>
		</div>
	</div>

</section>
@endif