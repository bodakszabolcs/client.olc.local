<?php

Route::group(['namespace' => 'Modules\ModelBuilder\Http\Controllers', 'prefix' => 'admin/builder', 'middleware' => ['web','auth', 'acl', 'accesslog']], function ()
{
    Route::match(['get','post'], '/settings', 'ModelBuilderController@Settings')->name('admin_builder_settings');
});
