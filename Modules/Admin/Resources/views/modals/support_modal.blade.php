<div class="modal fade slide-up disable-scroll" id="modalSlideUp" tabindex="-1" role="dialog"
     aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5 class="modal-title">
                        {{__('Hibabejelentés')}}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                    </button>
                </div>
                {!! \App\Http\Helper::formOpen('support-modal','POST','',array('class'=>'form-horizontal', 'role' => 'form', 'autocomplete' => 'off')) !!}
                <div class="modal-body">
                    <div class="form-group-attached">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>{{__('Hiba leírása')}}</label>
                                    <textarea class="form-control" name="error-detail" id="error-detail" required
                                              rows="4" style="height: 150px"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-group-default">
                                    <label>{{__('Bejelentés típusa')}}</label>
                                    <select class="form-control" name="error-type" id="error-type">
                                        <option value="{{__('Hiba')}}">{{__('Hiba')}}</option>
                                        <option value="{{__('Fejlesztés')}}">{{__('Fejlesztés')}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-group-default">
                                    <label>{{__('Hibabejelentő')}}</label>
                                    <input type="text" name="error-name" id="error-name"
                                           class="form-control"
                                           value="{{Auth::user()->lastname.' '.Auth::user()->firstname}}"
                                           readonly>
                                    <input type="hidden" name="error-user" id="error-user"
                                           value="{{Auth::user()->id}}"/>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group form-group-default">
                                    <label>{{__('Hiba oldal')}}</label>
                                    <input type="text" class="form-control" name="error-upage"
                                           id="error-upage" value="{{Request::url()}}">
                                    <input type="hidden" name="error-page" id="error-page"
                                           value="{{Request::url()}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="p-t-20 clearfix p-l-10 p-r-10">

                            </div>
                        </div>
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <button type="submit"
                                    class="btn m-btn--square btn-primary btn-block m-t-5">{{__('Elküldés')}}</button>
                        </div>
                    </div>
                    {!! \App\Http\Helper::formClose() !!}
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>