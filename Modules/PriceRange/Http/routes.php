<?php 
Route::group(['namespace' => 'Modules\PriceRange\Http\Controllers', 'prefix' => 'admin/pricerange', 'middleware' => ['web','auth', 'acl', 'accesslog']], function () { 
		Route::match(['get'], '/list', 'PriceRangeController@PriceRangeList')->name('admin_pricerange_list');
		Route::match(['get', 'post', 'put'], '/edit/{id?}/{tab?}', 'PriceRangeController@Edit')->name('admin_pricerange_edit');
		Route::match(['get'], '/delete/{id}', 'PriceRangeController@Delete')->name('admin_pricerange_delete'); 
});
