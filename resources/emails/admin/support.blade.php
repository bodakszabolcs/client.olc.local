@component('mail::message')
    # {{__('Hibabejelentés')}}

    @component('mail::table', get_defined_vars())
        | Adatok        | Értkékek                |
        | ------------- |:-----------------------:|
        | Dátum         | {{date("Y-m-d H:i:s")}} |
        | Hiba oldal    | {{$error_page}}         |
        | Hiba típus    | {{$error_type}}         |
        | Felhasználó   | {{$user->firstname()}}  |
        | Felhasználó ID| {{$user->id}}           |
        | Hiba leírás   | {{$error_detail}}       |
    @endcomponent

    {{__('Üdvözlettel')}},
    {{ config('app.name') }}
@endcomponent
