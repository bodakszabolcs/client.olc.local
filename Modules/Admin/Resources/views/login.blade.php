<!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>
        {{config('app.name')}} | {{__('Bejelentkezés')}}
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="{{url('assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{url('assets/demo/default/style.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{url('assets/demo/default/media/img/logo/favicon.ico')}}"/>
    <style type="text/css">span.im-caret {
            -webkit-animation: 1s blink step-end infinite;
            animation: 1s blink step-end infinite;
        }

        @keyframes blink {
            from, to {
                border-right-color: black;
            }
            50% {
                border-right-color: transparent;
            }
        }

        @-webkit-keyframes blink {
            from, to {
                border-right-color: black;
            }
            50% {
                border-right-color: transparent;
            }
        }

        span.im-static {
            color: grey;
        }

        div.im-colormask {
            display: inline-block;
            border-style: inset;
            border-width: 2px;
            -webkit-appearance: textfield;
            -moz-appearance: textfield;
            appearance: textfield;
        }

        div.im-colormask > input {
            position: absolute;
            display: inline-block;
            background-color: transparent;
            color: transparent;
            -webkit-appearance: caret;
            -moz-appearance: caret;
            appearance: caret;
            border-style: none;
            left: 0; /*calculated*/
        }

        div.im-colormask > input:focus {
            outline: none;
        }

        div.im-colormask > input::-moz-selection{
            background: none;
        }

        div.im-colormask > input::selection{
            background: none;
        }
        div.im-colormask > input::-moz-selection{
            background: none;
        }
        .m-login.m-login--2 .m-login__wrapper .m-login__container .m-login__form .m-form__group .form-control {
            border-radius: 40px;
            border: none;
            padding: 1.5rem 1.5rem;
            margin-top: 1.5rem;
        }
        .m-login.m-login--2.m-login-2--skin-2 .m-login__container .m-login__form .form-control {
            color: #91899f;
            background: #f7f6f9;
        }
        .form-control, .form-control[readonly] {
            border-color: #ebedf2;
            color: #575962;
        }
        div.im-colormask > div {
            color: black;
            display: inline-block;
            width: 100px; /*calculated*/
        }</style><style type="text/css">/* Chart.js */
        @-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}
		.im-colormask {
			-webkit-animation: chartjs-render-monitor 5s infinite;
			-moz-animation: chartjs-render-monitor infinite;
			-o-animation: chartjs-render-monitor 5s infinite;
			animation: chartjs-render-monitor 5s infinite;
		}
	</style>
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2"
         id="m_login" style="background: url(../../../assets/app/media/img//bg/bg-3.jpg);">
        <div class="m-grid__item m-grid__item--fluid	m-login__wrapper ">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="#">
								   <img height="100" src="{{url('/assets/frontend/img/logo.png')}}">
							</a>
						</div>
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">
									Belépés az admin felületre
								</h3>
							</div>
                            {!! \App\Http\Helper::formOpen('form-login','POST',route('admin_login'),array('class'=>'m-login__form m-form', 'role' => 'form')) !!}
                            @if (count($errors) > 0)
                                <div class="m-alert m-alert--icon alert alert-danger" role="alert">
                            <div class="m-alert__icon">
                                <i class="flaticon-danger"></i>
                            </div>
                            <div class="m-alert__text">
                                <strong>
                                    {{__('Hiba történt!')}}
                                </strong><br>
                                @foreach ($errors->all() as $error)
                                    {{ $error }}<br/>
                                @endforeach
                            </div>
                        </div>
                            @endif
                            @if ($disabled_until != '')
                                <div class="m-alert m-alert--icon alert alert-danger" role="alert">
                            <div class="m-alert__icon">
                                <i class="flaticon-danger"></i>
                            </div>
                            <div class="m-alert__text">
                                <strong>
                                    {{__('A fiókja letiltásra került a sikertelen bejelentkezések miatt!')}}
                                </strong>
                            </div>
                            <div class="m-alert__actions" style="width: 220px;">
                                <button type="button" class="btn btn-outline-light btn-sm m-btn m-btn--hover-brand"
                                        data-dismiss="alert1" aria-label="Close">
                                    {{__('Bezárás')}}
                                </button>
                            </div>
                        </div>
                            @endif
                            {!! \App\Http\Helper::input(__('Email'),'email',old('email'),array('class'=>'form-control m-input', 'div_class'=>'m-form__group', 'has_label' => false)) !!}

                            {!! \App\Http\Helper::password(__('Jelszó'),'password',null,array('class'=>'form-control m-input', 'div_class'=>'m-form__group', 'has_label' => false)) !!}
                            <div class="row m-login__form-sub">
                        <div class="col m--align-left m-login__form-left">
                            <label class="m-checkbox  m-checkbox--focus">
                                <input type="checkbox" name="remember" value="1" checked>
                                {{__('Emlékezz rám')}}
                                <span></span>
                            </label>
                        </div>
                        <div class="col m--align-right m-login__form-right">

                        </div>
                    </div>
                    <div class="m-login__form-action">
                        <button id="m_login_signin_submit"
                                class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary"
                                type="submit" style="background: #ef5350;border-color:#ef5350">
                            {{__('Belépés')}}
                        </button>
                    </div>
                            {!! \App\Http\Helper::formClose() !!}
						</div>

					</div>
				</div>

    </div>
</div>
<!-- end:: Page -->
<!--begin::Base Scripts -->
<script src="{{url('assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{url('assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
<!--end::Base Scripts -->
</body>
<!-- end::Body -->
</html>
