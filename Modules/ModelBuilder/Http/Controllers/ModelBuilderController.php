<?php

namespace Modules\ModelBuilder\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Modules\Admin\Http\Controllers\AdminController;

class ModelBuilderController extends AdminController
{
    public $types = ['TEXT' => 'TEXT', 'INT' => 'INT', 'FLOAT' => 'FLOAT', 'VARCHAR(255)' => 'VARCHAR(255)', 'DATE' => 'DATE', 'DATETIME' => 'DATETIME'];

    public $niceNames = [
        'model_name' => 'Model neve',
        'table_name' => 'Tábla neve',
        'menu_name' => 'Menu neve',
        'column_name' => 'Oszlop neve',
        'label_name' => 'Label',
    ];

    public function Settings(Request $request)
    {
        View::share('title', __('Model builder'));
        $errors = [];


        if ($request->isMethod('POST')) {
            $response = $this->validateData($request);
            $model_error = [];

            if (file_exists(base_path('Modules') . '/' . ucfirst($request->input('model_name')) . '/Http/Controllers/' . ucfirst($request->input('model_name')) . 'Controller.php')) {

                $model_error[] = __('A Modul már létezik');
                $response['status'] = 'error';
            }

            if (Schema::hasTable($request->input('table_name'))) {
                $model_error[] = __('A Tábla már létezik');
                $response['status'] = 'error';
            }
            if (sizeof($model_error) > 0) {
                if (!isset($response['cust_errors'])) {
                    $response['cust_errors'] = [];

                }
            }
            $request->session()->put('create_errors', $model_error);


            if ($response['status'] == 'success') {
                try {
                    $this->createModule($request);
                    $this->createTable($request->all());
                    $this->createRoute($request->all());
                    $this->createModel($request->all());
                    $this->createController($request->all());
                    $this->createView($request->all());
                    $this->createMenuItem($request->all());
                } catch (\Exception $e) {
                    $request->session()->flash('error_message', __('Hiba a mentés során.' . $e->getMessage()));
                    return redirect(route('admin_builder_settings'))->withErrors([])->withInput();
                }

                $request->session()->flash('success_message',__('Modul sikeresen létrehozva!'));
                return redirect(route('admin_builder_settings'));

            } else {
                $request->session()->flash('error_message', trans('frontend.Hiba a mentés során. Kérem javítsa a hibákat!'));

                $request->session()->put('field_errors', $response['cust_errors']);
                if (isset($response['validator'])) {
                    return redirect(route('admin_builder_settings'))->withErrors($response['validator']->messages()->toArray())->withInput();
                } else {
                    return redirect(route('admin_builder_settings'))->withErrors([])->withInput();
                }
            }
            return redirect(route('admin_builder_settings'))->withErrors([])->withInput();
        }
        return view('modelbuilder::view', ['controller' => $this]);
    }

    public function validateData($request)
    {
        $rules = [
            'model_name' => 'required',
            'table_name' => 'required',
            'menu_name' => 'required',

        ];
        foreach ($request->input('column_name', []) as $k => $v) {
            $rules = array_merge($rules, ['column_name.' . $k => 'required']);
            $rules = array_merge($rules, ['column_label.' . $k => 'required']);
            $this->niceNames = array_merge($this->niceNames, ['column_name.' . $k => 'Oszlop neve']);
            $this->niceNames = array_merge($this->niceNames, ['column_label.' . $k => 'Label']);
        }
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($this->niceNames);
        $not_valid = $validator->fails();
        if ($not_valid) {
            //dump($validator->messages()->toArray());

            return ['status' => 'error', 'cust_errors' => $validator->messages()->toArray(), 'validator' => $validator];
        }
        return ['status' => 'success', 'success_message' => trans('frontend.A mentés sikeres')];


    }

    private function createModule($request)
    {
        Artisan::call('module:make', ['name' => [ucfirst($request->input('model_name'))]]);
    }

    private function createTable($table_data)
    {

        if (!Schema::hasTable($table_data['table_name'])) {

            Artisan::call('module:make-migration', ['name' => 'create_' . strtolower($table_data['model_name']) . '_table', 'module' => ucfirst($table_data['model_name'])]);

            Schema::connection('mysql')->create($table_data['table_name'], function ($table) use ($table_data) {

                $html = '$table->increments("id");'."\n";

                $table->charset = 'utf8';
                $table->collation = 'utf8_general_ci';
                $table->increments('id');
                foreach ($table_data['column_name'] as $k => $col) {
                    switch ($table_data['column_type'][$k]) {
                        case 'INT':
                            $table->integer(strtolower($col))->comment($table_data['column_comment'][$k])->default(0)->unsigned();
                            $html .= '$table->integer("'.strtolower($col).'")->comment("'.$table_data['column_comment'][$k].'"")->default(0)->unsigned();\n';
                            break;
                        case 'FlOAT':
                            $table->float(strtolower($col))->comment($table_data['column_comment'][$k]);
                            $html .= '$table->float("'.strtolower($col).'")->comment("'.$table_data['column_comment'][$k].'");'."\n";
                            break;
                        case 'VARCHAR(255)':
                            $table->string(strtolower($col), 255)->comment($table_data['column_comment'][$k]);
                            $html .= '$table->string("'.strtolower($col).'", 255)->comment("'.$table_data['column_comment'][$k].'");'."\n";
                            break;
                        case 'DATE':
                            $table->date(strtolower($col))->comment($table_data['column_comment'][$k])->default(null);
                            $html .= '$table->date("'.strtolower($col).'")->comment("'.$table_data['column_comment'][$k].'")->default(null);'."\n";
                            break;
                        case 'DATETIME':
                            $table->dateTime(strtolower($col))->comment($table_data['column_comment'][$k])->default(null);
                            $html .= '$table->dateTime("'.strtolower($col).'")->comment("'.$table_data['column_comment'][$k].'")->default(null);'."\n";
                            break;
                        default:
                            $table->text(strtolower($col))->comment($table_data['column_comment'][$k]);
                            $html .= '$table->text("'.strtolower($col).'")->comment("'.$table_data['column_comment'][$k].'");'."\n";
                            break;
                    }
                }
                if ($table_data['order']) {
                    $table->integer('o')->comment('Rendezés')->unsigned();
                    $html .= '$table->integer("o")->comment("Rendezés")->unsigned();'."\n";
                }
                $table->softDeletes();
	            $table->timestamps();
                $table->index(['deleted_at']);
                $html .= ' $table->timestamps();'."\n".' $table->softDeletes();'."\n".'$table->index(["deleted_at"]);'."\n";

                $link = Storage::disk('modules')->files($table_data['model_name'].'/Database/Migrations')[1];
                $temp = Storage::disk('modules')->get($link);

                $temp = str_replace('$table->increments(\'id\');',$html,$temp);

                Storage::disk('modules')->put($link, $temp);
            });
        }
    }

    private function createModel($table_data)
    {
        Artisan::call('module:make-model', ['model' => ucfirst($table_data['model_name']), 'module' => ucfirst($table_data['model_name'])]);
        $model = $table_data['model_name'];
        $table = $table_data['table_name'];
        $model = ucfirst($model);
        $template = view('modelbuilder::template.model', ['model' => $model, 'table' => $table, 'data' => $table_data])->render();
        Storage::disk('modules')->put(ucfirst($table_data['model_name']) . '/Entities/' . $model . '.php', $template);
    }

    private function createController($table_data)
    {

        $model = ucfirst($table_data['model_name']);
        $root = strtolower($table_data['model_name']);
        $template = view('modelbuilder::template.controller', ['model' => $model, 'root' => $root, 'data' => $table_data])->render();
        Storage::disk('modules')->put(ucfirst($table_data['model_name']) . '/Http/Controllers/' . $model . 'Controller.php', $template);

    }

    private function createView($table_data)
    {
        $model = ucfirst($table_data['model_name']);
        $root = strtolower($table_data['model_name']);
        if ($table_data['edit']) {
            $template = view('modelbuilder::template.edit', [
                'model' => $model, 'root' => $root, 'data' => $table_data
            ])->render();
            Storage::disk('modules')->put(ucfirst($table_data['model_name']) . '/Resources/views/edit.blade.php', $template);
        }

        $template = view('modelbuilder::template.list', [
            'model' => $model, 'root' => $root, 'data' => $table_data
        ])->render();
        Storage::disk('modules')->put(ucfirst($table_data['model_name']) . '/Resources/views/list.blade.php', $template);

        Storage::disk('modules')->delete(ucfirst($table_data['model_name']) . '/Resources/views/index.blade.php');
        Storage::disk('modules')->delete(ucfirst($table_data['model_name']) . '/Resources/views/layouts/master.blade.php');
        Storage::disk('modules')->deleteDirectory(ucfirst($table_data['model_name']) . '/Resources/views/layouts');

    }

    private function createRoute($table_data)
    {
        $root = strtolower($table_data['model_name']);
        $controller = ucfirst($table_data['model_name']);
        $template = view('modelbuilder::template.route', ['root' => $root, 'controller' => $controller, 'data' => $table_data])->render();
        Storage::disk('modules')->put(ucfirst($table_data['model_name']) . '/Http/routes.php', $template);


    }

    private function createMenuItem($table_data)
    {

        $root = strtolower($table_data['model_name']);
        $new = "array(
			'icon' => 'fa " . $table_data['menu_icon'] . "',
			'title' => '" . $table_data['menu_name'] . "',
			'route' => array(
				array(
					'icon' => 'fa fa-columns',
					'title' => '" . $table_data['menu_name'] . "',
					'route' => 'admin_" . $root . "_list'
				),
				array(
					'icon' => 'fa fa-newspaper-o',
					'title' => 'Új " . $table_data['menu_name'] . "',
					'route' => 'admin_" . $root . "_edit'
				),
			)
		),\n";
        $actualRoutes = Storage::disk('config')->get('menu.php');
        $indexOfInsert = strpos($actualRoutes, '//BuilderMenu//');
        if ($indexOfInsert !== false) {
            $newRoutes = substr($actualRoutes, 0, $indexOfInsert) . $new . '//BuilderMenu//' . "\n" . substr($actualRoutes, $indexOfInsert + 15);
            Storage::disk('config')->put('menu.php', $newRoutes);
        }

    }
}
