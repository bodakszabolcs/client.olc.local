<?php

namespace App\Console\Commands;

use App\Mail\NewsletterEmail;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Modules\Company\Entities\Shop;
use Modules\Coupon\Entities\AutomaticCoupon;
use Modules\Coupon\Entities\Coupon;
use Modules\News\Entities\News;
use Modules\Newsletter\Entities\Newsletter;
use Modules\Newsletter\Entities\NewsletterList;
use Modules\Newsletter\Entities\NewsletterStatistic;
use Modules\Page\Entities\SiteBuilder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Modules\Purchase\Entities\Purchase;

class GenerateCoupon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:coupon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    private $shops=[];
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $shop = Shop::where('payed_package_id','>',1)->get();
        foreach ($shop as $s){
            $this->shops[$s->shop_id] = $s->shop_id;
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(empty($this->shops)){
            exit();
        }
        //userek akinek van vagy használt kupont az elmult 1 hétben;

        $rules = AutomaticCoupon::all();
        foreach ($rules as $rule){
            switch($rule->rule){
                case 1:{
                    $userHasCoupon = Coupon::where('updated_at','>',date('Y-m-d',strtotime('-7 day')))->where('expire','>',date('Y-m-d'))->groupBy('user_id')->get()->pluck('user_id','user_id')->toArray();
                    // Ha a havi vásárlások összege eléri a megadott összeget


                    $purchase = Purchase::where('handled','=',1)->where('created_at','>=',date('Y-m-d',strtotime('-30 day')))->groupBy('user_id')->having(DB::raw('sum(price)'),'>',$rule->value)->get();

                    foreach ($purchase as $p){

                        if(!in_array($p->user_id,$userHasCoupon)){
                            $this->generateCoupon($p->user_id,$rule->type,$rule->amount);
                        }
                    }


                }
                case 2:{

                    $userHasCoupon = Coupon::where('updated_at','>',date('Y-m-d',strtotime('-7 day')))->where('expire','>',date('Y-m-d'))->groupBy('user_id')->get()->pluck('user_id','user_id')->toArray();
                    // Ha a havi vásárlások száma több mint
                    $purchase = Purchase::where('handled','=',1)->where('created_at','>=',date('Y-m-d',strtotime('-30 day')))->groupBy('user_id')->having(DB::raw('count(*)'),'>',$rule->value)->get();
                    foreach ($purchase as $p){

                        if(!in_array($p->user_id,$userHasCoupon)){
                            $this->generateCoupon($p->user_id,$rule->type,$rule->amount);
                        }
                    }
                    break;
                }

                case 3:{
                    $userHasCoupon = Coupon::where('updated_at','>',date('Y-m-d',strtotime('-7 day')))->where('expire','>',date('Y-m-d'))->groupBy('user_id')->get()->pluck('user_id','user_id')->toArray();
                    // Ha már X napja nem vásárolt
                    $purchase = Purchase::select(DB::raw('MAX(created_at) as dare, purchases.*' ))->where('handled','=',1)->having(DB::raw('MAX(created_at)'),'<=',date('Y-m-d',strtotime("-$rule->value day")))->groupBy('user_id')->get();
                    foreach ($purchase as $p){

                        if(!in_array($p->user_id,$userHasCoupon)){
                            $this->generateCoupon($p->user_id,$rule->type,$rule->amount);
                        }
                    }
                    break;
                }
                case 4:{
                    $userHasCoupon = Coupon::where('updated_at','>',date('Y-m-d',strtotime('-7 day')))->where('expire','>',date('Y-m-d'))->groupBy('user_id')->get()->pluck('user_id','user_id')->toArray();
                    //Első várálás után
                    $purchase = Purchase::where('handled','=',1)->groupBy('user_id')->having(DB::raw('count(*) = 1'))->get();
                    foreach ($purchase as $p){

                        if(!in_array($p->user_id,$userHasCoupon)){
                            $this->generateCoupon($p->user_id,$rule->type,$rule->amount);
                        }
                    }
                    break;
                }
                default: break;

            }
        }


    }
    private function generateCoupon($user_id,$type,$amount){

        $coupon = new Coupon();
        $coupon->user_id = $user_id;
        $coupon->type= $type;
        $coupon->amount = $amount;
        $coupon->shop_id = $this->shops;
        $coupon->expire = date('Y-m-d',strtotime('+30 day'));
        $coupon->code = Coupon::randomCoupon();
        $coupon->save();
    }
	
}
