<!DOCTYPE html>
<html  lang="{{\App::getLocale()}}">
<head>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>


    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <link rel="stylesheet" href="/assets/frontend/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/frontend/datepicker/css/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="/assets/frontend/css/freelancer.min.css">
    <link rel="stylesheet" href="/assets/frontend/css/jquery-ui.css">
    <link rel="stylesheet" href="/assets/frontend/css/select2.min.css">
    <link rel="stylesheet" href="/assets/frontend/css/select2-bootstrap.css">
    <link rel="stylesheet" href="/assets/frontend/vendor/jquery-offcanvas/dist/css/bootstrap.offcanvas.min.css">
    @php
        $page_js = DB::table('lq_options')->where('id', 2)->first();
    @endphp
    {!! optional($page_js)->lq_value !!}
</head>
<body>
<div class="content">
@yield('content')
</div>

<script src='https://www.google.com/recaptcha/api.js?render=6LdV2XwUAAAAAHCT1-9czramNRqE2romWBJ7UjZ_'></script>
  <script>
  grecaptcha.ready(function() {
      grecaptcha.execute('6LdV2XwUAAAAAHCT1-9czramNRqE2romWBJ7UjZ_', {action: 'registration'}).then(function(token) {

          if($('form#form-regisztracio').length) {
              $('form#form-regisztracio').prepend('<input type="hidden" name="ga_token" value="' + token + '">');
              $('form#form-regisztracio').prepend('<input type="hidden" name="action" value="regiszter">');
          }
          if($('form#form-offer').length) {
              $('form#form-offer').prepend('<input type="hidden" name="ga_token" value="' + token + '">');
              $('form#form-offer').prepend('<input type="hidden" name="action" value="regiszter">');
          }
      });
  });
  </script>

<script src="/assets/frontend/vendor/jquery/jquery.min.js"></script>
<script src="/assets/frontend/vendor/bootstrap/js/bootstrap.js"></script>
<script src="/assets/frontend/js/freelancer.min.js"></script>
<script src="/assets/frontend/js/select2.min.js"></script>
<script src="/assets/frontend/datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/frontend/vendor/jquery-offcanvas/dist/js/bootstrap.offcanvas.min.js"></script>
<script src="/assets/frontend/js/main.js"></script>
</body>
</html>
