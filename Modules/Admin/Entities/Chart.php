<?php
	/**
	 * Created by PhpStorm.
	 * User: Bodák Szabolcs
	 * Date: 2018. 05. 08.
	 * Time: 10:19
	 */

	namespace Modules\Admin\Entities;
	class Chart
	{
		private static $colors =[
			
			0=>'#03A9F4',
			1=>'#E91E63',
			2=>'#b71c1c',
			3=>'#6a1b9a',
			4=>'#82b1ff',
			5=>'#f57f17',
			6=>'#f4511e',
			7=>'#9e9e9e',

		];

		public static function initChartJs()
		{
			return view('admin::chart/init');
		}
		
		/*
		 * $data_set=[
		 *  'label1'=>[
		 *          key1=> value1,
		 *          key2=> value2
		 *          .............
		 *      ],
		 *  'label2'=>[
		 *          key1=> value1,
		 *          key2=> value2
		 *          .............
		 *      ],
		 *      .................
		 * ]
		 *
		 * $type = line |bar|pie
		 * */

		public static  function drawLineChart($title,$unique_name,$data_set=[],$type='line',$sortLabel = true,$colors=[] )
		{
			$labels =[];
			foreach ($data_set as $k=>$label)
			{
				if(is_array($label))
				{
					foreach ($label as $k2 => $l)
					{
						$labels[$k2]=$k2;
					}
				}
				else
				{
					$labels[$label]=$label;
				}
			}

			//if($sortLabel)	sort($labels);
			if($type=='line') {
				return view('admin::chart.line', [
					'title' => $title, 'data' => $data_set, 'labels' => $labels, 'name' => $unique_name,'colors'=> $colors
				]);
			}
			if($type=='bar') {
				return view('admin::chart.bar', [
					'title' => $title, 'data' => $data_set, 'labels' => $labels, 'name' => $unique_name,'colors'=> $colors
				]);
			}
			if($type=='pie') {
				$labels =[];
				foreach ($data_set as $k=>$label)
				{

						$labels[$k]=$k;

				}
				return view('admin::chart.pie', [
					'title' => $title, 'data' => $data_set, 'labels' => $labels, 'name' => $unique_name,'colors'=> $colors
				]);
			}
			if($type=='doughnut') {
				$labels =[];
				foreach ($data_set as $k=>$label)
				{
					
					$labels[$k]=$k;
					
				}
				
				return view('admin::chart.doughnut', [
					'title' => $title, 'data' => $data_set, 'labels' => $labels, 'name' => $unique_name,'colors'=> $colors
				]);
			}
		}
		public static function rand_color($id=0) {
			
			return Chart::$colors[$id%sizeof(Chart::$colors)];
		}

	}