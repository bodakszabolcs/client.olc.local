<?php

	namespace Modules\PriceRange\Http\Controllers;

	use Modules\PriceRange\Entities\PriceRange;
	use App\Http\Controllers\Controller;
	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Session;
	use Validator;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\View;
	use Modules\Admin\Http\Controllers\AdminController;

	class PriceRangeController extends AdminController
	{
		public $niceNames = array(
			'price	=>	Havi kedvezmény határ',  
			'percent	=>	Kedvezmény',  

			);
		public function __construct()
		{
			View::share('sub_title', __('Kedvezmény sávok'));
			View::share('subtitle_link', route('admin_pricerange_list'));
		}

		public function PriceRangeList(Request $request)
		{
			$model = new PriceRange();
			$list = $model->searchInModel($request->input());
			$list = $list->orderBy('price', 'asc')->paginate(50)->withPath($request->path());
			View::share('title', __('Kedvezmény sávok lista'));

			return view('pricerange::list', [
				'model' => $list, 'filterHeader' => $model->drawSearchTableHeader($request)
			]);

		}

		public function Edit(Request $request, $id = 0,$tab='data')
		{
			View::share('title', __('Kedvezmény sávok szerkesztés'));

			$model = PriceRange::find($id);
			$errors = [];
			if (!is_null($model)) {

			} else {
				if ($id > 0) {
					return redirect(route('admin_pricerange_list'));
				}
				$model = new PriceRange();
			}
			if ($request->isMethod('post')) {

				$tab = $request->input('tab', 'data');
				switch ($tab) {
					case 'data': {
						$response = $this->validateData();
						break;
					}
					default:
						$response = $this->validateData();
						break;
				}
				if ($response['status'] == 'success') {
					$model->fill($request->all());
					$model->save();
						$request->session()->flash('success_message', __('A mentés sikeres'));
				} else {
					$request->session()->flash('error_message', __('Hiba a mentés során'));
					Session::put('field_errors', $response['cust_errors']);

					return redirect(route('admin_pricerange_edit', [$id, $tab]))->withErrors($response['validator'])->withInput();
				}
				if ($request->has('save_and_exit')) {
					return redirect()->intended(session()->get('redirect_url', route('admin_pricerange_list')));
				}
				if ($response['status'] == 'success') {

					return redirect()->intended(route('admin_pricerange_edit', ['id' => $model->id,'tab'=>$tab]));
				}
			}

			return view('pricerange::edit', ['model' => $model, 'id' => $id, 'tab' => $tab, 'errors' => $errors]);
		}
		public function validateData()
		{

			$rules = [
			'price'	=>	'required',  
			'percent'	=>	'required',  

			];
			$validator = Validator::make(Input::all(), $rules);
			$validator->setAttributeNames($this->niceNames);
			$not_valid = $validator->fails();
			if ($not_valid) {
				return ['status' => 'error', 'cust_errors' => $validator->messages()->toArray(), 'validator' => $validator];
			} else {
				return ['status' => 'success', 'message' => 'A mentés sikeres'];
			}
		}

		public function Delete(Request $request, $id)
		{
			try {
				if ($id == 0) {
					throw new ModelNotFoundException();
				}
				$model = PriceRange::findOrFail($id);
				$model->delete();
				$request->session()->flash('success_message', __('success_delete'));
			} catch (ModelNotFoundException $e) {
				$request->session()->flash('error_message', __('Az elem nem törölhető'));
			}
			return redirect()->intended(route('admin_pricerange_list'));
		}


	}