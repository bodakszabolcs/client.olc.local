(function () {

        var selected_content = 0;


        CKEDITOR.plugins.add('instagram', {

            init: function (editor) {

                editor.addCommand('instagram', new CKEDITOR.dialogCommand('instagram', {
                    allowedContent: true
                }));

                editor.ui.addButton('Instagram', {
                    label: 'Instagram begyazas',
                    command: 'instagram',
                    toolbar: 'insert',
                    icon: this.path + 'icon.png'
                });

                CKEDITOR.dialog.add('instagram', function (instance) {
                    var post, height;

                    return {
                        title: 'Instagram beágyazás',
                        minWidth: 510,
                        minHeight: 200,
                        contents:
                            [{
                                id: 'instagramEmbedPlugin',
                                expand: true,
                                elements:
                                    [
                                        {
                                            type : 'textarea',
                                            id : 'post_url',
                                            width : '400px',
                                            height: '400px',
                                            label : 'Instagram beágyazó kód',
                                            validate : function () {
                                                if (this.getValue()) {
                                                    post = this.getValue();
                                                }
                                                else {
                                                    alert('Kérem adja meg a instagram beágyazás kódot!');
                                                    return false;
                                                }
                                            }
                                        }
                                    ]

                            }],
                        onOk: function () {
                            var instance = this.getParentEditor();
                            post = this.getValueOf('instagramEmbedPlugin', 'post_url');


                            post = post.replace(/&/g, '&amp;');
                            post = post.trim();
                            post = '<div class="embeded">'+post+'</div>';

                            var element = CKEDITOR.dom.element.createFromHtml(post);
                            instance.insertElement(element);


                        }
                    };
                });
            }
        });
    }

)();