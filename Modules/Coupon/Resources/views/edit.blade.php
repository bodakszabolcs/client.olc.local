@extends('admin::layouts.layout')
@section('content')
    <?php
    $errors = Session::get('field_errors');
    Session::forget('field_errors');
    ?>
    <div class='row'>
        <div class='col-md-12'>
            <div class='m-portlet'>
                <div class='m-portlet__body'>
                    <input type='hidden' name='tab' value='data'>
                    <div class='row'>
                        <div class='col-md-12'>
                            {!!  \App\Http\Helper::formOpen('CouponForm','post',NULL,['class'=>'form '])  !!}
                            <div class='m-portlet__head'>
                                <div class='m-portlet__body'>
                                    <!--begin::Widget 29-->
                                    <div class='m-widget29'>
                                        <div class='m-widget_content'>
                                            <div class='m-widget_content-items'>
                                                <div class='row'>
                                                    <h5 class="col-md-12"><br>Kupon adatok
                                                        <br>
                                                        <br>
                                                    </h5>
                                                    <div class="col-md-6">
                                                        {!! \App\Http\Helper::input(trans('Kód'),'code',old('code',$model->code),array('class'=>'form-control','disabled'=>'disabled'),$errors) !!}
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div><lable>Elfogadóhelyek</lable></div>
                                                        @php
                                                            $shops = \Modules\Company\Entities\Shop::where('payed_package_id','>',1)->get();

                                                        @endphp
                                                        @foreach($shops as $s)
                                                            <label class="m-checkbox m-checkbox--square">
                                                                <input id="checkbox{{$loop->iteration}}" type="checkbox" class="chkacl"
                                                                       name="shop_id[{{$s->shop_id}}]" value="{{$s->shop_id}}"
                                                                       @if (array_get($model->shop_id,$s->shop_id) || empty($model->id)) checked="checked" @endif>
                                                                {{$s->name}}
                                                                <span></span>
                                                            </label><br>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! \App\Http\Helper::select(trans('Típus'),'type',[''=>'Kérem válasszon']+[0=>'Fix összeg',1=>'Százalékos kedvezmény'], old('type',$model->type),array('class'=>'form-control'),$errors) !!}
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! \App\Http\Helper::input(trans('Kedvezmény értéke'),'amount',old('amount',$model->amount),array('class'=>'form-control'),$errors) !!}
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! \App\Http\Helper::select(trans('Felhasználó'),'user_id',\Modules\Purchase\Entities\Purchase::groupBy('user_id')->orderBy('user_name')->get()->pluck('user_name','user_id')->toArray() ,old('user_id',$model->user_id),array('class'=>'form-control select2'),$errors) !!}
                                                    </div>
                                                    <div class="col-md-6">
                                                        {!! \App\Http\Helper::input(trans('Érvényesség'),'expire',old('expire',$model->expire),array('class'=>'form-control datepicker date-picker'),$errors) !!}
                                                    </div>
                                                                                                    </div>
                                            </div>
                                        </div>
                                        {!! \App\Http\Helper::formButtons('admin_coupon_list') !!}
                                    </div>
                                    {!! \App\Http\Helper::formClose() !!}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



@endsection 