-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2017. Ápr 01. 12:03
-- Kiszolgáló verziója: 10.1.16-MariaDB
-- PHP verzió: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `mobilestock`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tables`
--

CREATE TABLE `tables` (
  `id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `table_name` text NOT NULL,
  `model` text NOT NULL,
  `data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `tables`
--

INSERT INTO `tables` (`id`, `shop_id`, `user_id`, `table_name`, `model`, `data`) VALUES
(1, 9, 1, '', 'Service', 'a:1:{s:7:"service";a:12:{s:6:"number";a:2:{s:5:"label";s:8:"Sorszám";s:7:"checked";b:1;}s:10:"product_id";a:2:{s:5:"label";s:6:"Saját";s:7:"checked";b:1;}s:7:"service";a:2:{s:5:"label";s:18:"Szervizelés helye";s:7:"checked";b:1;}s:12:"product_name";a:2:{s:5:"label";s:7:"Telefon";s:7:"checked";b:1;}s:4:"imei";a:2:{s:5:"label";s:4:"IMEI";s:7:"checked";b:1;}s:5:"error";a:2:{s:5:"label";s:4:"Hiba";s:7:"checked";b:1;}s:11:"income_date";a:2:{s:5:"label";s:7:"Bejött";s:7:"checked";b:1;}s:9:"sent_date";a:2:{s:5:"label";s:9:"Elküldve";s:7:"checked";b:1;}s:9:"back_date";a:2:{s:5:"label";s:11:"Visszajött";s:7:"checked";b:1;}s:17:"change_product_id";a:2:{s:5:"label";s:12:"Cseretelefon";s:7:"checked";b:1;}s:5:"price";a:2:{s:5:"label";s:16:"Szervizköltség";s:7:"checked";b:1;}s:13:"price_service";a:2:{s:5:"label";s:14:"Javítás ára";s:7:"checked";b:1;}}}'),
(2, 9, 1, '', 'Accessories', 'a:1:{s:11:"accessories";a:9:{s:3:"sku";a:2:{s:5:"label";s:9:"Cikkszám";s:7:"checked";b:1;}s:4:"name";a:2:{s:5:"label";s:11:"Megnevezés";s:7:"checked";b:1;}s:8:"price_in";a:2:{s:5:"label";s:10:"Beszer ár";s:7:"checked";b:1;}s:9:"price_out";a:2:{s:5:"label";s:12:"Eladási ár";s:7:"checked";b:1;}s:13:"price_out_web";a:2:{s:5:"label";s:7:"Web ár";s:7:"checked";b:0;}s:8:"category";a:2:{s:5:"label";s:10:"Kategória";s:7:"checked";b:0;}s:3:"min";a:2:{s:5:"label";s:7:"Minimum";s:7:"checked";b:0;}s:6:"amount";a:2:{s:5:"label";s:8:"Készlet";s:7:"checked";b:1;}s:3:"max";a:2:{s:5:"label";s:7:"Maximum";s:7:"checked";b:0;}}}');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `tables`
--
ALTER TABLE `tables`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `tables`
--
ALTER TABLE `tables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
