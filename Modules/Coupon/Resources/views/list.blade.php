@extends('admin::layouts.layout')
@section('content')
	@php
		$type=[
			0=>'Fix összeg',
			1=>'Százalékos kedvezmény'
		];
	@endphp
	<div class = 'row'>
        <div class = 'col-12'>
            <div class = 'm-portlet'>
                <div class = 'm-portlet__head'>
                    <div class = 'head-btn'>
            {!! \App\Http\Helper::text_button(__('Új Hozzáadás'),array('route_name' => 'admin_coupon_edit', 'params' => array()),'fa-plus', array('class'=>'btn m-btn--square btn-primary')) !!}
          </div>
                </div>
                <div class = 'm-portlet__body'>
                {!! $filterHeader !!}
					<div class = 'table-responsive'>
                        <div class="alert alert-danger">
                            Figyelem a jóváhagyott kuponok nem törölhetők és nem módosíthatók! Kérlek ellenőrízd mielőtt jóváhagyod!
                        </div>
                        <table class = 'table table-bordered table-hover mb-150'>
                                <thead>
                                <tr>
                                    <th>{{__('Felhasználó')}}</th>
                                    <th>{{__('Kód')}}</th>
                                    <th>{{__('Típus')}}</th>
                                    <th>{{__('Mennyiség')}}</th>
                                    <th>{{__('Érvényesség')}}</th>
                                    <th>{{__('Műveletek')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($model as $m)

									<tr>
                                        <td>{{$m->user_name}}</td>
                                        <td>{{$m->code}}</td>
                                        <td>{{array_get($type,$m->type)}}</td>
                                        <td>{{$m->amount}}@if($m->type == 1) % @else Ft @endif</td>
                                        <td>{{$m->expire}}</td>
                                        <td>
                                            @if(!$m->approved)
                                                {!! \App\Http\Helper::text_button(__('Jóváhagyás'),array('route_name'=>'admin_coupon_approve', 'params' => array($m->id)),'fa-check', array('class'=>'dropdown-item')) !!}
                                                {!! \App\Http\Helper::text_button(__('Szerkesztés'),array('route_name'=>'admin_coupon_edit', 'params' => array($m->id)),'fa-edit', array('class'=>'dropdown-item')) !!}
											    {!! \App\Http\Helper::text_button(__('Törlés'),array('route_name'=>'admin_coupon_delete', 'params' => array($m->id)),'fa-trash', array('class'=>'dropdown-item confirm')) !!}
                                            @endif
                                        </td>
                                    </tr>
								@endforeach
                                </tbody>
                            </table>
                        </div>
					{!! $model->appends($_GET)->links() !!}
                        </div>
                </div>
            </div>
        
    </div>
@endsection 