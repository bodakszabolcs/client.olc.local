<div class="row">
	@if(isset($customer['data']))
<div class="col-xl-3 col-lg-4">
								<div class="m-portlet m-portlet--full-height  ">
									<div class="m-portlet__body">
										<div class="m-card-profile">
											<div class="m-card-profile__pic">
												<div class="m-card-profile__pic-wrapper">
													<img src="{{$customer['data']['avatar']}}" alt="">
												</div>
											</div>
											<div class="m-card-profile__details">
												<span class="m-card-profile__name">
													{{$customer['data']['user_name']}}
												</span>
											</div>
										</div>
										<div class="m-portlet__body-separator"></div>
										<div class="m-widget1 m-widget1--paddingless" style="padding:10px">
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col">
														<h3 class="m-widget1__title">
															Utolsó 1 hónap
														</h3>
														<span class="m-widget1__desc">

														</span>
													</div>
													<div class="col m--align-right">
														<span class="m-widget1__number m--font-brand">
															{{\Modules\Purchase\Entities\Purchase::getLast30Day($customer['data']['user_id'])}} Ft
														</span>
													</div>
												</div>
											</div>
											<div class="m-widget1__item">
												<div class="row m-row--no-padding align-items-center">
													<div class="col">
														<h3 class="m-widget1__title">
															Eddigi vásárlás
														</h3>
														<span class="m-widget1__desc">

														</span>
													</div>
													<div class="col m--align-right">
														<span class="m-widget1__number m--font-danger">
															{{\Modules\Purchase\Entities\Purchase::getAllPurchase($customer['data']['user_id'])}} Ft
														</span>
													</div>
												</div>
											</div>
											</div>
										</div>
									</div>
								</div>
<div class="col-xl-9 col-lg-8">
								<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
									<div class="m-portlet__head">

										<div class="m-portlet__head-tools">

										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item m-portlet__nav-item--last">
													<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
														<a href="#" class="m-portlet__nav-link btn btn-lg btn-success  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" id="save">
															<i class="la la-save"></i>

														</a>
														<a href="#" class="m-portlet__nav-link btn btn-lg btn-danger  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle" id="delete">
															<i class="la la-trash"></i>

														</a>
													</div>
												</li>
											</ul>
										</div>
									</div>
									<div class="tab-content">
										<div class="m-form m-form--fit ">
											<br>
											<br>
										<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-md-3 col-form-label text-success">
															Vásárlás összege
														</label>
														<div class="col-md-7">
															<div class="input-group">
																<input type="number" name="price" class="form-control m-input" placeholder="Vásárlás összege" aria-describedby="basic-addon2">
																<div class="input-group-append">
																	<span class="input-group-text" id="basic-addon2">
																		Ft
																	</span>
																</div>
															</div>
															<span class="info-box hide text-danger price-error" style="display: none">
																Kötelező árat megadni!
															</span>

														</div>
													</div>
											@php
												$coupon = \Modules\Coupon\Entities\Coupon::getCoupon(null,$customer['data']['coupon'])
											@endphp
											@if($coupon)
													<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-md-3 col-form-label">
															Kupon kedvezmény
														</label>

														<div class="col-md-7">
															<div class="input-group">
																<input type="text" name="coupon_v" disabled class="form-control m-input" aria-describedby="basic-addon2" value="{{$coupon->code}} - {{$coupon->amount}} {{($coupon->type==0)?' Ft':'%'}}">
																<input type="hidden" name="coupon_type" value="{{$coupon->type}}" >
																<input type="hidden" name="coupon_value" value="{{$coupon->amount}}" >
																<input type="hidden" name="coupon" value="{{$customer['data']['coupon']}}" >
																<div class="input-group-append">
																	<span class="input-group-text" id="basic-addon2">
																		Kupon kód
																	</span>
																</div>
															</div>
														</div>
													</div>
											@endif
											<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-md-3 col-form-label">
															Hűség kedvezmény
														</label>
														<div class="col-md-7">
															<div class="input-group">
																<input type="number" name="loyalty" class="form-control m-input"  aria-describedby="basic-addon2" value="{{\Modules\Purchase\Entities\Purchase::calculatePriceRange($customer['data']['user_id'])}}" disabled>
																<div class="input-group-append">
																	<span class="input-group-text" id="basic-addon2">
																		%
																	</span>
																</div>
															</div>

														</div>
													</div>
											<hr/>
											<div class="form-group m-form__group row">
														<label for="example-text-input" class="col-md-3 col-form-label  text-danger">
															Fizetendő végösszeg
														</label>
														<div class="col-md-7">
															 class="col-md-7">
															<div class="input-group">
																<input type="number" readonly name="sum" class="form-control m-input" placeholder="Végösszeg" aria-describedby="basic-addon2">
																<div class="input-group-append">
																	<span class="input-group-text" id="basic-addon2">
																		Ft
																	</span>
																</div>
															</div>

														</div>
													</div>

									</div>
										</div>
								</div>
							</div>
	@else
		<div class="col-md-12 text-center">
												<h3 class="bg-success jumbotron text-center">Jelenleg nincs várakozó vásárló</h3>
		</div>
	@endif
</div>
@if(isset($customer['data']))
<script type="text/javascript">
	$('[name="price"]').keyup(function () {
        $('.price-error').css('display','none');
	    var price = $(this).val();
	    var loyalty =$('[name="loyalty"]').val();
	    price = price - (price*(loyalty/100));
	    var coupon = $('[name="coupon_value"]').val();
	    if(coupon){
	        var type = $('[name="coupon_type"]').val();
			if(type == 1){
                price = price - (price*(coupon/100));
			}
			else{
			    price = price - coupon;
			}
		}
	    price = parseInt(price);
	    price = Math.round(price/10)*10;
	    if(price >0) {
            $('[name="sum"]').val(price);
        }else{
            $('[name="sum"]').val(0);
		}
    });
	$('#save').click(function(){
        var price = $('[name="price"]').val();
        if(price <=0){
            $('.price-error').css('display','block');
		}
        else{
            $.post('{{route('admin_purchase_save')}}',
				{
				    price:price,
					discount_price:  $('[name="sum"]').val(),
				    id: '{{$customer['data']['id']}}'
				},function (data) {
                	$('.customer-profile').html('');
            })
		}
	});
	$('#delete').click(function(){
		var price = $('[name="price"]').val();
		if(price <=0){
			$('.price-error').css('display','block');
		}
		else{
			$.post('{{route('admin_purchase_delete')}}',
					{

						id: '{{$customer['data']['id']}}'
					},function (data) {
						$('.customer-profile').html('');
					})
		}
	});
</script>
@endif
