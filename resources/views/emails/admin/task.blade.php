
	{{__('Feladatod érkezett')}}

		<table>
			<tr>
				<td>Küldő</td>
				<td>{{$sender->name}}</td>
			<tr>
			<tr>
				<td>Leírás</td>
				<td>{{$task->description}}</td>
			<tr>
			<tr>
				<td>Prioritás</td>
				<td>{{array_get(\Modules\Task\Entities\Task::$priority,$task->priority)}}</td>
			<tr>
			<tr>
				<td>Határidő</td>
				<td>{{$task->deadline}}</td>
			<tr>
			<tr>
				<td>Dátum</td>
				<td>{{date('Y-m-d')}}</td>
			<tr>
			<tr>
				<td>Link</td>
				<td><a href="{{route('admin_task_watch',['task_id'=>$task->id])}}">Ugrás a feladathoz</a></td>
			<tr>
		</table>
	<br>

	{{__('Üdvözlettel')}},
	{{ config('app.name') }}
