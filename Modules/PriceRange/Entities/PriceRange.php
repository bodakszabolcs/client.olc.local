<?php

	namespace Modules\PriceRange\Entities;

	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Database\Eloquent\SoftDeletes;
	use Mockery\Exception;
	use App\LiquidModel;
	use Sunra\PhpSimple\HtmlDomParser;
	use GeneaLabs\LaravelModelCaching\Traits\Cachable;

	class PriceRange extends LiquidModel
	{
		use SoftDeletes, Cachable;

		protected $table = 'price_ranges';

		protected $dates = ['deleted_at', 'created_at', 'updated_at'];
		/**
		 * The attributes that are mass assignable.
	 	*
		 * @var array
	 	*/
		protected $fillable = [
			'price', // 
			'percent', //Kedvezmény 
		];

		public $searchColumns = [
			'id' => 'text',
			];

		public $searchColumnLabels = [
			'id' => 'ID',
			];
		protected $casts = [
			];
	}
?>