<?php

	namespace Modules\Company\Entities;

	use App\Http\Helper;
	use GuzzleHttp\Client;
	use GuzzleHttp\RequestOptions;

	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Database\Eloquent\SoftDeletes;
	use Illuminate\Support\Facades\Log;
	use Mockery\Exception;
	use App\LiquidModel;
	use Sunra\PhpSimple\HtmlDomParser;
	use GeneaLabs\LaravelModelCaching\Traits\Cachable;

	class Shop extends LiquidModel
	{
		use SoftDeletes, Cachable;

		protected $table = 'shops';

		protected $dates = ['deleted_at', 'created_at', 'updated_at'];
		/**
		 * The attributes that are mass assignable.
	 	*
		 * @var array
	 	*/
		public static function getList(){
			return Shop::all()->pluck('name','shop_id')->toArray();
		}
		protected $fillable = [
			'name', //Név 
			'email', //Email 
			'phone', //Telefonszám 
			'address', //Cím 
			'longitude', //Szélességi fok 
			'latitude', //Hosszúsági fok 
			'webpage', //Weboldal 
			'facebook', //Facebook 
			'instragram', //Instagram 
			'lead', //Bemutatkozás 
			'cover', //Boritó
			'package_id'// Csomag azonosító
		];

		public $searchColumns = [
			'id' => 'text',
			'company_id'	=>	'text', 
			'name'	=>	'text', 
			'email'	=>	'text', 
			'phone'	=>	'text', 
			'address'	=>	'text', 
			'longitude'	=>	'text', 
			'latitude'	=>	'text', 
			'instragram'	=>	'text', 
			];

		public $searchColumnLabels = [
			'id' => 'ID',
			'company_id'	=>	'Cég', 
			'name'	=>	'Név', 
			'email'	=>	'Email', 
			'phone'	=>	'Telefonszám', 
			'address'	=>	'Cím', 
			'longitude'	=>	'Szélességi fok', 
			'latitude'	=>	'Hosszúsági fok', 
			'instragram'	=>	'Instagram', 
			];

		public static  function getShops(){
			return Shop::all()->pluck('name','id')->toArray();
		}
		public static  function getShopsShopId(){
			return Shop::all()->pluck('name','shop_id')->toArray();
		}
		public function opening(){
			return $this->hasMany('Modules\Company\Entities\ShopOpening');
		}
		public static function updateShopData($data){
			$client = new Client();
			try{
				$postData = Helper::encodeData($data,'shop');
				$response = $client->post( env('SERVER_URL') . '/api/update-shop', [
						RequestOptions::JSON =>
							['data' => $postData]
				],['Content-Type' => 'application/json']);

			}catch (\Exception $e){
				Log::info($e->getMessage());

			}
		}
	}
?>
