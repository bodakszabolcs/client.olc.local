<?php namespace Recze\Szamlazz;

use Illuminate\Support\ServiceProvider;

class SzamlazzServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        @mkdir(public_path().'/packages/recze/szamlaagent');
        @mkdir(public_path().'/packages/recze/szamlaagent/pdf');
        @mkdir(public_path().'/packages/recze/szamlaagent/xml');

        $this->publishes(
            [
                __DIR__.'/../config/invoiceagent.php' => config_path('invoiceagent.php'),
                __DIR__.'/../resources/cacert.pem' => public_path('packages/recze/szamlaagent/cacert.pem')
            ]
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }

}