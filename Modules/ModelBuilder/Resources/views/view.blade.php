@extends('admin::layouts.layout')
@section('content')
<?php
$errors= Session::get('field_errors');
$cust_errors= Session::get('create_errors');

Session::forget('field_errors');
Session::forget('create_errors');
?>
<div class="row">
        <div class="col-12">
            <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                            role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1"
                                   role="tab">
                                    <i class="flaticon-share m--hide"></i>
                                    {!! trans('Model beállítások') !!}
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="tab-content">
                    <div class="tab-pane active" id="m_user_profile_tab_1">
                @if(!is_null($cust_errors))
                    <div class="alert alert-danger">
                    @foreach($cust_errors as $v)
                        {{$v}}<br>
                    @endforeach
                    </div>
                @endif
                <div class="m-portlet__body">
                    {!! App\Http\Helper::formOpen('builderForm','POST') !!}
                    {!! App\Http\Helper::input('Model neve','model_name',old('model_name'),array(),$errors) !!}
					{!! App\Http\Helper::input('Tábla neve','table_name',old('table_name'),array(),$errors) !!}
					{!! App\Http\Helper::input('Menüpont neve','menu_name',old('menu_name'),array(),$errors) !!}
					{!! App\Http\Helper::input('Menüpont icon','menu_icon',old('menu_icon'),array(),$errors,'<a href="https://fontawesome.com/icons?d=gallery" target="_blank">FontAwesome 5</a> | <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank">FontAwesome 4.7</a>') !!}
                    <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">{{__('Műveletek')}}</label>
                    </div>
                    <div class="col-md-2">
                        {!! App\Http\Helper::select('Szerkesztés','edit',[1=>'IGEN',0=>'NEM'],old('edit',1),array()) !!}
                    </div>
                    <div class="col-md-2">
                        {!! App\Http\Helper::select('Törlés','delete',[1=>'IGEN',0=>'NEM'],old('delete',1),array()) !!}

                    </div>

                    <div class="col-md-2">
                        {!! App\Http\Helper::select('Rendezés','order',[1=>'IGEN',0=>'NEM'],old('order',0),array()) !!}

                    </div>
                    <div class="col-md-2">
                        {!! App\Http\Helper::select('Export','export',[1=>'IGEN',0=>'NEM'],old('export',1),array()) !!}
                    </div>
                    </div>
					{!! trans('Tábla/Model beállítások') !!}
                    <table class="table table-bordered table-hover mb-150" id="builderTable">
                         <tbody>
                         @if(is_array(old('column_name')))
                            @foreach(old('column_name') as $k =>$v)
                                <tr>
                                 <td>
                                     <table class="table">
                                          <tr>
                                            <td>{!! App\Http\Helper::input('Oszlop neve','column_name[]',old('column_name.'.$k),array(),((isset($errors['column_name.'.$k]))?['column_name[]'=>$errors['column_name.'.$k]]:[])) !!}</td>
                                            <td>{!! App\Http\Helper::input('Megjegyzés','column_comment[]',old('column_comment.'.$k),array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Típus','column_type[]',$controller->types,old('column_type.'.$k),array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Fillable?','column_fillable[]',[1=>'IGEN',0=>'NEM'],old('column_fillable.'.$k),array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Cast','column_cast[]',[1=>'IGEN',0=>'NEM'],old('column_cast.'.$k),array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Getter','column_getter[]',[1=>'IGEN',0=>'NEM'],old('column_getter.'.$k),array()) !!}</td>
                                            <td class="nowrap"><br/><button type="button" class="btn btn-danger deleteRow"><i class="fa fa-trash"></i> </button></td>
                                          </tr>
                                          <tr>
                                            <td>{!! App\Http\Helper::input('Label','column_label[]',old('column_label.'.$k),array(),((isset($errors['column_label.'.$k]))?['column_label[]'=>$errors['column_label.'.$k]]:[])) !!}</td>
                                            <td>{!! App\Http\Helper::select('Input','column_input[]',array('input'=>'Szöveg','date'=>'Dátum','select'=>'Select','checkbox'=>'checkbox','textarea'=>'Textarea','ckeditor'=>'Ckeditor','file'=>'Fájl'),old('column_input.'.$k)) !!}</td>
                                            <td>{!! App\Http\Helper::input('Érték select esetében','column_value[]',old('column_value.'.$k),array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Lista keresés','column_search[]',[1=>'IGEN',0=>'NEM'],old('column_name.'.$k),array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Keresés mező','column_search_input[]',['text'=>'text','interval'=>'interval','date_interval'=>'date_interval','select'=>'select'],old('column_search_input.'.$k),array()) !!}</td>
                                            <td>{!! App\Http\Helper::input('Select értékek','column_search_value[]',old('column_search_value.'.$k),array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Kötelező','required[]',[1=>'IGEN',0=>'NEM'],old('required.'.$k,1),array()) !!}</td>

                                            </tr>
                                    </table>
                                </td>
                           </tr>

                            @endforeach
                         @else
                            <tr>
                                 <td>
                                     <table class="table">
                                          <tr>
                                            <td>{!! App\Http\Helper::input('Oszlop neve','column_name[]','',array()) !!}</td>
                                            <td>{!! App\Http\Helper::input('Megjegyzés','column_comment[]','',array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Típus','column_type[]',$controller->types,null,array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Fillable?','column_fillable[]',[1=>'IGEN',0=>'NEM'],1,array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Cast','column_cast[]',[1=>'IGEN',0=>'NEM'],null,array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Getter','column_getter[]',[1=>'IGEN',0=>'NEM'],null,array()) !!}</td>
                                            <td class="nowrap"><br/><button type="button" class="btn btn-danger deleteRow"><i class="fa fa-trash"></i> </button></td>
                                          </tr>
                                          <tr>
                                            <td>{!! App\Http\Helper::input('Label','column_label[]','',array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Input','column_input[]',array('input'=>'Szöveg','date'=>'Dátum','select'=>'Select','checkbox'=>'checkbox','textarea'=>'Textarea','ckeditor'=>'Ckeditor','file'=>'Fájl'),'') !!}</td>
                                            <td>{!! App\Http\Helper::input('Érték select esetében','column_value[]','',array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Lista keresés','column_search[]',[1=>'IGEN',0=>'NEM'],null,array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Keresés mező','column_search_input[]',['text'=>'text','interval'=>'interval','date_interval'=>'date_interval','select'=>'select'],null,array()) !!}</td>
                                            <td>{!! App\Http\Helper::input('Select értékek','column_search_value[]',null,array()) !!}</td>
                                              <td>{!! App\Http\Helper::select('Kötelező','required[]',[1=>'IGEN',0=>'NEM'],1,array()) !!}</td>
                                            </tr>
                                    </table>
                                </td>
                           </tr>
                           @endif;
                         </tbody>
                         <tfoot>
                           <tr>
                                 <td><button type="button" class="btn btn-success pull-right addRow">{{trans('Új oszlop hozzáadása')}} <i class="fa fa-edit"></i> </button></td>
                            </tr>
                         </tfoot>
                    </table>
                    <div class="text-right">

					<button class="btn btn-success btn-cons" name="save_and_exit" type="submit"><i class="fa fa-floppy-o"></i> Mentés</button>
				</div>
                {!!   App\Http\Helper::formClose() !!}

                </div>
                </div>
            </div>
        </div>
    </div>
</div>
	<div id="templateRow" class="hide">
                    <table class="table">
                                          <tr>
                                            <td>{!! App\Http\Helper::input('Oszlop neve','column_name[]','',array()) !!}</td>
                                            <td>{!! App\Http\Helper::input('Megjegyzés','column_comment[]','',array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Típus','column_type[]',$controller->types,null,array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Fillable?','column_fillable[]',[1=>'IGEN',0=>'NEM'],1,array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Cast','column_cast[]',[1=>'IGEN',0=>'NEM'],null,array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Getter','column_getter[]',[1=>'IGEN',0=>'NEM'],null,array()) !!}</td>
                                            <td class="nowrap"><br/><button class="btn btn-danger deleteRow"><i class="fa fa-trash"></i> </button></td>
                                          </tr>
                                          <tr>
                                            <td>{!! App\Http\Helper::input('Label','column_label[]','',array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Input','column_input[]',array('input'=>'Szöveg','date'=>'Dátum','select'=>'Select','checkbox'=>'checkbox','textarea'=>'Textarea','ckeditor'=>'Ckeditor','file'=>'Fájl'),'') !!}</td>
                                            <td>{!! App\Http\Helper::input('Érték select esetében','column_value[]','',array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Lista keresés','column_search[]',[1=>'IGEN',0=>'NEM'],null,array()) !!}</td>
                                            <td>{!! App\Http\Helper::select('Keresés mező','column_search_input[]',['text'=>'text','interval'=>'interval','date_interval'=>'date_interval','select'=>'select'],null,array()) !!}</td>
                                            <td>{!! App\Http\Helper::input('Select értékek','column_search_value[]',null,array()) !!}</td>
                                             <td>{!! App\Http\Helper::select('Kötelező','required[]',[1=>'IGEN',0=>'NEM'],1,array()) !!}</td>
                                       </tr>
                                    </table>
    </div>
    <style>
        .hide{
            display:none;
        }
    </style>
@endsection