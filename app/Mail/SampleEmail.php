<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\User\Entities\User;

class SampleEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $to,$title, $subject, $reply, $sender,$content,$file;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject,$sender,$reply,$to,$content,$attachments=[])
    {
        $this->to= $to;
        $this->sender = $sender;
        $this->reply = $reply;
        $this->subject = $subject;
        $this->content = $content;
        $this->files = $attachments;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $markdown= $this->markdown('emails.admin.sample',['subject'=>$this->subject,'content'=>$this->content])
	        ->from($this->sender->email,$this->sender->name)
            ->subject($this->subject)
            ->to($this->to->email,$this->to->name)
            ->replyTo($this->reply->email,$this->reply->name);
		try {
			foreach ($this->files as $f) {

				$markdown = $markdown->attach($f->getRealPath(), [
					'as' => $f->getClientOriginalName(), 'mime' => $f->getClientMimeType()
				]);
			}
		}catch(\Exception $e)
		{

		}

        return $markdown;

    }
}
