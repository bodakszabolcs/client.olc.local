<?php

namespace  Modules\Translation\Console;

use Illuminate\Console\Command;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Mockery\Exception;

class Translation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translate:files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Collect translatable string from project files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
		Log::info('Translation start'.date('Y-m-d H:i:s'));
		$directories = config('translation.dir');
		$config = config('translation.config');

		$transData=[];
		foreach ($config as $file => $regexp)
		{
			try
			{
				$file_content = File::get(base_path($file));
				if (preg_match_all('/'.$regexp['prefix'].'(.*?)'.$regexp['sufix'].'/s', $file_content, $matches)) {

					foreach( $matches[1] as $m)
					{


						$m=str_replace(["'",'"'], ["\"","\""], $m);
						if(strpos($m,'$')===false) {
							$string = explode(',', $m);
							$transData[$string[0]] = $string[0];
						}


					}
				}
			}
			catch (\Exception $e)
			{
				Log::info('A fájl nem található: '.$file);
			}

		}
		foreach($directories as $dir)
		{
			$files = File::allFiles(base_path($dir));
			
			foreach ($files as $file)
			{

				$file_content=file_get_contents((string)$file);

				if (preg_match_all('/__\(\'(.*?)\'\)/s', $file_content, $matches)) {
					foreach( $matches[1] as $m)
					{
						
						
						$m=str_replace(["'",'"'], ["\"","\""], $m);
						if(strpos($m,'$')===false) {
							//$string = explode(',', $m);
       
							//$transData[$string[0]] = $string[0];
							$transData[$m] = $m;
						}
						

					}
				}
				if (preg_match_all('/__\("(.*?)"\)/s', $file_content, $matches)) {
					foreach( $matches[1] as $m)
					{
						$m=str_replace(["'",'"'], ["\"","\""], $m);
						if(strpos($m,'$')===false) {
                            //$string = explode(',', $m);
                            
                            //$transData[$string[0]] = $string[0];
                            $transData[$m] = $m;
						}
						
					}
				}
				//var_dump($file_content);
			}
			
		}
		$controller = null;
		
		foreach( \Illuminate\Support\Facades\Route::getRoutes() as $rt)
		{
			if (!isset($rt->action['middleware']) || !is_array($rt->action['middleware']) || !in_array('acl', $rt->action['middleware'])) continue;
			if (!isset($rt->action['controller'])) continue;
			try {
				$helper = (explode("@", $rt->action['controller']))[1];
			} catch (\Exception $e)
			{
				continue;
			}
			$name = (!empty($rt->action['as'])) ? $rt->action['as'] : $helper;
			$transData[$name]=$name;
			$ctrl = explode("\\", explode("@", $rt->action['controller'])[0]);
			$ctrl = end($ctrl);
			$transData[$ctrl] = $ctrl;
		}
		
		//var_dump($transData);
		$langs = config('app.languages');
		foreach($langs as $l=> $lang)
		{
			if(file_exists(base_path('resources/lang/'.$l.'.json')))
				{	$data = file_get_contents(base_path('resources/lang/'.$l.'.json'));
					
					$avilable= json_decode($data,true);
					
					
					if(is_array($avilable)) {

						foreach ($avilable as $k =>$v)
						{
							if(isset($transData[$k]))
							{
								 $transData[$k]=$v;
								
							}

						}
						$new_array = array_merge($avilable, $transData);
					}
					else
					{
						$new_array =$transData;
					}



				}
				else
				{
					if(!File::exists(base_path('resources/lang/'))) {
						File::makeDirectory(base_path('resources/lang/', $mode = 0777, true, true));
					}
					$new_array =$transData;
				}

				
				$temp=json_encode($new_array,JSON_UNESCAPED_UNICODE);
				file_put_contents(base_path('resources/lang/'.$l.'.json'),$temp );

			


		}
		Log::info('Translation end '.date('Y-m-d H:i:s'));
		
	
	
		
    }
   
	
}
