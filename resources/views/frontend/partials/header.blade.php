
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header page-scroll">

			<div class="container-fluid">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
				</button>
				<?php if(strpos($_SERVER['REQUEST_URI'],'munka') !== false): ?>
				<button type="button" class="navbar-toggle offcanvas-toggle pull-right hidden-md hidden-sm hidden-lg " data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas" >
					<span class="sr-only">Toggle navigation</span>
                             <span>
                             <span>Kategóriák</span>

                        </span>
				</button>
				<?php endif; ?>
			</div>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<a class=" " href="/"><img class="logo hidden-md hidden-sm hidden-xs " height="95"  src="/assets/frontend/img/mads_logo.jpg" alt="Mads Logó - A munka erő" title="Mads Logó - A munka erő"></a>

		</div>
	</div>
</nav>