<madsWork>
		
		
		<?php
	$server = 'http://'.$_SERVER['HTTP_HOST'];
	$data = \Modules\Work\Entities\Work::where('is_full','=',0)->where('archive','=',0)->get()->toArray();
		
		foreach($data as $row): ?>
				<?php
				$c=\Modules\Workcategory\Entities\Workcategory::find($row['workcategory_id']);
				$head_category = \Modules\Workcategory\Entities\Workcategory::find($c->parent_id);
				$url= $server.route('frontend_work',[$head_category->slug,$c->slug,$row['slug']]);
				$projekt =\Modules\User\Entities\User::find($row['responsible']);
				$projekt_text = $projekt->name."<br/>".
					"Email: <a href='mailto:".$projekt->email."'><b>".$projekt->email."</b></a><br/>".
					"Telefon: <b>".$projekt->phone."</b>";
			    ?>
			<work>
				<id><?=$row['id']?></id>
				<url><![CDATA[<?= $url ?>]]></url>


				<description><![CDATA[<?=$row['lead']?>]]></description>
				<category><?=$head_category->name?>/<?=$c->name?></category>
				<place><?=$row['work_place']?></place>
				<project><![CDATA[<?=$row['task']?>]]></project>
				<condition><![CDATA[<?=$row['conditions']?>]]></condition>
				<advantage><![CDATA[<?=$row['preferences']?>]]></advantage>
				<workingHours><![CDATA[<?=$row['time']?>]]></workingHours>
				<duration><![CDATA[<?=$row['working_time']?>]]></duration>
				<beginning><![CDATA[<?=$row['start']?>]]></beginning>
				<grossHourlyWage><![CDATA[<?=$row['wage']?>]]></grossHourlyWage>
				<signin><![CDATA[<?=$row['apply']?>]]></signin>

				<projektmanagger><?=$projekt_text?></projektmanagger>
				<time><?=$row['updated_at']?></time>
			</work>
			
	<?php endforeach; ?>
</madsWork>