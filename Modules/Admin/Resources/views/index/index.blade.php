@extends('admin::layouts.layout')
@section('content')
	@push('scripts')
		{!!  \Modules\Admin\Entities\Chart::initChartJs()!!}
	@endpush
    <div class="row">
		<div class="col-md-12">
			<div class="row">
				@if(\Illuminate\Support\Facades\Auth::user()->shop_id==0)
					@php
						$shop = \Modules\Company\Entities\Shop::all();
					@endphp
					<div class="col-md-12">
						<h4 style="padding:20px" class="bg-primary m-portlet__head">PDF letöltése</h4>
						<hr/>
						<ul class="list-inline text-center">
							@foreach($shop as $s)
							<li class="text-center  list-inline-item" style="border:solid 1px silver;border-radius: 10px;padding:10px">
								<a href="{{route('admin_pdf_preview',['shop_id'=>$s->shop_id])}}" target="_blank">
								<svg width="70px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="file-pdf" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-file-pdf fa-w-12 fa-9x"><path fill="currentColor" d="M181.9 256.1c-5-16-4.9-46.9-2-46.9 8.4 0 7.6 36.9 2 46.9zm-1.7 47.2c-7.7 20.2-17.3 43.3-28.4 62.7 18.3-7 39-17.2 62.9-21.9-12.7-9.6-24.9-23.4-34.5-40.8zM86.1 428.1c0 .8 13.2-5.4 34.9-40.2-6.7 6.3-29.1 24.5-34.9 40.2zM248 160h136v328c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V24C0 10.7 10.7 0 24 0h200v136c0 13.2 10.8 24 24 24zm-8 171.8c-20-12.2-33.3-29-42.7-53.8 4.5-18.5 11.6-46.6 6.2-64.2-4.7-29.4-42.4-26.5-47.8-6.8-5 18.3-.4 44.1 8.1 77-11.6 27.6-28.7 64.6-40.8 85.8-.1 0-.1.1-.2.1-27.1 13.9-73.6 44.5-54.5 68 5.6 6.9 16 10 21.5 10 17.9 0 35.7-18 61.1-61.8 25.8-8.5 54.1-19.1 79-23.2 21.7 11.8 47.1 19.5 64 19.5 29.2 0 31.2-32 19.7-43.4-13.9-13.6-54.3-9.7-73.6-7.2zM377 105L279 7c-4.5-4.5-10.6-7-17-7h-6v128h128v-6.1c0-6.3-2.5-12.4-7-16.9zm-74.1 255.3c4.1-2.7-2.5-11.9-42.8-9 37.1 15.8 42.8 9 42.8 9z" class=""></path></svg>
									<div>{{$s->name}}</div>
								</a>
							</li>
							@endforeach
						</ul>
						<hr/>
					</div>
				@else
					@php
						$shop = \Modules\Company\Entities\Shop::where('shop_id','=',\Illuminate\Support\Facades\Auth::user()->shop_id)->first();
					@endphp
					<div class="col-md-12">
						<h4 style="padding:20px" class="bg-primary m-portlet__head">PDF letöltése</h4>
						<hr/>
						<ul class="list-inline text-center">

								<li class="text-center  list-inline-item" style="border:solid 1px silver;border-radius: 10px;padding:10px">
									<a href="{{route('admin_pdf_preview',[$shop->shop_id])}}" target="_blank">
										<svg width="70px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="file-pdf" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-file-pdf fa-w-12 fa-9x"><path fill="currentColor" d="M181.9 256.1c-5-16-4.9-46.9-2-46.9 8.4 0 7.6 36.9 2 46.9zm-1.7 47.2c-7.7 20.2-17.3 43.3-28.4 62.7 18.3-7 39-17.2 62.9-21.9-12.7-9.6-24.9-23.4-34.5-40.8zM86.1 428.1c0 .8 13.2-5.4 34.9-40.2-6.7 6.3-29.1 24.5-34.9 40.2zM248 160h136v328c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V24C0 10.7 10.7 0 24 0h200v136c0 13.2 10.8 24 24 24zm-8 171.8c-20-12.2-33.3-29-42.7-53.8 4.5-18.5 11.6-46.6 6.2-64.2-4.7-29.4-42.4-26.5-47.8-6.8-5 18.3-.4 44.1 8.1 77-11.6 27.6-28.7 64.6-40.8 85.8-.1 0-.1.1-.2.1-27.1 13.9-73.6 44.5-54.5 68 5.6 6.9 16 10 21.5 10 17.9 0 35.7-18 61.1-61.8 25.8-8.5 54.1-19.1 79-23.2 21.7 11.8 47.1 19.5 64 19.5 29.2 0 31.2-32 19.7-43.4-13.9-13.6-54.3-9.7-73.6-7.2zM377 105L279 7c-4.5-4.5-10.6-7-17-7h-6v128h128v-6.1c0-6.3-2.5-12.4-7-16.9zm-74.1 255.3c4.1-2.7-2.5-11.9-42.8-9 37.1 15.8 42.8 9 42.8 9z" class=""></path></svg>
										<div>{{$shop->name}}</div>
									</a>
								</li>

						</ul>
						<hr/>
					</div>
				@endif
							<div class="col-md-6">
								<!--begin:: Widgets/Audit Log-->
								<div class="m-portlet m-portlet--full-height ">
									<div class="m-portlet__head bg-primary">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text ">
													Vásárlások eloszlása
												</h3>
											</div>
										</div>

									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<canvas id="id1" style="height:400px"></canvas>

										</div>
									</div>
								</div>
								<!--end:: Widgets/Audit Log-->
							</div>
							<div class="col-md-6">
					<!--begin:: Widgets/Audit Log-->
					<div class="m-portlet m-portlet--full-height ">
						<div class="m-portlet__head bg-primary">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
									<h3 class="m-portlet__head-text ">
										Bolt terheltsége óránként
									</h3>
								</div>
							</div>

						</div>
						<div class="m-portlet__body">
							<div class="tab-content">
								<canvas id="id5" style="height:400px"></canvas>

							</div>
						</div>
					</div>
					<!--end:: Widgets/Audit Log-->
				</div>
				 			@if(\Illuminate\Support\Facades\Auth::user()->shop_id == 0)
							<div class="col-md-6">
					<!--begin:: Widgets/Audit Log-->
								<div class="m-portlet m-portlet--full-height ">
									<div class="m-portlet__head bg-primary">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text ">
													Vásárlók boltonkénti eloszlása
												</h3>
											</div>
										</div>

									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<canvas id="id6" style="height:400px"></canvas>

										</div>
									</div>
								</div>
								<!--end:: Widgets/Audit Log-->
							</div>
							@endif
							<div class="col-md-6">
								<!--begin:: Widgets/Audit Log-->
								<div class="m-portlet m-portlet--full-height ">
									<div class="m-portlet__head bg-primary">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text ">
													Napi vásárlásók száma
												</h3>
											</div>
										</div>

									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<div class="tab-pane active" id="m_widget4_tab1_content">
												<canvas id="id2" style="height:400px"></canvas>
											</div>

										</div>
									</div>
								</div>
								<!--end:: Widgets/Audit Log-->
							</div>
							<div class="col-md-6">
								<!--begin:: Widgets/Audit Log-->
								<div class="m-portlet m-portlet--full-height ">
									<div class="m-portlet__head bg-primary">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text ">
													Új vásárlók
												</h3>
											</div>
										</div>

									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<canvas id="id3" style="height:400px"></canvas>

										</div>
									</div>
								</div>
								<!--end:: Widgets/Audit Log-->
							</div>
							<div class="col-md-6">
					<!--begin:: Widgets/Audit Log-->
								<div class="m-portlet m-portlet--full-height ">
									<div class="m-portlet__head bg-primary">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text ">
													TOP vásárlók
												</h3>
											</div>
										</div>

									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<canvas id="id4" style="height:400px"></canvas>

										</div>
									</div>
								</div>
								<!--end:: Widgets/Audit Log-->
							</div>

			</>
		</div>
        <div class="col-md-12">
            <div class="m-portlet  m-portlet--bordered-semi">
               <div class="row m-row--no-padding m-row--col-separator-xl">

			   </div>
            </div>
        </div>

    </div>


@endsection
@push('scripts')


	{!! \Modules\Admin\Entities\Chart::drawLineChart('Vásárlások eloszlása','id1',\Modules\Purchase\Entities\Purchase::getPriceByDate(date('Y-m-d',strtotime('-1 month')),date("Y-m-d")),'bar')->render(); !!}
	{!! \Modules\Admin\Entities\Chart::drawLineChart('Vásárlók eloszlása','id2',\Modules\Purchase\Entities\Purchase::getUsersByDate(date('Y-m-d',strtotime('-1 month')),date("Y-m-d")),'bar')->render(); !!}
	{!! \Modules\Admin\Entities\Chart::drawLineChart('Vásárlók eloszlása','id3',\Modules\Purchase\Entities\Purchase::getNewUsers(date('Y-m-d',strtotime('-1 month')),date("Y-m-d")),'bar')->render(); !!}
	{!! \Modules\Admin\Entities\Chart::drawLineChart('Vásárlók eloszlása','id4',\Modules\Purchase\Entities\Purchase::getTopUsers(date('Y-m-d',strtotime('-1 month')),date("Y-m-d")),'bar')->render(); !!}
	{!! \Modules\Admin\Entities\Chart::drawLineChart('Napi eloszlás','id5',\Modules\Purchase\Entities\Purchase::getUserByHour(date('Y-m-d',strtotime('-1 month')),date("Y-m-d")),'line')->render(); !!}
	{!! \Modules\Admin\Entities\Chart::drawLineChart('Boltonkénti eloszlás','id6',\Modules\Purchase\Entities\Purchase::getUserByShop(date('Y-m-d',strtotime('-1 month')),date("Y-m-d")),'pie')->render(); !!}
	@endpush