<?php
	echo "
@extends('admin::layouts.layout')
@section('content')
	<?php
		\$errors= Session::get('field_errors');
		Session::forget('field_errors');
	?>
<div class='row'>
		<div class='col-md-12'>
			<div class='m-portlet'>
				<div class='m-portlet__body'>
					<input type='hidden' name='tab' value='data'>
					<div class = 'row'>
						<div class = 'col-md-12'>	"."\n"."		{!!  \App\Http\Helper::formOpen('".$model."Form','post',NULL,['class'=>'form '])  !!}";
	echo			"<div class = 'm-portlet__head'>
									<div class = 'm-portlet__head-caption'>
										<div class = 'm-portlet__head-title'>
											<h3 class = 'm-portlet__head-text'>
												{{__('Tartalom adatok')}}
											</h3>
										</div>
									</div>
								</div>
								<div class = 'm-portlet__body'>
									<!--begin::Widget 29-->
									<div class = 'm-widget29'>
										<div class = 'm-widget_content'>
											<div class = 'm-widget_content-items'>
												<div class='row'>
													<div class='col-md-6'>	";
	$index=0;
	foreach ($data['column_fillable'] as $k =>$bool)
	{

		if($bool)
		{
			echo				'<div class="col-md-6">';
			switch($data['column_input'][$k])
			{
				case "input":
					echo "					{!! \App\Http\Helper::input(trans('".$data['column_label'][$k]."'),'".$data['column_name'][$k]."',old('".$data['column_name'][$k]."',\$model->".$data['column_name'][$k]."),array('class'=>'form-control'),\$errors) !!}		\n";
					break;
				case "file":
					echo "					{!! \App\Http\Helper::fileSelector(trans('".$data['column_label'][$k]."'),'".$data['column_name'][$k]."','browse-image',old('".$data['column_name'][$k]."',\$model->".$data['column_name'][$k]."),array('class'=>'form-control'),\$errors) !!}
						@if(!empty(old('".$data['column_name'][$k]."',\$model->".$data['column_name'][$k].")))
							<img class=\"img-responsive holder-lead_image img-fluid\" id=\"holder-lead_image\" src=\"{{old('".$data['column_name'][$k]."',\$model->".$data['column_name'][$k].")}}\">
						@endif";
					break;
				case "textarea":
					echo "					{!! \App\Http\Helper::textarea(trans('".$data['column_label'][$k]."'),'".$data['column_name'][$k]."',old('".$data['column_name'][$k]."',\$model->".$data['column_name'][$k]."),array('class'=>'form-control'),\$errors) !!}		\n";
					break;
				case "ckeditor":
					echo "					{!! \App\Http\Helper::textarea(trans('".$data['column_label'][$k]."'),'".$data['column_name'][$k]."',old('".$data['column_name'][$k]."',\$model->".$data['column_name'][$k]."),array('class'=>'form-control  wyswyg'),\$errors) !!}		\n";
					break;
				case "date":
					echo "					{!! \App\Http\Helper::input(trans('".$data['column_label'][$k]."'),'".$data['column_name'][$k]."',old('".$data['column_name'][$k]."',\$model->".$data['column_name'][$k]."),array('class'=>'form-control datepicker date-picker'),\$errors) !!}		\n";
					break;
				case "select":
					echo "					{!! \App\Http\Helper::select(trans('".$data['column_label'][$k]."'),'".$data['column_name'][$k]."',[''=>'Kérem válasszon']+".$data['column_value'][$k]. ", old('".$data['column_name'][$k]."',\$model->".$data['column_name'][$k]."),array('class'=>'form-control'),\$errors) !!}		\n";
					break;
				case "checkbox":
					echo "					{!! App\Http\Helper::checkbox(trans('".$data['column_label'][$k]."'),'".$data['column_name'][$k]."',old('".$data['column_name'][$k]."',\$model->".$data['column_name'][$k]."),array(),\$errors) !!}";
					break;
				default:
					break;

			}
			echo				'</div>'."\n";
		}

	}
	echo "										</div>

										</div>
									</div>
								</div>";
	echo "
		 {!! \App\Http\Helper::formButtons('admin_".$root."_list') !!}
		 </div>
		 {!! \App\Http\Helper::formClose() !!}

			</div>
					</div>
				</div>
			</div>
		</div>
	</div>



@endsection ";
?>
