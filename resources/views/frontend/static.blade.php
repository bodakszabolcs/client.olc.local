@extends('frontend.layout')
@section('content')
<section id="munkak" class="margin60">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 text-center">
            <h2>{{$page->title}}</h2>
         </div>
         <div class="clearfix"></div>
         <br>
          {!! $page->content !!}

          <div class="clearfix"></div>
      </div>
   </div>
</section>
@endsection