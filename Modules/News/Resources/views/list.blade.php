@extends('admin::layouts.layout')
@section('content')
    @php
        $shops = \Modules\Company\Entities\Shop::where('payed_package_id','>',1)->disableCache()->get();
        $days=[
        1 => 'Hétfő',
        2 => 'Kedd',
        3 => 'Szerda',
        4 => 'Csütörtök',
        5 => 'Péntek',
        6 => 'Szombat',
        7 => 'Vasárnap'
        ];
    @endphp
    <style>
        .col-md-3{
            padding:5px !important;
        }
        .preview-container {
            border: solid 1px #ebedf2;
            padding-bottom: 36px;
        }
        h2 {
            font-size: 2rem;
            padding: 10px;
            border-bottom: solid 1px #ebedf2;
        }
    </style>
    <div class='row'>
        <div class='col-12'>
            <div class='m-portlet'>

                <div class='m-portlet__body' style="padding:20px !important;">

                    <ul class="nav nav-tabs  m-tabs-line" role="tablist">
                        @foreach($shops as $shop)
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link @if($loop->iteration == 1) active show @endif"
                                   data-toggle="tab" href="#{{str_slug($shop->name)}}" role="tab" aria-selected="true">
                                    {{$shop->name}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($shops as $shop)
                            <div class="tab-pane @if($loop->iteration == 1) active show @endif"
                                 id="{{str_slug($shop->name)}}" role="tabpanel">
                                <div class="row">
                                @foreach($days as $k=> $day)
                                    @php
                                    $new = \Modules\News\Entities\News::where('day','=',$k)->where('shop_id','=',$shop->shop_id)->disableCache()->first();
                                    $data=[];
                                    if($new){
                                        $data['link']=$new->link;
                                        $data['shop_name']= $shop->name;
                                        $data['image']=$new->image;
                                        $data['title']=$new->title;
                                        $data['description']=$new->description;
                                    }

                                    @endphp
                                        <div class="col-md-3">
                                            <div class="preview-container">
                                            <h2 class="text-left">{{$day}}</h2>
                                                @if($new)
                                                    <a style="float:right;margin-right: 100px; margin-top: -50px;" href="{{route('admin_news_change_status',['id'=>$new->id])}}" class="btn btn-{{$new->active?'warning':'info'}} btn-brand">{{$new->active?'Inaktívál':'Aktivál'}}</a>
                                                @endif
                                                <a style="float: right; margin-top: -50px;" href="{{route('admin_news_edit',['day'=>$k,'shop_id'=>$shop->shop_id,'id'=>optional($new)->id])}}" class="btn btn-success btn-brand">Szerkeszt</a>
                                            <div class="clearfix"></div>
                                            <div class="marvel-device" style="{{(!$new || !$new->active)?'opacity:0.7':''}}" >

                                                <iframe style="border:none" src="{{route('admin_new_preview',['data'=>$data])}}"  width="100%" height="100%">

                                                </iframe>



                                            </div>
                                            <div class="clearfix"></div>

                                            </div>
                                        </div>

                                @endforeach
                                </div>
                            </div>
                        @endforeach

                    </div>


                    <div class="alert alert-info">
                        Amennyiben hírdetni szeretne a mobilalkalmazásban elhelyezett hírdető felületen. Kérlek lépjen
                        kapcsolatba velünk!
                    </div>
                    {!! $model->appends($_GET)->links() !!}
                </div>
            </div>
        </div>

    </div>
@endsection 
