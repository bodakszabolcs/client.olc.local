(function () {

        var selected_content = 0;

        CKEDITOR.plugins.add('reczeimage', {

            init: function (editor) {

                editor.addCommand('reczeimage', new CKEDITOR.dialogCommand('reczeimage', {
                    allowedContent: true
                }));

                editor.ui.addButton('Kép', {
                    label: 'Kép begyazas',
                    command: 'reczeimage',
                    toolbar: 'insert',
                    icon: this.path + 'icon.png'
                });

                CKEDITOR.dialog.add('reczeimage', function (instance) {
                    var post, desc;

                    return {
                        title: 'Kép beágyazás',
                        minWidth: 510,
                        minHeight: 200,
                        contents:
                            [{
                                id: 'reczeimageEmbedPlugin',
                                expand: true,
                                elements:
                                    [
                                        {
                                            type : 'text',
                                            id : 'image_url',
                                            width : '200px',
                                            label : 'Kép beágyazó kód',
                                            validate : function () {
                                                if (this.getValue()) {
                                                    post = this.getValue();
                                                }
                                                else {
                                                    alert('Kérem adja meg a képet!');
                                                    return false;
                                                }
                                            }
                                        },
                                        {
                                            type: "button",
                                            id: "browse",
                                            hidden: true,
                                            label: editor.lang.common.browseServer,
                                            filebrowser: {
                                                action: 'Browse',
                                                params: {type: 'Images'},
                                                target: 'reczeimageEmbedPlugin:image_url'
                                            }
                                        },
                                        {
                                            type : 'textarea',
                                            id : 'image_description',
                                            width : '200px',
                                            height : '200px',
                                            label : 'Kép felirat',
                                            validate : function () {
                                                if (this.getValue()) {
                                                    desc = this.getValue();
                                                }
                                                else {
                                                    desc = '';
                                                }
                                            }
                                        }
                                    ]

                            }],
                        onOk: function () {
                            var instance = this.getParentEditor();

                            var html = "<div class='embeded'><figure><img class='img-responsive img-fluid' src='";

                            post = this.getValueOf('reczeimageEmbedPlugin', 'image_url');
                            desc = this.getValueOf('reczeimageEmbedPlugin', 'image_description');

                            post = post.replace(/&/g, '&amp;');
                            post = post.trim();

                            desc = desc.replace(/&/g, '&amp;');
                            desc = desc.trim();


                            html += post + "' />";

                            console.log(desc);
                            if (desc != '' && desc != "&nbsp;")
                            {
                                html += "<figcaption>"+desc+"</figcaption>";
                            }

                            html +="</figure></div>";


                            var element = CKEDITOR.dom.element.createFromHtml(html);
                            instance.insertElement(element);


                        }
                    };
                });
            }
        });
    }

)();