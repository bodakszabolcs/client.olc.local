<?php

namespace Modules\Company\Transformers;

use Illuminate\Http\Resources\Json\Resource;



class ShopListResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'webpage' => $this->webpage,
            'facebook' => $this->facebook,
            'instagram'=> $this->instagram,
            'cover' => url($this->cover),
        ];
    }


}
