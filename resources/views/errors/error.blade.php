@extends('errors.maintance_layout')
@section('content')

    <div class="container content zerotop">
        <div class="errorPage">

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <h1 class="h2 text-center">{{__('Valami hiba történt, kérem próbálja meg később!')}}</h1>
                </div>
            </div>
        </div>
    </div>
@endsection
