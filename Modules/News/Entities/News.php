<?php

	namespace Modules\News\Entities;

	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Database\Eloquent\SoftDeletes;
	use Mockery\Exception;
	use App\LiquidModel;
	use Sunra\PhpSimple\HtmlDomParser;
	use GeneaLabs\LaravelModelCaching\Traits\Cachable;

	class News extends LiquidModel
	{
		use SoftDeletes, Cachable;

		protected $table = 'news';

		protected $dates = ['deleted_at', 'created_at', 'updated_at'];
		/**
		 * The attributes that are mass assignable.
	 	*
		 * @var array
	 	*/
		protected $fillable = [
			'title', //Cím 
			'description', //Leírás 
			'image', //Kép 
			'date', //Kiküldés dátuma
            'shop_id',
            'link',
            'day',
		];

		public $searchColumns = [
			'id' => 'text',
			'title'	=>	'text', 
			'description'	=>	'text', 
			'image'	=>	'text', 
			'date'	=>	'text', 
			];

		public $searchColumnLabels = [
			'id' => 'ID',
			'title'	=>	'Cím', 
			'description'	=>	'Leírás', 
			'image'	=>	'Kép', 
			'date'	=>	'Kiküldés dátuma', 
			];
		protected $casts = [
			];
	}
?>
