<?php

namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('lq_options')->insert(
            [
                'lq_key' => 'logo',
                'lq_value' => ''
            ]
        );

        DB::table('lq_options')->insert(
            [
                'lq_key' => 'analytics',
                'lq_value' => '<script type="text/javascript"></script>'
            ]
        );

        DB::table('lq_options')->insert(
            [
                'lq_key' => 'front_page',
                'lq_value' => '1'
            ]
        );

        DB::table('lq_options')->insert(
            [
                'lq_key' => 'webshop',
                'lq_value' => '1'
            ]
        );

        DB::table('lq_options')->insert(
            [
                'lq_key' => 'socials_facebook',
                'lq_value' => '#'
            ]
        );
        DB::table('lq_options')->insert(
            [
                'lq_key' => 'socials_google',
                'lq_value' => '#'
            ]
        );
        DB::table('lq_options')->insert(
            [
                'lq_key' => 'socials_instagram',
                'lq_value' => '#'
            ]
        );
        DB::table('lq_options')->insert(
            [
                'lq_key' => 'socials_youtube',
                'lq_value' => '#'
            ]
        );
    }
}
