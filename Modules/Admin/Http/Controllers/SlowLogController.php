<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class SlowLogController extends AdminController
{
    public function __construct()
    {
        View::share('sub_title', __('Slow Query Log'));
        View::share('subtitle_link', route('admin_slowlog_list'));
    }

    public function SlowLogList(Request $request)
    {
        View::share('title', __('Slow Query Log'));
        $dir = 'storage/'.env('SQL_LOGGER_DIRECTORY','log/sql');
        Storage::disk('storage')->makeDirectory(env('SQL_LOGGER_DIRECTORY','log/sql'));

        $content = '';
        $active = '';
        $files = File::allFiles(base_path($dir));
        $files = array_reverse($files);
        if($request->isMethod('post'))
        {
            $content = file_get_contents($request->file);
            $active =$request->file;
        }
        else if (sizeof($files) > 0) {
            $content = file_get_contents($files[0]);
            $active = $files[0];
        }
        return view('admin::slowlog.list', ['files'=>$files,'content'=>$content,'active_file'=>$active]);

    }
}
