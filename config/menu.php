<?php

	return array(


    array(
        'icon' => 'fas fa-tachometer-alt',
        'title' => 'Kezdőlap',
        'route' => 'admin_dashboard',
		'divider' => 'Felhasználók'
    ),
	/* - - - - - - - - - - - - - - - - - - - - - */
    array(
        'icon' => 'fas fa-users-cog',
        'title' => 'Csoportok',
        'route' => 'admin_acl_list',
    ),
    array(
	    'icon' => 'fas fa-users',
	    'title' => 'Felhasználók',
	    'route' => 'admin_user_list',
	    'divider' => 'Cég és bolt adatok'
    ),
    array(
	    'icon' => 'fa fa-building',
	    'title' => 'Cég adatok',
	    'route' => 'admin_company_edit',

    ),
    array(
	    'icon' => 'fa fa-industry',
	    'title' => 'Bolt adatok',
	    'route' => 'admin_shop_list',

    ),
        array(
            'icon' => 'fa fa-euro',
            'title' => 'Vásárlások',
            'route' => 'admin_purchase_list',
            ),
    array(
            'icon' => 'fa fa-cc-visa',
            'title' => 'Számlák',
            'route' => 'admin_invoice_list',
            'divider' => 'Beállítások'
        ),
    array(
	    'icon' => 'fa  fa-credit-card',
	    'title' => 'Kuponok',
	    'route' => 'admin_coupon_list'
    ),
        array(
	    'icon' => 'fa  fa-calculator',
	    'title' => 'Automatikus kupongenerálás',
	    'route' => 'admin_coupon_automatic_coupon'
    ),
        array(
            'icon' => 'fa fa-share-square-o',
            'title' => 'Vásárlói értesítések',
            'route' => 'admin_news_list'
        ),
    array(
	    'icon' => 'fa fa fa-percent',
	    'title' => 'Kedvezmény sávok',
	    'route' =>'admin_pricerange_list',
	    'divider'=>'Adminisztráció'
    ),
	/* - - - - - - - - - - - - - - - - - - - - - */
    array(
        'icon' => 'fa fa-gear',
        'title' => 'Beállítások',
        'route' => array(

            array(
                'icon' => 'fa fa-eye',
                'title' => 'Virtuális URLek',
                'route' => 'admin_virtual_url_list'
            ),
            array(
                'icon' => 'fas fa-globe',
                'title' => 'Fordítások',
                'route' => 'admin_translation_list'
            ),

        ),

    ),

    array(
        'icon' => 'fa fa-wrench',
        'title' => 'Fejlesztés',
        'route' => array(
            array(
                'icon' => 'fas fa-terminal',
                'title' => 'Parancsok',
                'route' => 'admin_run_command'
            ),
            array(
                'icon' => 'fa fa-archive',
                'title' => 'Logok',
                'route' => 'log-viewer::dashboard'
            ),
            array(
                'icon' => 'fa fa-archive',
                'title' => 'Slow Query Log',
                'route' => 'admin_slowlog_list'
            ),
            array(
                'icon' => 'fa fa-columns',
                'title' => 'Widget Layout',
                'route' => 'admin_layout_list'
            ),
            array(
                'icon' => 'fa fa-columns',
                'title' => 'Model builder',
                'route' => 'admin_builder_settings'
            ),
            array(
                'icon' => 'fa fa-puzzle-piece',
                'title' => 'Modulok',
                'route' => 'admin_modules'
            ),
            array(
                'icon' => 'fas fa-ticket-alt',
                'title' => 'Hibajegyek',
                'route' => 'admin_ticket_list'
            ),
        )
    ),







//BuilderMenu//
















































);
