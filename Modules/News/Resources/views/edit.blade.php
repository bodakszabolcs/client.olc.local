@extends('admin::layouts.layout')
@section('content')
    <?php
    $errors = Session::get('field_errors');
    Session::forget('field_errors');
    ?>
    <div class='row'>
        <div class='col-md-12'>
            <div class='m-portlet'>
                <div class='m-portlet__body'>
                    <input type='hidden' name='tab' value='data'>
                    <div class='row'>
                        <div class='col-md-12'>
                            {!!  \App\Http\Helper::formOpen('NewsForm','post',NULL,['class'=>'form ','enctype'=>'multipart/form-data'])  !!}
                            <div class='m-portlet__body'>
                                <div class='m-widget29'>
                                    <div class='m-widget_content'>
                                        <div class='m-widget_content-items'>
                                            <div class='row'>
                                                <div class="col-md-6">
                                                    <div class="col-md-12">
                                                        <div class="dropzone" id="myDropzone">

                                                            <div class="dz-message">
                                                                <div>
                                                                    <input type="hidden" name="file" value="{{$model->image }}">
                                                                    <h4> {{__('Értesítés háttere')}}:</h4><div class="text-muted">(720x1280 pixel, formátumok: JPG, JPEG, PNG)</div>
                                                                </div>
                                                            </div>
                                                        </div>



                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="hidden" name="shop_id" value="{{$shop_id}}">
                                                        <input type="hidden" name="day" value="{{$day}}">
                                                        {!! \App\Http\Helper::input(trans('Cím'),'title',old('title',$model->title),array('class'=>'form-control'),$errors) !!}
                                                        {!! \App\Http\Helper::textarea(trans('Leírás'),'description',old('description',$model->description),array('class'=>'form-control'),$errors) !!}
                                                    </div>
                                                    <div class="col-md-12">

                                                        {!! \App\Http\Helper::select(trans('Bolt'),'shop_id',\Modules\Company\Entities\Shop::getList(),old('shop_id',$shop_id),array('class'=>'form-control',"disabled"=>'disabled'),$errors) !!}
                                                    </div>



                                                <div class="col-md-12">
                                                    {!! \App\Http\Helper::input(trans('Hivatkozás'),'link',old('link',$model->link),array('class'=>'form-control'),$errors) !!}
                                                </div>

                                            </div>
                                                <div class="col-md-6">
                                                    <div class="clearfix"></div>
                                                    <div class="marvel-device"  >

                                                        <iframe style="border:none" src="{{route('admin_new_preview')}}"  width="100%" height="100%">

                                                        </iframe>



                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    {!! \App\Http\Helper::formButtons('admin_news_list') !!}
                                </div>
                                {!! \App\Http\Helper::formClose() !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



@endsection 