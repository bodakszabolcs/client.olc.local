@php

@endphp

{!! \App\Http\Helper::formOpen('subscribe','post',null,['enctype'=>'multipart/form-data']) !!}
<div class="row">
	@if(Session::get('success_message'))
		@php
			Session::forget('success_message');
		@endphp
		<div class="alert alert-info text-center">
				<h3>Köszönjük a jelentkezést! Hamarosan felvesszük Önnel a kapcsolatot!</h3>
			</div>
		@php
			$model=[];

		@endphp
	@endif
	@if($data= Session::get('error_message'))
		<div class="alert alert-danger text-center has-danger">
				<h3>{{$data}}</h3>
			</div>
			@php
				Session::forget('error_message');

			@endphp
	@endif
				<input type="hidden" name="id" value="{{$id}}">
				<div class="col-md-12">
					{!! \App\Http\Helper::input('Név <span class="text-danger">*</span>','name',old('name',array_get($model,'name')),array('form-control'),$error) !!}
				</div>
				<div class="col-md-6">
					{!! \App\Http\Helper::input('E-mail <span class="text-danger">*</span>','email',old('email',array_get($model,'email')),array('form-control'),$error) !!}
				</div>

				<div class="col-md-6">
					{!! \App\Http\Helper::input('Telefon <span class="text-danger">*</span>','phone',old('phone',array_get($model,'phone')),array('form-control'),$error) !!}
				</div>
				<div class="col-md-12">
					{!! \App\Http\Helper::fileSelector('Önéletrajz(pdf)','cv',null,null,array('class'=>'form-control','accept'=>'application/pdf'),$error) !!}
				</div>
			<div class="col-md-12">
					<div class="row">
					<div class="col-md-1 col-xs-2">
						<input type="checkbox" class="form-control {{sizeof(array_get($error,'hirlev',[]))?' has-danger':''}}" name="hirlev" value="1">
					</div>
					<div class="col-md-11 col-xs-10">
						<p>Hozzájárulok ahhoz, hogy a MADS Work Kft. elektronikus hirdetést és karrierhírlevelet küldjön számomra.</p>
						@foreach (array_get($error,'hirlev',[]) as $s)
							<small class="text-danger">{{$s}}</small>
						@endforeach
					</div>
				</div>
					<div class="row">
					<div class="col-md-1 col-xs-2">
						<input type="checkbox" class="form-control" name="hirlev2" value="1">
					</div>
					<div class="col-md-11 col-xs-10" {{sizeof(array_get($error,'hirlev2',[]))?' has-danger':''}}>
						<p>Hozzájárulok ahhoz, hogy a megadott adataim továbbításra kerüljenek olyan harmadik személyek/külső szolgáltatást fogadók részére, akik az általam megjelölt típusú feladat teljesítésére keresnek munkavállalókat.</p>
						@foreach (array_get($error,'hirlev2',[]) as $s)
							<small class="text-danger">{{$s}}</small>
						@endforeach
					</div>
				</div>
					<div class="row">
					<div class="col-md-1 col-xs-2">
						<input type="checkbox" class="form-control" name="hirlev3" value="1">
					</div>
					<div class="col-md-11 col-xs-10 {{sizeof(array_get($error,'hirlev3',[]))?' has-danger':''}}">
						<p>Az <a href="/torvenyek-jogszabalyok">Adatkezelési Tájékoztató</a>ban foglaltakat elolvastam, megértettem, és a regisztrációmmal hozzájárulok ahhoz, hogy személyes adataimat a megjelölt célból és feltételekkel a MADS Work Kft. kezelje.</p>
						@foreach (array_get($error,'hirlev3',[]) as $s)
							<small class="text-danger">{{$s}}</small>
						@endforeach
					</div>
				</div>

				</div>
</div>
{!! \App\Http\Helper::formClose() !!}
