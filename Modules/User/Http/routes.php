<?php

Route::group(['namespace' => 'Modules\User\Http\Controllers', 'prefix' => 'admin/user', 'middleware' => ['web','auth', 'acl', 'accesslog']], function ()
{
    Route::match(['get'], '/list', 'UserController@UserList')->name('admin_user_list');
    Route::match(['get', 'post', 'put'], '/edit/{id?}/{tab?}', 'UserController@Edit')->name('admin_user_edit');
    Route::match(['get'], '/unban/{email}', 'UserController@ResetLoginLog')->name('admin_unban_user');
    Route::match(['get'], '/force-login/{id?}', 'UserController@ForceLogin')->name('admin_user_force_login');
    Route::match(['get'], '/delete/{id}', 'UserController@Delete')->name('admin_user_delete');
    Route::match(['get'], '/export/', 'UserController@Export')->name('admin_user_export');
    Route::match(['get'], '/get-signo/{id?}', 'UserController@GetSigno')->name('admin_user_get_signo');
    Route::match(['get'], '/avatar', 'UserController@Avatar')->name('admin_user_avatar');
});

Route::group(['namespace' => 'Modules\User\Http\Controllers', 'prefix' => 'admin', 'middleware' => ['web','auth', 'acl', 'accesslog']], function ()
{
    Route::match(['get', 'post'], '/profile/{tab?}', 'UserController@Profile')->name('admin_profile');
});
