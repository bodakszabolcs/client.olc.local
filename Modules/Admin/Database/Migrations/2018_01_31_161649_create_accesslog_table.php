<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccesslogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);

        Schema::create('_accesslog', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->comment('Felhasználó');
            $table->string('route')->comment('Route');
            $table->string('action')->comment('Művelet');
            $table->text('extra')->comment('Extra info');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['user_id','route']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_accesslog');
    }
}
