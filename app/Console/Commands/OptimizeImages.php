<?php

namespace App\Console\Commands;

use App\Article;
use Modules\Page\Entities\SiteBuilder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redis;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class OptimizeImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'optimize:images {--all}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Optimize uploaded images';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		Log::info('Cron - OptimizeImage start '.date('Y-m-d H:i:s'));
        $all = $this->option('all');
        $folders = ['uploads', '169', '43', '32'];

        $excluded_files = Cache::get('excluded_files');

        if ($excluded_files == null) {
            $excluded_files = [];
        } else {
            $excluded_files = json_decode($excluded_files);
        }

        if ($all) {
            $excluded_files = [];

            foreach ($folders as $folder) {

                switch($folder)
                {
                    case 'uploads':
                        $width = 1920;
                        $height = 1080;
                        break;
                    case '169':
                        $width = 1170;
                        $height = 658;
                        break;
                    case '43':
                        $width = 1170;
                        $height = 877;
                        break;
                    case '32':
                        $width = 1170;
                        $height = 780;
                        break;
                    default:
                        $width = 1920;
                        $height = 1080;
                        break;
                }

                $files = File::allFiles(public_path($folder));
                foreach ($files as $f) {

                    $img = Image::make($f->getPathName());
                    if ($img->width() > $width)
                    {
                        $img->resize($width, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save();
                    } elseif ($img->height() > $height) {
                        $img->resize(null, $height, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save();
                    }

                    ImageOptimizer::optimize($f->getPathName());
                    $excluded_files[] = $f->getPathName();
                    $this->line('Optimize: ' . $f->getPathName());
                }
            }
            
            Cache::forever('excluded_files', json_encode($excluded_files));

        } else {

            foreach ($folders as $folder) {

                switch($folder)
                {
                    case 'uploads':
                        $width = 1920;
                        $height = 1080;
                        break;
                    case '169':
                        $width = 1170;
                        $height = 658;
                        break;
                    case '43':
                        $width = 1170;
                        $height = 877;
                        break;
                    case '32':
                        $width = 1170;
                        $height = 780;
                        break;
                    default:
                        $width = 1920;
                        $height = 1080;
                        break;
                }

                $files = File::allFiles(public_path($folder));
                foreach ($files as $f) {
                    if (in_array($f->getPathName(), $excluded_files)) continue;

                    $img = Image::make($f->getPathName());
                    if ($img->width() > $width)
                    {
                        $img->resize($width, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save();
                    } elseif ($img->height() > $height) {
                        $img->resize(null, $height, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save();
                    }

                    ImageOptimizer::optimize($f->getPathName());
                    $this->line('Optimize: ' . $f->getPathName());

                }
            }

            Cache::forever('excluded_files', json_encode($excluded_files));
        }
		Log::info('Cron - OptimizeImage end '.date('Y-m-d H:i:s'));
    }
}
