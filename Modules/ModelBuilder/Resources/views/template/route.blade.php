<?php

echo "<?php \n";

echo "Route::group(['namespace' => 'Modules\\".ucfirst($controller)."\\Http\Controllers', 'prefix' => 'admin/".$root."', 'middleware' => ['web','auth', 'acl', 'accesslog']], function () { \n";

	echo "		Route::match(['get'], '/list', '".$controller."Controller@".$controller."List')->name('admin_".$root."_list');"."\n";

if($data['edit'])
{
	echo "		Route::match(['get', 'post', 'put'], '/edit/{id?}/{tab?}', '".$controller."Controller@Edit')->name('admin_".$root."_edit');"."\n";
}
if($data['delete'])
{
	echo "		Route::match(['get'], '/delete/{id}', '".$controller."Controller@Delete')->name('admin_".$root."_delete'); "."\n";
}
echo "});";
echo "\n";
?>