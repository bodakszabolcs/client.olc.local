(function () {

        var selected_content = 0;

        CKEDITOR.plugins.add('facebook', {

            init: function (editor) {

                editor.addCommand('facebook', new CKEDITOR.dialogCommand('facebook', {
                    allowedContent: true
                }));

                editor.ui.addButton('Facebook', {
                    label: 'Facebook begyazas',
                    command: 'facebook',
                    toolbar: 'insert',
                    icon: this.path + 'icon.png'
                });

                CKEDITOR.dialog.add('facebook', function (instance) {
                    var post, height;

                    return {
                        title: 'Facebook beágyazás',
                        minWidth: 510,
                        minHeight: 200,
                        contents:
                            [{
                                id: 'facebookEmbedPlugin',
                                expand: true,
                                elements:
                                    [
                                        {
                                            type : 'text',
                                            id : 'post_url',
                                            width : '400px',
                                            label : 'Facebook url',
                                            validate : function () {
                                                if (this.getValue()) {
                                                    post = this.getValue();
                                                }
                                                else {
                                                    alert('Kérem adja meg a facebook post urlt!');
                                                    return false;
                                                }
                                            }
                                        }
                                    ]

                            }],
                        onOk: function () {
                                var instance = this.getParentEditor();
                                post = this.getValueOf('facebookEmbedPlugin', 'post_url');

                                post = post.replace(/&/g, '&amp;');
                                post = post.replace("/\width=\'\d+\'/g", "width='100%'");
                                post = post.replace('/\width=\"\d+\"/g', 'width="100%"');
                                post = post.replace(/(width=")\d+/, '$1100%');
                                //post = preg_replace('/width="\d+/i"', 'width="100%"', post);
                            console.log(post);
                                post = post.trim();
                                post = '<div class="embeded">'+post+'</div>';

                                var element = CKEDITOR.dom.element.createFromHtml(post);
                                instance.insertElement(element);


                        }
                    };
                });
            }
        });
    }

)();