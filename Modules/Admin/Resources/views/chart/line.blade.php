
	<script>
		var colors =['#03A9F4',
			'#E91E63',
			'#f57f17',
			'#b71c1c',
			'#6a1b9a',
			'#82b1ff',
			'#f57f17',
			'#f4511e',
			'#9e9e9e',
			'#03A9F4',
			'#E91E63',
			'#b71c1c',
			'#6a1b9a',
			'#82b1ff',
			'#f4511e',
			'#9e9e9e',
			'#03A9F4',
			'#E91E63',
			'#b71c1c',
			'#6a1b9a',
			'#82b1ff',
			'#f57f17',
			'#f4511e',
			'#9e9e9e',
			'#03A9F4',
			'#E91E63',
			'#b71c1c',
			'#6a1b9a',
			'#82b1ff',
			'#f57f17',
			'#f4511e',
			'#9e9e9e',
			'#03A9F4',
			'#E91E63',
			'#b71c1c',
			'#6a1b9a',
			'#82b1ff',
			'#f57f17',
			'#f4511e',
			'#9e9e9e',];
	<?php if(empty($colors)) $colors=['#00c5dc']; ?>
	if ($("#{{$name}}").length) {
        var lineChart{{$name}} = $("#{{$name}}");

        // line chart data
        var lineData = {
            labels: [
				@foreach($labels as $l)
                    '{{$l}}' @if(!$loop->last) , @endif
				@endforeach
			],
            datasets: [

				@foreach($data as $k => $d)

					<?php $color =$colors[$loop->iteration%1]; ?>
                {
                    label: '{{$k}}',
                    pointBackgroundColor:colors[{{$loop->iteration-1}}],
                    backgroundColor:colors[{{$loop->iteration-1}}],
                    borderColor:colors[{{$loop->iteration-1}}],
                    data: [
						@foreach($labels as $l)
							@if( isset($d[$l]) )
                        { y: {{$d[$l]}},x:'{{$l}}' }
							@else
                        { y: 0,x:'{{$l}}' }
						@endif
						@if(!$loop->last) , @endif
						@endforeach
                    ],
					fill:false
                   // spanGaps: false
                }
				@if(!$loop->last) , @endif
					@endforeach
                ]
        };

        // line chart init
        ctx = document.getElementById('{{$name}}');
         new Chart(ctx, {
            type: 'line',
            data: lineData,

             options: {
                 title: {
                     display: !1
                 }
                 ,
                 tooltips: {
                     mode: "nearest", intersect: !1, position: "nearest", xPadding: 10, yPadding: 10, caretPadding: 10
                 }

                 ,
                 responsive:!0,
                 maintainAspectRatio:!1,
                 scales: {
                     xAxes:[ {

                         scaleLabel: {
                             display: !1, labelString: "Month"
                         }

                     }
                     ],
                     yAxes:[ {


                         scaleLabel: {
                             display: !1, labelString: "Value"
                         }
                         ,
                         ticks: {
                             beginAtZero: !0
                         }
                     }
                     ]
                 }
                 ,

                 layout: {
                     padding: {
                         left: 0, right: 0, top: 10, bottom: 0
                     }
                 }
             }
        });
    }

	</script>
