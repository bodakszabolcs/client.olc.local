<?php

namespace Tests;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Contracts\Console\Kernel as BaseKernel;

abstract class TestCase extends BaseTestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(BaseKernel::class)->bootstrap();

        return $app;
    }

    protected function deleteObject(Model $object)
    {
        if (!is_null($object))
        {
            $object->forceDelete();
        }
    }
}
