<?php
header("Content-type: text/xml");
header("Content-type: text/xml; charset=utf-8");
echo '<?xml version="1.0" encoding="UTF-8" ?>';
$server = 'http://'.$_SERVER['HTTP_HOST'];
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
	<url>
        <loc><?= $server;?></loc>
		<lastmod><?= date('Y-m-d') ?></lastmod>
		<changefreq>daily</changefreq>
    </url>
    <url>
        <loc><?= $server; ?>/rolunk</loc>
    </url>

	 <url>
        <loc><?= $server ; ?>/letoltheto-dokumentumok</loc>
    </url>
	 <url>
        <loc><?= $server ; ?>/ajanlatkeres</loc>
    </url>
	<url>
        <loc><?= $server ; ?>/kapcsolat</loc>
    </url>
	<?php $works = \Modules\Work\Entities\Work::where('archive','=',0)->get(); ?>
	<?php foreach($works as $w):
	$cat_1 = \Modules\Workcategory\Entities\Workcategory::find($w->workcategory_id);
	$cat_2 = \Modules\Workcategory\Entities\Workcategory::find($cat_1->parent_id);
	?>
	<url>
        <loc><?= $server; ?>/munkak/<?= $cat_2->slug; ?>/<?= $cat_1->slug; ?>/<?= $w->slug; ?></loc>
	    <lastmod><?= date('Y-m-d') ?></lastmod>
	    <changefreq>daily</changefreq>
    </url>
	<?php endforeach; ?>
</urlset>