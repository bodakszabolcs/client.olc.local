<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginDisable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);

        Schema::create('_logins', function (Blueprint $table) {
            $table->increments('id')->comment('Egyedi azonosító')->unique();
            $table->string('email')->comment('Email cím');
            $table->string('password')->comment('Beírt jelszó');
            $table->ipAddress('visitor')->comment('IP cím');
            $table->dateTime('disabled_until')->nullable()->comment('Kizárás ideje');
            $table->timestamps();
            $table->index(['email','visitor','disabled_until']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('_logins');
    }
}
