<?php 
Route::group(['namespace' => 'Modules\News\Http\Controllers', 'prefix' => 'admin/news', 'middleware' => ['web','auth', 'acl', 'accesslog']], function () { 
		Route::match(['get'], '/list', 'NewsController@NewsList')->name('admin_news_list');
		Route::match(['get'], '/preview', 'NewsController@preview')->name('admin_new_preview');
		Route::match(['get', 'post', 'put'], '/edit/{day}/{shop_id}/{id?}', 'NewsController@Edit')->name('admin_news_edit');
		Route::match(['get'], '/delete/{id}', 'NewsController@Delete')->name('admin_news_delete'); 
		Route::match(['get'], '/change-status/{id}', 'NewsController@ChangeStatus')->name('admin_news_change_status');
});
