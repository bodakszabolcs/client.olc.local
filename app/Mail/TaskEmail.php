<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\User\Entities\User;

class TaskEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $to,$title, $subject, $reply, $sender,$content,$statistic_id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject,$sender,$to,$task)
    {
        $this->to= $to;
        $this->sender = $sender;
        $this->reply = $sender;
        $this->subject = $subject;
        $this->task = $task;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $markdown = $this->markdown('emails.admin.task',['subject'=>$this->subject,'task'=>$this->task,'sender'=>$this->sender])
	        ->from($this->sender->email,$this->sender->name)
            ->subject($this->subject)
            ->to($this->to->email,$this->to->name)
            ->replyTo($this->reply->email,$this->reply->name);
	    return $markdown;

    }
}
