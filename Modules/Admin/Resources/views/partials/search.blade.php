<div class="m-list-search__results">
    <span class="m-list-search__result-message m--hide">
		{{__('Nincs találat')}}
	</span>
    @if (sizeof($results) > 0)
    <span class="m-list-search__result-category m-list-search__result-category--first">
		{{__('Results')}}
	</span>
    @endif
    @foreach($results as $result)
        <a href="{{$result['route']}}" class="m-list-search__result-item">
        <span class="m-list-search__result-item-icon">
            <i class="{{$result['icon']}}"></i>
        </span>
            <span class="m-list-search__result-item-text" >{{$result['name']}}</span>
			<span class="pull-right m-list-search__result-item-text">{{$result['type']}}</span>

        </a>
    @endforeach
</div>