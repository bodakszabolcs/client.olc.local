<?php
echo "<?php

	namespace Modules\\".ucfirst($model)."\\Entities;

	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Database\Eloquent\SoftDeletes;
	use Mockery\Exception;
	use App\LiquidModel;
	use Sunra\PhpSimple\HtmlDomParser;
	use GeneaLabs\LaravelModelCaching\Traits\Cachable;

	class ".$model." extends LiquidModel
	{
		use SoftDeletes, Cachable;

		protected \$table = '".$table."';

		protected \$dates = ['deleted_at', 'created_at', 'updated_at'];
		/**
		 * The attributes that are mass assignable.
	 	*
		 * @var array
	 	*/
		protected \$fillable = [\n";
			foreach ($data['column_fillable'] as $k =>$bool)
			{
				if($bool)
				{
					echo "			'".$data['column_name'][$k]."', //".$data['column_comment'][$k]." \n";
				}
			}

	echo "		];\n
		public \$searchColumns = [
			'id' => 'text',\n";
		foreach ($data['column_search'] as $k =>$bool)
		{
			if($bool)
			{
				echo "			'".$data['column_name'][$k]."'	=>	'text', \n";
			}
		}
	echo "			];

		public \$searchColumnLabels = [
			'id' => 'ID',\n";
			foreach ($data['column_search'] as $k =>$bool)
			{
				if($bool)
				{
					echo "			'".$data['column_name'][$k]."'	=>	'".$data['column_label'][$k]."', \n";
				}
			}


	echo "			];\n";

	foreach ($data['column_getter'] as $k =>$bool)
		{
			if($bool)
			{
			echo	"		public function get".ucfirst($data['column_name'][$k])."()
		{
			return \$this->".$data['column_name'][$k].";
		}\n\n";
			}
		}
	echo "		protected \$casts = [\n";
	foreach ($data['column_cast'] as $k =>$bool)
	{
		if($bool)
		{
			echo "			'".$data['column_name'][$k]."'	=>	'array', \n";
		}
	}

	echo"			];\n";
echo "	}
?>"
	?>