@extends('admin::layouts.layout')
@section('content')
    <div class="">
        <div class="alert alert-info">Figyelem! Az itt megjeleneő adatok nem minősülnek számlának!</div>
        <a href="{{route('admin_invoice_list')}}" class="btn btn-primary pull-right">Vissza</a>
    </div>
    <style>
        .invoice-box {
            max-width: 800px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:nth-child(2),.invoice-box table tr td:nth-child(3)  {
            text-align: right;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
            margin-top:50px;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2),.invoice-box table tr.total td:nth-child(3) {
            border-top: 2px solid #eee;
            font-weight: bold;
            padding-top:70px;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
    </style>
    <br>
    <br>
    <br>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="3">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="/images/logo.png" height="50"
                                     >
                            </td>

                            <td>
                                Azonosító : {{$model->transaction_id}}<br>
                                Kiállítás dátuma: {{\App\Http\Helper::date(explode(' ',$model->created_at)[0])}}<br>
                                Fizetési határidő: {{\App\Http\Helper::date($model->deadline)}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information">
                <td colspan="3">
                    @php
                    $company = \Modules\Company\Entities\Company::first();
                    @endphp
                    <table>
                        <tr>
                            <td>
                                {{$company->name}}<br>
                               {{$company->shipping_zip}} {{$company->shipping_city}} <br>
                                {{$company->shipping_address}}
                            </td>

                            <td col>
                                OLC.<br>
                                Online hűség kedvezmény<br>
                                info@online-loyality.card.hu
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>



            <tr class="heading">
                <td>
                    Tétel
                </td>

                <td>
                    Nettó
                </td>
                <td>
                    Bruttó
                </td>
            </tr>
            @foreach(json_decode($model->items,1) as $item )
                <tr class="item">
                    <td>
                        {{$item['name']}} csomag
                    </td>

                    <td>
                        {{intval($item['price']/1.27)}} Ft
                    </td>
                    <td>
                        {{intval($item['price'])}} Ft
                    </td>
                </tr>
            @endforeach


            <tr class="total">
                <td></td>
                <td>Nettó összesen: {{intval($model->final_gross /1.27) }} Ft</td>
                <td>
                    Bruttó összesen: {{$model->final_gross}} Ft
                </td>
            </tr>
        </table>
    </div>
@endsection