@extends('admin::logs.log_layout')

@section('sub_content')
    <div class="row">
        <div class="col-12">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="head-btn">
                        <a class="btn m-btn--square btn-primary" href="{{ route('log-viewer::dashboard') }}"><i
                                    class="fa fa-home"></i> {{__('Log kezdőlap')}}</a>
                        <a class="btn m-btn--square btn-info" href="{{ route('log-viewer::logs.list') }}"><i
                                    class="fa fa-archive"></i> {{__('Logok')}}</a>
                    </div>
                </div>
                <div class="m-portlet__body">

                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <canvas id="stats-doughnut-chart" height="300" class="mb-3"></canvas>
                        </div>

                        <div class="col-md-6 col-lg-9">
                            <div class="row">
                                @foreach($percents as $level => $item)
                                    <div class="col-sm-6 col-md-12 col-lg-4 mb-3">
                                        <div class="box level-{{ $level }} {{ $item['count'] === 0 ? 'empty' : '' }}">
                                            <div class="box-icon">
                                                {!! log_styler()->icon($level) !!}
                                            </div>

                                            <div class="box-content">
                                                <span class="box-text">{{ $item['name'] }}</span>
                                                <span class="box-number">
                                    {{ $item['count'] }} entries - {!! $item['percent'] !!} %
                                </span>
                                                <div class="progress" style="height: 3px;">
                                                    <div class="progress-bar"
                                                         style="width: {{ $item['percent'] }}%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            new Chart(document.getElementById("stats-doughnut-chart"), {
                type: 'doughnut',
                data: {!! $chartData !!},
                options: {
                    legend: {
                        position: 'bottom'
                    }
                }
            });
        });
    </script>
@endsection