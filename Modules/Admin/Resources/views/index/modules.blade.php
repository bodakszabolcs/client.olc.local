@extends('admin::layouts.layout')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="m-portlet">
                <div class="m-portlet__body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover mb-150">
                            <tr>
                                <th>{{__('Modul neve')}}</th>
                                <th>{{__('Állapot')}}</th>
                                <th width="15%">{{__('Művelet')}}</th>
                            </tr>
                            @foreach(Module::getByStatus(1) as $m)
                                @continue($m->name == 'Admin')
                                <tr>
                                    <td>
                                        {{$m->name}}
                                    </td>
                                    <td>
                                        {{__('Bekapcsolva')}}
                                    </td>
                                    <td>
                                        {!! \App\Http\Helper::formOpen($m->name,'POST',route('admin_modules',[$m->name])) !!}
                                        {!! \App\Http\Helper::save('save',__('Kikapcsolás'),array('class'=>'btn btn-danger', 'name'=>'turn_off')) !!}
                                        {!! \App\Http\Helper::formClose() !!}
                                    </td>
                                </tr>
                            @endforeach
                            @foreach(Module::getByStatus(0) as $m)
                                @continue($m->name == 'Admin')
                                <tr>
                                    <td>
                                        {{$m->name}}
                                    </td>
                                    <td>
                                        {{__('Kikapcsolva')}}
                                    </td>
                                    <td>
                                        {!! \App\Http\Helper::formOpen($m->name,'POST', route('admin_modules',[$m->name])) !!}
                                        {!! \App\Http\Helper::save('save',__('Bekapcsolás '),array('class'=>'btn btn-success', 'name'=>'turn_on')) !!}
                                        {!! \App\Http\Helper::formClose() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection