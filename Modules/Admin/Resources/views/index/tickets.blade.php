@extends('admin::layouts.layout')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="m-portlet">
                <div class="m-portlet__body">
                    {!! $filterHeader !!}
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover mb-150">
                            <tr>
                                <th class="sorting" data-column="id">{{__('ID')}}</th>
                                <th class="sorting" data-column="created_at">{{__('Dátum')}}</th>
                                <th>{{__('Beküldő')}}</th>
                                <th class="sorting" data-column="type">{{__('Típus')}}</th>
                                <th class="sorting" data-column="page">{{__('Oldal')}}</th>
                                <th class="sorting" data-column="content">{{__('Leírás')}}</th>
                            </tr>
                            @foreach($model as $m)
                                <tr>
                                    <td>
                                        {{$m->id}}
                                    </td>
                                    <td>
                                        {{date("Y.m.d. H:i",strtotime($m->created_at))}}
                                    </td>
                                    <td>
                                        {{$m->user->lastname.' '.$m->user->firstname}}
                                    </td>
                                    <td>
                                        {{$m->type}}
                                    </td>
                                    <td>
                                        {{$m->page}}
                                    </td>
                                    <td>
                                        {{$m->content}}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    {!! $model->appends($_GET)->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection