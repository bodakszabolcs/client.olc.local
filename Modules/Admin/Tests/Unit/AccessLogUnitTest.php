<?php

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Admin\Entities\Accesslog;
use Tests\TestCase as BaseTestCase;

class AccessLogUnitTest extends BaseTestCase
{

    public function testCreateAccessLogSuccess()
    {
        $acl = new Accesslog();
        $acl->user_id = 1;
        $acl->route = route('admin_login');
        $acl->action = 'GET';
        $acl->extra = [];
        $acl->save();

        $this->assertTrue(!is_null($acl->id));
    }

}
