<div id="addWidget" class="modal fade" role="dialog" data-index="-1">
    <div class="modal-dialog" style="width: 60% !important;  min-width: 60% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    {{__('Widget beállítások')}}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                </button>
            </div>
            <div class="modal-body">
                <form>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="widgetSave">{{__('Mentés')}}</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">{{__('Bezár')}}</button>
            </div>
        </div>
    </div>
</div>