@extends('admin::layouts.layout')
@section('content')
    <?php
    $errors = Session::get('field_errors');
    Session::forget('field_errors');
    ?>
    <div class="row">
        <div class="col-12">
            <div class="m-portlet">
                <div class="m-portlet__body">
                    {!!  \App\Http\Helper::formOpen('pageForm','post',NULL)  !!}
                    <input type="hidden" name="tab" value="data"/>
                    <div class="row">
                        <div class="col-md-6">
                            {!! \App\Http\Helper::input(__('Honnan'),'from_url',old('from_url',$model->from_url),array('class'=>'form-control'),$errors) !!}
                            {!! \App\Http\Helper::textarea(__('Header értékek (soronként)'),'headers',old('headers',$model->headers),array('class'=>'form-control'),$errors) !!}
                        </div>
                        <div class="col-md-6">
                            {!! \App\Http\Helper::input(__('Hova'),'to_url',old('to_url',$model->to_url),array('class'=>'form-control'),$errors) !!}
                            {!! \App\Http\Helper::input(__('HTTP kód'),'code',old('code',$model->code),array('class'=>'form-control'),$errors) !!}
                        </div>
                    </div>


                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions" style="margin-top: 15px">
                            <button class="btn btn-success btn-cons" type="submit"><i
                                        class="fa fa-floppy-o"></i> {{__('Mentés')}}
                            </button>
                            <button class="btn btn-info btn-cons" type="submit"><i
                                        class="fa fa-floppy-o"></i> {{__('Mentés és folytatás')}}
                            </button>
                            <a href="{{session()->get('redirect_url',route('admin_virtual_url_list'))}}"
                               class="btn btn-danger btn-cons"><i class="fa fa-undo"></i> {{__('Mégse')}}</a>

                        </div>
                    </div>
                    {!! \App\Http\Helper::formClose() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
