<?php

namespace Modules\Admin\Entities;

use App\LiquidModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Accesslog extends LiquidModel
{
    use SoftDeletes,Cachable;

    protected $table = '_accesslog';
	protected $created = false;
    protected $dates =['deleted_at','created_at','updated_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'route',
        'action',
        'extra'

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'extra' => 'array',
    ];

    public function __construct($attributes = [])
    {
        config(['laravel-model-caching.cache-prefix' => str_slug(config('app.name'))]);

        parent::__construct($attributes);
    }
}
