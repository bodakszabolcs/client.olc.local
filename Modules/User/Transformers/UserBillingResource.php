<?php

namespace Modules\User\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\User\Entities\UserBilling;
use Modules\User\Entities\UserShipping;

class UserBillingResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'name' => $this->name,
            'country' => $this->country,
            'zip' => $this->zip,
            'city' => $this->city,
            'address' => $this->address,
            'phone' => $this->phone,
            'tax_number' => $this->tax_number,
        ];
    }

}
