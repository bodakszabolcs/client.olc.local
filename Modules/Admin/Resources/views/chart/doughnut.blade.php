
	<script>
	<?php if(empty($colors)) $colors=['#ffb822','#00c5dc','#716aca']; ?>
	<?php


		?>
	if ($("#{{$name}}").length) {
        var pieChart{{$name}} = $("#{{$name}}");

        // line chart data




        // line chart init
        var {{$name}} = new Chartist.Pie("#{{$name}}",  {
                 series:[
					 @foreach($data as $k=> $v)
                     {
                         value:{{$v}}, className:"custom", meta: {
                         color: "{{$colors[$loop->iteration%sizeof($data)]}}"
                     }
                     } @if(!$loop->last) , @endif
					@endforeach


                 ], labels:[
						@foreach($data as $k=> $v)
                      '{{$k}}' @if(!$loop->last) , @endif
						@endforeach
					]
             }
             , {
                 donut: !0, donutWidth: 17, showLabel: !1
             }
         );
		{{$name}}.on("draw", function(e) {
                if("slice"===e.type) {
                    var t=e.element._node.getTotalLength();
                    e.element.attr( {
                            "stroke-dasharray": t+"px "+t+"px"
                        }
                    );
                    var a= {
                            "stroke-dashoffset": {
                                id: "anim"+e.index, dur: 1e3, from: -t+"px", to: "0px", easing: Chartist.Svg.Easing.easeOutQuint, fill: "freeze", stroke: e.meta.color
                            }
                        }
                    ;
                    0!==e.index&&(a["stroke-dashoffset"].begin="anim"+(e.index-1)+".end"), e.element.attr( {
                            "stroke-dashoffset": -t+"px", stroke: e.meta.color
                        }
                    ), e.element.animate(a, !1)
                }
            }
        ),
		{{$name}}.on("created", function() {
		    //console.log(window.__anim21278907124);
                    window.__anim21278907124&&(clearTimeout(window.__anim21278907124), window.__anim21278907124=null), window.__anim21278907124=setTimeout({{$name}}.update.bind({{$name}}), 15e3)

                }
            )



    }

	</script>
