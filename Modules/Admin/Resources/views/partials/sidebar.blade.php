<div id="m_quick_sidebar" class="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light">
    <div class="m-quick-sidebar__content m--hide">
				<span id="m_quick_sidebar_close" class="m-quick-sidebar__close">
					<i class="la la-close"></i>
				</span>
        <ul id="m_quick_sidebar_tabs" class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_quick_sidebar_tabs_messenger" role="tab">
                   {{__('Értesítések')}}
                </a>
            </li>
            <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link " 		data-toggle="tab" href="#m_quick_sidebar_tabs_settings" role="tab">
                    {{__('Beállítások')}}
                </a>
            </li>

        </ul>
        <div class="tab-content">

            <div class="tab-pane active m-scrollable" id="m_quick_sidebar_tabs_messenger" role="tabpanel">
<div class="m-list-timeline">
	<?php /*@foreach(\Modules\Task\Entities\Task::getMyTaskList() as $noti)
		@php
			$users = \Modules\User\Entities\User::getSelectList();
		@endphp
		<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert @if($noti->priority == 3)) alert-danger @elseif($noti->priority==2) alert-warning @else alert-info @endif alert-dismissible fade show" role="alert">
											<div class="m-alert__icon">
												<a class="btn btn-outline-secondary" href="{{route('admin_task_watch',['id'=>$noti->id])}}"><i class="fa		fa-chevron-circle-right fa-2x"></i></a>
												<span></span>
											</div>
											<div class="m-alert__text">
												<strong>
													{{array_get($users,$noti->sender)}} üzenetet küldött
												</strong>
												{!! str_limit($noti->description,30) !!}
											</div>
											<div class="m-alert__close">
												<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
											</div>
										</div>
	@endforeach
			@foreach(\Modules\User\Entities\UserNotification::where('user_id','=',Auth::user()->id)->where('type','=',2)->orderBy('created_at','desc')->get() as $noti)
				@php
					$data = json_decode($noti->message,true);
				@endphp
                    <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible fade show" role="alert">
											<div class="m-alert__icon">
												<a class="btn btn-outline-secondary" href="{{$noti->link}}"><i class="fa		fa-chevron-circle-right fa-2x"></i></a>
												<span></span>
											</div>
											<div class="m-alert__text">
												<strong>
													{{$data['header']}}
												</strong>
												{!!  $data['content'] !!}
											</div>
											<div class="m-alert__close">
												<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
											</div>
										</div>
				@endforeach
				@foreach(\Modules\User\Entities\UserNotification::where('user_id','=',Auth::user()->id)->where('type','=',1)->orderBy('created_at','desc')->get() as $noti)
					@php
						$data = json_decode($noti->message,true);
					@endphp
					<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-info alert-dismissible fade show" role="alert">
											<div class="m-alert__icon">
												<a class="btn btn-outline-secondary" href="{{$noti->link}}"><i class="fa		fa-chevron-circle-right fa-2x"></i></a>
												<span></span>
											</div>
											<div class="m-alert__text">
												<strong>
													{{$data['header']}}
												</strong>
												{!!  $data['content'] !!}
											</div>
											<div class="m-alert__close">
												<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
											</div>
										</div>
				@endforeach
            </div>
            </div>
*/?>
            <div class="tab-pane  m-scrollable" id="m_quick_sidebar_tabs_settings" role="tabpanel">
                <div class="m-list-settings">
                    <div class="m-list-settings__group">
                        <div class="m-list-settings__heading">
                            {{__('General Settings')}}
                        </div>
                        <div class="m-list-settings__item">
									<span class="m-list-settings__item-label">
										{{__('Language')}}
									</span>
                            <?php
                            $language = Cache::tags(['user-settings'])->get(Auth::id() . '-language');
                            ?>
                            <span class="m-list-settings__item-control">
                                        {!! \App\Http\Helper::select("",'language',config('app.languages'),$language) !!}

									</span>
                        </div>
                        <div class="m-list-settings__item">
									<span class="m-list-settings__item-label">
										{{__('Compact Mode')}}
									</span>
                            <?php
                            $mm = \Illuminate\Support\Facades\Cache::tags(['user-settings'])->get(Auth::id().'-compact-mode');
                            ?>
                            <span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" class="compact-switcher" @if ($mm) checked="checked" @endif name="">
												<span></span>
											</label>
										</span>
									</span>
                        </div>
                    </div>
                    <div class="m-list-settings__group">
                        <div class="m-list-settings__heading">
                            {{__('System Settings')}}
                        </div>
                        <div class="m-list-settings__item">
									<span class="m-list-settings__item-label">
										{{__('Maintance Mode')}}
									</span>
                            <?php
                            $mm = $_SESSION['maintenance_mode'];
                            ?>
                            <span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" class="maintenance-switcher" @if ($mm) checked="checked" @endif name="">
												<span></span>
											</label>
										</span>
									</span>
                        </div>
                        <div class="m-list-settings__item">
									<span class="m-list-settings__item-label">
										{{__('Error Reporting')}}
									</span>
                            <?php
                            $mm = \Illuminate\Support\Facades\Cache::get(Auth::id().'-error-reporting');
                            ?>
                            <span class="m-list-settings__item-control">
										<span class="m-switch m-switch--outline m-switch--icon-check m-switch--brand">
											<label>
												<input type="checkbox" class="error-switcher" @if ($mm) checked="checked" @endif name="">
												<span></span>
											</label>
										</span>
									</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
