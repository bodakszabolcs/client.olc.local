@extends('admin::layouts.layout')
@section('content')
    <?php
    $black_logo = DB::table('lq_options')->where('id', 1)->first();
    $page_js = DB::table('lq_options')->where('id', 2)->first();
    $front_page = DB::table('lq_options')->where('id', 4)->first();
    $socials = DB::table('lq_options')->where('lq_key', 'LIKE', '%social%')->get();
    $show_view = DB::table('lq_options')->where('id', 9)->first();

    if ($show_view != null) {
        $show_view = $show_view->lq_value;
    }

    $show_app = DB::table('lq_options')->where('lq_key', 'LIKE', 'show_appicons')->first();

    if ($show_app != null) {
        $show_app = $show_app->lq_value;
    }

    ?>
    <div class="row">
        <div class="col-12">
            <div class="m-portlet">
                {!! App\Http\Helper::formOpen('theme-edit','POST','',array( 'role' => 'form', 'autocomplete' => 'off', 'enctype' => 'multipart/form-data')) !!}
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="control-label">{{__('Analytics kód')}}</label>
                            <textarea class="form-control" name="page_js" rows="10"
                                      style="height: auto !important;">{{$page_js->lq_value}}</textarea>
                        </div>
                    </div>

                </div>
                {!! \App\Http\Helper::formButtons('admin_theme_edit', false, false) !!}
                {!! App\Http\Helper::formClose() !!}
            </div>
        </div>
    </div>
@endsection