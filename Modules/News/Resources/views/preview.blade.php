<?php

$url=(isset($data['link']))?$data['link']:'#';
$shop_name = (isset($data['shop_name']))?$data['shop_name']:'Üzlet neve';
$image2=(isset($data['image']))?$data['image']:'https://source.unsplash.com/random/?tech&size=900x1600';
$text=(isset($data['title']))?$data['title']:'Minden reggel jobban indul egy kávéval';
$description = (isset($data['description']))?$data['description']:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.';


?>
        <!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Template X</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:400&amp;subset=latin-ext" rel="stylesheet">

    <style>
        @font-face {
            font-family: 'SourceSansPro';
            url('/assets/font/source-sans-pro-bold.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;

        }
        font-face {
            font-family: 'SourceSansProRegular';
        url('/assets/font/source-sans-pro-regular.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;

        }
        body {
            margin:0;
            background-color: #ff6100;
            font-family: 'SourceSansProRegular';
            overflow: hidden;

        }

        body a {

            display: block;
            text-decoration: none;
        }
        body > div.img{
            width: calc(100% - 20px);
            height: calc(86vh + 12px);
            padding: 10px;
            color: white;

        }

        h1{
            font-size: 40px !important;
        }
        .success-order {
            font-size: 18px !important;
            text-align: left !important;
            font-weight: 600;
            margin-top: -9px;
            text-transform: none;
            line-height: 1.1;
        }
        .body {
            text-align: justify;
            font-size: 12px;
            margin-top: 10px;
        }
        .header{
            background-color:#181818 ;
            color:white;
            padding:10px 20px;
            text-transform: uppercase;


        }
        .clearfix::after {
            content: "";
            clear: both;
            display: table;
        }
        div.img .success-order{
            font-size: 22px;
            text-align: left;
            margin-bottom: 20px;
        }
    </style>
</head>
<body>

    <div class="header" >

        <div style="width:95%;float: left;font-size: 10px;font-weight:500;">
        {{$shop_name}}
        </div>
        <div style="width:5%;float: right;font-size: 10px">
            X
        </div>
        <div class="clearfix"></div>

    </div>
    <div class="img" style="background: #000 url('@if(strlen($text)>0) {{$image2}} @else {{$image2}} @endif') center center no-repeat; background-size: cover;">
        <div class="">
        <div class="success-order"><h1>{{$text}}</h1></div>
        <div class="body">{{$description}}</div>
        <button style="position: absolute;bottom:10px;left:22%;margin:auto;padding:15px 50px;background-color:#ffa726 !important;border-radius: 25px;color:white;border:none" href ="{{$url}}">Tovább</button>
    </div>

</body>
</html>
