<?php
	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;
	class CreateShopopeningTable extends Migration
	{
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::create('shopopening', function (Blueprint $table) {
				$table->increments("id");
				$table->integer("day")->comment("Nap")->default(0)->unsigned();
				$table->integer("is_closed")->comment("Zárva")->default(0)->unsigned();
				$table->date("open_from")->comment("Tól")->default(null);
				$table->date("open_to")->comment("Ig")->default(null);
				$table->integer("shop_id")->comment("Bolt azonosító")->default(0)->unsigned();
				$table->timestamps();
				$table->softDeletes();
				$table->index(["deleted_at"]);
				$table->timestamps();
			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::dropIfExists('shopopening');
		}
	}
