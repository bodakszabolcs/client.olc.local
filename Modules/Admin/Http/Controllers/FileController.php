<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\View;

class FileController extends AdminController
{
    public function View(Request $request)
    {
        View::share('title', __('Médiatár'));

        return view('admin::file.view');
    }
}
