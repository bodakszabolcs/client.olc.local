@php
	$marklar = \Modules\Content\Entities\Content::find(6);

@endphp
@if(!is_null($marklar))
<section class="hatodik" id="marklar">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<img style="" src="/assets/frontend//img/marklar.png" alt="Marklar" title="Marklar">

				<h3>{{$marklar->title}}</h3>

			</div>
		</div>
		<div class="row">
			{!! $marklar->content !!}
		</div>
		<div class="col-lg-8 col-lg-offset-2 text-center">
			<a href="http://marklar.co.hu/" class="btn btn-lg btn-outline">
				Ajánlatkérés
			</a>
		</div>
	</div>

</section>
@endif