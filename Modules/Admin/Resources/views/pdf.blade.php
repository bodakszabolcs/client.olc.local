<html>
    <head>
        <title></title>

    </head>
    <body>
    <style>
        .step {


            font-size: 20px;
            padding:10px;
            margin:10px;


            border-radius: 14px;

        }
        p{
            color: #626262;
        }
    </style>
    <table class="table-header" style="width:100%">
        <tr>
            <td><img width="150" src="/images/logo.png"></td>
            <td align="right"><h1>OLC - Online hűség kedvezmény</h1>
                <p>weboldal: http://online-loyalty-card.hu</p>
                <p>Telefon: +36 30 8111365</p>
            </td>
        </tr>
    </table>
    <hr>
    <h2>{{$shop->name}}</h2>
    <h3>Hűségét megjutalmazzuk!!</h3>
    <p>Kövesse az alábbi lépéseket a kedvezmény igénybevéeléhez!</p>
    <table class="table" style="width:100%">

            <tr>
            <th style="background: #e8e8e8;background-image-resize:6"> <div class="step">
                    &nbsp;&nbsp;&nbsp; 1. lépés &nbsp;&nbsp;&nbsp;&nbsp;
                </div></th>
            <th style="background: #e8e8e8;background-image-resize:6;padding:10px"> <div class="step"> &nbsp;&nbsp;&nbsp; 2. lépés &nbsp;&nbsp;&nbsp;&nbsp;</div></th>
            <th style="background: #e8e8e8;background-image-resize:6"><div class="step"><span> &nbsp;&nbsp;&nbsp; 3. lépés&nbsp;&nbsp;&nbsp;&nbsp;</span></div></th>
            </tr>
        <tr>
            <td align="center">

                <div>
                    <br>
                <img height="150" src="/images/1-step.png">
                    <br>
                </div>
                <p class="step-description"><br>Indítsa el és lépjen be az OLC alkalmazásba</p>
            </td>
            <td  align="center">

                <div>
                    <br>
                <img height="150" src="/images/2-step.png">

                </div>
                <p class="step-description"><br>Kattintson a képernyő alján lévő QR kód gombra</p>
            </td>
            <td  align="center">

                <div>
                    <br>
                <img height="150" src="/images/3-step.png">
                    <br>
                </div>
                <p class="step-description"><br>Tartsa az alábbi kódot a zöld négyzeten belül</p>
            </td>
        </tr>
    </table>

    <div style="text-align: center">

         <br>
    <img src = "{{url('/api/qr-code/'.$shop->shop_id)}}" width="400">
        <br>

    </div>
    <hr>
    <table class="footer" style="text-align: justify">

        <tr>
            <td colspan="2"><p>Az alkalmazás elérhető az <b>Google play</b> és az <b>App Store</b> árhuzázakban. Az alkalmazés teljesen ingyenes. A részletekért kérjük látogasson el honlapunkra: <i>http://online-loyalty-card.hu</i></p></td>
        </tr>
        <tr>
            <td><img height="50" src="/images/appstore.jpg"></td>
            <td align="right"><img height="50" src="/images/playstore.jpg"></td>
        </tr>
    </table>
    </body>

</html>
