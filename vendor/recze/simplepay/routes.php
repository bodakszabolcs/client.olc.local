<?php

    Route::match(['get', 'post'], '/ipn', 'SimplepayController@Ipn')->name('simple_ipn');
    Route::match(['get', 'post'], '/irn', 'SimplepayController@Irn')->name('simple_irn');
    Route::match(['get', 'post'], '/idn', 'SimplepayController@Idn')->name('simple_idn');
    Route::match(['get', 'post'], '/ios', 'SimplepayController@Ios')->name('simple_ios');
    Route::match(['get', 'post'], '/timeout/{lang?}', 'SimplepayController@TimeOut')->name('simple_timeout');
    Route::match(['get', 'post'], '/backref/{lang?}', 'SimplepayController@BackRef')->name('simple_backref');