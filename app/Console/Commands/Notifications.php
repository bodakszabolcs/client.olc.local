<?php

namespace App\Console\Commands;

use App\Article;
use Illuminate\Support\Facades\DB;
use Modules\NotificationText\Entities\NotificationText;
use Modules\Page\Entities\SiteBuilder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redis;
use Modules\Project\Entities\ProjectTask;
use Modules\Project\Entities\ProjectWorkerStatus;
use Modules\User\Entities\UserNotification;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class Notifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate user notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		Log::info('Notifocation - generate start '.date('Y-m-d H:i:s'));
		//1 státusz értesítések
	    $workerNotification = ProjectWorkerStatus::select(DB::raw('projects.*,user_id,firstname, workers.name as worker_name, workers.id as  worker_id,project_worker_status.id as pws_id'))->leftJoin('workers','workers.id','=','worker_id')->leftJoin('projects','projects.id','=','project_id')->join('users','users.id','=','user_id')->where('notified','=',0)->where('notifycation_date','<>',null)->where('notifycation_date','<=',date('Y-m-d'))->get();
	    foreach ($workerNotification as $notification)
	    {
	    	$userNofitication = new UserNotification();
	    	$userNofitication->user_id = $notification->user_id;
	    	$userNofitication->link = route('admin_project_view',$notification->id);
	    	$message['header']= $notification->firstname.'! '.NotificationText::getRandomText();
	    	$message['content'] = '<a href="'.route('admin_work_edit',$notification->worker_id).'">'.$notification->worker_name.'</a> már régóta ugyanabban a stáruszban a státuszban van! Foglalkozz vele!';
	    	$userNofitication->message = json_encode($message);
	    	$userNofitication->type=1;
	    	$userNofitication->save();
	    	$noty= ProjectWorkerStatus::find($notification->pws_id);
	    	$noty->notified=1;
	    	$noty->save();

	    }
	    //2 projeckt feladatok

	    $projeckTask = ProjectTask::select(DB::raw('project_tasks.*,users.firstname,users.id as user_id,notified'))->disableCache()->leftJoin('projects','projects.id','=','project_id')->leftJoin('users','users.id','=','projects.user_id')->where('project_tasks.deadline','=',date('Y-m-d',strtotime('-2 day')))->where('notified','=',0)->where('completed','=',0)->get();

	    foreach ($projeckTask as $notification)
        {
            $userNofitication = new UserNotification();
            $userNofitication->user_id = $notification->user_id;
            $userNofitication->link = route('admin_project_view',$notification->id);
            $message['header']= $notification->firstname.'! '.NotificationText::getRandomText();
            $message['content'] = 'Közeleg a határidő!'.$notification->description.' '.$notification->deadline;
            $userNofitication->message = json_encode($message);
            $userNofitication->type=2;
            $userNofitication->save();
            $notification->notified=1;
            $notification->save();
        }
		Log::info('Notifications - generate end '.date('Y-m-d H:i:s'));
    }
}
