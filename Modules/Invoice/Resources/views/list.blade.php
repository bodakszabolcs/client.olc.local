@extends('admin::layouts.layout')
@section('content')
    @php
        $status =[
        1 =>"Kiállítva",
        2 =>"Folyamatban",
        3 =>"Hiba a fizetés során",
        4 =>"Hiba a számlagenerálás során",
        5 => "Teljesítve",
        ];
        $payment_method = [
            0=>'Nincs megadva',
            1=>'Bankkártya',
            2=>'Utalás',

        ];

    @endphp
   <div class='row'>
        <div class='col-12'>
            <div class='m-portlet'>

                <div class='m-portlet__body'>
                {!! $filterHeader !!}
               <div class='table-responsive'>
                        <table class='table table-bordered table-hover mb-150'>
                                <thead>
                                <tr>
                                    <th class='sorting' data-column='id'>{{__('ID')}}</th>
                                    		<th class='sorting' data-column='0'>{{__('Tranzakció azonosító')}}</th>
                                    		<th class='sorting' data-column='1'>{{__('Fizetési határidő')}}</th>
                                    		<th class='sorting' data-column='2'>{{__('Bruttó összeg')}}</th>

                                    		<th class='sorting' data-column='4'>{{__('Állapot')}}</th>
                                    		<th class='sorting' data-column='5'>{{__('Fizetési mód')}}</th>
                                    		<th class='sorting' data-column='6'>{{__('Számla')}}</th>
                                            <th width='15%'>{{__('Művelet')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($model as $m)

                                    <tr>
                                        <td>{{$m->id}}</td>
                                        <td>{{$m->transaction_id}}</td>
                                        <td>{{$m->deadline}}</td>
                                        <td>{{$m->final_gross}}</td>
                                        <td>{{array_get($status,$m->status)}}</td>
                                        <td>{{array_get($payment_method,$m->payment_method)}}</td>
                                        <td>@if($m->invoice)<a href="{{$m->invoice}}">Számla letöltése</a>@endif</td>

                                        <td>

                                            {!! \App\Http\Helper::text_button(__('Részletek'),array('route_name'=>'admin_invoice_preview', 'params' => array($m->id)),'fa-info', array('class'=>'dropdown-item')) !!}
                                           @if($m->status != 5 && $m->status != 4 ) <a class="btn btn-info" data-content="Befizetés" data-toggle="m-popover" href="{{env('SERVER_URL')}}/simplepay/{{$m->transaction_id}}"><i class="fa fa-euro"></i> </a>@endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                             {!! $model->appends($_GET)->links() !!}
                        </div>
                </div>
            </div>
        
    </div>
@endsection 
