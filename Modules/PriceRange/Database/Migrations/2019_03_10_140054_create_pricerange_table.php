<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricerangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricerange', function (Blueprint $table) {
            $table->increments("id");
$table->integer("price")->comment(""")->default(0)->unsigned();\n$table->text("percent")->comment("Kedvezmény");
 $table->timestamps();
 $table->softDeletes();
$table->index(["deleted_at"]);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricerange');
    }
}
