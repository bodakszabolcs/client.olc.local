<?php

	namespace Modules\Company\Http\Controllers;

	use Illuminate\Support\Facades\Storage;
	use Modules\Company\Entities\Shop;
	use App\Http\Controllers\Controller;
	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Session;
	use Modules\Company\Entities\ShopOpening;
    use Spatie\ImageOptimizer\Image;
    use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;
    use Validator;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\View;
	use Modules\Admin\Http\Controllers\AdminController;

	class ShopController extends AdminController
	{
		public $niceNames = array(
			'company_id'	=>	'Cég',
			'name'	=>	'Név',
			'email'	=>	'Email',
			'phone'	=>	'Telefonszám',
			'address'	=>	'Cím',
			'longitude'	=>	'Szélességi fok',
			'latitude'	=>	'Hosszúsági fok',
			'webpage'	=>	'Weboldal',
			'facebook'	=>	'Facebook',
			'instragram'	=>	'Instagram',
			'lead'	=>	'Bemutatkozás',
			'cover'	=>	'Boritó',

			);
		public function __construct()
		{
			View::share('sub_title', __('Boltok'));
			View::share('subtitle_link', route('admin_shop_list'));
		}

		public function ShopList(Request $request)
		{
			$model = new Shop();
			$list = $model->searchInModel($request->input());
			$list = $list->orderBy('id', 'asc')->orderBy('id', 'desc')->paginate(50)->withPath($request->path());
			View::share('title', __('Boltok lista'));

			return view('company::shop-list', [
				'model' => $list, 'filterHeader' => $model->drawSearchTableHeader($request)
			]);

		}
		public function Edit(Request $request, $id = 0,$tab='data')
		{
			View::share('title', __('Bolt szerkesztés'));
			$model = Shop::find($id);
			$errors = [];
			$packages =json_decode(file_get_contents(env('SERVER_URL').'/api/packages'),true);
			if (is_null($model)) {
				return redirect(route('admin_shop_list'));
			}
			if ($request->isMethod('post')) {

				$tab = $request->input('tab', 'data');
				switch ($tab) {
					case 'data': {
						$response = $this->validateData();
						break;
					}
					default:
						$response = $this->validateData();
						break;
				}
				if ($response['status'] == 'success') {
					$model->fill($request->all());
					if($request->has('file')) {



						$file = Storage::disk('uploads')->put('uploads/' . $model->id, $request->file('file'));
                        $image_resize = \Intervention\Image\Facades\Image::make($file);
                        $image_resize->resize(600,null,function ($constraint) {
                            $constraint->aspectRatio();
                        })->save();
						ImageOptimizer::optimize(public_path($file));
						$model->cover=url($file);
					}
					$model->save();
					$from= $request->input('from');

					foreach($from as $k =>$v)
					{
						$opening = ShopOpening::firstOrCreate(['shop_id'=>$model->id,'day'=>$k]);

						$opening->is_closed =$request->input('closed.'.$k,0);
						$opening->day =$k;
						$opening->open_from =$request->input('from.'.$k,null);
						$opening->open_to =$request->input('to.'.$k,null);
						$opening->shop_id= $model->id;
						$opening->save();
					}
					$postData = $model->toArray();
					$postData['opening'] = ShopOpening::where('shop_id','=',$model->id)->get()->toArray();
					Shop::updateShopData($postData);


					$request->session()->flash('success_message', __('A mentés sikeres'));

				} else {
					$request->session()->flash('error_message', __('Hiba a mentés során'));
					Session::put('field_errors', $response['cust_errors']);

					return redirect(route('admin_shop_edit', [$id]))->withErrors($response['validator'])->withInput();
				}
				if ($request->has('save_and_exit')) {
					return redirect()->intended(session()->get('redirect_url', route('admin_shop_list')));
				}
				if ($response['status'] == 'success') {

					return redirect()->intended(route('admin_shop_edit', ['id' => $model->id,'tab'=>$tab]));
				}
			}

			return view('company::shop.edit', ['model' => $model, 'id' => $id,'packages'=>$packages, 'tab' => $tab, 'errors' => $errors]);
		}
		public function validateData()
		{

			$rules = [
			'name'	=>	'required',  
			'email'	=>	'required',  
			'phone'	=>	'required',  
			'address'	=>	'required',  
			'longitude'	=>	'required',  
			'latitude'	=>	'required',  

			];
			$validator = Validator::make(Input::all(), $rules);
			$validator->setAttributeNames($this->niceNames);
			$not_valid = $validator->fails();
			if ($not_valid) {
				return ['status' => 'error', 'cust_errors' => $validator->messages()->toArray(), 'validator' => $validator];
			} else {
				return ['status' => 'success', 'message' => 'A mentés sikeres'];
			}
		}




	}
