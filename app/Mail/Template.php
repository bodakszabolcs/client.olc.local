<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Template extends Mailable
{
    use Queueable, SerializesModels;

    public  $user_id = '';
    public  $htmlContent = '';
    public  $unsubscribe = '';
    public  $name = '';
    public  $email = '';
    public  $subject = '';

	/**
	 * Create a new message instance.
	 *
	 * @param string $user_id
	 * @param string $htmlContent
	 * @param string $unsubscribe
	 * @param string $name
	 * @param string $email
	 * @param string $subject
	 */
    public function __construct($user_id = '' ,$htmlContent = '', $unsubscribe = '', $name = 'Bodak Szabolcs',$email = 'bodak.szabolcs@gmail.com',$subject = '')
    {
        $this->user_id = $user_id;
        $this->htmlContent = $htmlContent;
        $this->unsubscribe = $unsubscribe;
        $this->name = $name;
        $this->email = $email;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
	    $this->withSwiftMessage(function (\Swift_Message $message) {
		    $message->getHeaders()
			    ->addTextHeader('Content-Type', 'text/html; charset=utf-8');
	    });
        return $this->markdown('emails.tamp');

    }
}
