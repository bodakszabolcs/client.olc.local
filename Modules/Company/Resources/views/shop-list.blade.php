@extends('admin::layouts.layout')
@section('content')
    @php
        $packages =json_decode(file_get_contents(env('SERVER_URL').'/api/packages'),true);
        $names =[];
        foreach ($packages['packages']  as $p){
            $names[$p['id']]=$p['name'];
        }

    @endphp
   <div class='row'>
        <div class='col-12'>
            <div class='m-portlet'>

                <div class='m-portlet__body'>
                {!! $filterHeader !!}
               <div class='table-responsive'>
                        <table class='table table-bordered table-hover mb-150'>
                                <thead>
                                <tr>

                                    		<th class='sorting' data-column='0'>{{__('Név')}}</th>
                                            <th class='sorting' data-column='4'>{{__('Csomag')}}</th>
                                            <th width='15%'>{{__('Művelet')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($model as $m)

                                    <tr>
                                        <td>{{$m->name}}</td>
                                        <td>{{array_get($names,$m->package_id)}}</td>

                                        <td>{!! \App\Http\Helper::text_button(__('Szerkesztés'),array('route_name'=>'admin_shop_edit', 'params' => array($m->id)),'fa-edit', array('class'=>'dropdown-item')) !!}

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                             {!! $model->appends($_GET)->links() !!}
                        </div>
                </div>
            </div>
        
    </div>
@endsection 