<?php

namespace App\Console\Commands;

use App\Article;
use Illuminate\Support\Facades\DB;
use Modules\NotificationText\Entities\NotificationText;
use Modules\Page\Entities\SiteBuilder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redis;
use Modules\Project\Entities\ProjectTask;
use Modules\Project\Entities\ProjectWorkerStatus;
use Modules\User\Entities\UserNotification;
use Modules\Worker\Entities\Worker;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'teszt';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		Log::info('JOB TEST start '.date('Y-m-d H:i:s'));

    }
}
