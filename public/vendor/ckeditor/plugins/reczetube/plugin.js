(function () {

        var selected_content = 0;

        CKEDITOR.plugins.add('reczetube', {

            init: function (editor) {

                editor.addCommand('reczetube', new CKEDITOR.dialogCommand('reczetube', {
                    allowedContent: true
                }));

                editor.ui.addButton('Youtube', {
                    label: 'Youtube begyazas',
                    command: 'reczetube',
                    toolbar: 'insert',
                    icon: this.path + 'icon.png'
                });

                CKEDITOR.dialog.add('reczetube', function (instance) {
                    var post, height;

                    return {
                        title: 'Youtube beágyazás',
                        minWidth: 510,
                        minHeight: 200,
                        contents:
                            [{
                                id: 'reczetubeEmbedPlugin',
                                expand: true,
                                elements:
                                    [
                                        {
                                            type : 'textarea',
                                            id : 'post_url',
                                            width : '400px',
                                            height: '400px',
                                            label : 'Youtube beágyazó kód',
                                            validate : function () {
                                                if (this.getValue()) {
                                                    post = this.getValue();
                                                }
                                                else {
                                                    alert('Kérem adja meg a youtube beágyazás kódot!');
                                                    return false;
                                                }
                                            }
                                        }
                                    ]

                            }],
                        onOk: function () {
                            var instance = this.getParentEditor();

                        var html = "<div class='embeded'><div class=\"embed-responsive embed-responsive-16by9\">";

                            post = this.getValueOf('reczetubeEmbedPlugin', 'post_url');
                            post = post.replace(/(width=")\d+/, '$1100%');
                            //post = post.replace('width="200"','');
                            post = post.replace('height="150"','');


                            post = post.replace(/&/g, '&amp;');
                            post = post.trim();

                            html += post+'</div></div>';

                            var element = CKEDITOR.dom.element.createFromHtml(html);
                            instance.insertElement(element);


                        }
                    };
                });
            }
        });
    }

)();