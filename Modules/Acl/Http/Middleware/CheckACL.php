<?php

namespace Modules\Acl\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use Nwidart\Modules\Facades\Module;

class CheckACL
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $disabled_modules = Module::getByStatus(0);
        foreach ($disabled_modules as $em)
        {
            if ($em->name == Module::find('acl')->name)
            {
                return $next($request);
            }
        }

        $routeName = Route::getFacadeRoot()->current()->getName();

        if (!Controller::checkAccessForUri($routeName))
        {
            $menu = config('menu');

            $redirect = null;
            foreach ($menu as $item):
                if (!is_array($item['route']) && Controller::checkAccessForUri($item['route'])) {
                    $redirect = $item['route'];
                    break;
                } else if (is_array($item['route']))
                {
                    foreach ($item['route'] as $rt)
                    {
                        if (Controller::checkAccessForUri($rt['route']) && strpos($rt['route'],'list'))
                        {
                            $redirect = $rt['route'];
                            break;
                        }
                    }
                }
            endforeach;

            if ($redirect != null) {
                return redirect()->intended(route($redirect));
            }

            return response()->view('errors.401', [], 401);
        }

        return $next($request);
    }
}
