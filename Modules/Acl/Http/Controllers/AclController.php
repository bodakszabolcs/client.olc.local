<?php

namespace Modules\Acl\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Modules\Acl\Entities\Role;
use Modules\Acl\Entities\RoleAcl;
use Modules\Admin\Http\Controllers\AdminController;

class AclController extends AdminController
{
    public function __construct()
    {
        View::share('sub_title',__('ACL csoportok'));
        View::share('subtitle_link',route('admin_acl_list'));
    }

    public function AclList(Request $request)
    {
        $model = new Role();
        $list=$model->searchInModel($request->input());
        $list = $list->paginate(5)->withPath($request->path());

        View::share('title',__('ACL Lista'));

        return view('acl::list', ['model' => $list, 'filterHeader'=>$model->drawSearchTableHeader($request)]);
    }

    public function Edit(Request $request, $id = 0)
    {
        View::share('title',__('ACL Szerkesztés'));

        $model = Role::find($id);
        if (!is_null($model)) {
            $acls = $model->acls->toArray();
        } else {
            if ($id > 0)
            {
                return redirect(route('admin_acl_list'));
            }
            $model = new Role();
            $acls = array();
        }

        if ($request->isMethod('post')) {
            if ($model->id > 0)
            {
                $rules = [
                    'name' => 'required|unique:roles,name,'.$model->name.',name',
                    'description' => 'required'
                ];
            } else {
                $rules = [
                    'name' => 'required|unique:roles',
                    'description' => 'required'
                ];
            }

            $niceNames = [
              'name' => 'Név',
              'description' => 'Leírás'
            ];

            $validator = Validator::make(Input::all(), $rules);
            $validator->setAttributeNames($niceNames);

            if ($validator->fails()) {
                return redirect(route('admin_acl_edit',[$model->id]))
                    ->withErrors($validator)
                    ->with('errors',$validator->messages()->toArray())
                    ->withInput();
            }
            $model = Role::firstOrCreate(['name' => $request->name, 'description' => $request->description]);
            $model->save();


            foreach ($model->acls as $acl)
            {
                $acl->delete();
            }

            if ($request->has('acl_list') && !empty($request->input('acl_list'))) {
                foreach ($request->input('acl_list') as $al) {
                    $acl = new RoleAcl;
                    $acl->role_id = $model->id;
                    $acl->path = $al;
                    $acl->save();
                }
            }

            $request->session()->flash('success_message',__('success_save'));

            if ($request->has('save_and_exit')) {
                return redirect()->intended(session()->get('redirect_url', route('admin_acl_list')));
            }

            return redirect()->intended(route('admin_acl_edit', [$model->id]));
        }

        $routes = Route::getRoutes();

        return view('acl::edit', ['acl' => $acls, 'model' => $model, 'routes' => $routes]);
    }

    public function Delete(Request $request, $id)
    {
        try {
            if ($id == 1 || $id == 2)
            {
                throw new ModelNotFoundException();
            }
            $model = Role::findOrFail($id);
            $model->delete();

            $request->session()->flash('success_message',__('success_delete'));
        } catch (ModelNotFoundException $e)
        {
            $request->session()->flash('error_message',__('Az elem nem törölhető'));
        }

        return redirect()->back();
    }
}
