<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class ThemeController extends AdminController
{
    public function __construct()
    {
        View::share('sub_title',__('Beállítások'));
        View::share('subtitle_link','#');
    }

    public function Edit(Request $request, $id = 0)
    {

        View::share('title',__('Oldal specifikus beállítások'));

        if ($request->isMethod('post'))
        {

            DB::table('lq_options')
                ->where('id', 2)
                ->update(['lq_value' => $request->input('page_js')]);


            foreach($request->input('socials') as $key => $val)
            {
                DB::table('lq_options')
                    ->where('id', $key)
                    ->update(['lq_value' => $val]);
            }

            $request->session()->flash('success_message',__('success_save'));
            return redirect(route('admin_theme_edit'));
        }

        return view('admin::theme.edit', []);
    }
}
