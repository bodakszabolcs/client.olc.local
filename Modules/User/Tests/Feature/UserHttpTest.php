<?php

namespace Tests;

use Illuminate\Support\Str;
use Modules\User\Entities\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase as BaseTestCase;

class UserHttpTest extends TestCase
{
    protected $password = "";
    protected $user = "";
    protected $last_user = "";

    public function setUp()
    {
        parent::setUp();
        $this->user = User::find(1);
        $this->password = Str::random(10);
    }

    public function testUserHttp()
    {

        $response = $this->actingAs($this->user)
            ->get(route('admin_user_list'))
            ->assertStatus(200);

        $response = $this->actingAs($this->user)
            ->get(route('admin_user_edit'))
            ->assertStatus(200);

        $response = $this->actingAs($this->user)
            ->get(route('admin_user_edit',[1]))
            ->assertStatus(200);

    }

    public function testUserProfile()
    {
        $response = $this->actingAs($this->user)
            ->get(route('admin_profile'))
            ->assertStatus(200);
    }

    public function testForceLogin()
    {
        $response = $this->actingAs($this->user)
            ->get(route('admin_user_force_login',[1]))
            ->assertStatus(302);
    }

    public function testUserHttpEditDelete()
    {
        $response = $this->actingAs($this->user)
            ->post(route('admin_user_edit'),
                [
                    'lastname' => 'Lastname',
                    'firstname' => 'Firstname',
                    'email' => Str::random(10).'@example.com',
                    'password' => $this->password,
                    'password_again' => $this->password
                ])
            ->assertStatus(302);

        $this->last_user = $last_user = User::orderBy('id','DESC')->first();

        $response = $this->actingAs($this->user)
            ->post(route('admin_user_edit',[$last_user->id, 'general']),
                [
                    'lastname' => 'Lastname',
                    'firstname' => 'Firstname',
                    'email' => Str::random(10).'@example.com',
                    'password' => $this->password,
                    'password_again' => $this->password
                ])
            ->assertStatus(302);

        $response = $this->actingAs($this->user)
            ->post(route('admin_user_edit',[$last_user->id, 'general']),
                [
                    'lastname' => 'Lastname',
                    'firstname' => 'Firstname',
                    'email' => Str::random(10).'@example.com'
                ])
            ->assertStatus(302);

        $response = $this->actingAs($this->user)
            ->post(route('admin_user_edit',[$last_user->id, 'permission']),
                [
                    'acl_list' => [
                        1, 2
                    ]
                ])
            ->assertStatus(302);

        $response = $this->actingAs($this->user)
            ->post(route('admin_user_edit',[$last_user->id, 'shipping']),
                [
                    'name' => Str::random(10),
                    'country' => Str::random(10),
                    'zip' => rand(1000,9999),
                    'city' => Str::random(10),
                    'address' => Str::random(10),
                    'phone' => rand(1000000000,9000000000),
                    'note' => Str::random(50)
                ])
            ->assertStatus(302);

        $response = $this->actingAs($this->user)
            ->post(route('admin_user_edit',[$last_user->id, 'billing']),
                [
                    'name' => Str::random(10),
                    'country' => Str::random(10),
                    'zip' => rand(1000,9999),
                    'city' => Str::random(10),
                    'address' => Str::random(10),
                    'tax_number' => Str::random(16)
                ])
            ->assertStatus(302);


        $response = $this->actingAs($this->user)
            ->get(route('admin_user_delete',[$this->last_user->id]))
            ->assertStatus(302);

        $response = $this->actingAs($this->user)
            ->get(route('admin_user_edit',[$this->last_user->id]))
            ->assertRedirect(route('admin_user_list'));
    }

}
