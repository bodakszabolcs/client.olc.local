@php
    get_defined_vars();

@endphp
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>{!! $subject !!}</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<style type="text/css">
			* {
                -ms-text-size-adjust:100%;
                -webkit-text-size-adjust:none;
                -webkit-text-resize:100%;
                text-resize:100%;
            }
            a{
                outline:none;
                color:#40aceb;
                text-decoration:underline;
            }
            a:hover{text-decoration:none !important;}
            .nav a:hover{text-decoration:underline !important;}
            .title a:hover{text-decoration:underline !important;}
            .title-2 a:hover{text-decoration:underline !important;}
            .btn:hover{opacity:0.8;}
            .btn a:hover{text-decoration:none !important;}
            .btn{
                -webkit-transition:all 0.3s ease;
                -moz-transition:all 0.3s ease;
                -ms-transition:all 0.3s ease;
                transition:all 0.3s ease;
            }
            table td {border-collapse: collapse !important;}
            .ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}
            @media only screen and (max-width:500px) {
                table[class="flexible"]{width:100% !important;}
                table[class="center"]{
                    float:none !important;
                    margin:0 auto !important;
                }
                *[class="hide"]{
                    display:none !important;
                    width:0 !important;
                    height:0 !important;
                    padding:0 !important;
                    font-size:0 !important;
                    line-height:0 !important;
                }
                td[class="img-flex"] img{
                    width:100% !important;
                    height:auto !important;
                }
                td[class="aligncenter"]{text-align:center !important;}
                th[class="flex"]{
                    display:block !important;
                    width:100% !important;
                }
                td[class="wrapper"]{padding:0 !important;}
                td[class="holder"]{padding:30px 15px 20px !important;}
                td[class="nav"]{
                    padding:20px 0 0 !important;
                    text-align:center !important;
                }
                td[class="h-auto"]{height:auto !important;}
                td[class="description"]{padding:30px 20px !important;}
                td[class="i-120"] img{
                    width:120px !important;
                    height:auto !important;
                }
                td[class="footer"]{padding:5px 20px 20px !important;}
                td[class="footer"] td[class="aligncenter"]{
                    line-height:25px !important;
                    padding:20px 0 0 !important;
                }
                tr[class="table-holder"]{
                    display:table !important;
                    width:100% !important;
                }
                th[class="thead"]{display:table-header-group !important; width:100% !important;}
                th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}
            }
		</style>
	</head>
	<body style="margin:0; padding:0;" bgcolor="#f5f8fa">
		<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#f5f8fa">
			<!-- fix for gmail -->
			<tr>
				<td class="hide">
					<table width="600" cellpadding="0" cellspacing="0" style="width:600px !important;">
						<tr>
							<td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="wrapper" style="padding:0 10px;">
                    <table data-module="module-1"  width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td data-bgcolor="bg-module" >
                                <table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0" bgcolor="#ef5350" >
                                    <tr>
                                        <td style="padding:10px;">
                                            <table width="100%" cellpadding="0" cellspacing="0" >
                                                <tr>
                                                    <th class="flex" width="300" align="left" style="padding:0;">
                                                        <table class="center" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="line-height:0;">
                                                                    <a target="_blank" style="text-decoration:none;" href="http://www.madswork.hu"><img src="http://admin.madswork.hu/assets/image/logo.png" border="0" style="font:bold 12px/12px Arial, Helvetica, sans-serif; color:#606060;" align="left" vspace="0" hspace="0" width="113"  alt="MADS Work" /></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </th>
                                                    <th class="flex" align="left" style="padding:0;">
                                                        <table class="center" align="right" cellpadding="0" cellspacing="0" >
															<tbody><tr>
																<td class="btn" valign="top" style="line-height:0; padding:3px 0 0;">
																	<a target="_blank" style="text-decoration:none;" href="https://www.facebook.com/madswork/"><img src="{{config('app.frontend')}}/assets/newsletter/images/ico-facebook.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="25" height="25" ></a>
																</td>
																<td width="20"></td>
																<td class="btn" valign="top" style="line-height:0; padding:3px 0 0;">
																	<a target="_blank" style="text-decoration:none;" href="http://wwww.madswork.hu"><img src="{{config('app.frontend')}}/assets/newsletter/images/ico-twitter.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="25" height="25" ></a>
																</td>
																<td width="19"></td>
																<td class="btn" valign="top" style="line-height:0; padding:3px 0 0;">
																	<a target="_blank" style="text-decoration:none;" href="http://wwww.madswork.hu"><img src="{{config('app.frontend')}}/assets/newsletter/images/ico-google-plus.png" border="0" style="font:12px/15px Arial, Helvetica, sans-serif; color:#797c82;" align="left" vspace="0" hspace="0" width="25" height="25" ></a>
																</td>

															</tr>
														</tbody></table>
                                                    </th>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>



                    <table   width="100%" cellpadding="0" cellspacing="0">
					<tbody><tr>
						<td  >
							<table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">

								<tbody><tr>
									<td style="padding:10px">

									{!! preg_replace('/<li class(.*)\></','<',str_replace('</li>','',$content)); !!}
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>

                </td>
            </tr>
            <tr>
				<td style="line-height:0;"><div style="display:none; white-space:nowrap; font:15px/1px courier;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td>
			</tr>
		</table>
			<table data-module="module-5" data-thumb="thumbnails/05.png" width="100%" cellpadding="0" cellspacing="0">
						<tbody><tr>
							<td data-bgcolor="bg-module" bgcolor="#f5f8fa">
								<table class="flexible" width="600" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tbody>
									 <tr>
										<td data-bgcolor="bg-block" class="holder" style="padding:10px" bgcolor="#292c34">
											<table width="100%" cellpadding="0" cellspacing="0">
												<tbody>

												<tr>
													<td style="padding:0 0 20px;">
														<table width="134" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
															<tbody><tr>
																<td data-bgcolor="bg-button" data-size="size button" data-min="10" data-max="16" class="btn" align="center" style="font:12px/14px Arial, Helvetica, sans-serif; color:#292c34; text-transform:uppercase; mso-padding-alt:12px 10px 10px; border-radius:2px;" bgcolor="#f9f9f9">
																	<a target="_blank" style="text-decoration:none; color:#292c34; display:block; padding:12px 10px 10px;" href="http://madswork.hu/kapcsolat">Kapcsolat</a>
																</td>

															</tr>
														</tbody></table>
													</td>
												</tr>
												<tr>
													<td align="center" style="font:12px/14px Arial, Helvetica, sans-serif">

																MADS Marketing, {{date('Y')}}.   All Rights Reserved.
																</td>
												</tr>
												<tr>

												</tr>
											</tbody></table>
										</td>
									</tr>
									<tr><td height="28"></td></tr>
								</tbody></table>
							</td>
						</tr>
					</tbody></table>
	</body>
</html>
