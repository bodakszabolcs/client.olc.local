<?php

	namespace Modules\Company\Entities;

	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Database\Eloquent\SoftDeletes;
	use Mockery\Exception;
	use App\LiquidModel;
	use Sunra\PhpSimple\HtmlDomParser;
	use GeneaLabs\LaravelModelCaching\Traits\Cachable;

	class ShopOpening extends LiquidModel
	{
		use SoftDeletes, Cachable;

		protected $table = 'shop_opening';

		protected $dates = ['deleted_at', 'created_at', 'updated_at'];
		/**
		 * The attributes that are mass assignable.
	 	*
		 * @var array
	 	*/
		protected $fillable = [
			'is_closed', //Zárva 
			'open_from', //Tól 
			'open_to', //Ig 
		];

		public $searchColumns = [
			'id' => 'text',
			'day'	=>	'text', 
			'is_closed'	=>	'text', 
			'open_to'	=>	'text', 
			'shop_id'	=>	'text', 
			];

		public $searchColumnLabels = [
			'id' => 'ID',
			'day'	=>	'Nap', 
			'is_closed'	=>	'Zárva', 
			'open_to'	=>	'Ig', 
			'shop_id'	=>	'Bolt azonosító', 
			];
		protected $casts = [
			];
	}
?>
