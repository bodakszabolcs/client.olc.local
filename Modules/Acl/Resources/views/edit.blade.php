@extends('admin::layouts.layout')
@section('content')
    <div class="m-portlet">
        {!! \App\Http\Helper::formOpen('acl','POST','',array('role' => 'form', 'autocomplete' => 'off', 'class'=>'m-form')) !!}
        <div class="m-portlet__body">
            {!! \App\Http\Helper::input(__('Név'),'name',old('name',$model->name),array('class'=>'form-control'),$errors)!!}
            {!! \App\Http\Helper::input(__('Leírás'),'description',old('description',$model->description),array('class'=>'form-control'), $errors)!!}


            <label class="m-checkbox m-checkbox--square">
                <input id="checkbox_all" type="checkbox"
                       name="check_all" value="1" />
                {{__('Összes kijelölése/törlése')}}
                <span></span>
            </label>


            <div class="row">
                <?php
                $controller = null;
                $first = true;
                ?>
                @foreach($routes as $rt)
                    <?php
                    $checked = false;
                    if (!isset($rt->action['middleware']) || !is_array($rt->action['middleware']) || !in_array('acl', $rt->action['middleware'])) continue;
                    if (!isset($rt->action['controller'])) continue;

                    try {
                        $helper = (explode("@", $rt->action['controller']))[1];
                    } catch (\Exception $e)
                    {
                        continue;
                    }
                    $name = (!empty($rt->action['as'])) ? $rt->action['as'] : $helper;
                    foreach ($acl as $a) {
                        if ($a['path'] == $name) {
                            $checked = true;
                        }
                    }
                    $ctrl = explode("\\", explode("@", $rt->action['controller'])[0]);
                    $ctrl = end($ctrl);

                    ?>
                    @if ($controller != $ctrl)
                        @if (!$first)
            </div>
        </div>
    </div>
    @endif
    <?php $first = false ; ?>
    <div class="col-sm-6 col-md-4">
        <div class="m-form__group form-group">
            <label style="font-weight: bold">
                {{__(''.$ctrl)}}
            </label>
            <div class="m-checkbox-list">
                <?php $controller = $ctrl; ?>
                @endif
                <label class="m-checkbox m-checkbox--square">
                    <input id="checkbox{{$loop->iteration}}" type="checkbox" class="chkacl"
                           name="acl_list[{{$name}}]" value="{{$name}}"
                           @if ($checked) checked="checked" @endif>
                    {{__(''.$name)}}
                    <span></span>
                </label>
                @endforeach
            </div>
        </div>
    </div>
    </div>
    </div>

    {!! \App\Http\Helper::formButtons('admin_acl_list') !!}
    {!! \App\Http\Helper::formClose() !!}
    </div>

    <style>
        .m-form__group {
            margin-top: 20px;
        }
    </style>
@endsection
