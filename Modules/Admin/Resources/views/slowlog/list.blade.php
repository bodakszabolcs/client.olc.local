@extends('admin::layouts.layout')
@section('content')
    <div class="card card-transparent">
        <div class="card-block">
        <div class="row">
           <div class="col-md-2">

           {!! App\Http\Helper::formOpen('file','post') !!}
            @foreach($files as $f)
                <?php $pathinfo = pathinfo((string)$f); ?>
                <button type="submit" name="file" value="{{$f}}" class="btn @if($active_file==$f) btn-success @else btn-danger @endif active form-control">{{ $pathinfo['filename']}}</button>
            @endforeach
           {!! App\Http\Helper::formClose() !!}

           </div>
           <div class="col-md-10">
           <div class="table-responsive">
                <table class="table table-hover  ">
                <thead>
                        <tr>
                            <th>{{__('Futás')}}</th>
                            <th>{{__('Query')}}</th>
                            <th>{{__('Infó')}}</th>

                        </tr>
                        <tr>
                            <th></th>
                            <th>{!! App\Http\Helper::input('','search_query',null, array('form-control')) !!}</th>
                            <th>{!! App\Http\Helper::input('','search_request',null, array('form-control')) !!}</th>

                        </tr>
                        </thead>
                <tbody>
                @foreach(explode('/*==================================================*/',$content) as $c)
                    <?php $data = explode('*/',str_replace('\r\n','',$c));

                    ?>

                    <tr>
                       <?php preg_match('/\[(.*?)\]/',$data[0],$match);
                    ?>
                    <td>@if(isset($match[1])){{$match[1]}}@endif</td>

                    <td>@if(isset($data[1])){{$data[1]}}@endif</td>
                      <td>{{str_replace('/* Origin (request): ','',$data[0])}}</td>
                    </tr>
                @endforeach
                </tbody>
                </table>
           </div>
           </div>
        </div>
        </div>
    </div>
@endsection