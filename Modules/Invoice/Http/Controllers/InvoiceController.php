<?php

	namespace Modules\Invoice\Http\Controllers;

	use Modules\Company\Entities\Shop;
    use Modules\Coupon\Entities\Coupon;
    use Modules\Invoice\Entities\Invoice;
	use App\Http\Controllers\Controller;
	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Session;
	use Validator;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\View;
	use Modules\Admin\Http\Controllers\AdminController;

	class InvoiceController extends AdminController
	{
		public $niceNames = array(
			'transaction_id	=>	Tranzakció azonosító',  
			'deadline	=>	Fizetési határidő',  
			'final_gross	=>	Bruttó összeg',  
			'items	=>	Tételek',  
			'status	=>	Állapot',  
			'payment_method	=>	Fizetési mód',  

			);
		public function __construct()
		{
			View::share('sub_title', __('Számlák'));
			View::share('subtitle_link', route('admin_invoice_list'));
		}

		public function InvoiceList(Request $request)
		{
			$model = new Invoice();
			$list = $model->searchInModel($request->input());
			$list = $list->orderBy('id', 'desc')->paginate(50)->withPath($request->path());
			View::share('title', __('Számlák lista'));

			return view('invoice::list', [
				'model' => $list, 'filterHeader' => $model->drawSearchTableHeader($request)
			]);

		}
		public function Edit(Request $request,$id){
            View::share('title', __('Számla részletek'));

            $model = Invoice::find($id);



            return view('invoice::preview', ['model' => $model, 'id' => $id]);
        }


	}