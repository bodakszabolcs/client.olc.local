function statusChangeCallback(response) {
    console.log('statusChangeCallback');

    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        testAPI();
    } else {
        // The person is not logged into your app or we are unable to tell.
        testAPI();
    }
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
    FB.init({
        appId      : '126681941380470',
        cookie     : true,  // enable cookies to allow the server to access
                            // the session
        xfbml      : true,  // parse social plugins on this page
        version    : 'v2.8' // use graph api version 2.8
    });

    // Now that we've initialized the JavaScript SDK, we call
    // FB.getLoginStatus().  This function gets the state of the
    // person visiting this page and can return one of three states to
    // the callback you provide.  They can be:
    //
    // 1. Logged into your app ('connected')
    // 2. Logged into Facebook, but not your app ('not_authorized')
    // 3. Not logged into Facebook and can't tell if they are logged into
    //    your app or not.
    //
    // These three cases are handled in the callback function.

   /* FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });*/

};
function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me?fields=id,name,last_name,first_name,link, email,picture.width(720).height(720)', function(response) {
       $.ajax({
           method:'post',
           url:'/sorsolas',
           data:{id:response.id,name:response.name,email:response.email},
           success:function(data)
           {

               location.reload();
           }

       })
    });
}

$(function(){
    var items = [ ];
    $('.users').each(function()
    {
       items.push([$(this).val(),$(this).data('vote')]);
    });

    console.log(items);
        var  r = 150, i = 0, cx = 180, cy = 380, x1, x2, y1, y2, index, angle, prev_color;

        // Snap SVG Initialization
        var snap = new Snap('#circle_split');
        snap.attr({
            fill: '#cccccc',
            width: '460',
            height: '600'
        });

        //Background Circle
        snap.circle(cx, cy, r);
        // To set the papers for pushing it into the element
        var papers = snap.selectAll();
        papers.clear();

        var initial_angle = 0, n = 4, actual_angle = 360 / n, rad = Math.PI / 180, sectors, img, j = n;

        for (i = 0; i < n; i++) {
            end_angle = eval(initial_angle + actual_angle);
            popangle = eval(initial_angle + (actual_angle / 2));
            sector_path = sector(cx, cy, r, initial_angle, end_angle, rad);
            j--;

            popangle = sector_path.getPointAtLength().alpha;

            //Triangle Mid Point Concept
            //console.log('x1  '+ x1+'    y1  ' +  y1 +'    x2  ' +  x2 +'  y2  '+ y2);
            // Line of a midpoint formula x+x2/2 , y1+y2/2
            // Center point of y
            var mpx1 = (x1+x2)/2;
            var mpy1 = (y1+y2)/2;

            // Centre point of x
            var mpx2 = (cx+mpx1) / 2;
            var mpy2 = (cy+mpy1) / 2;
            //var cpc = snap.circle(mpx2, mpy2, 2);

            // Text
            img = img_path(mpx2, mpy2, i);

            sectors = snap.group(sector_path, img);
            papers.push(sectors);
            initial_angle = end_angle;
        }
        // Dynamic Sector Split Up
        function sector(cx, cy, r, startAngle, endAngle, rad) {
            x1 = parseFloat(cx + r * Math.cos(-startAngle * rad));
            x2 = parseFloat(cx + r * Math.cos(-endAngle * rad));
            y1 = parseFloat(cy + r * Math.sin(-startAngle * rad));
            y2 = parseFloat(cy + r * Math.sin(-endAngle * rad));
            var flag = (endAngle - startAngle > 180) ? 1 : 0;
            sectorpath = "M "+ cx + " "+ cy + " L"+ x1 +" "+ y1 + " A"+ r +" "+ r +" "+ 0 + " " + flag + " "+ 0 +" "+ x2 +" "+" "+ y2 +"z";
            console.log(j / n);
            return snap.path(sectorpath).attr({
                //fill: '#'+Math.floor(Math.random()*16777215).toString(16),
                fill: 'rgba(34,34,34,'+ (j / n) +')',
                class: 'sectors',
                stroke: "#666"
            });
        }

        function img_path(x, y, i) {
            console.log('..index'+n-i-1);
            var text = snap.text(x, y, items[n-i-1][0]);

            text.attr({
                fill: '#ffffff',
                fontSize: '14px'
            });
            text.transform( 't0'+','+text.getBBox().height/4+'r-'+((i+1)*actual_angle-actual_angle+actual_angle/2)+','+x+','+(y-text.getBBox().height/4));
            return text;
        }

        // Matrix Translate and rotate
        var t = new Snap.Matrix();
        var text = snap.text(10, 80, "Sorsolás");
        text.attr({
            fontSize:50,
            fill: '#000'
        });

        // Grouping an entire sectors and text so that can animate entire circle.
        var g = snap.group(papers);

        // Selector Handle
        var tpx1, tpx2, tpy1, tpy2, tpcx, tpcy;
        tpx1 = cx+25;
        tpx2 = cx-25;
        tpcy = cy-r-10;
        tpy1 = tpcy+25;

        var trianglePoly = tpx1+','+tpcy+' '+cx+','+tpy1+' '+tpx2+','+tpcy;

        var triangle = snap.polyline(trianglePoly);
        triangle.attr({
            id: "pointer",
            fill: "rgba(5,111,113,.9)"
        });

        $(".spin").unbind('click').bind('click', function(e){
            e.preventDefault();
            angle = randomization();
            $('.text').fadeOut();
            g.animate({ transform: 'r'+angle+','+ cx +','+ cy }, 3000, mina.easeinout, function(){
                // Stop Rotate Wheel
                arc = Math.PI / (n/2);
                var sa = angle * Math.PI / 180;
                var degrees = sa * 180 / Math.PI + 90;
                var arcd = arc * 180 / Math.PI;
                index = Math.floor((360 - degrees % 360) / arcd);
                console.log(index);

                prev_color = papers[n-index-1][0].attr('fill');
                papers[n-index-1][0].animate({
                    fill: '#ffffff',
                    stroke: '#ffffff'
                }, 300, mina.easeinout);
                papers[n-index-1][1].animate({
                    fill: '#222222'
                }, 300, mina.easeinout);
                $('.repos').fadeIn();
                items[index][1]=1;
                $.ajax({
                    method:'post',
                    url:'/sorsolas',
                    data:{vote:"save_voting",nev :items[index][0]},
                    success:function(data)
                    {
                        console.log(data);
                        //location.reload();
                    }

                })
            });
        });
        $(".reset").unbind('click').bind('click', function(e){
            e.preventDefault();
            $('.text').fadeIn();
            papers[n-index-1][0].attr({
                fill: prev_color,
                stroke: '#666666'
            });
            papers[n-index-1][1].attr({
                fill: '#ffffff'
            });
            g.transform('r0'+','+cx+','+cy );
            $(".repos").fadeOut();
        });

        // Generating Random Number
        function randomization(){
            // Generating a random integer less than 24 * 15 = 360
            var angle_dev = Math.floor((Math.random() * 24*10 )+1);
            // Generating a random integer between 2 and 8
            var rotate_count = Math.floor((Math.random() * (8 - 2 + 1) + 2));
            var rotate_angle_cal = rotate_count * (angle_dev * 15) + (actual_angle/2);

            while(items[get_index(rotate_angle_cal)][1]==1)
            {
                rotate_angle_cal +=90;
            }
            return rotate_angle_cal;
        }
        function get_index(angle)
        {
            arc = Math.PI / (n/2);
            var sa = angle * Math.PI / 180;
            var degrees = sa * 180 / Math.PI + 90;
            var arcd = arc * 180 / Math.PI;
            var index = Math.floor((360 - degrees % 360) / arcd);
            return index;

        }
    });
function writeTime(){
    $.ajax({
        method:'post',
        url:'/sorsolas',
        data:{settimestamp:"true"},
        success:function(data)
        {
            console.log(data);
            setTimeout(writeTime, 1000);
            //location.reload();
        }


    })
}
setTimeout(writeTime, 1000);