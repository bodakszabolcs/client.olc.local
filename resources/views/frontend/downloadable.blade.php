@extends('frontend.layout')
@section('content')
   <style>img.small{width:50px !important;}</style>
<section id="munkak" class="margin60">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 text-center">
            <h2>{{optional($page)->title}}</h2>
         </div>
         <div class="clearfix"></div>
         <br>
          {!! optional($page)->content !!}
         @php

         @endphp
          <div class="clearfix"></div>
         <table class="table table-condensed">
            @foreach(\Modules\DownloadableContent\Entities\DownloadableContent::all() as $doc)
               <tr>
                  <td>{{$doc->name}}</td>
                  <td><a href="{{$doc->file}}" target="_blank"><img width="50" class="pull-right small " src="/assets/frontend/img/pdf.png" title="{{$doc->name}}" alt="{{$doc->name}}"></a></td>
               </tr>
            @endforeach
         </table>
      </div>
   </div>
</section>

@endsection