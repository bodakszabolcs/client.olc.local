<?php

namespace App;

use App\Http\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Facades\Image;

abstract class LiquidModel extends Model
{
	protected $searchColumns = [];
	protected $searchColumnLabels = [];
	protected $created = true;

	public function save(array $options = [])
	{
		if($this->created){
			if ($this->hasAttribute('created')) {

				$this->updateCreated();
			}
			if ($this->hasAttribute('updated')) {
				$this->updateUpdated();
			}
		}
		parent::save($options = []);

	}

	public function updateCreated()
	{
		if(is_null($this->created) && Auth::user()){

			$this->created= Auth::user()->id;


		}
	}
	public function updateUpdated()
	{

			if( Auth::user()) {
				$this->updated = Auth::user()->id;
			}


	}
	public function hasAttribute($attr)
	{
		return Schema::hasColumn($this->getTable(), $attr);

	}
    public function drawSearchTableHeader($filter)
    {

        $tableHeaderRow = "
        <div class=\"m-portlet m-portlet--primary m-portlet--head-solid-bg m-portlet--head-sm m-portlet--collapse\" m-portlet=\"true\" >
        <form method='get' class='filterTable'>
        <input type='hidden' name='tab' value='" . old('tab', (isset($filter['tab']))?$filter['tab']:'') . "'>
        <input type='hidden' name='direction' value='" . old('direction', (isset($filter['direction']))?$filter['direction']:'') . "'>
		<input type='hidden' name='orderBy' value='" . old('orderBy', (isset($filter['direction']))? $filter['orderBy']:'') . "'>
        <div class=\"m-portlet__head\">
        		<div class=\"m-portlet__head-caption\">
					<div class=\"m-portlet__head-title\">
						<span class=\"m-portlet__head-icon\">
								<i class=\"flaticon-search-2\"></i>
						</span>
						<h3 class=\"m-portlet__head-text d-none d-md-table-cell\">
						" . __('Keresés') . "
						</h3>
					
	
					</div>
				</div>
				<div class=\"m-portlet__head-tools\">
					<ul class=\"m-portlet__nav\">
												<li class=\"m-portlet__nav-item\">
													<input class=\"form-control m-input\" placeholder='" . __('Keresés') . "' type=\"text\" value=\"" . old('search-fillable',  (isset($filter['search-fillable']))?$filter['search-fillable']:'') . "\" name=\"search-fillable\" id=\"example-text-input\">
												</li>
												
					<li  class=\"m-portlet__nav-item\">
					<div class='btn-group' role='group' aria-label='" . __('Keresés') . "'>
						<a href='?tab=".old('tab', (isset($filter['tab']))?$filter['tab']:'')."' class='m-btn btn btn-danger'>
							<i class='fa fa-history'></i>
						</a>
						<button type='submit' class='m-btn btn btn-success'>
							<i class='fa fa-filter'></i>
						</button>
					</div>
					</li>
					</ul>
					
				</div>
		</div>
		<div  class=\"m-portlet__body collapse\" id=\"search-header\">
        <div class='row'>
        ";
        foreach ($this->searchColumns as $key => $col) {
            switch ($col) {
                case 'text' :
                    $tableHeaderRow .= '<div class="col-md-6 col-lg-4 col-xl-2">' . \App\Http\Helper::inputInlineLabel($this->searchColumnLabels[$key], $key, old($key, array_get($filter,$key)), array('class' => 'form-control input-sm')) . '</div>';
                    break;
                case 'interval':
                    $tableHeaderRow .= '<div class="col-md-6 col-lg-4 col-xl-2">' . \App\Http\Helper::inputInlineLabel($this->searchColumnLabels[$key] . ' -tól', $key . '_from', old($key . '_from', array_get($filter,$key . '_from')), array('class' => 'form-control input-sm')) . '</div><div class="col-md-6 col-lg-4 col-xl-2">'
                        . \App\Http\Helper::inputInlineLabel($this->searchColumnLabels[$key] . ' -ig', $key . '_to',
                            old($key . '_to', $filter[$key . '_to']), array('class' => 'form-control input-sm')) . '</div>';
                    break;
                case 'date_interval':
                    $tableHeaderRow .= '<div class="col-md-6 col-lg-4 col-xl-2">' . \App\Http\Helper::inputInlineLabel($this->searchColumnLabels[$key], $key . '_from_to', old($key . '_from_to', array_get($filter, $key . '_from_to', null)), array('class' => 'form-control  daterangepicker-liquid')) . '</div>';
                    break;
                default:
                    if (is_array($col)) {
                        $tableHeaderRow .= '<div class="col-md-6 col-lg-4 col-xl-2">' . \App\Http\Helper::selectInlineLabel($this->searchColumnLabels[$key], $key, $col['select'], old($key, array_get($filter,$key)), array('class'=>'select2')) . '</div>';
                    }
                    break;


            }
        }

        return $tableHeaderRow . "</div></div></form></div>";
    }

    public function searchInModel($filter, $query = false)
    {

        if ($query == false) {
            $class = get_class($this);
            $list = $class::where($class::getTable() . '.id', '>', 0);
        } else {

            $list = $query;
        }
        $array_search = $this->searchColumns;
        if (isset($filter['search-fillable']) && !empty($filter['search-fillable'])) {
            $where = "";
            foreach ($this->searchColumns as $k => $v) {
                if (isset($this->extraColumns[$k])) {
                    $where .= $this->extraColumns[$k] . " LIKE '%" . $filter['search-fillable'] . "%' or ";
                } else {
                    $where .= $k . " LIKE '%" . $filter['search-fillable'] . "%' or ";
                }

            }
            if (!empty($where)) {
                $list = $list->whereRaw(DB::raw('(' . substr($where, 0, strlen($where) - 3)) . ')');
            }
        }
        foreach ($filter as $key => $value) {

            $is_interval = false;

            if (strpos($key, '_from_to') !== false) {

                $is_interval = true;

            }
            if ($is_interval) {
                $actual_key = str_replace('_from_to', '', $key);


                $from_to = explode(" - ", $filter[$actual_key . '_from_to']);
                $from = $from_to[0];
                if (isset($from_to[1])) {
                    $to = $from_to[1];
                }

                if (isset($from) && !empty($from) && $from != $to) {
                    if (isset($this->extraColumns[$actual_key])) {
                        $list = $list->where(DB::raw($this->extraColumns[$actual_key]), '>=', $from);
                    } else {
                        $list = $list->where($actual_key, '>=', $from);
                    }

                }
                if (isset($to) && !empty($to) && $from != $to) {
                    if (isset($this->extraColumns[$actual_key])) {
                        $list = $list->where(DB::raw($this->extraColumns[$actual_key]), '<=', $to);
                    } else {


                        $list = $list->where($actual_key, '<=', $to);
                    }

                }
                unset($array_search[$actual_key]);

            } else {

                if (!empty($value) && $value !== '-' && array_key_exists($key, $this->searchColumns)) {
                    if (isset($this->extraColumns[$key])) {
                        $list = $list->where(DB::raw($this->extraColumns[$key]), 'LIKE', '%' . $value . '%');
                    } else {
                        $list = $list->where($key, 'LIKE', '%' . $value . '%');
                    }
                }
            }


        }
        if (!empty($filter['direction']) && !empty($filter['orderBy'])) {
            if (isset($this->extraColumns[$filter['orderBy']])) {
                $list = $list->orderBy(DB::raw($this->extraColumns[$filter['orderBy']]), $filter['direction']);
            } else {
                $list = $list->orderBy($filter['orderBy'], $filter['direction']);
            }
        }

        return $list;

    }


    protected function getMetaImageSize()
    {

        if ($this->og_image != '' && file_exists(public_path($this->lead_image))) {
            $img = Image::make(public_path($this->og_image));
            $width = $img->width();
            $height = $img->height();

            return array('width' => $width, 'height' => $height);
        }

        if ($this->lead_image != '' && file_exists(public_path($this->lead_image))) {

            $img = Image::make(public_path($this->lead_image));
            $width = $img->width();
            $height = $img->height();

            return array('width' => $width, 'height' => $height);
        }

        try {
            $img = Image::make(public_path('/assets/frontend/img/mads_logo.jpg'));
            $width = $img->width();
            $height = $img->height();
        } catch (NotReadableException $e) {
            $img = Image::make(public_path('/assets/frontend/img/mads_logo.jpg'));
            $width = $img->width();
            $height = $img->height();
        }

        return array('width' => $width, 'height' => $height);
    }
}
