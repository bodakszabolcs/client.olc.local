<?php

namespace App\Contracts;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

interface BaseControllerInterface
{
    public function index(Request $request, $extraSearch);
    public function __create(FormRequest $request);
    public function show(Request $request, $id, $auth);
    public function __update(FormRequest $request, $id, $auth);
    public function destroy($id, $auth);
}
