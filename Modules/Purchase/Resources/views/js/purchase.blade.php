<script>
	console.log('futok');
	var lastUpdate = 0;
	var actualData = [];
	setInterval(getHasUpdate,1000);
	function getHasUpdate(){
        var time=new Date().getTime();
	    $.get('/app/updated{{\Illuminate\Support\Facades\Auth::user()->shop_id}}.json?v='+time,function(data){
	        if(data.updated > lastUpdate){
	            refreshData();
	            lastUpdate = data.updated;
			}
		})
	}
	function refreshData(){
        var time=new Date().getTime();
        $.get('/app/purchase{{\Illuminate\Support\Facades\Auth::user()->shop_id}}.json?v='+time,function(data){
            actualData = data;

            $.post('{{route('admin_purchase_get_customers')}}',{data:actualData},function (data) {
				$('.customer-list').html(data);
				if(!hasUserProfile()){
				    getUserProfile($('#customerPurchase li .active').data('id'));
				}
            })
        });
	}
	function hasUserProfile(){
	    if($('.customer-profile').html()){
	        return false;
		}
	    has=false;
	    actualData.forEach(item=>{
	       if(item.id == $('.customer-profile').data('id')){
	           has=true
		   }
		});
	    return has;
	}
	function getUserProfile(id){
	    var selectedCustomer;
        actualData.forEach(item=>{
            if(item.id == id){
                selectedCustomer = item;
            }
        });
        $.post('{{route('admin_purchase_get_customer_profile')}}',{data:selectedCustomer},function (data) {
            $('.customer-profile').html(data);

        })
	}
    $(document).on('click','#customerPurchase a',function(){
       getUserProfile($(this).attr('data-id'));
    })

</script>