<script type="text/javascript">

    var toggled = 0;

    var toggleAll = function () {
        $("#checkbox_all").on('click', function() {
           if (toggled == 0)
           {
               $(".chkacl").prop('checked', true)
               toggled = 1;
           } else {
               $(".chkacl").prop('checked', false)
               toggled = 0;
           }
        });
    };

    $(document).ready(function (event) {

        toggleAll();

    });
</script>