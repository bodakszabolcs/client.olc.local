@extends('admin::layouts.layout')
@section('content')
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            <div class="m-portlet m-portlet--full-height  ">
                <div class="m-portlet__body">
                    <div class="m-card-profile">
                        <div class="m-card-profile__title m--hide">
                            {{__('Profil szerkesztése')}}
                        </div>
                        <div class="m-card-profile__pic">
                            <div class="m-card-profile__pic-wrapper">
                                <img src="{{$model->getAvatar()}}" alt=""/>
                            </div>
                        </div>
                        <div class="m-card-profile__details">
												<span class="m-card-profile__name">
													{{$model->getName()}}
												</span>
                            <a href="mailto:{{$model->email}}" class="m-card-profile__email m-link">
                                {{$model->email}}
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                            role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1"
                                   role="tab">
                                    <i class="flaticon-share m--hide"></i>
                                    {{__('Alapadatok')}}
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="m_user_profile_tab_1">
                        {!! \App\Http\Helper::formOpen('userForm','POST','',array('class'=>'m-form m-form--fit m-form--label-align-right')) !!}
                        <input type="hidden" name="tab" value="general"/>
                        <div class="m-portlet__body">
                            <div class="col-md-12">
                                    <div class="row">


                                    <h5 class="col-md-12"></br>
                                        {{__('Felhasználó adatok')}}
                                        <br><br>
                                    </h5>

                                    </div>
                                    </div>
                            {!! \App\Http\Helper::input(__('Vezetéknév'),'lastname',old('lastname',$model->lastname),array('class'=>'form-control m-input', 'div_class'=>'form-group m-form__group row', 'label_class'=>'col-2 col-form-label'),$errors)!!}
                            {!! \App\Http\Helper::input(__('Keresztnév'),'firstname',old('firstname',$model->firstname),array('class'=>'form-control m-input', 'div_class'=>'form-group m-form__group row', 'label_class'=>'col-2 col-form-label'),$errors)!!}
                            {!! \App\Http\Helper::input(__('Email'),'email',old('email',$model->email),array('class'=>'form-control m-input', 'div_class'=>'form-group m-form__group row', 'label_class'=>'col-2 col-form-label'),$errors)!!}
                            {!! \App\Http\Helper::select(__('Jogosultság'),'role',['5'=>'Tulajdonos','4'=>'Boltos'], old('role',$model->role), array('class'=>'form-control m-input', 'div_class'=>'form-group m-form__group row', 'label_class'=>'col-2 col-form-label'),$errors)!!}
                            {!! \App\Http\Helper::select(__('Bolt kiválasztása'),'shop_id',\Modules\Company\Entities\Shop::getShopsShopId(), old('shop_id',$model->shop_id), array('class'=>'form-control m-input', 'div_class'=>'form-group m-form__group row', 'label_class'=>'col-2 col-form-label'),$errors)!!}
                            {!! \App\Http\Helper::password(__('Jelszó'),'password',old('password',$model->password),array('class'=>'form-control m-input', 'div_class'=>'form-group m-form__group row', 'label_class'=>'col-2 col-form-label'),'',$errors)!!}
                            {!! \App\Http\Helper::password(__('Jelszó újra'),'password_again',old('password_again',$model->password_again),array('class'=>'form-control m-input', 'div_class'=>'form-group m-form__group row', 'label_class'=>'col-2 col-form-label'),'',$errors)!!}
                            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>


                        </div>
                        {!! \App\Http\Helper::formButtons('admin_user_list') !!}
                        {!! \App\Http\Helper::formClose() !!}
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function(){
            $('#form-role').change(function(){
                console.log($(this).val());
                if($(this).val() == "5"){
                    $('#form-shop_id').closest('.form-group').hide();
                }
                else{
                    $('#form-shop_id').closest('.form-group').show();
                }
            }).change();
        });
    </script>
@endpush