@extends('admin::layouts.layout')
@section('content')
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid m-error-4" style="background-image: url(/assets/app/media/img//error/bg4.jpg);">
            <div class="m-error_container">
                <h1 class="m-error_number">
                    404
                </h1>
                <p class="m-error_title">
                    {{__('Hiba!')}}
                </p>
                <p class="m-error_description">
                    {{__('Az oldal nem található!')}}
                </p>
            </div>
        </div>
    </div>
@endsection
