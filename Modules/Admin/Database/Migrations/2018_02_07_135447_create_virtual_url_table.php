<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVirtualUrlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);

        Schema::create('virtual_url', function (Blueprint $table) {
            $table->increments('id');
            $table->string('from_url')->comment('Kezdő url');
            $table->string('to_url')->comment('Cél url');
            $table->text('headers')->comment('Headerek');
            $table->string('code')->comment('Kód');
            $table->softDeletes();
            $table->timestamps();
            $table->index(['deleted_at','from_url']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('virtual_url');
    }
}
