<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\User\Entities\User;

class RequestEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $to,$title, $subject, $reply, $sender,$content,$hash;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject,$sender,$reply,$to,$content)
    {
        $this->to= $to;
        $this->sender = $sender;
        $this->reply = $reply;
        $this->subject = $subject;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $markdown = $this->markdown('emails.admin.request',['subject'=>$this->subject,'content'=>$this->content])
	        ->from($this->sender->email,$this->sender->name)
            ->subject($this->subject)
            ->to($this->to->email,$this->to->name)
            ->replyTo($this->reply->email,$this->reply->name);
	    return $markdown;
    }
}
