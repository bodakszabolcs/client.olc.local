
<div class="col-sm-3 hidden-sm">
	<div id="sidebar-wrapper hidden-sm">
		<nav class="navbar navbar-default  category-list " role="navigation">
			<div class="navbar-offcanvas navbar-offcanvas-touch  category_block" id="js-bootstrap-offcanvas" >
					<h3>Kategóriák</h3>
					<hr/>
					<ul class= "sidebar-nav list-group"  >

						@foreach($categories as $c)
							@php

 							$s = "/munkak".((!empty($headCat))?'/'.$headCat->slug:'')."/$c->slug";
							@endphp
							<li class="col-xs-12 list-group-item @if(strpos(url()->current(),$s) !== false ) active @endif"><a class="" href="{{$s}}">{{$c->name}}</a></li>
						@endforeach
					</ul>

					<div class="clearfix"></div>
					<hr/>
					@if(!is_null($subCat))
					<div style="background: #fff   url('{{$subCat->image}}')   center center no-repeat; background-size: cover;height:300px"></div>


					@elseif(!is_null($headCat))
					<div style="background: #fff   url('{{$headCat->name}}')   center center no-repeat; background-size: cover;height:300px"></div>

					@endif

				</div>
		</nav>
	</div>
</div>