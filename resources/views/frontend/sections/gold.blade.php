@php
	$gold = \Modules\Content\Entities\Content::find(14);

@endphp
@if(!is_null($gold))
<section class="otodik" id="gold">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<img style="" src="/assets/frontend//img/madsgold_logo.png" title="diákmunka" alt="diákmunka">
				<h3>{{$gold->title}}</h3>
			</div>
		</div>
		<div class="row">
			{!! $gold->content !!}
		</div>
		<div class="col-lg-8 col-lg-offset-2 text-center">
			<a href="https://www.madsgold.hu" class="btn btn-lg btn-outline">
				Ajánlatkérés
			</a>
		</div>
	</div>

</section>
@endif