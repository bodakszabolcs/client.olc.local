@extends('admin::layouts.layout')
@section('content')
	<?php
	$errors = Session::get('field_errors');
	Session::forget('field_errors');
	?>
	<div class = 'row'>
		<div class = 'col-md-12'>
			<div class = 'm-portlet'>
				<div class = 'm-portlet__body'>
					@if(\Illuminate\Support\Facades\Auth::user()->id == 1)
						<a href="{{route('admin_company_sync')}}" class="btn btn-primary"><i class="fa fa-refresh"></i> Cég és Bolt adatok letöltése</a>
					@endif
					<input type = 'hidden' name = 'tab' value = 'data'>
					<div class = 'row'>
						<div class = 'col-md-12'>
							{!!  \App\Http\Helper::formOpen('CompanyForm','post',NULL,['class'=>'form '])  !!}
							@if(!is_null($model))
								<div class = 'm-portlet__body'>
								<!--begin::Widget 29-->
								<div class = 'm-widget29'>
									<div class = 'm-widget_content'>
										<div class = 'm-widget_content-items'>
											<div class = 'row'>
												<h5 class = "col-md-12"><br>Cég adatok
													<br>
													<br>
												</h5>

													<div class = "col-md-6"> {!! \App\Http\Helper::input(trans('Cég neve'),'name',old('name',$model->name),array('class'=>'form-control'),$errors) !!}
													</div>
												<div class = "col-md-6">{!! \App\Http\Helper::input(trans('Telefonszám'),'phone',old('phone',$model->phone),array('class'=>'form-control'),$errors) !!}
												</div>

												<h5 class = "col-md-12"><br>Számlázási adatok
													<br>
													<br>
												</h5>
												<div class = "col-md-6">                    {!! \App\Http\Helper::select(trans('Ország'),'shipping_country',config('country'), old('shipping_country',$model->shipping_country),array('class'=>'form-control'),$errors) !!}
												</div>
												<div class = "col-md-6">                    {!! \App\Http\Helper::input(trans('Város'),'shipping_city',old('shipping_city',$model->shipping_city),array('class'=>'form-control'),$errors) !!}
												</div>
												<div class = "col-md-6">                    {!! \App\Http\Helper::input(trans('Irányítószám'),'shipping_zip',old('shipping_zip',$model->shipping_zip),array('class'=>'form-control'),$errors) !!}
												</div>
												<div class = "col-md-6">                    {!! \App\Http\Helper::input(trans('Cím'),'shipping_address',old('shipping_address',$model->shipping_address),array('class'=>'form-control'),$errors) !!}
												</div>
													<div class = "col-md-6">                    {!! \App\Http\Helper::input(trans('Adószám'),'tax_number',old('tax_number',$model->tax_number),array('class'=>'form-control'),$errors) !!}
													</div>
												<div class = "col-md-6">                    {!! \App\Http\Helper::input(trans('Email'),'email',old('email',$model->email),array('class'=>'form-control'),$errors) !!}
												</div>

											</div>
										</div>
									</div>
									{!! \App\Http\Helper::formButtons('admin_dashboard',false,true) !!}
								</div>
								{!! \App\Http\Helper::formClose() !!}

							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>



@endsection 
