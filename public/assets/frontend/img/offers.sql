-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2018. Nov 25. 18:52
-- Kiszolgáló verziója: 10.1.28-MariaDB
-- PHP verzió: 7.1.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `cukraszda`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `offers`
--

CREATE TABLE `offers` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'tipus',
  `name` text NOT NULL COMMENT 'Cég neve',
  `address` text NOT NULL COMMENT 'Cég címe',
  `contact_name` text NOT NULL COMMENT 'Kapcsolattartó neve',
  `contact_phone` text NOT NULL COMMENT 'Telefon',
  `contact_email` text NOT NULL COMMENT 'Kapcsolattartó email címe',
  `work_type` text NOT NULL COMMENT 'Feladat típusa',
  `work_place` text NOT NULL COMMENT 'Munkavégzés helye',
  `time` text NOT NULL COMMENT 'Megbízás időtartama',
  `work_time` text NOT NULL COMMENT 'Munkarend',
  `description` text NOT NULL COMMENT 'Leírás',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `offers`
--

INSERT INTO `offers` (`id`, `type`, `name`, `address`, `contact_name`, `contact_phone`, `contact_email`, `work_type`, `work_place`, `time`, `work_time`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 0, '\"Beverly Hills\"', '\"5205 Beverly Dr\"', '\"Beverly Hills\"', '\"3105555205\"', '\"bhills_5205@mailinator.com\"', '\"Gtapf\"', '\"Woomc\"', '\"Rgjdl\"', '\"Ceenr\"', '\"Yftasv lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\"', NULL, '2018-11-25 08:24:21', '2018-11-25 08:24:21');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offers_deleted_at_index` (`deleted_at`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
