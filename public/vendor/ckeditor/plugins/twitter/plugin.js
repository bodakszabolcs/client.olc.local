(function () {

        var selected_content = 0;

        CKEDITOR.plugins.add('twitter', {
            init: function (editor) {

                editor.addCommand('twitter', new CKEDITOR.dialogCommand('twitter', {
                    allowedContent: true
                }));

                editor.ui.addButton('Twitter', {
                    label: 'Twitter begyazas',
                    command: 'twitter',
                    toolbar: 'insert',
                    icon: this.path + 'icon.png'
                });

                CKEDITOR.dialog.add('twitter', function (instance) {
                    var post, height;

                    return {
                        title: 'Twitter beágyazás',
                        minWidth: 510,
                        minHeight: 200,
                        contents:
                            [{
                                id: 'twitterEmbedPlugin',
                                expand: true,
                                elements:
                                    [
                                        {
                                            type : 'textarea',
                                            id : 'post_url',
                                            width : '400px',
                                            height: '400px',
                                            label : 'Twitter beágyazó kód',
                                            validate : function () {
                                                if (this.getValue()) {
                                                    post = this.getValue();
                                                }
                                                else {
                                                    alert('Kérem adja meg a twitter beágyazás kódot!');
                                                    return false;
                                                }
                                            }
                                        }
                                    ]

                            }],
                        onOk: function () {
                            var instance = this.getParentEditor();
                            post = this.getValueOf('twitterEmbedPlugin', 'post_url');


                            post = post.replace(/&/g, '&amp;');
                            post = post.trim();
                            post = '<div class="embeded">'+post+'</div>';

                            var element = CKEDITOR.dom.element.createFromHtml(post);
                            instance.insertElement(element);


                        }
                    };
                });
            }
        });
    }

)();