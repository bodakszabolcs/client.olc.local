<?php

	namespace Modules\Purchase\Http\Controllers;

	use App\Http\Helper;
    use GuzzleHttp\Client;
    use GuzzleHttp\RequestOptions;
    use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\Log;
	use LaravelFCM\Facades\FCM;
	use LaravelFCM\Message\OptionsBuilder;
	use LaravelFCM\Message\PayloadDataBuilder;
	use LaravelFCM\Message\PayloadNotificationBuilder;
    use Mockery\Exception;
    use Modules\Coupon\Entities\Coupon;
    use Modules\PriceRange\Entities\PriceRange;
    use Modules\Purchase\Entities\Purchase;
	use App\Http\Controllers\Controller;
	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Session;
	use Validator;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\View;
	use Modules\Admin\Http\Controllers\AdminController;

	class PurchaseController extends AdminController
	{
		public $niceNames = array(
			'user_id	=>	Felhasználó',  
			'user_name	=>	Felhasználónév',  
			'price	=>	Vásárlás összege',  
			'discount	=>	Kedvezmény',  

			);
		public function __construct()
		{
			View::share('sub_title', __('Vásárlások'));
			View::share('subtitle_link', route('admin_purchase_list'));
		}

		public function PurchaseList(Request $request)
		{
			$model = new Purchase();
			$list = $model->searchInModel($request->input());
			$list = $list->orderBy('id', 'desc')->paginate(50)->withPath($request->path());
			View::share('title', __('Vásárlások lista'));

			return view('purchase::list', [
				'model' => $list, 'filterHeader' => $model->drawSearchTableHeader($request)
			]);

		}
		public function GetCustomers(Request $request){

			return view('purchase::customers',['customers'=>$request->all()])->render();
		}
		public function Save(Request $request){
				$purchase = Purchase::find($request->input('id'));
				if(!is_null($purchase)) {
					$purchase->price = $request->input('price');
					$purchase->discount = $request->input('discount_price');
					$purchase->handled = 1;
					Coupon::useCoupon($purchase->user_id, $purchase->coupon);
					$purchase->save();
					$this->sendNotification($purchase->token,$purchase);
                    $client = new Client();
                    try {
                        $userData = [
                          'user_id' => $purchase->user_id,
                          'shop_id' => $purchase->shop_id,
                          'price_range'=>Purchase::calculatePriceRange($purchase->user_id),
                          'price'=> Purchase::getLast30Day($purchase->user_id),
                          'discount'=> Purchase::getAllDiscount30Day($purchase->user_id)
                        ];
                        $postData = Helper::encodeData($userData, 'purchase');
                        $client->post(env('SERVER_URL') . '/api/update-user-data', [
                            RequestOptions::JSON =>
                                ['data' => $postData]
                        ],
                            ['Content-Type' => 'application/json']);
                    }
                    catch(Exception $e){
                        Log::info($e->getMessage());
                    }
				}

				file_put_contents('app/purchase'.$purchase->shop_id.'.json', json_encode(Purchase::where('handled', '=',0)->where('shop_id','=',$purchase->shop_id)->get()->toArray()));
				file_put_contents('app/updated'.$purchase->shop_id.'.json', json_encode(['updated' => time()]));
				return response()->json(['status'=>'OK','message'=>'A mentés sikerse'],200);

		}
		public function GetCustomerProfile(Request $request){

			return view('purchase::customer_profile',['customer'=>$request->all()])->render();
		}
		public function Edit(Request $request, $id = 0,$tab='data')
		{
			View::share('title', __('Vásárlások szerkesztés'));

			$model = Purchase::find($id);
			$errors = [];
			if (!is_null($model)) {

			} else {
				if ($id > 0) {
					return redirect(route('admin_purchase_list'));
				}
				$model = new Purchase();
			}
			if ($request->isMethod('post')) {

				$tab = $request->input('tab', 'data');
				switch ($tab) {
					case 'data': {
						$response = $this->validateData();
						break;
					}
					default:
						$response = $this->validateData();
						break;
				}
				if ($response['status'] == 'success') {
					$model->fill($request->all());
					$model->save();
						$request->session()->flash('success_message', __('A mentés sikeres'));
				} else {
					$request->session()->flash('error_message', __('Hiba a mentés során'));
					Session::put('field_errors', $response['cust_errors']);

					return redirect(route('admin_purchase_edit', [$id, $tab]))->withErrors($response['validator'])->withInput();
				}
				if ($request->has('save_and_exit')) {
					return redirect()->intended(session()->get('redirect_url', route('admin_purchase_list')));
				}
				if ($response['status'] == 'success') {

					return redirect()->intended(route('admin_purchase_edit', ['id' => $model->id,'tab'=>$tab]));
				}
			}

			return view('purchase::edit', ['model' => $model, 'id' => $id, 'tab' => $tab, 'errors' => $errors]);
		}
		public function validateData()
		{

			$rules = [
			'user_id'	=>	'required',  
			'user_name'	=>	'required',  
			'price'	=>	'required',  
			'discount'	=>	'required',  

			];
			$validator = Validator::make(Input::all(), $rules);
			$validator->setAttributeNames($this->niceNames);
			$not_valid = $validator->fails();
			if ($not_valid) {
				return ['status' => 'error', 'cust_errors' => $validator->messages()->toArray(), 'validator' => $validator];
			} else {
				return ['status' => 'success', 'message' => 'A mentés sikeres'];
			}
		}

		public function Delete(Request $request)
		{
            $id = Purchase::find($request->input('id'));
			try {
				if ($id == 0) {
					throw new ModelNotFoundException();
				}
				$model = Purchase::findOrFail($id);
				$model->delete();


                file_put_contents('app/purchase'.$model->shop_id.'.json', json_encode(Purchase::where('handled', '=',0)->where('shop_id','=',$model->shop_id)->get()->toArray()));
                file_put_contents('app/updated'.$model->shop_id.'.json', json_encode(['updated' => time()]));
                return response()->json(['status'=>'OK','message'=>'A törlés sikerse'],200);

			} catch (ModelNotFoundException $e) {
				$request->session()->flash('error_message', __('Az elem nem törölhető'));
			}
			return redirect()->intended(route('admin_purchase_list'));
		}
		public function sendNotification($token, $purchase){
				//$token ='eYdx9R10AuE:APA91bFJpY9rxn_WV_1-zqEXNKtEVXZesBjWkRtUlHU7CC5OBh4nPPduW2MV7EX4amLXThuXSn4BoC_UQRiLcVDWoms8NR_xjgUVtP3NOXYQuYa-vfcbOa9ZxHzz7V8Sg9CB-kI9sIU5';
				$optionBuilder = new OptionsBuilder();
				$optionBuilder->setTimeToLive(60 * 20);
				$notificationBuilder = new PayloadNotificationBuilder('Köszönjük vásárlásodat');
                $notificationBuilder->setClickAction('FCM_PLUGIN_ACTIVITY');
				$notificationBuilder->setBody('Vásárlás visszajelzés')
					->setSound('default');
				$dataBuilder = new PayloadDataBuilder();
				$dataBuilder->addData([
				    'type' => 'order','title'=>'Köszönjük vásárlásodat',
                    'body'=>'A legutóbbi vásárlásod:',
                    'price'=> Helper::price($purchase->price).' Ft',
                    'info'=> 'Jelnlegi kedvezményi szinted',
                    'discount' =>Purchase::calculatePriceRange($purchase->user_id),
                    'next'=> 'A következő szin elérésehez '.Purchase::calculateNextLevel($purchase->user_id).' ft vásárlás szükséges',
                    'data'=> PriceRange::orderBy('price','asc')->select(DB::raw('price,percent'))->get()->toArray(),

                ]);
				$option = $optionBuilder->build();
				$notification = $notificationBuilder->build();
				$data = $dataBuilder->build();
				$downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
				$downstreamResponse->numberSuccess();
				$downstreamResponse->numberFailure();
				$downstreamResponse->numberModification();

		}

	}
