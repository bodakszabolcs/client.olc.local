<?php 
Route::group(['namespace' => 'Modules\Invoice\Http\Controllers', 'prefix' => 'admin/invoice', 'middleware' => ['web','auth', 'acl', 'accesslog']], function () { 
		Route::match(['get'], '/list', 'InvoiceController@InvoiceList')->name('admin_invoice_list');
		Route::match(['get'], '/preview/{id}', 'InvoiceController@Edit')->name('admin_invoice_preview');
});
