@extends('admin::layouts.layout')
@section('content')
	@php
		$type=[
			0=>'Fix összeg',
			1=>'Százalékos kedvezmény'
		];
	@endphp
	<div class = 'row'>
        <div class = 'col-12'>
            <div class = 'm-portlet'>
                <div class = 'm-portlet__body'>
					<div class = ''>
                        <div class="alert alert-info">
                            A kupon generálási szabályok napi rendszerességgel futnak. Az adott szabálynak megfelelő legnagyobb kedvezményt részesítik elényben, azoknál a felhasználóknál akik nem rendelkeznek kuponnal az elmult 1 hétben.
                        </div>
                        <div class="col-md-12">
                        <div id="m_repeater_3">
                            <div class="">
                                <div data-repeater-create="" class="btn btn btn-primary m-btn m-btn--icon">
																<span>
																	<i class="la la-plus"></i>
																	<span>
																		Szabály hozzáadása
																	</span>
																</span>
                                </div>
                                <hr/>
                            </div>

                            {!!  \App\Http\Helper::formOpen('CouponForm','post',NULL,['class'=>'form '])  !!}

                            <div data-repeater-list="rules" class="col-md-12">
                                @if(is_array($model) || (old('rules') && is_array(old('rules'))))
                                    @php
                                        $args = old('rules',$model);
                                    @endphp
                                    @foreach($args as $item)
                                        @if(!empty($item['value']) && !empty($item['amount']))

                                        <div data-repeater-item="" class="row">
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <input type="hidden" name="id" value="{{$item['id']}}">
                                                    <select  name="rule" class="form-control form-control-danger" placeholder="Szabály">
                                                        @foreach( \Modules\Coupon\Entities\AutomaticCoupon::$rules as $k =>$rule)
                                                            <option @if($item['rule'] == $k) selected @endif value="{{$k}}"> {{$rule}} </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="input-group">
                                                    <input type="text" name="value" value="{{$item['value']}}" class="form-control form-control-danger" placeholder="Érték">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="input-group">
                                                    <select  name="type" class="form-control form-control-danger" placeholder="Típus">
                                                        <option @if($item['type'] == 0) selected @endif value="0">Fix összeg </option>
                                                        <option @if($item['type'] == 1) selected @endif value="1">Százalékos kedvezmény </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="input-group">
                                                    <input type="text" name="amount" value="{{$item['amount']}}" class="form-control form-control-danger" placeholder="Kedvezmény értéke">
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <a href="#" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                                    <i class="la la-remove"></i>
                                                </a>
                                            </div>
                                        </div>
                                        @endif
                                    @endforeach
                                @endif

                                <div data-repeater-item="" class="row">
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <input type="hidden" name="id">
                                            <select  name="rule" class="form-control form-control-danger" placeholder="Szabály">
                                                @foreach( \Modules\Coupon\Entities\AutomaticCoupon::$rules as $k =>$rule)
                                                <option @if($loop->iteration ==1) selected @endif value="{{$k}}"> {{$rule}} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            <input type="text" name="value" class="form-control form-control-danger" placeholder="Érték">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            <select  name="type" class="form-control form-control-danger" placeholder="Típus">
                                                    <option selected value="0">Fix összeg </option>
                                                    <option value="1">Százalékos kedvezmény </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            <input type="text" name="amount" class="form-control form-control-danger" placeholder="Kedvezmény értéke">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <a href="#" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                            <i class="la la-remove"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>
                            {!! \App\Http\Helper::formButtons('admin_dashboard') !!}
                            {!! \App\Http\Helper::formClose() !!}
                            </div>

                        </div>
                    </div>

                        </div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                </div>

            </div>

    </div>
@endsection
@push('scripts')
    <script src="\assets\demo\default\custom\crud\forms\widgets\form-repeater.js"></script>
@endpush