@extends('frontend.layout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">

                   @if($success)
                        <h1>Sikeresene leiratkoztál a hírlevélelünkről!</h1>
                        <h3>Elnézést kérünk amennyiben hírlevelünnkel zavartunk</h3>
                        <h5>Üdvözlettel: a MadsWork csapata!</h5>
                   @else
                        <h1>Kedves {{$worker->name}} ! Épp most készülsz leiratkozni a hírlevelünkről!</h1>
                       {!! \App\Http\Helper::formOpen('unsubscribe','post') !!}
                        {!! \App\Http\Helper::textarea('Megjegyzés','comment',null,['form-control']); !!}
                        <button type="submit" class="btn btn-danger">Leiratkozás</button>
                       {!! \App\Http\Helper::formClose() !!}
                   @endif

            </div>
        </div>
    </div>
@endsection