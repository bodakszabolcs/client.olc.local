@extends('errors.maintance_layout')
@section('content')

    <section id="munkak" class="">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 text-center">
             <h1 class="h2 text-center">{{__('Az oldalon karbantartás zajlik! Hamarosan újra elérhető lesz.')}}</h1>
             <img src="/assets/frontend/img/footer_logo.png" class="img-responsive" alt="Mads  logo" title="Mads logo">
            </div>
        </div>
    </div>
    </section>
@endsection
