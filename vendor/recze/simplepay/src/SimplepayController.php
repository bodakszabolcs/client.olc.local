<?php

namespace Recze\SimplePay;

use App\Events\SimpleTimeOut;
use App\Events\SimpleBackRef;
use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;
use App\Events\SimpleIpn;
use App;
use Illuminate\Support\Facades\Session;

class SimplepayController extends Controller
{
    public function Ipn(Request $request)
    {
        $config = config('simplepay');

        $orderCurrency = (isset($_REQUEST['CURRENCY'])) ? $_REQUEST['CURRENCY'] : 'HUF';

        /**
         * Start IPN
         */
        $ipn = new \Recze\SimplePay\SimpleIpn($config, $orderCurrency);

        /**
         * IPN successful
         * This is the real end of successful payment
         */
        if ($ipn->validateReceived()) {
            /**
             * End of payment: SUCCESSFUL
             * echo <EPAYMENT> (must have)
             */
            $ipn->confirmReceived();

            /**
             * Your code here
             */
            if ($_REQUEST['ORDERSTATUS'] == 'COMPLETE') {
                /*
                 * $_REQUEST
                 * ________________
                 * REFNO
                 * REFNOEXT
                 * SALEDATE
                 * PAYMETHOD
                 *
                 *
                 */
                event(new SimpleIpn($_REQUEST));

                return response('OK',200);
            }

        }


        /**
         * Error and debug info
         */
        $ipn->errorLogger();

        if ($ipn->debug) {
            foreach ($ipn->debugMessage as $debug) {
                print $debug . "\n";
            }
        }
        if (count($ipn->errorMessage) > 0) {
            foreach ($ipn->debugMessage as $debug) {
                print $debug . "\n";
            }
            foreach ($ipn->errorMessage as $error) {
                print $error . "\n";
            }
        }
    }

    public function Irn(Request $request)
    {
        $config = config('simplepay');

        $orderCurrency = (isset($_REQUEST['ORDER_CURRENCY'])) ? $_REQUEST['ORDER_CURRENCY'] : 'HUF';

        /**
         * Start IRN
         */
        $irn = new SimpleIrn($config, $orderCurrency);


        /**
         * Set needed fields
         */
        $data['REFNOEXT'] = isset($_REQUEST['order_ref']) ? $_REQUEST['order_ref'] : 'N/A';
        $data['ORDER_REF'] = isset($_REQUEST['payrefno']) ? $_REQUEST['payrefno'] : 'N/A';
        $data['ORDER_AMOUNT'] = isset($_REQUEST['ORDER_AMOUNT']) ? $_REQUEST['ORDER_AMOUNT'] : 'N/A';
        $data['ORDER_CURRENCY'] = $orderCurrency;
        $data['IRN_DATE'] = @date("Y-m-d H:i:s");
        $data['AMOUNT'] = isset($_REQUEST['AMOUNT']) ? $_REQUEST['AMOUNT'] : 'N/A';
        $response = $irn->requestIrn($data);


        /**
         * Check response
         */
        if (isset($response['RESPONSE_CODE'])) {
            if ($irn->checkResponseHash($response)) {
                /*
                * your code here
                */

                /*
                print "<pre>";
                print_r($response);
                print "</pre>";
                */
                return response('OK',200);
            }
        }

        $irn->errorLogger();

        $message = "SimplePay reference number" . ': <b>' . $response['ORDER_REF'] . '</b><br/>';
        $message .= "Response code" . ': <b>' . $response['RESPONSE_CODE'] . '</b><br/>';
        $message .= "Response message" . ': <b>' . $response['RESPONSE_MSG'] . '</b><br/>';
        $message .= "Date" . ': <b>' . $response['IRN_DATE'] . '</b><br/>';

        $response['PAYREFNO'] = $response['ORDER_REF'];
        $response['REFNOEXT'] = $_REQUEST['order_ref'];

        $message .= sprintf("<p>Az IRN eredményét a kereskedői vezérlőpulton a <b>%s</b> tranzakció adatainál tudja nyomon követni</p>", $response['ORDER_REF']);
    }

    public function Idn(Request $request)
    {
        $config = config('simplepay');

        $orderCurrency = (isset($_REQUEST['ORDER_CURRENCY'])) ? $_REQUEST['ORDER_CURRENCY'] : 'HUF';


        /**
         * Start IDN
         */
        $idn = new SimpleIdn($config, $orderCurrency);


        /**
         * Set needed fields
         */
        $data['REFNOEXT'] = $_REQUEST['order_ref'];
        $data['ORDER_REF'] = (isset($_REQUEST['payrefno'])) ? $_REQUEST['payrefno'] : 'N/A';
        $data['ORDER_AMOUNT'] = (isset($_REQUEST['ORDER_AMOUNT'])) ? $_REQUEST['ORDER_AMOUNT'] : 'N/A';
        $data['ORDER_CURRENCY'] = $orderCurrency;
        $data['IDN_DATE'] = @date("Y-m-d H:i:s");
        $response = $idn->requestIdn($data);


        /**
         * Check response
         */
        if (isset($response['RESPONSE_CODE'])) {
            if ($idn->checkResponseHash($response)) {
                /*
                * your code here
                */

                /*
                print "<pre>";
                print_r($response);
                print "</pre>";
                */
                return response('OK',200);
            }
        }

        $idn->errorLogger();

        $message = "Order id" . ': <b>' . $response['ORDER_REF'] . '</b><br/>';
        $message .= "Response code" . ': <b>' . $response['RESPONSE_CODE'] . '</b><br/>';
        $message .= "Response message" . ': <b>' . $response['RESPONSE_MSG'] . '</b><br/>';
        $message .= "Date" . ': <b>' . $response['IDN_DATE'] . '</b><br/>';

        $response['PAYREFNO'] = $response['ORDER_REF'];
        $response['REFNOEXT'] = $_REQUEST['order_ref'];
    }

    public function Ios(Request $request)
    {
        $config = config('simplepay');

        $orderCurrency = (isset($_REQUEST['ORDER_CURRENCY'])) ? $_REQUEST['ORDER_CURRENCY'] : 'HUF';

        /**
         * Set transaction external reference ID
         */
        $orderexternalId = (isset($_REQUEST['order_ref'])) ? $_REQUEST['order_ref'] : 'N/A';

        //IOS
        $ios = new SimpleIos($config, $orderCurrency, $orderexternalId);

        $ios->errorLogger();

        $message = "Date" . ':<b>' . $ios->status['ORDER_DATE'] . '</b><br/>';
        $message .= "SimplePay referenciaszám" . ':<b>' . $ios->status['REFNO'] . '</b><br/>';
        $message .= "Order id" . ':<b>' . $ios->status['REFNOEXT'] . '</b><br/>';
        $message .= "Status" . ':<b>' . $ios->status['ORDER_STATUS'] . '</b><br/>';
        $message .= "Payment method" . ':<b>' . $ios->status['PAYMETHOD'] . '</b><br/>';
    }

    public function Timeout(Request $request, $lang = 'hu')
    {
        $config = config('simplepay');

        App::setLocale($lang);

        $timeOut = new SimpleLiveUpdate($config, 'HUF');

        if (@$_REQUEST['redirect'] == 1) {
            $message = '<b><font color="red">' . trans('frontend.Megszakított tranzakció') . '</font></b><br/>';
            $log['TRANSACTION'] = 'ABORT';
        } else {
            $message = '<b><font color="red">' . trans('frontend.Időtúllépéses tranzakció') . '</font></b><br/>';
            $log['TRANSACTION'] = 'TIMEOUT';
        }

        $message .= trans('frontend.Ön megszakította a fizetést, vagy lejárt a tranzakció maximális ideje!') . '<br/><br/>';
        $message .= trans('frontend.Dátum') . ': <b>' . date('Y-m-d H:i:s', time()) . '</b><br/>';
        $message .= trans('frontend.Megrendelés azonosító') . ': <b>' . $_REQUEST['order_ref'] . '</b><br/>';
        $log['ORDER_ID'] = (isset($_REQUEST['order_ref'])) ? $_REQUEST['order_ref'] : 'N/A';
        $log['CURRENCY'] = (isset($_REQUEST['order_currency'])) ? $_REQUEST['order_currency'] : 'N/A';
        $log['REDIRECT'] = (isset($_REQUEST['redirect'])) ? $_REQUEST['redirect'] : '0';
        $timeOut->logFunc("Timeout", $log, $log['ORDER_ID']);
        $timeOut->errorLogger();

        event(new SimpleTimeOut($message));

        $response = [
            'request' => $_REQUEST,
            'message' => $message
        ];

        $request->session()->flash('error_message',$message);

        return redirect(route('admin_dashboard'));
    }

    public function Backref(Request $request, $lang = 'hu')
    {
        $config = config('simplepay');
        $app_config = config('app');

        App::setLocale($lang);

        $orderCurrency = (isset($_REQUEST['order_currency'])) ? $_REQUEST['order_currency'] : 'HUF';

        /**
         * Start backref
         */
        $backref = new \Recze\SimplePay\SimpleBackRef($config, $orderCurrency);

        /**
         * Add order reference number from merchant system (ORDER_REF)
         */
        $backref->order_ref = (isset($_REQUEST['order_ref'])) ? $_REQUEST['order_ref'] : 'N/A';


        $message = '';

        if(isset($_REQUEST['err'])){
            $backref->logFunc("BackRef", $_REQUEST, $_REQUEST['order_ref']);

            /**
             * Check backref
             */
        } elseif ($backref->checkResponse()){

            /**
             * SUCCESSFUL card authorizing
             * Notify user and wait for IPN
             * Need to notify user
             *
             */
            $backStatus = $backref->backStatusArray;

            // Notification by payment method
            //CCVISAMC
            if ($backStatus['PAYMETHOD'] == 'Visa/MasterCard/Eurocard') {
                $message .= '<b><font color="green">' . SUCCESSFUL_CARD_AUTHORIZATION . '</font></b><br/>';
                if ($backStatus['ORDER_STATUS'] == 'IN_PROGRESS') {
                    $message .= '<b><font color="green">' . WAITING_FOR_IPN . '</font></b><br/>';
                } elseif ($backStatus['ORDER_STATUS' ] == 'PAYMENT_AUTHORIZED') {
                    $message .= '<b><font color="green">' . WAITING_FOR_IPN . '</font></b><br/>';
                } elseif ($backStatus['ORDER_STATUS'] == 'COMPLETE') {
                    $message .= '<b><font color="green">'. CONFIRMED_IPN .'</font></b><br/>';
                }
            }
            //WIRE
            elseif ($backStatus['PAYMETHOD'] == 'Bank/Wire transfer') {
                $message = '<b><font color="green">' . SUCCESSFUL_WIRE . '</font></b><br/>';
                if ($backStatus['ORDER_STATUS'] == 'PAYMENT_AUTHORIZED' || $backStatus['ORDER_STATUS'] == 'COMPLETE') {
                    $message .= '<b><font color="green">' . CONFIRMED_WIRE . '</font></b><br/>';
                }
            }

            /**
             * Your code here
             */


        } else {

            /**
             * UNSUCCESSFUL card authorizing
             * END of transaction
             * Need to notify user
             *
             */
            $backStatus = $backref->backStatusArray;

            $message = '<b><font color="red">' . UNSUCCESSFUL_TRANSACTION . '</font></b><br/>';
            $message .= '<b><font color="red">' . END_OF_TRANSACTION . '</font></b><br/>';
            $message .= UNSUCCESSFUL_NOTICE . '<br/><br/>';

            /**
             * Your code here
             */

        }
        $backref->errorLogger();

        /**
         * Notification
         */
        if(!isset($_REQUEST['err'])){
            $message .= PAYREFNO . ': <b>' . $backStatus['PAYREFNO'] . '</b><br/>';
            $message .= ORDER_ID . ': <b>' . $backStatus['REFNOEXT'] . '</b><br/>';
            $message .= BACKREF_DATE . ': <b>' . $backStatus['BACKREF_DATE'] . '</b><br/>';
        }

        if (isset($_REQUEST['err'])) {
            $message .= '<hr>';
            $message .= '<b><font color="red">ERROR: </font></b>  ' . $_REQUEST['err'] . '<br/>';
        }

        $response = [
            'request' => $_REQUEST,
            'message' => $message
        ];

        if (isset($_REQUEST['err']) || !$backref->checkResponse())
        {
            $request->session()->flash('error_message',$message);
        } else {
            $request->session()->flash('success_message',$message);
        }

        return redirect(route('admin_dashboard'));
    }
}