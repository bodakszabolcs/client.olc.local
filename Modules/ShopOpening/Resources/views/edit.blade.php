@extends('admin::layouts.layout')
@section('content')
	<?php
		$errors= Session::get('field_errors');
		Session::forget('field_errors');
	?>
<div class='row'>
		<div class='col-md-12'>
			<div class='m-portlet'>
				<div class='m-portlet__body'>
					<input type='hidden' name='tab' value='data'>
					<div class = 'row'>
						<div class = 'col-md-12'>	
		{!!  \App\Http\Helper::formOpen('ShopOpeningForm','post',NULL,['class'=>'form '])  !!}<div class = 'm-portlet__head'>
									<div class = 'm-portlet__head-caption'>
										<div class = 'm-portlet__head-title'>
											<h3 class = 'm-portlet__head-text'>
												{{__('Tartalom adatok')}}
											</h3>
										</div>
									</div>
								</div>
								<div class = 'm-portlet__body'>
									<!--begin::Widget 29-->
									<div class = 'm-widget29'>
										<div class = 'm-widget_content'>
											<div class = 'm-widget_content-items'>
												<div class='row'>
													<div class='col-md-6'>	<div class="col-md-6">					{!! App\Http\Helper::checkbox(trans('Zárva'),'is_closed',old('is_closed',$model->is_closed),array(),$errors) !!}</div>
<div class="col-md-6">					{!! \App\Http\Helper::input(trans('Tól'),'open_from',old('open_from',$model->open_from),array('class'=>'form-control'),$errors) !!}		
</div>
<div class="col-md-6">					{!! \App\Http\Helper::input(trans('Ig'),'open_to',old('open_to',$model->open_to),array('class'=>'form-control'),$errors) !!}		
</div>
										</div>

										</div>
									</div>
								</div>
		 {!! \App\Http\Helper::formButtons('admin_shopopening_list') !!}
		 </div>
		 {!! \App\Http\Helper::formClose() !!}

			</div>
					</div>
				</div>
			</div>
		</div>
	</div>



@endsection 