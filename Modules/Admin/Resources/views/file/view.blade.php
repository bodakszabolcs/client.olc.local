@extends('admin::layouts.layout')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="m-portlet">
                <div class="m-portlet__body">
                    <iframe src="/laravel-filemanager"
                            style="width: 100%; height: 800px; overflow: hidden; border: none;"></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection
