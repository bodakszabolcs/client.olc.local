<?php

	namespace Modules\News\Http\Controllers;

	use Illuminate\Support\Facades\Storage;
    use Modules\News\Entities\News;
	use App\Http\Controllers\Controller;
	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Session;
	use Validator;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\View;
	use Modules\Admin\Http\Controllers\AdminController;

	class NewsController extends AdminController
	{
		public $niceNames = array(
			'title'	=>	'Cím',
			'description'	=>	'Leírás',
			'image'	=>	'Kép',
			'date'	=>	'Kiküldés dátuma',

			);
		public function __construct()
		{
			View::share('sub_title', __('Vásárlói értesítések'));
			View::share('subtitle_link', route('admin_news_list'));
		}

		public function NewsList(Request $request)
		{
			$model = new News();
			$list = $model->searchInModel($request->input());
			$list = $list->orderBy('id', 'desc')->disableCache()->paginate(50)->withPath($request->path());
			View::share('title', __('Vásárlói értesítések lista'));
			return view('news::list', [
				'model' => $list, 'filterHeader' => $model->drawSearchTableHeader($request)
			]);
		}
        public function CloneNews(Request $request,$id){
		    try {
                    $old = News::find($id);
                    if(is_null($old)){
                        throw new ModelNotFoundException();
                    }
                    $new = new News();
                    $new->fill($old->toArray());
                    $new->save();
                    $request->session()->flash('success_message', __('A másolás sikeres'));
                    return redirect()->intended(route('admin_news_edit',['id'=>$new->id]));


                } catch (ModelNotFoundException $e) {
                $request->session()->flash('error_message', __('Az elem nem törölhető'));
            }
            return redirect()->intended(route('admin_news_list'));

        }
		public function Edit(Request $request,$day,$shop_id, $id = 0,$tab='data')
		{
			View::share('title', __('Vásárlói értesítések szerkesztés'));

			$model = News::find($id);
			$errors = [];
			if (!is_null($model)) {

			} else {
				if ($id > 0) {
					return redirect(route('admin_news_list'));
				}
				$model = new News();
			}
			if ($request->isMethod('post')) {

				$tab = $request->input('tab', 'data');
				switch ($tab) {
					case 'data': {
						$response = $this->validateData($model);
						break;
					}
					default:
						$response = $this->validateData($model);
						break;
				}

				if ($response['status'] == 'success') {


					$model->fill($request->all());
					$model->shop_id= $shop_id;
					$model->day= $day;
                   $model->image= url($request->input('file'));
					$model->save();
					$request->session()->flash('success_message', __('A mentés sikeres'));

					//return redirect(route('admin_news_edit', [$id, $tab]))->withErrors($response['validator'])->withInput();
				}
				else{
                    $request->session()->flash('error_message', __('Hiba a mentés során'));
                    Session::put('field_errors', $response['cust_errors']);
                    return redirect()->intended(route('admin_news_edit', ['id' => $model->id,'day'=>$day,'shop_id'=>$shop_id]));
                }
				if ($request->has('save_and_exit')) {
					return redirect()->intended(session()->get('redirect_url', route('admin_news_list')));
				}
				if ($response['status'] == 'success') {

                } else {
                    $request->session()->flash('error_message', __('Hiba a mentés során'));
                    Session::put('field_errors', $response['cust_errors']);
					return redirect()->intended(route('admin_news_edit', ['id' => $model->id,'day'=>$day,'shop_id'=>$shop_id]));
				}
			}

			return view('news::edit', ['model' => $model, 'id' => $id,'day'=>$day,'shop_id'=>$shop_id, 'errors' => $errors]);
		}
		public function validateData($model)
		{

			$rules = [
                'title'	=>	'required',
                'link'	=>	'required',
                'description'	=>	'required',
                'file'	=>	'required',

			];
			if($model->id >0){
			    unset($rules['file']);
            }
			$validator = \Illuminate\Support\Facades\Validator::make(Input::all(), $rules);
			$validator->setAttributeNames($this->niceNames);
			$not_valid = $validator->fails();
			if ($not_valid) {
				return ['status' => 'error', 'cust_errors' => $validator->messages()->toArray(), 'validator' => $validator];
			} else {
				return ['status' => 'success', 'message' => 'A mentés sikeres'];
			}
		}

		public function Delete(Request $request, $id)
		{
			try {
				if ($id == 0) {
					throw new ModelNotFoundException();
				}
				$model = News::findOrFail($id);
				$model->delete();
				$request->session()->flash('success_message', __('success_delete'));
			} catch (ModelNotFoundException $e) {
				$request->session()->flash('error_message', __('Az elem nem törölhető'));
			}
			return redirect()->intended(route('admin_news_list'));
		}
		public function preview(Request $request){


            return view('news::preview',['data'=>$request->input('data',$request->all())]);
        }
        public function ChangeStatus(Request $request, $id)
        {
            try {
                if ($id == 0) {
                    throw new ModelNotFoundException();
                }
                $model = News::findOrFail($id);
                $model->active = !$model->active;
                $model->save();
                $request->session()->flash('success_message', __('A státusz sikeresen megváltoztatva'));
            } catch (ModelNotFoundException $e) {
                $request->session()->flash('error_message', __('Hiba a mentés közben'));
            }
            return redirect()->intended(route('admin_news_list'));
        }

	}
