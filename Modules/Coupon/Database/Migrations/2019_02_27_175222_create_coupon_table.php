<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon', function (Blueprint $table) {
            $table->increments("id");
$table->integer("user_id")->comment("user"")->default(0)->unsigned();\n$table->integer("shop_id")->comment("Bolt"")->default(0)->unsigned();\n$table->text("code")->comment("kód");
$table->integer("type")->comment(""")->default(0)->unsigned();\n$table->text("amount")->comment("Mennyiség");
$table->date("expire")->comment("Érvényesség")->default(null);
 $table->timestamps();
 $table->softDeletes();
$table->index(["deleted_at"]);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon');
    }
}
