<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments("id");
$table->text("title")->comment("Cím");
$table->text("description")->comment("Leírás");
$table->text("image")->comment("Kép");
$table->dateTime("date")->comment("Kiküldés dátuma")->default(null);
 $table->timestamps();
 $table->softDeletes();
$table->index(["deleted_at"]);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
