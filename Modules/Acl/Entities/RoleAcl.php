<?php

namespace Modules\Acl\Entities;

use App\LiquidModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;

class RoleAcl extends LiquidModel
{
    use Cachable;

    public $timestamps = false;

    protected $table = 'roles_acls';

    public function __construct($attributes = [])
    {
        config(['laravel-model-caching.cache-prefix' => str_slug(config('app.name'))]);

        parent::__construct($attributes);
    }
}
