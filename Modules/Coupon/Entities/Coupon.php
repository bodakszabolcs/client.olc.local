<?php

	namespace Modules\Coupon\Entities;

	use App\Http\Helper;
    use GuzzleHttp\Client;
    use GuzzleHttp\RequestOptions;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Database\Eloquent\SoftDeletes;
    use Illuminate\Support\Facades\Log;
    use Mockery\Exception;
	use App\LiquidModel;
	use Sunra\PhpSimple\HtmlDomParser;
	use GeneaLabs\LaravelModelCaching\Traits\Cachable;

	class Coupon extends LiquidModel
	{
		use SoftDeletes, Cachable;

		protected $table = 'coupons';

		protected $dates = ['deleted_at', 'created_at', 'updated_at'];
		/**
		 * The attributes that are mass assignable.
	 	*
		 * @var array
	 	*/
		protected $fillable = [
			'user_id', //user 
			'shop_id', //Bolt 
			'code', //kód 
			'type', // 
			'amount', //Mennyiség 
			'expire', //Érvényesség 
		];

		public $searchColumns = [
			'id' => 'text',
			'user_name'	=>	'text',
			'shop_id'	=>	'text', 
			'code'	=>	'text', 
			'type'	=>	'text', 
			'amount'	=>	'text', 
			'expire'	=>	'text', 
			];

		public $searchColumnLabels = [
			'id' => 'ID',
			'user_name'	=>	'Felhasználó',
			'shop_id'	=>	'Bolt', 
			'code'	=>	'Kód', 
			'type'	=>	'Típus', 
			'amount'	=>	'Mennyiség', 
			'expire'	=>	'Érvényesség', 
			];
		protected $casts = [
		    'shop_id' => 'array',
			];

		public static function getCoupon($user_id,$code){
			return \Modules\Coupon\Entities\Coupon::where('id','=',$code)->first();
		}
		public static function useCoupon($user_id,$code){
		    $coupon =Coupon::where('id','=',$code)->first();
		    if($coupon) {
                $coupon->used = date('Y-m-d H:i:s');
                $coupon->save();
                $client = new Client();
                try {

                    $postData = Helper::encodeData($coupon->toArray(), 'coupon');
                    $client->post(env('SERVER_URL') . '/api/delete-coupon', [
                        RequestOptions::JSON =>
                            ['data' => $postData]
                    ],
                        ['Content-Type' => 'application/json']);
                }
                catch(Exception $e){
                    Log::info($e->getMessage());
                }
            }

		}
        public static function randomCoupon(){
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 10; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }
	}
?>
