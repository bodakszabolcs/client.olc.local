@php
	$kolcsonzes = \Modules\Content\Entities\Content::find(2);

@endphp
@if(!is_null($kolcsonzes))
<section class="masodik" id="kolcsonzes">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<img title="Munkaerő kölcsönzés" alt="Munkaerő kölcsönzés" src="/assets/frontend//img/munkaero.png">

				<h3>{{$kolcsonzes->title}}</h3>

			</div>
		</div>
		<div class="row">
			{!! $kolcsonzes->content !!}
		</div>
		<div class="col-lg-8 col-lg-offset-2 text-center">
			<a class="btn btn-lg btn-outline" href="/ajanlatkeres">
				Ajánlatkérés
			</a>
		</div>
	</div>

</section>
@endif