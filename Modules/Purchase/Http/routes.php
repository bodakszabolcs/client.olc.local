<?php 
Route::group(['namespace' => 'Modules\Purchase\Http\Controllers', 'prefix' => 'admin/purchase', 'middleware' => ['web','auth', 'acl', 'accesslog']], function () {
	Route::match(['post'], '/get-customers', 'PurchaseController@GetCustomers')->name('admin_purchase_get_customers');
	Route::match(['post'], '/get-customer-profile', 'PurchaseController@GetCustomerProfile')->name('admin_purchase_get_customer_profile');
	Route::match(['get'], '/list', 'PurchaseController@PurchaseList')->name('admin_purchase_list');
	Route::match(['get'], '/send-noti', 'PurchaseController@sendNotification')->name('admin_purchase_send_noti');
	Route::match(['get'], '/send-news', 'PurchaseController@sendNewsletterNotification')->name('admin_purchase_send_news_noti');
	Route::match(['post','get'], '/save', 'PurchaseController@Save')->name('admin_purchase_save');
	Route::match(['post'], '/delete', 'PurchaseController@Delete')->name('admin_purchase_delete');
	//Route::match(['get'], '/delete/{id}', 'PurchaseController@Delete')->name('admin_purchase_delete');
});

