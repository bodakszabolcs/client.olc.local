<jobs>
		
		
		<?php
	$server = 'http://'.$_SERVER['HTTP_HOST'];
	$data = \Modules\Work\Entities\Work::where('is_full','=',0)->where('archive','=',0)->get()->toArray();
		
		foreach($data as $row): ?>
				<?php
	$c=\Modules\Workcategory\Entities\Workcategory::find($row['workcategory_id']);
	$head_category = \Modules\Workcategory\Entities\Workcategory::find($c->parent_id);
	$url= $server.route('frontend_work',[$head_category->slug,$c->slug,$row['slug']]);
	$projekt =\Modules\User\Entities\User::find($row['responsible']);
	$projekt_text = $projekt->name."<br/>".
		"Email: <a href='mailto:".$projekt->email."'><b>".$projekt->email."</b></a><br/>".
		"Telefon: <b>".$projekt->phone."</b>";
	?>

			<job>
				<id><?=$row['id']?></id>
				<pubDate><?= $row['updated_at'] ?></pubDate>
				<?php $city=\Modules\City\Entities\City::find($row['city']); ?>
				<city><?= $city->name?></city>
				<title><![CDATA[<?= $row['name'] ?>]]></title>
				<description><![CDATA[<h3> <?=$row['name']?></h3></br>Leírás: <?=$row['lead']?></br> Feladat: <?=$row['task']?></br>Feltétel: <?=$row['conditions']?> </br> Kapcsolattartó: <?=$projekt_text?> ]]></description>
				<sal><?=$row['wage']?></sal>
				<worktime><?=$row['time']?></worktime>
				<category><?=$head_category->name?>/<?=$c->name?></category>
				<link><![CDATA[<?= $url ?>]]></link>
			</job>
			
	<?php endforeach; ?>
</jobs>