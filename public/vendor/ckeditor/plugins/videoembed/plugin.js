(function () {

    var selected_content = 0;

        CKEDITOR.plugins.add('videoembed', {

            init: function (editor) {

                editor.addCommand('videoembed', new CKEDITOR.dialogCommand('videoembed', {
                    allowedContent: true
                }));

                editor.ui.addButton('VideoEmbed', {
                    label: 'Video begyazas',
                    command: 'videoembed',
                    toolbar: 'insert',
                    icon: this.path + 'icon.png'
                });

                CKEDITOR.dialog.add('videoembed', function (instance) {
                    var video;

                    return {
                        title: 'Videó beágyazás',
                        minWidth: 510,
                        minHeight: 200,
                        contents:
                            [{
                                id: 'videoEmbedPlugin',
                                expand: true,
                                elements:
                                    [
                                        {
                                            type: 'select',
                                            id: 'videos',
                                            label: 'Videó kiválasztása',
                                            items: [['Kérem válasszon',0]],
                                            'default' : "0",
                                            onLoad: function(api) {
                                                this.clear();

                                                var ezazelem = this;
                                                ezazelem.add('Kérem válasszon',0);

                                                $.ajax({
                                                    type: 'GET',
                                                    dataType: 'json',
                                                    url: '/admin/video/list_json',
                                                    success: function (data) {
                                                        $(data.items).each(function (index, item) {
                                                            ezazelem.add(item.title,item.id);
                                                        });

                                                    }
                                                });
                                            },
                                            onChange: function (api) {
                                                // this = CKEDITOR.ui.dialog.select
                                                selected_content = this.getValue();
                                            }
                                        }]

                            }],
                        onOk: function () {
                            if (selected_content != 0)
                            {
                                var content = '';
                                var instance = this.getParentEditor();

                                $.ajax({
                                    type: 'GET',
                                    dataType: 'json',
                                    url: '/admin/video/list_json/'+selected_content,
                                    success: function(data) {
                                        content = data.content;
										content = '<div class="embeded">'+content+'</div>';
                                        var element = CKEDITOR.dom.element.createFromHtml(content);
                                        instance.insertElement(element);
                                    }
                                });
                            }
                        }
                    };
                });
            }
        });
    }

)();
