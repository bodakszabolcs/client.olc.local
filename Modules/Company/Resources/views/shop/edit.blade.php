@extends('admin::layouts.layout')
@section('content')
	<?php
	$errors = Session::get('field_errors');
	Session::forget('field_errors');
	?>
	<?php
	$days = ['1'=>'Hétfő',2=>'Kedd',3=>'Szerda',4=>'Csütörtök',5=>'Péntek',6=>'Szombat',7=>'Vasárnap'];
	$hours=[];
	$package_icons=[
		'Basic'=>'fa flaticon-piggy-bank',
		'Professional'=>'fa flaticon-confetti',
		'Extended' =>'fa flaticon-rocket'
	];
	for($i=0;$i<24;$i++)
	{
		$hour = str_pad($i,2,'0',STR_PAD_LEFT).":00";
		// $hour = str_pad($i,2,'0',STR_PAD_LEFT).":15";
		$hour1 = str_pad($i,2,'0',STR_PAD_LEFT).":30";
		//  $hour = str_pad($i,2,'0',STR_PAD_LEFT).":45";
		$hours[$hour]=$hour;
		$hours[$hour1]=$hour1;

	}
	?>
	<div class = 'row'>
		<div class = 'col-md-12'>
			<div class = 'm-portlet'>
				<div class = 'm-portlet__body'>
					<input type = 'hidden' name = 'tab' value = 'data'>
					<div class = 'row'>
						<div class = 'col-md-12'>
							{!!  \App\Http\Helper::formOpen('ShopForm','post',NULL,['class'=>'form ','enctype'=>'multipart/form-data'])  !!}
							<div class = 'm-portlet__body'>
								<!--begin::Widget 29-->
								<div class = 'm-widget29'>
									<div class = 'm-widget_content'>
										<div class = 'm-widget_content-items'>
												<div class="row">
												<h5 class = "col-md-12"><br>Elérhetőség adatok
													<br>
													<br>
												</h5>
													<div class="col-sm-6">
														{!! \App\Http\Helper::input('Üzlet neve','name',old('name',$model->name),array('class'=>'form-control'),$errors); !!}
													</div>
													<div class=" col-sm-6">
														{!! \App\Http\Helper::input('E-mail cím','email',old('email',$model->email),array('class'=>'form-control'),$errors); !!}
													</div>
													<div class=" col-sm-6">
														{!! \App\Http\Helper::input('Telefon','phone',old('phone',$model->phone),array('class'=>'form-control'),$errors); !!}
													</div>
													<div class=" col-sm-6">
														{!! \App\Http\Helper::input('Weboldal','webpage',old('webpage',$model->webpage),array('class'=>'form-control'),$errors); !!}
													</div>
													<div class="col-md-6">
														{!! \App\Http\Helper::fileSelector(trans('Borító'),'file','browse-image',old('file',$model->cover),array('class'=>''),$errors) !!}
													</div>
													<div class="col-md-6">
														@if(!empty($model->cover))
															<img src="{{$model->cover}}" class="img-responsive img-fluid" >
															@endif
													</div>
												</div>
												<div class="row">
													<h5 class = "col-md-12"><br>Csomagok
													<br>
													<br>
												</h5>
												</div>
													<div class="row">
														<div class="col-md-12">
															<div class=" alert alert-info">
																Figyelem! A csomagokhoz tartozó szolgáltatások csak a következő havi számláza teljesítése után lesznek elérhetőek. A csomagokról és szolgáltatásokról bővebben itt olvashatsz <a style="color:red" href="{{$packages['details']}}" target="_blank">Részletek</a>
															</div>
														<div class="m-pricing-table-1">
															<div class="m-pricing-table-1__items row">
																<div class="table"></div>
																@foreach($packages['packages'] as $package)
																<div class="m-pricing-table-1__item col-md-4">
																	<div class="m-pricing-table-1__visual">
																	<div class="m-pricing-table-1__hexagon1"></div>
																	<div class="m-pricing-table-1__hexagon2"></div>
																	<span class="m-pricing-table-1__icon m--font-brand">
																		<i class="{{$package_icons[$package['name']]}}"></i>
																	</span>
																</div>
																	<span class="m-pricing-table-1__price">
																		{{$package['price']}}
																		<span class="m-pricing-table-1__label">
																			Ft
																		</span>
																	</span>
																	<h2 class="m-pricing-table-1__subtitle">
																		{{$package['name']}}
																	</h2>

																	<div class="m-pricing-table-1__btn">
																		<label class="select-package btn btn-brand m-btn m-btn--custom m-btn--pill m-btn--wide m-btn--uppercase m-btn--bolder m-btn--sm @if($model->package_id == $package['id']) active @endif" >
																		<span class="selected-package-text">
																			@if($model->package_id == $package['id'])
																				Aktív
																			@else
																				Kiválaszt
																			@endif
																		</span>
																			<input type="radio" style="display: none" name="package_id" id="package_id{{$package['id']}}" @if($model->package_id == $package['id']) checked @endif value="{{$package['id']}}">
																		</label>
																	</div>
																</div>
																	@endforeach

															</div>
														</div>
													</div>
														</div>
												<div class="row">
													<h5 class = "col-md-12"><br>Social adatok
													<br>
													<br>
												</h5>
													<div class=" col-sm-12">
														{!! \App\Http\Helper::input('Facebook','facebook',old('facebook',$model->facebook),array('class'=>'form-control'),$errors); !!}
													</div>
													<!-- /.form-group -->
													<div class=" col-sm-12">
														{!! \App\Http\Helper::input('Instagram','instagram',old('instagram',$model->instagram),array('class'=>'form-control'),$errors); !!}
													</div>

												</div>
												<div class="row">
												<h5 class = "col-md-12"><br>Cím adatok
													<br>
													<br>
												</h5>
												</div>
												<div @if(isset($errors['address'])) class="has-error" @endif >
													<input id="pac-input" class="controls form-control mb30" type="text" name="address" value="{{old('address',$model->address)}}" placeholder="Cím megadása" autocomplete="off" style="z-index: 0; position: inherit; left: 123px; margin-top: -11px !important;">
												</div>
												<div id="map-canvas">
												</div>
												<div class="row">
													<div class="col-sm-6">
														{!! \App\Http\Helper::input('Szélességi fok','latitude',old('latitude',$model->latitude),array('class'=>'form-control','id'=>'input-latitude'),$errors); !!}

													</div><!-- /.col-* -->

													<div class="col-sm-6">
														{!! \App\Http\Helper::input('Hosszúsági fok','longitude',old('longitude',$model->longitude),array('class'=>'form-control','id'=>'input-longitude'),$errors); !!}
													</div><!-- /.col-* -->
												</div><!-- /.row -->
												<div class="row">
												<h5 class = "col-md-12"><br>Bemutatkozás
													<br>
													<br>
												</h5>
												</div>
												<textarea class="form-control" name="lead" value="" rows="10">{{old('lead',$model->lead)}}</textarea>
												<div class="textarea-resize"></div>
											<div class="clearfix"><br></div>
											<div class="row">
												<h5 class = "col-md-12"><br>Nyítvatartás
													<br>
													<br>
												</h5>
												</div>

											<div class="clearfix"></div>
											<div class="row">
												<div class="col-md-8 col-md-offset-2">
													<table class="table table-condensed table-bordered" >
														@foreach($days as $k=> $d)
															<?php
															$opening =\Modules\ShopOpening\Entities\ShopOpening::firstOrCreate(['shop_id'=>$model->id,'day'=>$k],['day'=>$k,'shop_id'=>$model->id]);
															$from = '08:00';
															$to = '16:00';
															if(!empty($opening->open_from))
															{
																$from = date('H:i',strtotime($opening->open_from));
															}
															if(!empty($opening->open_to))
															{
																$to = date('H:i',strtotime($opening->open_to));
															}
															?>
															<tr>
																<th style="    vertical-align: baseline;    text-align: right; padding-top: 16px;"><b>{{$d}}</b></th>
																<td style="padding:1.3rem">
																	<label class="m-checkbox m-checkbox--square">
																		<input id="checkbox{{$k}}" type="checkbox" class="chkacl"
																			   name="closed[{{$k}}]" value="1"
																			   @if($opening->is_closed) checked="checked" @endif>
																		{{__('Zárva')}}
																		<span></span>
																	</label>

																</td>
																<td>{!! \App\Http\Helper::select('','from['.$k.']',$hours,old('from'.$k,$from),array('class'=>'form-control'),$errors); !!}</td>
																<td>{!! \App\Http\Helper::select('','to['.$k.']',$hours,old('to'.$k,$to),array('class'=>'form-control'),$errors); !!}</td>

															</tr>
														@endforeach
													</table>
													<div class="clearfix"></div>
												</div>
												</div>
												<div class="clearfix"></div>
												{!! \App\Http\Helper::formButtons('admin_shop_list') !!}
											</div>
											
										</div>

									</div>

								{!! \App\Http\Helper::formClose() !!}

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>


<style>
	#map-canvas{height:300px;margin:0px 0px 30px 0px}#map-canvas .form-control{right:42px !important;top:20px !important;width:300px !important}
	table .form-group {
		margin-bottom: .1rem !important;
	}
		 label {
			 display: initial !important;
			 margin-bottom: .5rem;
		 }
	h3 {
		padding-bottom: 20px;
		padding-top: 20px;
	}
	label.select-package.btn.btn-brand.m-btn.m-btn--custom.m-btn--pill.m-btn--wide.m-btn--uppercase.m-btn--bolder.m-btn--sm.active {
		background-color: #e91e63ad;
	}
</style>
@endsection
@push('scripts')
	<script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQlhY3yzuImGHOhMtuY0IYvHjLACaxpdw&libraries=places"></script>
			<script>
                $(document).ready(function() {
                    'use strict';
                    function initSearch() {
                        var input2 = document.getElementById('search_location');
                        var autocomplete2 = new google.maps.places.Autocomplete(input2);
                        google.maps.event.addListener(autocomplete2, 'place_changed', function() {

                            var place = autocomplete2.getPlace();

                            $('input[name="lat"]').val(place.geometry.location.lat());
                            $('input[name="lng"]').val(place.geometry.location.lng());
                            $('input[name="from"]').val(place.name);


                        });
                    }
                    function initialize() {

                        var mapOptions = {
                            center: new google.maps.LatLng(-33.8688, 151.2195),
                            zoom: 13
                        };
                        var map = new google.maps.Map(document.getElementById('map-canvas'),
                            mapOptions);

                        var input = /** @type {HTMLInputElement} */(
                            document.getElementById('pac-input'));

                        var types = document.getElementById('type-selector');
                        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

                        var autocomplete = new google.maps.places.Autocomplete(input);
                        autocomplete.bindTo('bounds', map);

                        var infowindow = new google.maps.InfoWindow();
                        var marker = new google.maps.Marker({

                            map: map,

                        });

                        google.maps.event.addListener(marker, "mouseup", function(event) {
                            $('#input-latitude').val(this.position.lat());
                            $('#input-longitude').val(this.position.lng());
                        });

                        google.maps.event.addListener(autocomplete, 'place_changed', function() {
                            infowindow.close();
                            marker.setVisible(false);
                            var place = autocomplete.getPlace();
                            if (!place.geometry) {
                                return;
                            }

                            // If the place has a geometry, then present it on a map.
                            if (place.geometry.viewport) {
                                map.fitBounds(place.geometry.viewport);
                            } else {
                                map.setCenter(place.geometry.location);
                                map.setZoom(17);
                            }
                            marker.setIcon(/** @type {google.maps.Icon} */({
                                url: place.icon,
                                size: new google.maps.Size(71, 71),
                                origin: new google.maps.Point(0, 0),
                                anchor: new google.maps.Point(17, 34),
                                scaledSize: new google.maps.Size(35, 35)
                            }));
                            marker.setPosition(place.geometry.location);
                            marker.setVisible(true);

                            $('#input-latitude').val(place.geometry.location.lat());
                            $('#input-longitude').val(place.geometry.location.lng());

                            var address = '';
                            if (place.address_components) {
                                address = [
                                    (place.address_components[0] && place.address_components[0].short_name || ''),
                                    (place.address_components[1] && place.address_components[1].short_name || ''),
                                    (place.address_components[2] && place.address_components[2].short_name || '')
                                ].join(' ');
                            }

                            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                            infowindow.open(map, marker);
                        });
                    }

                    if ($('#map-canvas').length != 0) {
                        google.maps.event.addDomListener(window, 'load', initialize);
                    }
                    if ($('#search_location').length != 0) {
                        google.maps.event.addDomListener(window, 'load', initSearch);

                    }
                    $(document).on('click','.select-package',function(){
                        $('.select-package').removeClass('active');
                        $(this).addClass('active');
                        $('.selected-package-text').text('Kiválaszt');
                        $(this).find('.selected-package-text').text('Aktív');
					})
                });

			</script>

@endpush

