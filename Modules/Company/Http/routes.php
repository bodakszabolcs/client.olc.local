<?php 
Route::group(['namespace' => 'Modules\Company\Http\Controllers', 'prefix' => 'admin/company', 'middleware' => ['web','auth', 'acl', 'accesslog']], function () {
		Route::match(['get', 'post', 'put'], '/edit/{id?}/{tab?}', 'CompanyController@Edit')->name('admin_company_edit');
		Route::match(['get', 'post', 'put'], 'shop/edit/{id?}', 'ShopController@Edit')->name('admin_shop_edit');
		Route::match(['get', 'post', 'put'], 'shop/list', 'ShopController@ShopList')->name('admin_shop_list');
		Route::match(['get'], 'company-sync', 'CompanyController@synCompanyAndShop')->name('admin_company_sync');

});
