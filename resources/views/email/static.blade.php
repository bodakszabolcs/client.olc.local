@extends('email.base')
@section('content')
    <div class="content formContent">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-lg-offset-4">

                    <h1 class="gradient-purple">{{$title}}</h1>

                    {!! $body !!}

                </div>
            </div>
        </div>
    </div>
@endsection