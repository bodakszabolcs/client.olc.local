<?php namespace Recze\SimplePay;

use Illuminate\Support\ServiceProvider;

class SimplepayServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes(
            [
                __DIR__.'/../config/simplepay.php' => config_path('simplepay.php'),
                __DIR__.'/../src/Listeners/SimplePayListeners.php' => app_path('Events/SimplePayListeners.php'),
                __DIR__.'/../src/Events/SimpleBackRef.php' => app_path('Events/SimpleBackRef.php'),
                __DIR__.'/../src/Events/SimpleIpn.php' => app_path('Events/SimpleIpn.php'),
                __DIR__.'/../src/Events/SimpleTimeout.php' => app_path('Events/SimpleTimeout.php'),

            ]
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app['router']->group(['namespace' => 'Recze\Simplepay','prefix' => 'simple', 'middleware' => ['web']], function () {
            require __DIR__.'/../routes.php';
        });
    }

}