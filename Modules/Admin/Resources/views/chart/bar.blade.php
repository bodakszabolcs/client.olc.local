
<script>
var colors =['#03A9F4',
	'#E91E63',
	'#f57f17',
	'#b71c1c',
	'#6a1b9a',
	'#82b1ff',
	'#f57f17',
	'#f4511e',
	'#9e9e9e',
	'#03A9F4',
	'#E91E63',
	'#b71c1c',
	'#6a1b9a',
	'#82b1ff',
	'#f4511e',
	'#9e9e9e',
	'#03A9F4',
	'#E91E63',
	'#b71c1c',
	'#6a1b9a',
	'#82b1ff',
	'#f57f17',
	'#f4511e',
	'#9e9e9e',
	'#03A9F4',
	'#E91E63',
	'#b71c1c',
	'#6a1b9a',
	'#82b1ff',
	'#f57f17',
	'#f4511e',
	'#9e9e9e',
	'#03A9F4',
	'#E91E63',
	'#b71c1c',
	'#6a1b9a',
	'#82b1ff',
	'#f57f17',
	'#f4511e',
	'#9e9e9e',];
	if ($("#{{$name}}").length) {

        var barChart{{$name}} = $("#{{$name}}");

        // line chart data
        var barData = {
            labels: [
				@foreach($labels as $l)
                    '{{$l}}' @if(!$loop->last) , @endif
				@endforeach
            ],
            datasets: [

				@foreach($data as $k => $d)
                {

                    label: '{{$k}}',

                    backgroundColor:colors[{{$loop->iteration-1}}],
                    data: [
							@foreach($labels as $l)
								@if( isset($d[$l]) )
                        			{ y: {{$d[$l]}},x:'{{$l}}' }
								@else
                        			{ y: 0,x:'{{$l}}' }
								@endif
								@if(!$loop->last) , @endif
								@endforeach

                    ],
                }
				@if(!$loop->last) , @endif
				@endforeach
            	]

        };


        // line chart init
	var {{$name}} =   new Chart(barChart{{$name}}, {
            type: 'bar',
            data: barData,
			options : {
                title: {
                    display: !1
                }
                ,
                tooltips: {
                    mode: "nearest", intersect: !1, position: "nearest", xPadding: 10, yPadding: 10, caretPadding: 10
                }
                ,

                responsive:!0,
                maintainAspectRatio:!1,
				scales: {
                    xAxes:[ {
                        //barThickness : 20,
                       // display:!1,
                        gridLines:!1,
                        scaleLabel: {
                            display: !1, labelString: "Nap"
                        }
                    }
                    ],
                    yAxes:[ {

                        scaleLabel: {
                            display: !1, labelString: "Érték"
                        }
                        ,
                        ticks: {
                            beginAtZero: !0
                        }
                    }
                    ]
				},
                elements: {
                    line: {
                        tension: .19
                    }
                    ,
                    point: {
                        radius: 4, borderWidth: 12
                    }
                }
                ,
                layout: {
                    padding: {
                        left: 0, right: 0, top: 10, bottom: 0
                    }
                }
			}

        });
    }

	</script>
