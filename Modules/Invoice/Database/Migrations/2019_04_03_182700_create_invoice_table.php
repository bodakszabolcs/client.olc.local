<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice', function (Blueprint $table) {
            $table->increments("id");
$table->text("transaction_id")->comment("Tranzakció azonosító");
$table->date("deadline")->comment("Fizetési határidő")->default(null);
$table->integer("final_gross")->comment("Bruttó összeg"")->default(0)->unsigned();\n$table->text("items")->comment("Tételek");
$table->integer("status")->comment("Állapot"")->default(0)->unsigned();\n$table->integer("payment_method")->comment("Fizetési mód"")->default(0)->unsigned();\n $table->timestamps();
 $table->softDeletes();
$table->index(["deleted_at"]);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice');
    }
}
