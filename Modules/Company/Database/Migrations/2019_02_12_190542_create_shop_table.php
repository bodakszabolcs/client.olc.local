<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop', function (Blueprint $table) {
            $table->increments("id");
			$table->integer("company_id")->comment("Cég")->default(0)->unsigned();
			$table->text("name")->comment("Név");
			$table->text("email")->comment("Email");
			$table->text("phone")->comment("Telefonszám");
			$table->text("address")->comment("Cím");
			$table->text("longitude")->comment("Szélességi fok");
			$table->text("latitude")->comment("Hosszúsági fok");
			$table->text("webpage")->comment("Weboldal");
			$table->text("facebook")->comment("Facebook");
			$table->text("instragram")->comment("Instagram");
			$table->text("lead")->comment("Bemutatkozás");
			$table->text("cover")->comment("Boritó");
			 $table->timestamps();
			 $table->softDeletes();
			$table->index(["deleted_at"]);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop');
    }
}
