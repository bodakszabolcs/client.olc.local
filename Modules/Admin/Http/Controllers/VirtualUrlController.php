<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Modules\Admin\Entities\VirtualUrl;

class VirtualUrlController extends AdminController
{
    public function __construct()
    {
        View::share('sub_title', __('Virtuális URLek'));
        View::share('subtitle_link', route('admin_virtual_url_list'));
    }

    public function VirtualUrlList(Request $request)
    {
        $model = new VirtualUrl();

        $list = $model->searchInModel($request->input());
        $list = $list->paginate(50)->withPath($request->path());

        View::share('title', __('Virtuális URL lista'));

        return view('admin::virtual_url.list', ['model' => $list, 'filterHeader' => $model->drawSearchTableHeader($request)]);


    }


    public function Edit(Request $request, $id = 0, $tab = 'data')
    {
        View::share('title', __('Virtuális URL szerkesztés'));

        $model = VirtualUrl::find($id);

        $errors = [];
        if (!is_null($model)) {

        } else {
            if ($id > 0) {
                return redirect(route('admin_virtual_url_list'));
            }
            $model = new VirtualUrl();
        }

        $tab = $request->input('tab', $tab);


        if ($request->isMethod('post')) {

            switch ($tab) {
                case 'data': {
                    $response = $this->validateData();
                    break;
                }

            }
            if ($response['status'] == 'success') {
                $model->fill($request->all());
                $model->save();
                session()->flash('success_message', $response['message']);
            } else {

                $request->session()->flash('error_message', 'Hiba a mentés során');
                $request->session()->put('field_errors', $response['cust_errors']);
                return redirect(route('admin_virtual_url_edit', [$id, $tab]))
                    ->withErrors($response['validator'])
                    ->withInput();
            }

            if ($request->has('save_and_exit')) {
                return redirect()->intended(session()->get('redirect_url', route('admin_virtual_url_list')));
            }

            if ($response['status'] == 'success') {

                return redirect()->intended(route('admin_virtual_url_edit', ['id' => $model->id, 'tab' => $tab]));
            }
        }

        return view('admin::virtual_url.edit', ['model' => $model, 'id' => $id, 'tab' => $tab, 'errors' => $errors]);
    }

    public function Delete(Request $request, $id)
    {
        try {
            if ($id == 0) {
                throw new ModelNotFoundException();
            }
            $model = VirtualUrl::findOrFail($id);
            $t_id = $model->id;
            $model->delete();
            $request->session()->flash('success_message', __('success_delete'));
        } catch (ModelNotFoundException $e) {
            $request->session()->flash('error_message', __('Az elem nem törölhető'));
        }

        return redirect()->back();
    }

    public function validateData()
    {
        $niceNames = array(
            'from_url' => 'Honnan',
            'code' => 'Kód'


        );
        $rules = [
            'from_url' => 'required',
            'code' => 'required'

        ];
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($niceNames);
        $not_valid = $validator->fails();
        if ($not_valid) {
            return ['status' => 'error', 'cust_errors' => $validator->messages()->toArray(), 'validator' => $validator];
        } else {
            return ['status' => 'success', 'message' => 'A mentés sikeres'];
        }

    }
}
