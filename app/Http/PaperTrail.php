<?php

namespace App\Http;

use Illuminate\Support\Facades\Auth;

class Papertrail
{
    private static $PAPERTRAIL_HOSTNAME;
    private static $PAPERTRAIL_PORT;

    private static $excluded = [
        'upload',
        'favicon',
        'refresh-message',
        'refresh-partnerlist',
        'get-notification',
        'get-all-notification'
    ];

    public static function send_remote_syslog($message, $component = "web", $program = "Trening24")
    {
        self::$PAPERTRAIL_HOSTNAME = config('app.PAPERTRAIL_HOST');
        self::$PAPERTRAIL_PORT = config('app.PAPERTRAIL_PORT');

        if (config('app.logging_env') == 'papertrail') {
            try {
                $program = str_replace("http://","",env('APP_URL'));
                $program = str_replace("https://","",env('APP_URL'));

                $sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
                foreach (explode("\n", $message) as $line) {
                    $syslog_message = "<22>" . date('M d H:i:s ') . $program . ' ' . $component . ': ' . $line;
                    if (strlen($syslog_message) > 65000)
                    {
                        $syslog_message = substr($syslog_message,0,64999);
                    }
                    socket_sendto($sock, $syslog_message, strlen($syslog_message), 0, self::$PAPERTRAIL_HOSTNAME, self::$PAPERTRAIL_PORT);
                }
                socket_close($sock);
            } catch (Exception $e) {
                //UDP missing
            }
        }
    }

    public static function request_method()
    {
        $method = (isset($_SERVER['REQUEST_METHOD'])) ? $_SERVER['REQUEST_METHOD'] : '-';
        $type = '';
        $params = '';
        if (isset($_SERVER['HTTP_HOST'])) {
            $route = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        } else {
            $route = 'CLI';
        }
        switch ($method) {
            case 'POST':
                $type = 'POST';
                $params = $_POST;
                break;
            case 'GET':
                $type = 'GET';
                $params = $_REQUEST;
                break;
            default:
                break;
        }
        return array(
            'type' => $type,
            'params' => $params,
            'url' => $route,
        );

    }

    static function getUserIP()
    {
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = @$_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = $remote;
        }

        return $ip;
    }

    public static function send_base_log()
    {
        $rm = self::request_method();
        ob_start();
        foreach(self::$excluded as $e)
        {
            if (strpos($rm['url'], $e) !== false)
            {
                $send = false;
            }
        }
        if (config('app.logging_env') == 'papertrail') {

            $uid = (Auth::check()) ? Auth::user()->id : '-1';

            self::send_remote_syslog(  'UID: '.$uid.' URL:' . $rm['url'] . ' TYPE:' . $rm['type'] . ' PARAMS:' . json_encode($rm['params']),self::getUserIP());
        }
        ob_clean();
    }

    public static function send_error_log($type, $message, $file = '', $line = '')
    {
        $rm = self::request_method();
        ob_start();
        foreach(self::$excluded as $e)
        {
            if (strpos($rm['url'], $e) !== false)
            {
                $send = false;
            }
        }
        if (config('app.logging_env') == 'papertrail') {

            $uid = (Auth::check()) ? Auth::user()->id : '-1';

            self::send_remote_syslog(  'UID: '.$uid.' URL:' . $rm['url'] . ' TYPE:' . $type . ' MESSAGE:' . $message.' FILE:'.$file.' LINE:'.$line,self::getUserIP());
        }
        ob_clean();
    }
}