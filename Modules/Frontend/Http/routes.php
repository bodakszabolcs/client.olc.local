<?php

Route::group(['namespace' => 'Modules\Frontend\Http\Controllers', 'prefix' => 'api', 'middleware' => ['api', 'auth:api', 'accesslog']], function ()
{

});



	Route::group(['namespace' => 'Modules\Frontend\Http\Controllers', 'prefix' => 'api', 'middleware' => ['web']], function () {

		Route::match(['post'], '/read-barcode/{id}', 'FrontendController@readBarcode')->name('frontend_read_barcode');
		Route::match(['post'], '/change-invoice-status/{transaction-id}', 'FrontendController@readBarcode')->name('frontend_read_barcode');
		Route::match(['post'], '/create-invoice', 'FrontendController@createInvoice')->name('frontend_create_invoice');
		Route::match(['post'], '/update-invoice', 'FrontendController@updateInvoice')->name('frontend_update_invoice');
		Route::match(['post'], '/get-coupon/{id}', 'FrontendController@getCoupon')->name('frontend_get_coupon');
		Route::match(['post'], '/get-my-purchase', 'FrontendController@getMyPurchase')->name('frontend_get_my_purchase');
		Route::get('qr-code/{id}', function ($id)
		{
            header('Content-type: image/ong');
			$data = [
				'scan'=> url('/api/read-barcode/'.$id),
				'coupon' =>url('/api/get-coupon/'.$id),
               // 'scan'=> 'http://192.168.1.17:8001/api/read-barcode/'.$id,
                //'coupon' =>'http://192.168.1.17:8001//api/get-coupon/'.$id,
                'chanel' => 'shop'.$id
			];
		$qrcode = \SimpleSoftwareIO\QrCode\Facades\QrCode::size(500)->generate(base64_encode(json_encode($data)));
        echo $qrcode;
		\Illuminate\Support\Facades\Storage::disk('uploads')->put('qr.png',$qrcode);
            die();
		});
	});
