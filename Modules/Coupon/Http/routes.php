<?php 
Route::group(['namespace' => 'Modules\Coupon\Http\Controllers', 'prefix' => 'admin/coupon', 'middleware' => ['web','auth', 'acl', 'accesslog']], function () { 
		Route::match(['get'], '/list', 'CouponController@CouponList')->name('admin_coupon_list');
		Route::match(['get', 'post', 'put'], '/edit/{id?}/{tab?}', 'CouponController@Edit')->name('admin_coupon_edit');
		Route::match(['get'], '/delete/{id}', 'CouponController@Delete')->name('admin_coupon_delete'); 
		Route::match(['get'], '/approve/{id}', 'CouponController@Approve')->name('admin_coupon_approve');
		Route::match(['get','post'], '/automatic-coupons', 'CouponController@automaticCoupon')->name('admin_coupon_automatic_coupon');
});
