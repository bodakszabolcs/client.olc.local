<?php

namespace App\Http\Middleware;

use Closure;
use App;
use DebugBar\DebugBar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CheckDebugbar
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!Session::get('is_superadmin')) {

        }
        \Debugbar::disable();
        return $next($request);
    }
}
