<?php

	namespace Modules\Company\Http\Controllers;

	use GuzzleHttp\Client;
	use GuzzleHttp\RequestOptions;
	use Illuminate\Support\Facades\Auth;
	use Modules\Company\Entities\Company;
	use App\Http\Controllers\Controller;
	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Session;
	use Modules\Company\Entities\Shop;
	use Validator;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\View;
	use Modules\Admin\Http\Controllers\AdminController;

	class CompanyController extends AdminController
	{
		public $niceNames = array(
			'name'	=>	'Cég neve',
			'tax_number'	=>	'Adószám',
			'email'	=>	'Email',
			'phone'	=>	'Telefonszám',
			'shipping_city'	=>	'Város',
			'shipping_zip'	=>	'Irányítószám',
			'shipping_address'	=>	'Cím',
			'shipping_country'	=>	'Ország',

			);
		public function __construct()
		{
			View::share('sub_title', __('Cégek'));
			View::share('subtitle_link', route('admin_company_edit'));
		}

		public function synCompanyAndShop(Request $request){
			if(Auth::user()->id == 1) {
				$client = new Client();
				try {
					$postData = rand(100, 999) . str_replace('=', '', base64_encode(json_encode([
										'url' => env('APP_URL'), 'date' => date('YmdHis')
									])));
					$params['form_params'] =['data' => $postData];
					$response = $client->post( env('SERVER_URL') . '/api/sync-company',
						[
							RequestOptions::JSON =>
								['data' => $postData]
						],
						['Content-Type' => 'application/json']);
					$response = json_decode($response->getBody()->getContents(),true);
					$company = $response["company"];
					$actualCompany = Company::first();
					if(is_null($actualCompany)){
						$actualCompany = new Company();
					}
					$actualCompany->fill($company);
					$actualCompany->save();
					$shops = $response["shops"];
					$actialShops = Shop::all();
					$ids =[];
					foreach ($shops as $s)
					{
						$ids[$s['id']]=$s['id'];
						$newShop = Shop::where('shop_id','=',$s['id'])->first();
						if(is_null($newShop)){
							$newShop  = new Shop();
						}
						$newShop->fill($s);
						$newShop->shop_id =$s['id'];
						$newShop->company_id = $actualCompany->id;
						$newShop->save();
					}
					foreach (Shop::whereNotIn('shop_id',$ids)->get() as $del){
						$del->delete();
					}

					$request->session()->flash('success_message', __('Sikeres szinkronizáció '));
					return redirect()->back();
				}catch (\Exception $e){
					$request->session()->flash('error_message', __('Hiba a szinkronizáció során ') .$e->getCode());
					return redirect()->back();
				}

			}
			else {
				$request->session()->flash('error_message', __('Nincs jogod a művelet elvégzéséhez'));
				return redirect()->back();
			}
		}

		public function Edit(Request $request, $tab='data')
		{
			View::share('title', __('Cég szerkesztés'));

			$model = Company::first();
			$errors = [];
			if ($request->isMethod('post')) {

				$tab = $request->input('tab', 'data');
				switch ($tab) {
					case 'data': {
						$response = $this->validateData();
						break;
					}
					default:
						$response = $this->validateData();
						break;
				}
				if ($response['status'] == 'success') {
					$model->fill($request->all());
					$model->save();
					Company::updateCompanyData($model->toArray());

						$request->session()->flash('success_message', __('A mentés sikeres'));
				} else {
					$request->session()->flash('error_message', __('Hiba a mentés során'));
					Session::put('field_errors', $response['cust_errors']);

					return redirect(route('admin_company_edit', [ $tab]))->withErrors($response['validator'])->withInput();
				}

				if ($response['status'] == 'success') {

					return redirect()->intended(route('admin_company_edit', ['id' => $model->id,'tab'=>$tab]));
				}
			}

			return view('company::edit', ['model' => $model,  'tab' => $tab, 'errors' => $errors]);
		}
		public function validateData()
		{

			$rules = [
			'name'	=>	'required',  
			'email'	=>	'required|email',
			'shipping_city'	=>	'required',  
			'shipping_zip'	=>	'required',  
			'shipping_address'	=>	'required',
			'shipping_country'	=>	'required',  

			];
			$validator = Validator::make(Input::all(), $rules);
			$validator->setAttributeNames($this->niceNames);
			$not_valid = $validator->fails();
			if ($not_valid) {
				return ['status' => 'error', 'cust_errors' => $validator->messages()->toArray(), 'validator' => $validator];
			} else {
				return ['status' => 'success', 'message' => 'A mentés sikeres'];
			}
		}




	}
