<?php

namespace Modules\Acl\Entities;

use App\LiquidModel;
use Illuminate\Database\Eloquent\Model;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends LiquidModel
{
    use Cachable, SoftDeletes;

    public $bannedUserRoles =[

        'admin_coupon_list',
        'admin_coupon_edit',
        'admin_coupon_delete',
        'admin_coupon_approve',
        'admin_pricerange_list',
        'admin_pricerange_edit',
        'admin_pricerange_delete',
        'admin_coupon_automatic_coupon',
        'admin_news_list',
        'admin_news_edit',
        'admin_news_delete',
        'admin_purchase_get_customers',
        'admin_purchase_get_customer_profile',
        'admin_purchase_list',
        'admin_purchase_send_noti',
        'admin_purchase_send_news_noti',
        'admin_purchase_save',
        'admin_purchase_delete',

    ];
    public $basicPackageRoles = [
        'admin_coupon_list',
        'admin_coupon_edit',
        'admin_coupon_delete',
        'admin_coupon_approve',


    ];
    public $proPackageRoles = [
        'admin_coupon_automatic_coupon',
        'admin_news_list',
        'admin_news_edit',
        'admin_news_delete',
    ];
    public $extendedPackageRoles = [
        'admin_coupon_automatic_coupon',
        'admin_news_list',
        'admin_news_edit',
        'admin_news_delete',
    ];

    protected $fillable = [
        'name', 'description',
    ];

    public $searchColumns = [
        'id' => 'text',
        'name' => 'text',
        'description' => 'text'
    ];

    public $searchColumnLabels = [
        'id' => 'ID',
        'name' => 'Név',
        'description' => 'Leírás'
    ];

    public function __construct($attributes = [])
    {
        config(['laravel-model-caching.cache-prefix' => str_slug(config('app.name'))]);

        parent::__construct($attributes);
    }

    public function acls()
    {
        return $this->hasMany('Modules\Acl\Entities\RoleAcl', 'role_id', 'id');
    }
}
