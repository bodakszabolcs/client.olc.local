@extends('frontend.layout')

@section('content')
   @include('frontend.partials.slider')
   @include('frontend.sections.top')
   @include('frontend.sections.workcategory')
   @include('frontend.sections.human')
   @include('frontend.sections.regisztracio')
   @include('frontend.sections.kolcsonzes')
   @include('frontend.sections.kozvetites')
   @include('frontend.sections.berszamfejtes')

   @include('frontend.sections.jogi')
   @include('frontend.sections.gold')
   @include('frontend.sections.mads')
   @include('frontend.sections.berkalkulator')
   @include('frontend.sections.marklar')



@endsection