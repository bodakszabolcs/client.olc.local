<div class="modal fade slide-up disable-scroll" id="cropperModal" tabindex="-1" role="dialog"
     aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5 class="modal-title">
                        {{__('Kép vágás')}}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <img id="image-for-crop" src="" class="img-fluid">
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-sm-8">
                            <div class="p-t-20 clearfix p-l-10 p-r-10">

                            </div>
                        </div>
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <button type="button"
                                    class="btn m-btn--square btn-primary btn-block m-t-5 send-new-image-size">{{__('Méretezés')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>