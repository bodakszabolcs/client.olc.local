<?php

Route::group(['namespace' => 'Modules\Acl\Http\Controllers', 'prefix' => 'admin/acl', 'middleware' => ['web','auth', 'acl', 'accesslog']], function ()
{
    Route::match(['get'], '/list', 'AclController@AclList')->name('admin_acl_list');
    Route::match(['get', 'post', 'put'], '/edit/{id?}', 'AclController@Edit')->name('admin_acl_edit');
    Route::match(['get'], '/delete/{id}', 'AclController@Delete')->name('admin_acl_delete');
});
