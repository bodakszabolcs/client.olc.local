<?php

namespace Modules\Frontend\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use App\Http\Helper;
use App\Http\Requests\CreateEventRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\Blog\Entities\Blog;
use Modules\Blog\Transformers\ListBlogResource;
use Modules\Blog\Transformers\ViewBlogResource;
use Modules\Cart\Entities\Cart;
use Modules\Cart\Http\Requests\CartCreateRequest;
use Modules\Cart\Http\Requests\CartUpdateRequest;
use Modules\Cart\Transformers\ListCartResource;
use Modules\Company\Entities\Shop;
use Modules\Country\Entities\Country;
use Modules\Country\Transformers\ListCountryResource;
use Modules\Coupon\Entities\Coupon;
use Modules\Events\Entities\Events;
use Modules\Events\Transformers\ListEventsResource;
use Modules\Events\Transformers\ViewEventsResource;
use Modules\Invoice\Entities\Invoice;
use Modules\Orders\Entities\OrderItems;
use Modules\Orders\Entities\Orders;
use Modules\Orders\Http\Requests\OrdersCreateRequest;
use Modules\Orders\Transformers\CustomOrderItemsResource;
use Modules\Orders\Transformers\ListOrdersResource;
use Modules\Product\Entities\Product;
use Modules\Product\Transformers\ListProductResource;
use Modules\Purchase\Entities\Purchase;

class FrontendController extends AbstractLiquidController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function createInvoice(Request $request)
    {
        $invoiceData = Helper::decodeData($request->input('data'));
        $invoice = new Invoice();
        $invoice->fill($invoiceData['invoice']);
        $invoice->save();
        return response()->json('OK');
    }
    public function updateInvoice(Request $request)
    {
        $invoiceData = Helper::decodeData($request->input('data'));


        $invoice = Invoice::where('transaction_id','=',$invoiceData['invoice']['transaction_id'])->first();
        $invoice->fill($invoiceData['invoice']);
        $invoice->save();
        if($invoice->status== 4 || $invoice->status==5 )
        {
            foreach (Shop::all() as $shop){
                $shop->payed_package_id = $shop->package_id;
                $shop->save();
            }
            Artisan::call('invoice:payed');
        }
        return response()->json('OK');
    }

    public function readBarcode(Request $request,$id)
    {
        $purchase = Purchase::where('handled', '=', 0)->where('user_id', '=', $request->input('id'))->where('shop_id','=',$id)->first();
        if (is_null($purchase)) {
            $purchase = new Purchase();
            $purchase->user_id = $request->input('id');
            $purchase->shop_id = $id;
            $purchase->user_name = $request->input('name', '');
            $purchase->token = $request->input('token', '');
            $purchase->avatar = $request->input('avatar', '');
            $purchase->coupon = $request->input('coupon', '');
            $purchase->save();

            file_put_contents('app/purchase'.$id.'.json', json_encode(Purchase::where('handled', '=', 0)->where('shop_id','=',$id)->get()->toArray()));
            file_put_contents('app/updated'.$id.'.json', json_encode(['updated' => time()]));
            return response()->json(['status' => 'OK', 'message' => 'A mentés sikerse'], 200);
        } else {
            return response()->json(['status' => 'OK', 'message' => 'Van megrendelésed'], 200);
        }
    }

    public function getCoupon(Request $request,$user_id)
    {
        $coupons = Coupon::where('user_id', '=', $user_id)->where('approved','=',1)->whereNull('used')->where('expire','>=',date('Y-m-d'))->get();
        return response()->json(['status' => 'OK', 'coupons' => $coupons->toArray()], 200);
    }

    public function getMyPurchase(Request $request)
    {
        //todo itt majd jön is a shop_id is
        //dd($request->all());
        $purchase = Purchase::select(DB::raw('sum(price) as price, discount, DATE(`created_at`) as created, coupon'))->where('user_id', '=', $request->input('user_id'))->where('handled', '=', 1)->orderBy('created_at', 'desc')->groupBy(DB::raw('DATE(`created_at`)'))->get();
        return response()->json(['status' => 'OK', 'purchase' => $purchase], 200);

    }
}
