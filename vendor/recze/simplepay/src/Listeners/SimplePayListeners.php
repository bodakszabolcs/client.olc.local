<?php

namespace App\Listeners;

use App\Events\SimpleIpn;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SimplePayListeners
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param $event
     */
    public function Ipn($event)
    {

    }

    /**
     * @param $event
     */
    public function TimeOut($event)
    {

    }

    /**
     * @param $event
     */
    public function BackRef($event)
    {

    }


    /**
     * @param $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\SimpleIpn',
            'App\Listeners\SimplePayListeners@Ipn'
        );

        $events->listen(
            'App\Events\SimpleTimeOut',
            'App\Listeners\SimplePayListeners@TimeOut'
        );

        $events->listen(
            'App\Events\SimpleBackRef',
            'App\Listeners\SimplePayListeners@BackRef'
        );
    }
}
