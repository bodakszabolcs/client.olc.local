@extends('frontend.layout')
@section('content')
   <style>
      img.small{width:50px !important;}
      table {
         background-color: transparent;
         border: none !important;
      }
      td {
         border: none;
         padding: 6px;
      }
      .gmap_canvas{
         overflow:hidden;
         padding-bottom:56.25%;
         position:relative;
         height:0;
      }
      .gmap_canvas iframe{
         left:0;
         top:0;
         height:100%;
         width:100%;
         position:absolute;
      }
   </style>
<section id="munkak" class="margin60">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 text-center">
            <h2>{{optional($page)->title}}</h2>
         </div>
         <div class="clearfix"></div>
         <br>
         <div class="row">
         <div class="col-md-6">
            {!! optional($page)->content !!}
         </div>
         <div class="col-md-6">
<div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=budapest%20erkel%20utca%203&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div></div>
         </div>
         </div>
         @php

         @endphp
          <div class="clearfix"></div>

      </div>
   </div>
</section>

@endsection