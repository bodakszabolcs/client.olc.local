<?php
echo "<?php

	namespace Modules\\".ucfirst($model)."\\Http\Controllers;

	use Modules\\".$model."\\Entities\\".$model.";
	use App\Http\Controllers\Controller;
	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Session;
	use Validator;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\View;
	use Modules\Admin\Http\Controllers\AdminController;

	class ".$model."Controller extends AdminController
	{
		public \$niceNames = array(\n";
			foreach ($data['column_name'] as $k =>$name)
			{
				echo "			'".$name."	=>	".$data['column_label'][$k]."',  \n";
			}
echo "
			);
		public function __construct()
		{
			View::share('sub_title', __('".$data['menu_name']."'));
			View::share('subtitle_link', route('admin_".$root."_list'));
		}
";

echo "
		public function ".$model."List(Request \$request)
		{
			\$model = new ".$model."();
			\$list = \$model->searchInModel(\$request->input());
			\$list = \$list->orderBy('id', 'desc')->paginate(50)->withPath(\$request->path());
			View::share('title', __('".$data['menu_name']." lista'));

			return view('".$root."::list', [
				'model' => \$list, 'filterHeader' => \$model->drawSearchTableHeader(\$request)
			]);

		}
";

if($data['edit'])
{

echo "
		public function Edit(Request \$request, \$id = 0,\$tab='data')
		{
			View::share('title', __('".$data['menu_name']." szerkesztés'));

			\$model = ".$model."::find(\$id);
			\$errors = [];
			if (!is_null(\$model)) {

			} else {
				if (\$id > 0) {
					return redirect(route('admin_".$root."_list'));
				}
				\$model = new ".$model."();
			}
			if (\$request->isMethod('post')) {

				\$tab = \$request->input('tab', 'data');
				switch (\$tab) {
					case 'data': {
						\$response = \$this->validateData();
						break;
					}
					default:
						\$response = \$this->validateData();
						break;
				}
				if (\$response['status'] == 'success') {
					\$model->fill(\$request->all());
					\$model->save();
						\$request->session()->flash('success_message', __('A mentés sikeres'));
				} else {
					\$request->session()->flash('error_message', __('Hiba a mentés során'));
					Session::put('field_errors', \$response['cust_errors']);

					return redirect(route('admin_".$root."_edit', [\$id, \$tab]))->withErrors(\$response['validator'])->withInput();
				}
				if (\$request->has('save_and_exit')) {
					return redirect()->intended(session()->get('redirect_url', route('admin_".$root."_list')));
				}
				if (\$response['status'] == 'success') {

					return redirect()->intended(route('admin_".$root."_edit', ['id' => \$model->id,'tab'=>\$tab]));
				}
			}

			return view('".$root."::edit', ['model' => \$model, 'id' => \$id, 'tab' => \$tab, 'errors' => \$errors]);
		}
		public function validateData()
		{

			\$rules = [\n";

	foreach ($data['required'] as $k =>$bool)
	{
		if($bool)
		{
			echo "			'".$data['column_name'][$k]."'	=>	'required',  \n";
		}
	}
	echo "
			];
			\$validator = Validator::make(Input::all(), \$rules);
			\$validator->setAttributeNames(\$this->niceNames);
			\$not_valid = \$validator->fails();
			if (\$not_valid) {
				return ['status' => 'error', 'cust_errors' => \$validator->messages()->toArray(), 'validator' => \$validator];
			} else {
				return ['status' => 'success', 'message' => 'A mentés sikeres'];
			}
		}
";
}
if($data['delete'])
{

echo "
		public function Delete(Request \$request, \$id)
		{
			try {
				if (\$id == 0) {
					throw new ModelNotFoundException();
				}
				\$model = ".$model."::findOrFail(\$id);
				\$model->delete();
				\$request->session()->flash('success_message', __('success_delete'));
			} catch (ModelNotFoundException \$e) {
				\$request->session()->flash('error_message', __('Az elem nem törölhető'));
			}
			return redirect()->intended(route('admin_".$root."_list'));
		}
";
}
if($data['export'])
{

}

echo "

	}";