<?php

namespace Modules\Admin\Http\Controllers;

use App\Mail\SupportEmail;
use App\Mail\Template;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Intervention\Image\Facades\Image;
use Jenssegers\Agent\Facades\Agent;
use Modules\Admin\Entities\Ticket;
use Modules\Admin\Entities\VirtualUrl;
use Modules\Article\Entities\Article;
use Modules\Article\Entities\Category;
use Modules\Banner\Entities\Banner;
use Modules\City\Entities\City;
use Modules\Company\Entities\Company;
use Modules\Company\Entities\CompanyContact;
use Modules\Company\Entities\CompanyIndustry;
use Modules\Document\Entities\Document;
use Modules\Driverlicense\Entities\Driverlicense;
use Modules\Education\Entities\Education;
use Modules\Email\Entities\Email;
use Modules\Feor\Entities\Feor;
use Modules\Language\Entities\Language;
use Modules\Menu\Entities\Menu;
use Modules\Nationality\Entities\Nationality;
use Modules\Page\Entities\Page;
use Modules\Position\Entities\Position;
use Modules\Project\Entities\Project;
use Modules\ProjectStatus\Entities\ProjectStatus;
use Modules\Specialtyeducation\Entities\Specialtyeducation;
use Modules\Teaor\Entities\Teaor;
use Modules\Trainingarea\Entities\Trainingarea;
use Modules\User\Entities\User;
use Modules\Work\Entities\Work;
use Modules\Workcategory\Entities\Workcategory;
use Modules\Worker\Entities\Worker;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use Nwidart\Modules\Facades\Module;
use phpseclib\Crypt\Hash;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

class AdminController extends \App\Http\Controllers\Controller
{
    use AuthenticatesUsers;

    public function SupportEmail(Request $request)
    {
        $config = config('app.emails');
        $appname = config('app.name');

        $ticket = new Ticket();
        $ticket->user_id = Auth::user()->id;
        $ticket->user_name = Auth::user()->lastname.' '.Auth::user()->firstname;
        $ticket->type = $request->input('error-type');
        $ticket->page = $request->input('error-page');
        $ticket->content = $request->input('error-detail');
        $ticket->status = 1;
        $ticket->save();

	    $coll = new Collection();
	    $coll->email = 'bodak.szabolcs@gmail.com';
	    $coll->name = 'Bodak Szabolcs';
	    $user = Auth::user();

        Mail::to($coll)
            ->send(new SupportEmail($request->all(), $user));

        return response()->json([
            'status' => 'OK',
            'message' => __('A hiba sikeresen elküldve')
        ]);
    }
    public function pdfPreview(Request $request,$shop_id){
        $pdfConfig = [
            'mode' => '',
            'format' => 'A4',
            'default_font_size' => 0,
            'default_font' => '',
            'margin_left' => 15,
            'margin_right' => 15,
            'margin_top' => 5,
            'margin_bottom' => 5,
            'margin_header' => 9,
            'margin_footer' => 9,
            'orientation' => 'P',
        ];
        $client = new Client();
        $shop = \Modules\Company\Entities\Shop::where('shop_id','=',$shop_id)->first();
        $mpdf = new Mpdf($pdfConfig);
        $mpdf->WriteHTML(view('admin::pdf',['shop'=>$shop])->render());
        $mpdf->debug=true;
        $mpdf->Output('test.pdf',Destination::INLINE);
        die();
    }

    public function Search(Request $request)
    {
        $param = $request->input('query');

        if (strlen($param) < 1)
        {
            return response()->view('admin::partials.search',['results' => []]);
        }
        $results =[];
		$model = new Worker();
        $list = $model->searchInModel(['search-fillable'=>$param]);
        $worker = $list->limit(10)->get() ;
        $users = User::where(DB::raw('CONCAT(lastname," ",firstname)'),'LIKE','%'.$param.'%')->limit(10)->orWhere('email','LIKE','%'.$param.'%')->get();
		$cities = City::where('name','Like','%'.$param.'%')->limit(10)->get();
		$company = Company::where('name','Like','%'.$param.'%')->limit(10)->get();
	    $project = Project::join('company_industries','company_industries.id','=','industry_id')->join('companies','companies.id','=','company_industries.company_id')
		    ->select(DB::raw('projects.*,companies.name as company_name, company_industries.name as industry_name'))
		    ->where('companies.name','Like','%'.$param.'%')->orWhere('company_industries.name','Like','%'.$param.'%')->limit(10)->get();
		$industry = CompanyIndustry::where('name','Like','%'.$param.'%')->limit(10)->get();
		$contact = CompanyContact::where('name','Like','%'.$param.'%')->whereOr('email','Like','%'.$param.'%')->limit(10)->get();
		$driverlicense = Driverlicense::where('name','Like','%'.$param.'%')->limit(10)->get();
		$education = Education::where('name','Like','%'.$param.'%')->limit(10)->get();
		$feor = Feor::where('name','Like','%'.$param.'%')->orWhere('key','Like','%'.$param.'%')->limit(10)->get();
		$language = Language::where('name','Like','%'.$param.'%')->limit(10)->get();
		$nationality = Nationality::where('name','Like','%'.$param.'%')->limit(10)->get();
		$position = Position::where('name','Like','%'.$param.'%')->limit(10)->get();
		$documents = Document::where('name','Like','%'.$param.'%')->limit(10)->get();
		$projectstatus = ProjectStatus::where('name','Like','%'.$param.'%')->limit(10)->get();
		$specialeducation = Specialtyeducation::where('name','Like','%'.$param.'%')->limit(10)->get();
		$teaor = Teaor::where('name','Like','%'.$param.'%')->where('key','Like','%'.$param.'%')->limit(10)->get();
		$trainingarea = Trainingarea::where('name','Like','%'.$param.'%')->limit(10)->get();
		$work = Work::where('name','Like','%'.$param.'%')->orWhere('description','Like','%'.$param.'%')->limit(10)->get();
		$workCategory = Workcategory::where('name','Like','%'.$param.'%')->limit(10)->get();

		 foreach($worker as $w)
	    {
		    $results[] = ['icon' => 'fa fa-briefcase', 'name' => $w->name, 'route' => route('admin_worker_edit',[$w->id]), 'extra' => $w->email,'type'=>'Jelentkezők'];
	    }
        foreach($users as $usr)
        {
            $results[] = ['icon' => 'fa fa-user', 'name' => $usr->lastname.' '.$usr->firstname, 'route' => route('admin_user_edit',[$usr->id]), 'extra' => $usr->email,'type'=>'Felhasználó'];
        }
	    foreach($cities as $c)
	    {
		    $results[] = ['icon' => 'fa fa-building', 'name' => $c->name, 'route' => route('admin_city_edit',[$c->id]), 'type'=>'Város'];
	    }
	    foreach($company as $c)
	    {
		    $results[] = ['icon' => 'fa fa-industry', 'name' => $c->name, 'route' => route('admin_company_edit',[$c->id]), 'type'=>'Cég'];
	    }
	    foreach($industry as $c)
	    {
		    $results[] = ['icon' => 'fa fa-industry', 'name' => $c->name, 'route' => route('admin_companyindustry_edit',[$c->company_id,$c->industry_id]), 'type'=>'Tevékenység'];
	    }
	    foreach($project as $c)
	    {
		    $results[] = ['icon' => 'fa fa-tasks', 'name' => $c->company_name.'/'.$c->industry_name, 'route' => route('admin_project_view',[$c->id]), 'type'=>'Project'];
	    }
	    foreach($contact as $c)
	    {
		    $results[] = ['icon' => 'fa fa-users', 'name' => $c->name, 'route' => route('admin_companycontact_edit',[$c->company_id,$c->industry_id]),'type'=>'Kapcsolattartó'];
	    }
	    foreach($driverlicense as $c)
        {
	        $results[] = ['icon' => 'fa fa-car', 'name' => $c->name, 'route' => route('admin_driverlicense_edit',[$c->id]), 'type'=>'Jogosítvány'];
        }
	    foreach($education as $c)
	    {
		    $results[] = ['icon' => 'fa fa-file-word-o', 'name' => $c->name, 'route' => route('admin_education_edit',[$c->id]), 'type'=>'Végzettség'];
	    }
	    foreach($feor as $c)
	    {
		    $results[] = ['icon' => 'fa fa-gavel', 'name' => $c->name, 'route' => route('admin_feor_edit',[$c->id]), 'type'=>'FEOR'];
	    }
	    foreach($language as $c)
	    {
		    $results[] = ['icon' => 'fa fa-columns', 'name' => $c->name, 'route' => route('admin_language_edit',[$c->id]), 'type'=>'Nyelv'];
	    }
	    foreach($nationality as $c)
	    {
		    $results[] = ['icon' => 'fa fa-user', 'name' => $c->name, 'route' => route('admin_nationality_edit',[$c->id]), 'type'=>'Állampolgárság'];
	    }
	    foreach($position as $c)
	    {
		    $results[] = ['icon' => 'fa fa-cubes', 'name' => $c->name, 'route' => route('admin_position_edit',[$c->id]), 'type'=>'Munkakör'];
	    }
	    foreach($documents as $c)
	    {
		    $results[] = ['icon' => 'fa fa-file', 'name' => $c->name, 'route' => route('admin_document_edit',[$c->id]), 'type'=>'Dokumentum'];
	    }
	    foreach($projectstatus as $c)
	    {
		    $results[] = ['icon' => 'fa fa-star', 'name' => $c->name, 'route' => route('admin_projectstatus_edit',[$c->id]), 'type'=>'Projekt státusz'];
	    }
	    foreach($specialeducation as $c)
	    {
		    $results[] = ['icon' => 'fa fa-user-secret', 'name' => $c->name, 'route' => route('admin_specialtyeducation_edit',[$c->id]), 'type'=>'Speciális végzettség'];
	    }
	    foreach($teaor as $c)
	    {
		    $results[] = ['icon' => 'fa fa-flag', 'name' => $c->name, 'route' => route('admin_teaor_edit',[$c->id]), 'type'=>'TEÁOR'];
	    }
	    foreach($trainingarea as $c)
	    {
		    $results[] = ['icon' => 'fa fa-code-fork', 'name' => $c->name, 'route' => route('admin_trainingarea_edit',[$c->id]), 'type'=>'Képzési terület'];
	    }
	    foreach($workCategory as $c)
	    {
		    $results[] = ['icon' => 'fa fa-briefcase', 'name' => $c->name, 'route' => route('admin_workcategory_edit',[$c->id]), 'type'=>'Munka kategória'];
	    }
       /* foreach($category as $cat)
        {
            $results[] = ['icon' => 'fa fa-th', 'name' => $cat->name, 'route' => route('admin_category_edit',[$cat->parent_id, $cat->id]), 'extra' => ''];
        }

        foreach($pages as $pg)
        {
            $results[] = ['icon' => 'fa fa-file', 'name' => $pg->title, 'route' => route('admin_page_edit',[$pg->id]), 'extra' => '/'.$pg->slug];
        }

        foreach($articles as $art)
        {
            $results[] = ['icon' => 'fa fa-file-text', 'name' => $art->title, 'route' => route('admin_article_edit',[$art->id]), 'extra' => '/'.$art->slug];
        }

        foreach($blog as $art)
        {
            $results[] = ['icon' => 'fa fa-file-text', 'name' => $art->title, 'route' => route('admin_article_edit',[$art->id]), 'extra' => '/'.$art->slug];
        }

        foreach($email as $e)
        {
            $results[] = ['icon' => 'fa fa-at', 'name' => $e->name, 'route' => route('admin_email_edit',[$e->id]), 'extra' => $e->subject];
        }

        foreach($banner as $b)
        {
            $results[] = ['icon' => 'fa fa-vcard', 'name' => $b->name, 'route' => route('admin_banner_edit',[$b->id]), 'extra' => ''];
        }

        foreach($menu as $m)
        {
            $results[] = ['icon' => 'fa fa-bars', 'name' => $m->name, 'route' => route('admin_menu_edit',[$m->id]), 'extra' => ''];
        }

        foreach($virtual_url as $vu)
        {
            $results[] = ['icon' => 'fa fa-eye', 'name' => $vu->from_url, 'route' => route('admin_virtual_url_edit',[$vu->id]), 'extra' => $vu->to_url];
        }*/

        return response()->view('admin::partials.search',['results' => $results]);
    }



    public function TicketList(Request $request)
    {
        $model = new Ticket();
        $list=$model->searchInModel($request->input());
        $list = $list->paginate(15)->withPath($request->path());

        View::share('title',__('Hibajegy Lista'));

        return view('admin::index.tickets', ['model' => $list, 'filterHeader'=>$model->drawSearchTableHeader($request)]);
    }

    public function RunCommand(Request $request)
    {

        if($request->method()=='POST')
        {
            try{
                dump($request->input('command'));
                Artisan::call($request->input('command'));
                $request->session()->flash('success_message',__('A parancs futtatása sikeres'));
                return redirect()->back();
            }
            catch(\Exception $e)
            {
                $request->session()->flash('error_message',__('Hiba a parancs futtatása közben'.$e->getMessage().$e->getLine()));
                return redirect()->back();
            }

        }

        return view('admin::index.command');


    }

    public function CopyContent(Request $request, $type, $id)
    {
        switch($type)
        {
            case 'article' : {
                $old_model = Article::where('id','=',$id)->first();
                $old_arr = clone($old_model);
                $old_arr = $old_arr->toArray();
                $new_model = new Article;
                $new_model->fill($old_arr);

                if ($old_arr['tags'] == null)
                {
                    $new_model->tags = array();
                }

                $new_model->save();

                $new_model->categories()->sync($old_model->categories);

                $request->session()->flash('success_message',$old_model->id.' ID-vel rendelkező tartalom másolása sikeres volt. Új tartalom ID: '.$new_model->id);
                return redirect(route('admin_article_list'));
                break;
            }
        }
    }

    public function CropImage(Request $request)
    {
        $name= explode('/',$request->input('image'));
        File::makeDirectory(public_path('/uploads/'.Auth::user()->id.'/cropped/'.$request->input('type').'/'), $mode = 0777, true, true);
        Image::make(file_get_contents(public_path(str_replace(env('APP_URL'),'',$request->input('image')))))
            ->crop($request->input('width'), $request->input('height'), $request->input('x'), $request->input('y'))
            ->save(public_path('/uploads/'.Auth::user()->id.'/cropped/'.$request->input('type').'/'.$name[sizeof($name)-1]));

        return response()->json([
            'status' => 'OK',
            'file'=>'/uploads/'.Auth::user()->id.'/cropped/'.$request->input('type').'/'.$name[sizeof($name)-1]
        ]);
    }

    public function MaintenanceMode(Request $request)
    {
        if ($_SESSION['maintenance_mode']) {
            Artisan::call('up');
        } else {

            Artisan::call('down');
        }

        return response()->json(['status' => 'OK'],200);
    }

    public function CompactMode(Request $request)
    {
        $cm = Cache::tags(['user-settings'])->get(Auth::id() . '-compact-mode');

        if ($cm == 1) {
            Cache::tags(['user-settings'])->forever(Auth::id() . '-compact-mode',0);
            return response()->json(['status' => '0'],200);
        } else {
            Cache::tags(['user-settings'])->forever(Auth::id() . '-compact-mode',1);
            return response()->json(['status' => '1'],200);
        }

    }
	public function Register(Request $request,$hash){
    	$user = User::where('hash_2','=',$hash)->first();
    	$error = '';
    	if(is_null($user)){
		    $error = 'A megadott link hibás. Kérlek lépj kapcsolatba velünk!';
		    return view('admin::register', ['user' => $user,'error'=>$error]);
	    }
    	if($user->hash_2_date < date('Y-m-d H:i:s')){
    		$error = 'A megadott link lejárt. Kérlek lépj kapcsolatba velünk!';
		    return view('admin::register', ['user' => $user,'error'=>$error]);
	    }
		if ($request->isMethod('post')) {

				$rules['password'] = 'required|min:8|max:16';
				$rules['password_again'] = 'same:password';


			$validator = Validator::make($request->all(), $rules);
			$validator->setAttributeNames(['password'=>'Jelszó','password_again'=>'Jelszó ismét']);
			if ($request->has('password') && $request->input('password') != $request->input('password_again'))
			{
				$validator->after(function($validator) {
					$validator->errors()->add('password', __('A jelszó és jelszó újra mezők nem egyeznek!'));
				});
			}
			if ($validator->fails()) {

				return redirect()->back()
					->withErrors($validator)
					->withInput();
			}
			$user->password = \Illuminate\Support\Facades\Hash::make($request->password);
			$user->hash_2='';
			$user->save();
			Auth::login($user);
			$request->session()->flash('success_message','Sikeres regisztráció');
			return redirect(route('admin_dashboard'));

		}
		return view('admin::register', ['user' => $user,'error'=>$error]);


	}
    public function Login(Request $request)
    {
        if ($request->isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect(route('admin_login'))
                    ->withErrors($validator)
                    ->withInput();
            }

            $email = $request->input('email');
            $password = $request->input('password');

            if ($request->has('remember') && $request->input('remember') == 1)
            {
                $remember = true;
            } else {
                $remember = false;
            }

            $disabled = DB::table('_logins')->where('visitor', $request->getClientIp())->where('disabled_until', '>', date("Y-m-d H:i:s"))->count();

            if (Auth::attempt(['email' => $email, 'password' => $password],$remember) && $disabled < 1) {


                // Authentication passed...
                DB::table('_logins')->where('visitor', $request->getClientIp())->delete();

                if (Auth::user()->roles()->where('role_id',2)->first() == null)
                {
                    $validator->errors()->add('email', __('Nincs jogosultsága a belépéshez!'));

                    return redirect(route('admin_login'))
                        ->withErrors($validator)
                        ->withInput();
                }

                if (Auth::user()->roles()->where('role_id', 1)->first() != null) {
                    return redirect()->intended(route('admin_dashboard'));
                } else {
                    $menu = config('menu');

                    $redirect = null;
                    foreach ($menu as $item):
                        if (!is_array($item['route']) && self::checkAccessForUri($item['route'])) {
                            $redirect = $item['route'];
                            break;
                        } else if (is_array($item['route']))
                        {
                            foreach ($item['route'] as $rt)
                            {
                                if (self::checkAccessForUri($rt['route']) && strpos($rt['route'],'list'))
                                {
                                    $redirect = $rt['route'];
                                    break;
                                }
                            }
                        }
                    endforeach;

                    if ($redirect != null) {
                        return redirect()->intended(route($redirect));
                    } else {
                        return response()->view('errors.401', [], 401);
                    }
                }

            } else {
                if ($disabled == 0) {
                    $logins = DB::table('_logins')->where('visitor', $request->getClientIp())->where('created_at', 'LIKE', '%' . date("Y-m-d") . '%')->count();

                    $validator->errors()->add('email', __('Hibás Email/Jelszó.'));


                    if ($logins+1 > 4) {
                        $disabled_until = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s") . ' + 1 day'));

                    } else {

                        $disabled_until = null;
                    }
                    DB::table('_logins')->insert([
                        'email' => $request->input('email'),
                        'password' => $request->input('password'),
                        'visitor' => $request->getClientIp(),
                        'created_at' => date("Y-m-d H:i:s"),
                        'disabled_until' => $disabled_until
                    ]);
                } else {
                    Notification::send(User::getSuperAdmins(),new ApplicationNotification(__('IP ban: ').$email,null,true,'admin_login',route('admin_login')));
                    $validator->errors()->add('email', __('IP címe tiltásra került 24 órára!'));
                }

                return redirect(route('admin_login'))
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        return view('admin::login', ['disabled_until' => '']);
    }

    public function Logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        unset($_SESSION['iss']);

        return redirect(route('admin_login'));
    }

    function Modules(Request $request, $id = '')
    {
        View::share('title',__('Modul Lista'));

        if ($request->isMethod('GET'))
        {
            return view('admin::index.modules');
        }

        if ($request->isMethod('POST') && !empty($id))
        {
            $module = Module::find($id);

            if ($request->has('turn_off') && !empty($request->input('turn_off')))
            {
                $module->disable();
            } else {
                $module->enable();
            }
        }

        return redirect()->back();

    }


    public function Dashboard(Request $request)
    {
        View::share('title',__('Dashboard'));

        return view('admin::index.index', []);
    }

    public function ChangeLanguage(Request $request)
    {
        $locale = $request->input('locale');

        $request->session()->put('locale', $locale);

        Cache::tags(['user-settings'])->forever(Auth::id() . '-language',$locale);

        return response()->json(['status' => 'OK'],200);
    }
    public function fileUpload(Request $request){
        $input = Input::all();
        $rules = array(
            'file' => 'image|max:3000',
        );

        $validation = Validator::make($input, $rules);

        if ($validation->fails())
        {
            return Response::make($validation->errors->first(), 400);
        }



        $file = Storage::disk('uploads')->put('uploads/news', $request->file('file'));
        $image_resize = \Intervention\Image\Facades\Image::make($file);
        $image_resize->resize(720,null,function ($constraint) {
            $constraint->aspectRatio();
        })->save();
        ImageOptimizer::optimize(public_path($file));
        return response()->json([
            'status' => 'OK',
            'file'=>$file
        ]);



    }
}
