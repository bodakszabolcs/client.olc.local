@extends('admin::layouts.layout')
@section('content')
   <div class='row'>
        <div class='col-12'>
            <div class='m-portlet'>
                <div class='m-portlet__head'>
                    <div class='head-btn'>
            {!! \App\Http\Helper::text_button(__('Új Hozzáadás'),array('route_name' => 'admin_shopopening_edit', 'params' => array()),'fa-plus', array('class'=>'btn m-btn--square btn-primary')) !!}
          </div>
                </div>
                <div class='m-portlet__body'>
                {!! $filterHeader !!}
               <div class='table-responsive'>
                        <table class='table table-bordered table-hover mb-150'>
                                <thead>
                                <tr>
                                    <th class='sorting' data-column='id'>{{__('ID')}}</th>
                                    		<th class='sorting' data-column='1'>{{__('Zárva')}}</th>
                                    		<th class='sorting' data-column='2'>{{__('Tól')}}</th>
                                    		<th class='sorting' data-column='3'>{{__('Ig')}}</th>
                              <th width='15%'>{{__('Művelet')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($model as $m)

                                    <tr>
                                        <td>{{$m->id}}</td>
                                        <td>{{$m->is_closed}}</td>
                                        <td>{{$m->open_from}}</td>
                                        <td>{{$m->open_to}}</td>
                                        <td>                                                    {!! \App\Http\Helper::text_button(__('Szerkesztés'),array('route_name'=>'admin_shopopening_edit', 'params' => array($m->id)),'fa-edit', array('class'=>'dropdown-item')) !!}
                                                    {!! \App\Http\Helper::text_button(__('Törlés'),array('route_name'=>'admin_shopopening_delete', 'params' => array($m->id)),'fa-trash', array('class'=>'dropdown-item confirm')) !!}

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                             {!! $model->appends($_GET)->links() !!}
                        </div>
                </div>
            </div>
        
    </div>
@endsection 