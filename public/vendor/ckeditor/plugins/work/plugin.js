(function () {

        var selected_content = 0;

        CKEDITOR.plugins.add('work', {
            init: function (editor) {

                var editor_name = editor.name;

                editor.addCommand('work', new CKEDITOR.dialogCommand('work', {
                    allowedContent: true
                }));

                editor.ui.addButton('work', {
                    label: 'Munkák beágyazása',
                    command: 'work',
                    toolbar: 'insert',
                    icon: this.path + 'icon.png'
                });

                CKEDITOR.dialog.add('work', function (instance) {
                    var video;

                    return {
                        title: 'Munkák beágyazása',
                        minWidth: 510,
                        minHeight: 200,
                        contents:
                            [{
                                id: 'videoEmbedPlugin',
                                expand: true,
                                elements:
                                    [
                                        {
                                            type: 'select',
                                            id: 'videos',
                                            label: 'Munka kiválasztása',
                                            items: [['Kérem válasszon',0]],
                                            'default' : "0",
                                            onLoad: function(api) {
                                                this.clear();

                                                var ezazelem = this;
                                                ezazelem.add('Kérem válasszon',0);

                                                $.ajax({
                                                    type: 'GET',
                                                    dataType: 'json',
                                                    url: '/admin/work/list_json',
                                                    success: function (data) {
                                                        $(data.items).each(function (index, item) {
                                                            ezazelem.add(item.title,item.id);
                                                        });

                                                    }
                                                });
                                            },
                                            onChange: function (api) {
                                                // this = CKEDITOR.ui.dialog.select
                                                selected_content = this.getValue();
                                            }
                                        }]

                            }],
                        onOk: function () {
                            if (selected_content != 0)
                            {
                                var content = '';
                                var instance = this.getParentEditor();
                                var id = editor_name;

                                $.ajax({
                                    type: 'GET',
                                    dataType: 'json',
                                    url: '/admin/work/list_json/'+selected_content,
                                    success: function(data) {
                                        content = data.content;

                                        console.log(content);

                                        content = content.replace(/&/g, '&amp;');
                                        content = content.trim();
                                        content = '<div class="embeded">'+content+'</div>';


                                        var element = CKEDITOR.dom.element.createFromHtml(content);
                                        instance.insertElement(element);
                                    }
                                });
                            }
                        }
                    };
                });
            }
        });
    }

)();