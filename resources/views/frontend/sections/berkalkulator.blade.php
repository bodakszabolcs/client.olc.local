@php
	$berkalkulator = \Modules\Content\Entities\Content::find(8);

@endphp
@if(!is_null($berkalkulator))
<section class="kilencedik" id="berkalkulator">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<h3>{{$berkalkulator->title}}</h3>
			</div>
		</div>
		<div class="row">
			{!! $berkalkulator->content !!}
		</div>
<div class="row">
				<div class="col-lg-6 ">
					<table class=" table table-condensed">
						<tbody><tr>
							<td><label>Havi bruttó munkabér</label></td>
							<td>
								<div class="input-group">
									<input class="form-control input-lg" placeholder="100000" type="text" name="havi_ber">
									<span class="input-group-addon">&nbsp;&nbsp; Ft &nbsp;&nbsp;</span>
								</div>
							</td>
						</tr>
						<tr>
							<td><label>Gyermekek száma</label></td>
							<td><select class="form-control input-lg" type="text" name="gyerek">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>

								</select></td>
						</tr>

					</tbody></table>
				</div>
				<div class="col-lg-6 ">
					<table class=" table table-condensed">
						<tbody><tr>
							<td><label>Éves bruttó jövedelem</label></td>
							<td><div class="input-group">
									<input class="form-control input-lg" readonly="" type="text" name="eves_ber">
									<span class="input-group-addon">&nbsp;&nbsp; Ft &nbsp;&nbsp;</span>
								</div>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<button class="btn btn-lg btn-success col-xs-12 calc"> Számítás</button>
							</td>
						</tr>

					</tbody></table>
				</div>
				<div class="col-lg-6 ">
					<p>Munkabérből levonásra kerülő adóterhek</p>
					<table class=" table table-condensed">
						<tbody><tr>
							<td><label>SZJA</label></td>
							<td>
								<div class="input-group">
									<input class="form-control input-lg" readonly="" type="text" name="szja">
									<span class="input-group-addon">&nbsp;&nbsp; Ft &nbsp;&nbsp;</span>
								</div>
							</td>
						</tr>
						<tr>
							<td><label>Nyugdíj járulék(10,0%)</label></td>
							<td>
								<div class="input-group">
									<input class="form-control input-lg" readonly="" type="text" name="nyugdij">
									<span class="input-group-addon">&nbsp;&nbsp; Ft &nbsp;&nbsp;</span>
								</div>
							</td>
						</tr>
						<tr>
							<td><label>Egészségbiztosítási járulék(7,0%)</label></td>
							<td>
								<div class="input-group">
									<input class="form-control input-lg" readonly="" type="text" name="egeszseg">
									<span class="input-group-addon">&nbsp;&nbsp; Ft &nbsp;&nbsp;</span>
								</div>
							</td>
						</tr>
						<tr>
							<td><label>Munkaerőpiaci járulék(1,5%)</label></td>
							<td>
								<div class="input-group">
									<input class="form-control input-lg" readonly="" type="text" name="munkaero">
									<span class="input-group-addon">&nbsp;&nbsp; Ft &nbsp;&nbsp;</span>
								</div>
							</td>
						</tr>
						<tr>
							<td><label>Adókedvezmény</label></td>
							<td>
								<div class="input-group">
									<input class="form-control input-lg" readonly="" type="text" name="kedvezmeny">
									<span class="input-group-addon">&nbsp;&nbsp; Ft &nbsp;&nbsp;</span>
								</div>
							</td>
						</tr>
						<tr>
							<td><label>Nettó bér</label></td>
							<td>
								<div class="input-group">
									<input class="form-control input-lg" readonly="" type="text" name="nettober">
									<span class="input-group-addon">&nbsp;&nbsp; Ft &nbsp;&nbsp;</span>
								</div>
							</td>
						</tr>


					</tbody></table>
				</div>
				<div class="col-lg-6 ">
					<p>Munkáltatót terhelő költségek</p>
					<table class=" table table-condensed">
						<tbody><tr>
							<td><label>Szociális hozzájárolási adó</label></td>
							<td>
								<div class="input-group">
									<input class="form-control input-lg" readonly="" type="text" name="szoc_ado">
									<span class="input-group-addon">&nbsp;&nbsp; Ft &nbsp;&nbsp;</span>
								</div>
							</td>
						</tr>
						<tr>
							<td><label>Szakképzési hozzájárulás</label></td>
							<td>
								<div class="input-group">
									<input class="form-control input-lg" readonly="" type="text" name="szak_ado">
									<span class="input-group-addon">&nbsp;&nbsp; Ft &nbsp;&nbsp;</span>
								</div>
							</td>
						</tr>
						<tr>
							<td><label>Munkáltató össz költsége</label></td>
							<td>
								<div class="input-group">
									<input class="form-control input-lg" readonly="" type="text" name="munkaltato_ossz">
									<span class="input-group-addon">&nbsp;&nbsp; Ft &nbsp;&nbsp;</span>
								</div>
							</td>
						</tr>


					</tbody></table>
				</div>

			</div>
	</div>

</section>
@endif