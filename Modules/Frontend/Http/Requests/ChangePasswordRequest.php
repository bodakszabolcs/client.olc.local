<?php

namespace Modules\Frontend\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hash' => 'required|exists:users,hash_2',
            'new_password' => 'required',
            'new_password_again' => 'required|same:new_password',
        ];
    }
}
