<?php 
Route::group(['namespace' => 'Modules\Translation\Http\Controllers', 'prefix' => 'admin/translation', 'middleware' => ['web','auth', 'acl', 'accesslog']], function () { 
		Route::match(['get'], '/list', 'TranslationController@TranslationList')->name('admin_translation_list');
		Route::match(['post'], '/save', 'TranslationController@TranslationSave')->name('admin_translation_save');
		
		
});
