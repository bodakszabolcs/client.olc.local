<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\User\Entities\User;

class SupportEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $error_page, $error_detail, $error_type, $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $request, User $user)
    {
        $this->error_page = $request['error-page'];
        $this->error_type = $request['error-type'];
        $this->error_detail = $request['error-detail'];
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.admin.support')
	        ->from($this->user->email,$this->user->firstname)
            ->subject(__('Hibabejelentés'))
            ->to(config('app.emails.support'),'Support');
    }
}
