<?php

namespace Modules\User\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\User\Entities\UserBilling;
use Modules\User\Entities\UserShipping;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'lastname' => $this->lastname,
            'firstname' => $this->firstname,
            'name' => $this->name,
            'email' => $this->email,
            'avatar' => $this->avatar,
            'roles' => $this->roles,
            'shipping' => $this->getShipping(),
            'billing' => $this->getBilling()
        ];
    }

    public function getBilling()
    {
        if (!is_null($this->billing))
        {
            return new UserBillingResource($this->billing);
        }

        return new UserBillingResource(new UserBilling());
    }

    public function getShipping()
    {
        if (!is_null($this->shipping))
        {
            return new UserBillingResource($this->shipping);
        }

        return new UserBillingResource(new UserShipping());
    }
}
