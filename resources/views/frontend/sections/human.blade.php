@php
	$human = \Modules\Content\Entities\Content::find(1);
@endphp
@if(!is_null($human))
<section class="elso" id="human">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<img class="img-responsive full" src="/assets/frontend//img/munkavallalok.png" alt="Munkavállalókank" title="Munkavállalóknak">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 text-center text-justify">
			{!!  $human->content!!}
			</div>
		</div>
	</div>

</section>
@endif