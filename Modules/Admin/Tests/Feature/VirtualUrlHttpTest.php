<?php

namespace Tests;

use Illuminate\Support\Str;
use Modules\User\Entities\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase as BaseTestCase;

class VirtualUrlHttpTest extends TestCase
{
    protected $user = "";

    public function setUp()
    {
        parent::setUp();
        $this->user = User::find(1);
    }

    public function testVirtualUrlHttp()
    {

        $response = $this->actingAs($this->user)
            ->get(route('admin_virtual_url_list'))
            ->assertStatus(200);

        $response = $this->actingAs($this->user)
            ->get(route('admin_virtual_url_edit'))
            ->assertStatus(200);

    }

    public function testVirtualUrlHttpEdit()
    {
        $response = $this->actingAs($this->user)
            ->post(route('admin_virtual_url_edit'),
                [
                    'from_url' => Str::random(16),
                    'to_url' => Str::random(16),
                    'headers' => Str::random(16),
                    'code' => rand(100,999)
                ])
            ->assertStatus(302);

        $last_url = User::orderBy('id','DESC')->first();

        $response = $this->actingAs($this->user)
            ->get(route('admin_virtual_url_edit',[1]))
            ->assertStatus(200);

        $response = $this->actingAs($this->user)
            ->post(route('admin_virtual_url_edit',[$last_url->id]),
                [
                    'from_url' => Str::random(16),
                    'to_url' => Str::random(16),
                    'headers' => Str::random(16),
                    'code' => rand(100,999)
                ])
            ->assertStatus(302);

        $response = $this->actingAs($this->user)
            ->get(route('admin_virtual_url_delete',[$last_url->id]))
            ->assertStatus(302);

        $response = $this->actingAs($this->user)
            ->get(route('admin_virtual_url_edit',[$last_url->id]))
            ->assertRedirect(route('admin_virtual_url_list'));

    }
}
