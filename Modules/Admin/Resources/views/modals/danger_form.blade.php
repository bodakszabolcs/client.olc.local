@foreach ($errors->all() as $error)
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <strong>
        {{__('Hiba!')}}
    </strong>l
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
    {{ $error }}
</div>
@endforeach
