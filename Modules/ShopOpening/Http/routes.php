<?php 
Route::group(['namespace' => 'Modules\ShopOpening\Http\Controllers', 'prefix' => 'admin/shopopening', 'middleware' => ['web','auth', 'acl', 'accesslog']], function () { 
		Route::match(['get'], '/list', 'ShopOpeningController@ShopOpeningList')->name('admin_shopopening_list');
		Route::match(['get', 'post', 'put'], '/edit/{id?}/{tab?}', 'ShopOpeningController@Edit')->name('admin_shopopening_edit');
		Route::match(['get'], '/delete/{id}', 'ShopOpeningController@Delete')->name('admin_shopopening_delete'); 
});
