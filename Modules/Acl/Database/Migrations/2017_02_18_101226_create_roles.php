<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);

        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id')->comment('Egyedi azonosító')->unique();
            $table->string('name')->comment('Slug megnevezés');
            $table->string('description')->comment('Megnevezés');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('roles_acls', function (Blueprint $table) {
            $table->increments('id')->comment('Egyedi azonosító')->unique();
            $table->integer('role_id')->comment('Szabály');
            $table->string('path')->comment('Útvonal');
            $table->index(['role_id','path']);
        });

        Schema::create('role_user', function (Blueprint $table) {
            $table->integer('user_id')->comment('Felhasználó');
            $table->string('role_id')->comment('Szabály');
            $table->index(['role_id','user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles');
        Schema::drop('roles_acls');
        Schema::drop('role_user');
    }
}
