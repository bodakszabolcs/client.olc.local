<script>
    $(function () {
        $( 'tr' ).each( function editAndSave( index, tr ){
            var $tr = $( tr );

            $tr.find( 'button[type=button]' ).on( 'click', function( e ){
                var toggle = $( e.target ).is( '.editButton' );

                if( toggle ){
                    $tr.find('td:nth-child(2)').attr( 'contenteditable', toggle );
                }
                else {
                    $tr.find('td:nth-child(2)').removeAttr( 'contenteditable' );
                }
            } )
        });
        $(document).on('click','.saveButton',function(){
        $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "{{route('admin_translation_save')}}",

                data: {
                    lang:$(this).closest('table').data('lang'),
                    key:$(this).closest('tr').data('key'),
                    value:$(this).closest('tr').find('td:nth-child(2)').first().text()
                },
                success: function (data) {
                    // console.log(data);


                },
                error: function (data) {
                    //  console.log(data);
                }
            });
        });
        $(document).on('keyup','#filter',function(){
            var value = $(this).val();

            $('table tbody tr').each(function() {

                if ($(this).find('td:nth-child(2)').text().search(value) > -1 || $(this).find('td:nth-child(1)').text().search(value) > -1) {
                    $(this).show();
                }
                else {
                    $(this).hide();
                }
            });
        });

    });
</script>