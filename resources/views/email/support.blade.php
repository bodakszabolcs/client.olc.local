<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <meta charset="utf-8"/>
    <title>{{config('app.name')}} | @yield('title')</title>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    @foreach(config('assets.stylesheet') as $css)
        <link rel="stylesheet" href="{{$css}}">
        @endforeach
    <!--[if lte IE 9]>
        <link href="assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen"/>
        <![endif]-->
</head>
<body>
    <div class="page-container ">
        <div class="page-content-wrapper ">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">Support Email</h2>
                    <h4 class="text-center">{{@$inputs['error-page']}}</h4>
                    <h5 class="text-center">{{date("Y.m.d. H:i:s")}}</h5>
                    <p class="text-center">
                        <b>Hiba oldal:</b> {{@$inputs['error-upage']}}<br/>
                        <b>Felhasználó:</b> {{@$inputs['error-name']}}<br/>
                        <b>Felhasználó ID:</b> {{@$inputs['error-user']}}<br/>
                    </p>
                    <hr/>
                    <p class="text-center">{{@$inputs['error-detail']}}</p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>