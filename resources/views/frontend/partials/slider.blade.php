@php
			$slides = \Modules\Slider\Entities\Slider::where('active','=',1)->orderBy('o','asc')->get();
@endphp
@if(sizeof($slides)>0)

<div id="myCarousel" class="  carousel slide" data-ride="carousel" >
  <!-- Indicators -->
            <ol class="carousel-indicators">
				@foreach( $slides as $s)
							<li data-target="#myCarousel" data-slide-to="{{$loop->iteration}}" class="{{$loop->iteration==1 ?'active':''}}"></li>
				@endforeach
			</ol>
			<div class="carousel-inner" >
			@foreach( $slides as $s)
				 <div class="item {{$loop->iteration==1 ?'active':''}}"> <img src="{{$s->image}}"  alt="{{$s->title}}">
			      <div class="container">
			        <div class="carousel-caption">
						<h1>{{$s->title}}</h1>
						  <p class="text-center">
						  @if(!empty($s->link))
							  <a class="btn-lg btn-primary" href="{{$s->link}}">
						  @endif
							 {{$s->link_title}}
						  @if(!empty($s->link))
							</a>
						  @endif
						</p>
			        </div>
			      </div>
			    </div>
			@endforeach
			</div>
</div>
@endif