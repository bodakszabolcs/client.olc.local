<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\User\Entities\User;

class NewsletterEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $to,$title, $subject, $reply, $sender,$content,$statistic_id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject,$sender,$reply,$to,$content,$statistic_id,$hash,$attachments=[])
    {
        $this->to= $to;
        $this->hash= $hash;
        $this->sender = $sender;
        $this->reply = $reply;
        $this->subject = $subject;
        $this->content = $content;
        $this->statistic_id = $statistic_id;
        $this->files = $attachments;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $markdown = $this->markdown('emails.admin.newsletter',['subject'=>$this->subject,'content'=>$this->content,'statistic_id'=>$this->statistic_id,'hash'=>$this->hash])
	        ->from($this->sender->email,$this->sender->name)
            ->subject($this->subject)
            ->to($this->to->email,$this->to->name)
            ->replyTo($this->reply->email,$this->reply->name);
	    foreach ($this->files as $f){

		    $markdown = $markdown->attach($f->getRealPath(),['as'=>$f->getClientOriginalName(),'mime' => $f->getClientMimeType()]);
	    }
	    return $markdown;

    }
}
