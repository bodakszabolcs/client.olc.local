<?php

namespace Modules\Coupon\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Company\Transformers\ShopListResource;
class CouponListResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'shop' => new ShopListResource($this->shop),
            'type' => $this->type,
            'amount' => $this->amount,
            'expire' => $this->expire,
            'used' => $this->used,

        ];
    }


}
