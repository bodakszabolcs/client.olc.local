<?php

return array(
    'defaults' =>
        array(
            'log_level' => 3, //0 - off, 1 - error, 2 - warn, 3 - debug
            'log_email' => 'recze01@gmail.com', //we'll send only emails when an error event occurs,
            'return_pdf_as_result' => true,
            'return_invoice_number_as_result' => true,
            'pdf_file_save_path' => './packages/recze/szamlaagent/pdf',
            'xml_file_save_path' => './packages/recze/szamlaagent/xml',
            'cookiejar' => './packages/recze/szamlaagent/cookiejar',
            'agent_url' => 'https://www.szamlazz.hu/szamla/',
            'root_cert_file' => './packages/recze/szamlaagent/cacert.pem',
            'call_method' => 'auto', //auto,default,legacy
            ),
    'beallitasok' =>
        array(
            'username' => 'creativeteszt',
            'password' => 'aA123456'
            )
        );
