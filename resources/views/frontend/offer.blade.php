@extends('frontend.layout')
@php
	$error = \Illuminate\Support\Facades\Session::get('field_errors');
	\Illuminate\Support\Facades\Session::forget('field_errors');
@endphp
@section('content')
	<section class="kilencedik margin60" id="berszamfejtes">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<h3>Ajánlatkérés</h3>
			</div>
		</div>
		<div class="row">
			@if(Session::get('success_message'))
				<div class="alert alert-info text-center">
				<h3>Köszönjük az ajánlatkérést! Hamarosan felvesszük Önnel a kapcsolatot!</h3>
			</div>
			@endif
			@if($data=Session::get('error_message'))
				<div class="alert alert-danger text-center has-danger">
				<h3>{{$data}}</h3>
			</div>
			@endif
		</div>
		<div class="row">
			{!! \App\Http\Helper::formOpen('offer','post') !!}
				<div class="col-md-6">

					<h3 class="form-section">Céges adatok</h3>

					{!! \App\Http\Helper::select('Ajánlatkérés tárgya <span class="text-danger">*</span>','type',[3=>'Munkaerő kölcsönzés',1=>'Diákmunka',2=>'Munkaerő közvetítés',4=>'Nyugdíjas foglalkoztatás'],old('type',3),array('class'=>'form-control'),$error) !!}
					{!! \App\Http\Helper::input('Cég neve <span class="text-danger">*</span>','name',old('name'),array('class'=>'form-control'),$error) !!}
					{!! \App\Http\Helper::input('Cím <span class="text-danger">*</span>','address',old('address'),array('class'=>'form-control'),$error) !!}
				</div>
				<div class="col-md-6">

					<h3 class="form-section">Kapcsolattartó adatok</h3>
					{!! \App\Http\Helper::input('Név <span class="text-danger">*</span>','contact_name',old('contact_name'),array('class'=>'form-control'),$error) !!}
					{!! \App\Http\Helper::input('Telefon <span class="text-danger">*</span>','contact_phone',old('contact_phone'),array('class'=>'form-control'),$error) !!}
					{!! \App\Http\Helper::input('Email <span class="text-danger">*</span>','contact_email',old('contact_email'),array('class'=>'form-control'),$error) !!}
				</div>

				<div class="col-lg-12">
					<h3 class="form-section">Elvégzendő feladat adatok</h3>
				</div>
				<div class="col-md-6">
					{!! \App\Http\Helper::input('Típusa <span class="text-danger">*</span>','work_type',old('work_type'),array('class'=>'form-control'),$error) !!}
					</div>
				<div class="col-md-6">
					{!! \App\Http\Helper::input('Munkavégzés helye <span class="text-danger">*</span>','work_place',old('name'),array('class'=>'form-control'),$error) !!}
					</div>
				<div class="col-md-6">
					{!! \App\Http\Helper::input('Megbízás időtartama','time',old('time'),array('class'=>'form-control'),$error) !!}
					</div>
				<div class="col-md-6">
					{!! \App\Http\Helper::input('Munkarend','work_time',old('work_time'),array('class'=>'form-control'),$error) !!}
					</div>
					<div class="col-md-12">
					{!! \App\Http\Helper::textarea('Tevékenység rövid bemutatása, speciális igények','description',old('description'),array('class'=>'form-control','rows'=>5),$error) !!}
						<button type="submit" class="btn btn-warning  pull-right">Ajánlat elküldése</button>
					</div>

					{!! \App\Http\Helper::formClose() !!}
				</div>



			</div>
	</div>

</section>



@endsection