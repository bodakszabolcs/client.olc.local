@php
   $slides = \Modules\Slider\Entities\Slider::where('active','=',1)->orderBy('o','asc')->get();
@endphp
<section id="munkak" class="@if(sizeof($slides)==0) margin60 @endif">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 text-center">
            <h2>Kiemelt munkáink</h2>
         </div>
         <div class="clearfix"></div>
         <br>
         @foreach(\Modules\Work\Entities\Work::where('priority','=',1)->where('archive','=',0)->orderBy('updated_at','desc')->limit(2)->get() as $w)
            @php
               $sub = \Modules\Workcategory\Entities\Workcategory::find($w->workcategory_id);
               $head = \Modules\Workcategory\Entities\Workcategory::find($sub->parent_id);
            @endphp
         <div class="col-sm-6">
            <a href="/munkak/{{$head->slug}}/{{$sub->slug}}/{{$w->slug}}" >
               <div style="background: #fff   url('{{$w->image}}')   center center no-repeat; background-size: cover;height:300px"></div>

            </a>
            <h3>{{$w->name}}</h3>
            <div class="clearfix"></div>
            <p>{{$w->lead}}</p>
            <a class="btn btn-success form-control" href="/munkak/{{$head->slug}}/{{$sub->slug}}/{{$w->slug}}">Részletek<i class="fa fa-arrow-right pull-right"></i></a>
         </div>
         @endforeach

         <div class="clearfix"></div>
      </div>
   </div>
</section>