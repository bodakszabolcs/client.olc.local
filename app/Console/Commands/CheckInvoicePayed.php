<?php

namespace App\Console\Commands;

use App\Mail\NewsletterEmail;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Modules\Company\Entities\Shop;
use Modules\Coupon\Entities\AutomaticCoupon;
use Modules\Coupon\Entities\Coupon;
use Modules\Invoice\Entities\Invoice;
use Modules\News\Entities\News;
use Modules\Newsletter\Entities\Newsletter;
use Modules\Newsletter\Entities\NewsletterList;
use Modules\Newsletter\Entities\NewsletterStatistic;
use Modules\Page\Entities\SiteBuilder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Modules\Purchase\Entities\Purchase;

class CheckInvoicePayed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:payed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(empty($this->shops)){
            exit();
        }
        //userek akinek van vagy használt kupont az elmult 1 hétben;
        if(date('Y-m-d') >=date('Y-m-15')) {
            $in = Invoice::where('deadline', '=', date('Y-m-15'))->where('status', '<', 4)->find();
            if ($in) {
                foreach (Shop::all() as $s) {
                    $s->payed_package_id = 0;
                    $s->save();
                }
            }else{
                foreach (Shop::all() as $s) {
                    $s->payed_package_id = $s->package_id;
                    $s->save();
                }
            }
        }
    }

	
}
