@php
	$berszamfejtes = \Modules\Content\Entities\Content::find(4);

@endphp
@if(!is_null($berszamfejtes))
<section class="negyedik" id="berszamfejtes">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<img style="" src="/assets/frontend//img/berszamfejtes.png" alt="Bérszámfejtés" title="Bérszámfejtés">

				<h3>{{$berszamfejtes->title}}</h3>

			</div>
		</div>
		<div class="row">
			{!! $berszamfejtes->content !!}
		</div>
		<div class="col-lg-8 col-lg-offset-2 text-center">
			<a class="btn btn-lg btn-outline" href="/ajanlatkeres">
				Ajánlatkérés
			</a>
		</div>
	</div>

</section>
@endif