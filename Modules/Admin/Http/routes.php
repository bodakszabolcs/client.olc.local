<?php

Route::group(['namespace' => 'Modules\Admin\Http\Controllers', 'prefix' => 'admin', 'middleware' => ['web']], function ()
{
    Route::match(['get', 'post'], '/login', 'AdminController@Login')->name('admin_login');
    Route::match(['get', 'post'], '/register/{hash}', 'AdminController@Register')->name('admin_login_hash');
    Route::match(['get', 'post'], '/language', 'AdminController@ChangeLanguage')->name('admin_language_change');
    Route::match(['get'], '/logout', 'AdminController@Logout')->name('admin_logout');
});

Route::group(['namespace' => 'Modules\Admin\Http\Controllers', 'prefix' => 'admin', 'middleware' => ['web','auth', 'acl', 'accesslog']], function ()
{
    Route::match(['get'], '/refresh-cache', 'AdminController@RefreshRedis')->name('admin_refresh_redis');
    Route::match(['get'], '/pdf-preview/{shop_id}', 'AdminController@pdfPreview')->name('admin_pdf_preview');
    Route::match(['get'], '/maintenance_mode', 'AdminController@MaintenanceMode')->name('admin_maintenance_mode');
    Route::match(['get'], '/compact_mode', 'AdminController@CompactMode')->name('admin_compact_mode');
    Route::match(['post'], '/file-upload', 'AdminController@fileUpload')->name('admin_fileupload');
    Route::match(['get'], '/', 'AdminController@Dashboard')->name('admin_dashboard');
    Route::match(['post'], '/crop-image', 'AdminController@CropImage')->name('admin_crop_image');
    Route::match(['get'], '/copy-content/{type}/{id}', 'AdminController@CopyContent')->name('admin_copy_content');
    Route::match(['get', 'post'], '/support_email', 'AdminController@SupportEmail')->name('admin_support_email');
    Route::match(['get', 'post', 'put'], '/files', 'FileController@View')->name('admin_files_view');
    Route::match(['get', 'post'], '/theme', 'ThemeController@Edit')->name('admin_theme_edit');
    Route::match(['get','post'], '/slowlog/list', 'SlowLogController@SlowLogList')->name('admin_slowlog_list');
    Route::match(['get','post'], '/tickets/list', 'AdminController@TicketList')->name('admin_ticket_list');
    Route::match(['get','post'], '/modules/{id?}', 'AdminController@Modules')->name('admin_modules');
    Route::match(['get'], '/search', 'AdminController@Search')->name('admin_search');
    Route::match(['get','post'], '/run-command', 'AdminController@RunCommand')->name('admin_run_command');
    Route::match(['get'], '/send-news', function(){
        \Illuminate\Support\Facades\Artisan::call('send:news');
    });
});
