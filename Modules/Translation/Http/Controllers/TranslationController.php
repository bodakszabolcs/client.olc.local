<?php

	namespace Modules\Translation\Http\Controllers;

	use Modules\Translation\Entities\Translation;
	use App\Http\Controllers\Controller;
	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Session;
	use Validator;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\View;
	use Modules\Admin\Http\Controllers\AdminController;

	class TranslationController extends AdminController
	{
		public $niceNames = array(
			'dd	=>	dd',  

			);
		public function __construct()
		{
			View::share('sub_title', __('Fordítások'));
			View::share('subtitle_link', route('admin_translation_list'));
		}

		public function TranslationList(Request $request)
		{
			
			View::share('title', __('Fordítások lista'));

			return view('translation::list');

		}
		public function TranslationSave(Request $request)
		{
			if(!empty($request->input('lang'))&& !empty($request->input('key')) )
			{
				if(file_exists(base_path('resources/lang/'.$request->input('lang').'.json'))) {
					$data = file_get_contents(base_path('resources/lang/' . $request->input('lang') . '.json'));
					
					$avilable = json_decode($data, TRUE);
					
					
					if(is_array($avilable)) {
						
						$avilable[$request->input('key')] = $request->input('value');
						$temp=json_encode($avilable,JSON_UNESCAPED_UNICODE);
						file_put_contents(base_path('resources/lang/'.$request->input('lang').'.json'),$temp );
						return response()->json('success');
					}
					
				}
				
			}
			return response()->json('error');
		}

		


	}