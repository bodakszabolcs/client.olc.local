@php
	$kozvetites = \Modules\Content\Entities\Content::find(3);

@endphp
@if(!is_null($kozvetites))
<section class="harmadik" id="kozvetites">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<img src="/assets/frontend//img/munkaero.png" alt="Munkaerő kölcsönzés" title="Munkaerő kölcsönzés">

				<h3>{{$kozvetites->title}}</h3>

			</div>
		</div>
		<div class="row">
			{!! $kozvetites->content !!}
		</div>
		<div class="col-lg-8 col-lg-offset-2 text-center">
			<a class="btn btn-lg btn-outline" href="/ajanlatkeres">
				Ajánlatkérés
			</a>
		</div>
	</div>
</section>
@endif