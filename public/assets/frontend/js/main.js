$(document).ready(function(){
    $('.select2').select2();
});

$('.calc').click(function(){

    var havi = $('input[name="havi_ber"]').val();
    $('input[name="szja"]').val(parseInt($('input[name="havi_ber"]').val()*1*0.15 ));
    $('input[name="nyugdij"]').val(parseInt($('input[name="havi_ber"]').val()*1*0.10 ));
    $('input[name="egeszseg"]').val(parseInt($('input[name="havi_ber"]').val()*1*0.07 ));
    $('input[name="munkaero"]').val(parseInt($('input[name="havi_ber"]').val()*1*0.015 ));
    $('input[name="szoc_ado"]').val(parseInt($('input[name="havi_ber"]').val()*1*0.195 ));
    $('input[name="szak_ado"]').val(parseInt($('input[name="havi_ber"]').val()*1*0.015 ));
    $('input[name="munkaltato_ossz"]').val(parseInt($('input[name="szoc_ado"]').val()*1 + $('input[name="szak_ado"]').val()*1 + havi*1 ));

    var kedv=0;
    var munkaero = $('input[name="munkaero"]').val();


    var netto = $('input[name="havi_ber"]').val() -$('input[name="szja"]').val()- $('input[name="nyugdij"]').val() - $('input[name="egeszseg"]').val() - $('input[name="munkaero"]').val();
    if($('select[name="gyerek"]').val() == 1){

        console.log(netto*1 + munkaero*1 +10000);
        if(netto*1 + munkaero*1 +10000 <= havi) {
            kedv = 10000;
        }
        else
        {
            kedv= havi*1-munkaero*1-netto*1;
        }
    }
    if($('select[name="gyerek"]').val() == 2) {
        if(netto*1 + munkaero*1 +25000 <= havi) {
            kedv = 25000;
        }
        else
        {
            kedv= havi*1-munkaero*1-netto*1;
        }
    }
    if($('select[name="gyerek"]').val() == 3)
    {
        if(netto*1 + munkaero*1 +99000 <= havi) {
            kedv = 99000;
        }
        else
        {
            kedv = havi*1-munkaero*1-netto*1;
        }
    }

    $('input[name="kedvezmeny"]').val(parseInt(kedv));
    $('input[name="nettober"]').val(parseInt(kedv+netto));
    $('input[name="eves_ber"]').val(parseInt(havi*1*12));

});
$('.calc_diak').click(function(){
    var sum = parseInt($('input[name="diak_brutto"]').val());
    if($('input[name="diak_brutto_ora_ber"]').val().length && $('input[name="diak_brutto_ora"]').val().length )
    {
        sum = parseInt($('input[name="diak_brutto_ora_ber"]').val()) * parseInt($('input[name="diak_brutto_ora"]').val());
    }
    $('input[name="diak_brutto_jovedelem"]').val(sum);
    $('input[name="diak_szja"]').val(sum*0.15);
    $('input[name="diak_netto"]').val(sum*0.85);


})

$(document).ready(function () {

    $('.apply').click(function(){
        var data = new FormData();

//Form data
        var form_data = $('#form-subscribe').serializeArray();
        $.each(form_data, function (key, input) {
            data.append(input.name, input.value);
        });

        var file_data = $('input[name="cv"]')[0].files;

        data.append("cv", file_data[0]);


//Custom data

       $.ajax({
           url: '/work-apply',
           method: 'post',
           data:data,
           processData: false,
           contentType: false,
           success:function(data){
               $('#form-content').html(data);
               grecaptcha.ready(function() {
                   grecaptcha.execute('6LdV2XwUAAAAAHCT1-9czramNRqE2romWBJ7UjZ_', {action: 'registration'}).then(function (token) {

                       if ($('form#form-subscribe').length) {

                           $('form#form-subscribe').prepend('<input type="hidden" name="ga_token" value="' + token + '">');
                           $('form#form-subscribe').prepend('<input type="hidden" name="action" value="regiszter">');
                       }
                   });
               });
           }
       })
    });
    var $container = $("html,body");
    $.each($('span:contains("##at##")'),function(){
        $(this).text($(this).text().split('##at##').join('@'));
    });
    $.each($('a[href*="##at##"]'),function(){
        $(this).attr('href',$(this).attr('href').split('##at##').join('@'));
    });

    var $scrollTo = $('.has-danger');
    if($scrollTo.length) {
        $container.animate({
            scrollTop: $scrollTo.offset().top - $container.offset().top - 200 + $container.scrollTop(),
            scrollLeft: 0
        }, 1000);
    }
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        language: 'hu',
        weekStart: 1
    });
    $('[data-toggle=offcanvas]').click(function () {
        if ($('.sidebar-offcanvas').css('background-color') == 'rgb(255, 255, 255)') {
            $('.list-group-item').attr('tabindex', '-1');
            $('#sidebar').hide();
        } else {
            $('.list-group-item').attr('tabindex', '');
            $('#sidebar').show();
        }
        $('.row-offcanvas').toggleClass('active');


    });
});