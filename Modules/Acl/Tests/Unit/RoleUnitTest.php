<?php

namespace Tests;

use Modules\Acl\Entities\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase as BaseTestCase;

class RoleUnitTest extends BaseTestCase
{

    public function testCreateRoleSuccess()
    {
        $acl = new Role();
        $acl->name = "Teszt Role";
        $acl->description = "Teszt Role Description";
        $acl->save();

        $this->assertTrue(!is_null($acl->id));
    }

}
