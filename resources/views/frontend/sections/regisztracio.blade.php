@php
	$regisztracio = \Modules\Content\Entities\Content::find(9);

    $error = \Illuminate\Support\Facades\Session::get('field_errors');

    \Illuminate\Support\Facades\Session::forget('field_errors');

@endphp
@if(!is_null($regisztracio))
<section class="negyedik" id="regisztracio">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<h3>{{$regisztracio->title}}</h3>
			</div>
		</div>
		<div class="row">
			{!! $regisztracio->content !!}
		</div>
		@if(Session::get('success_message'))
			<div class="alert alert-info text-center">
				<h3>Köszönjük regisztációját! Hamarosan felvesszük Önnel a kapcsolatot!</h3>
			</div>
		@endif
		@if($data=Session::get('error_message'))
			<div class="alert alert-danger text-center has-danger">
				<h3>{{$data}}</h3>
			</div>
		@endif
		<div class="col-lg-12 ">
			{!! \App\Http\Helper::formOpen('regisztracio','post','/regisztracio') !!}
			<div class="row">

				<div class="col-md-6">
					{!! \App\Http\Helper::input('Név <span class="text-danger">*</span>','name',old('name'),array('form-control'),$error) !!}
				</div>
				<div class="col-md-6">
					{!! \App\Http\Helper::input('E-mail <span class="text-danger">*</span>','email',old('email'),array('form-control'),$error) !!}
				</div>
				<div class="clearfix"></div>
				<div class="col-md-6">
					{!! \App\Http\Helper::input('Telefon <span class="text-danger">*</span>','phone',old('phone'),array('form-control'),$error) !!}
				</div>
				<div class="col-md-6">
					{!! \App\Http\Helper::input('Születési dátum <span class="text-danger">*</span>','birthdate',old('birthdate'),array('class'=>'form-control datepicker'),$error) !!}
				</div>
				<div class="clearfix"></div>
				<div class = "col-md-6">
					{!! \App\Http\Helper::select(trans('Neme <span class="text-danger">*</span>'),'gender',[''=>'Kérem válasszon']+[0=>'Férfi',1=>'Nő'], old('gender'),array('class'=>'form-control'),$error) !!}
				</div>
				 <div class = "col-md-6">
					 {!! \App\Http\Helper::select(trans('Milyen munka érdekli?'),'work_category[]',\Modules\Workcategory\Entities\Workcategory::getWorkCategory(), old('work_category'),array('class'=>'form-control select2','multiple'=>'multiple'),$error) !!}
				 </div>
				<div class="clearfix"></div>
				<div class = "col-md-6">
					{!! \App\Http\Helper::select(trans('Hol szeretne dolgozni? <span class="text-danger">*</span>'),'work_place',[''=>'Kérem válasszon']+\Modules\City\Entities\City::getCities(), old('work_place'),array('class'=>'form-control select2'),$error) !!}
				</div>
				<div class = "col-md-6">
					{!! \App\Http\Helper::select(trans('Legmagasabb iskolai végzettség'),'education[]',\Modules\Education\Entities\Education::getEducation(), old('education'),array('class'=>'form-control select2','multiple'=>'multiple'),$error) !!}
				</div>
				<div class="clearfix"></div>
				<div class="col-md-6">
					<div class="row">
					<div class="col-md-1 col-xs-2">
						<input type="checkbox" class="form-control {{sizeof(array_get($error,'hirlev',[]))?' has-danger':''}}" name="hirlev" value="1">
					</div>
					<div class="col-md-11 col-xs-10">
						<p>Hozzájárulok ahhoz, hogy a MADS Work Kft. elektronikus hirdetést és karrierhírlevelet küldjön számomra.</p>
						@foreach (array_get($error,'hirlev',[]) as $s)
							<small class="text-danger">{{$s}}</small>
						@endforeach
					</div>
				</div>
					<div class="row">
					<div class="col-md-1 col-xs-2">
						<input type="checkbox" class="form-control" name="hirlev2" value="1">
					</div>
					<div class="col-md-11 col-xs-10" {{sizeof(array_get($error,'hirlev2',[]))?' has-danger':''}}>
						<p>Hozzájárulok ahhoz, hogy a megadott adataim továbbításra kerüljenek olyan harmadik személyek/külső szolgáltatást fogadók részére, akik az általam megjelölt típusú feladat teljesítésére keresnek munkavállalókat.</p>
						@foreach (array_get($error,'hirlev2',[]) as $s)
                			<small class="text-danger">{{$s}}</small>
            			@endforeach
					</div>
				</div>
					<div class="row">
					<div class="col-md-1 col-xs-2">
						<input type="checkbox" class="form-control" name="hirlev3" value="1">
					</div>
					<div class="col-md-11 col-xs-10 {{sizeof(array_get($error,'hirlev3',[]))?' has-danger':''}}">
						<p>Az <a href="/torvenyek-jogszabalyok">Adatkezelési Tájékoztató</a>ban foglaltakat elolvastam, megértettem, és a regisztrációmmal hozzájárulok ahhoz, hogy személyes adataimat a megjelölt célból és feltételekkel a MADS Work Kft. kezelje.</p>
						@foreach (array_get($error,'hirlev3',[]) as $s)
							<small class="text-danger">{{$s}}</small>
						@endforeach
					</div>
				</div>

				</div>
				<div class = "col-md-6">
					{!! \App\Http\Helper::select(trans('Nyelv'),'language[]',\Modules\Language\Entities\Language::getLanguages(), old('language'),array('class'=>'form-control select2', 'multiple'=>'multiple'),$error) !!}
					<button type="submit" class="btn btn-warning  pull-right">Regisztráció</button>
					</div>
			</div>
			{!! \App\Http\Helper::formClose() !!}
		</div>
	</div>

</section>
@endif