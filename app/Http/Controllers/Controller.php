<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use App;
use DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Modules\Admin\Entities\VirtualUrl;
use Modules\User\Entities\User;
use Nwidart\Modules\Facades\Module;
use Recze\Szamlazz\SzamlazzServiceProvider;
use Recze\SimplePay\SimplepayServiceProvider;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redis;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function getSafeRoute($uri, $params)
    {
        try {
            $rt = route($uri, $params);
        } catch (\Exception $e)
        {
            $rt = "#!";
        }

        return $rt;
    }

    public static function checkAccessForUri($uri)
    {
        if (Session::get('is_superadmin')) {
            return true;
        }

        if (is_array($uri)) {
            $uri = $uri[0];
        }

        $acl_cache = Session::get('acl_cache', array());

        if (in_array($uri, $acl_cache)) return true;

        $user = User::whereid(Auth::user()->id)->first();

        $roles = $user->roles;
        //TODO ide kell megírni a logikát shopID alapján
        $can = false;
        foreach ($roles as $role) {
            if ($role->name == "superadmin") {
                Session::put('is_superadmin', true);
                return true;
            }
            foreach ($role->acls as $acls) {
                if ($acls->path == $uri) {
                    $acl_cache[$acls->path] = $acls->path;
                    $can = true;
                }
            }
        }

        Session::put('acl_cache', $acl_cache);
        return $can;
    }

    public function generateImages($model)
    {
        $image = Image::make(file_get_contents(public_path($model->lead_image)));
        $image2 = Image::make(file_get_contents(public_path($model->lead_image)));
        $image3 = Image::make(file_get_contents(public_path($model->lead_image)));

        if (!file_exists(dirname(str_slug('169', '_') . $model->lead_image))) {
            mkdir(dirname(str_slug('169', '_') . $model->lead_image), 0777, true);
        }

        if (!file_exists(dirname(str_slug('32', '_') . $model->lead_image))) {
            mkdir(dirname(str_slug('32', '_') . $model->lead_image), 0777, true);
        }

        if (!file_exists(dirname(str_slug('43', '_') . $model->lead_image))) {
            mkdir(dirname(str_slug('43', '_') . $model->lead_image), 0777, true);
        }

        $width = $image->width();
        $height = $image->height();

        $w_h = (int)($width / 16 * 9);
        $w_h_2 = (int)($width / 4 * 3);
        $w_h_3 = (int)($width / 3 * 2);
        if ($w_h_3 > $height) {
            $image->crop((int)($height / 2 * 3), $height)->save(public_path('/32' . $model->lead_image));
        } else {
            $image->crop($width, $w_h_3)->save(public_path('/32' . $model->lead_image));
        }

        if ($w_h > $height) {
            $image2->crop((int)($height / 9 * 16), $height)->save(public_path('/169' . $model->lead_image));
        } else {
            $image2->crop($width, $w_h)->save(public_path('/169' . $model->lead_image));
        }

        if ($w_h_2 > $height) {
            $image3->crop((int)($height / 3 * 4), $height)->save(public_path('/43' . $model->lead_image));
        } else {
            $image3->crop($width, $w_h_2)->save(public_path('/43' . $model->lead_image));
        }
    }

    public function get_string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }

    public function redirectVirtualUrl($url)
    {
        try {
            $virtualurl = VirtualUrl::where('from_url', 'LIKE', "'%".$url."%'")->first();
            if(is_null($virtualurl)) {
                $urls = explode('/', $url);

                for ($i = sizeof($urls); $i > 0; $i--) {
                    $slug="";
                    foreach ($urls as $u) {
                        $slug.= $u.'/';
                    }
                    unset($urls[$i-1]);

                    $virtualurl = VirtualUrl::where('from_url', 'LIKE', '%'.$slug.'*%')->first();

                    if(!is_null($virtualurl))
                    {

                        break;
                    }


                }

            }

            if(is_null($virtualurl))
            {
                abort(404);
            }
            $head_arr = [];
            $headers = explode("\r\n", $virtualurl->headers);

            foreach ($headers as $head) {
                $h_tmp = explode(":", $head);
                if (sizeof($h_tmp) == 2) {
                    $key = trim($h_tmp[0]);
                    $val = trim($h_tmp[1]);
                    $head_arr[$key] = $val;
                }
            }

            return redirect(
                $virtualurl->to_url,
                $virtualurl->code,
                $head_arr);
        } catch (ModelNotFoundException $e)
        {
            abort(404);
        }
    }


}
