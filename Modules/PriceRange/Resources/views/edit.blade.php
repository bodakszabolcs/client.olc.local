@extends('admin::layouts.layout')
@section('content')
	<?php
		$errors= Session::get('field_errors');
		Session::forget('field_errors');
	?>
<div class='row'>
		<div class='col-md-12'>
			<div class='m-portlet'>
				<div class='m-portlet__body'>
					<input type='hidden' name='tab' value='data'>
					<div class = 'row'>
						<div class = 'col-md-12'>	
		{!!  \App\Http\Helper::formOpen('PriceRangeForm','post',NULL,['class'=>'form '])  !!}<div class = 'm-portlet__head'>


								<div class = 'm-portlet__body'>
									<!--begin::Widget 29-->
									<div class = 'm-widget29'>
										<div class = 'm-widget_content'>
											<div class = 'm-widget_content-items'>
												<div class='row'>
														<div class="col-md-4">					{!! \App\Http\Helper::input(trans('Havi kedvezmény határ(Ft)'),'price',old('price',$model->price),array('class'=>'form-control'),$errors) !!}
</div>
<div class="col-md-4">					{!! \App\Http\Helper::input(trans('Kedvezmény(%)'),'percent',old('percent',$model->percent),array('class'=>'form-control'),$errors) !!}
</div>


										</div>
									</div>
								</div>
		 {!! \App\Http\Helper::formButtons('admin_pricerange_list') !!}
		 </div>
		 {!! \App\Http\Helper::formClose() !!}

			</div>
					</div>
				</div>
			</div>
		</div>
	</div>



@endsection 