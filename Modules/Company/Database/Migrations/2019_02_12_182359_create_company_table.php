<?php
	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;
	class CreateCompanyTable extends Migration
	{
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::create('company', function (Blueprint $table) {
				$table->increments("id");
				$table->text("name")->comment("Név");
				$table->text("tax_number")->comment("Adószám");
				$table->text("e-mail")->comment("Email");
				$table->text("phone")->comment("Telefonszám");
				$table->text("shipping_city")->comment("város");
				$table->text("shipping_zip")->comment("Irányítószám");
				$table->text("shippping_address")->comment("Cím");
				$table->text("shipping_country")->comment("Ország");
				$table->timestamps();
				$table->softDeletes();
				$table->index(["deleted_at"]);
				$table->timestamps();
			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::dropIfExists('company');
		}
	}
