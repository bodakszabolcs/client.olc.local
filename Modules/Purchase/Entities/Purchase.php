<?php

	namespace Modules\Purchase\Entities;

	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Database\Eloquent\SoftDeletes;
	use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
	use App\LiquidModel;
use Modules\Company\Entities\Shop;
use Modules\PriceRange\Entities\PriceRange;
	use Sunra\PhpSimple\HtmlDomParser;
	use GeneaLabs\LaravelModelCaching\Traits\Cachable;
	use App\Events\PurchaseCreate;

	class Purchase extends LiquidModel
	{
		use SoftDeletes, Notifiable;


		protected $table = 'purchases';


		protected $dates = ['deleted_at', 'created_at', 'updated_at'];
		/**
		 * The attributes that are mass assignable.
	 	*
		 * @var array
	 	*/
		protected $fillable = [
			'user_id', //User 
			'user_name', //Felhasználónév 
			'price', //Vásárlás összege 
			'discount', //Kedvezmény 
		];

		public $searchColumns = [
			'id' => 'text',
			'user_id'	=>	'text', 
			'user_name'	=>	'text', 
			'price'	=>	'text', 
			'discount'	=>	'text', 
			];

		public $searchColumnLabels = [
			'id' => 'ID',
			'user_id'	=>	'Felhasználó', 
			'user_name'	=>	'Felhasználónév', 
			'price'	=>	'Vásárlás összege', 
			'discount'	=>	'Kedvezmény', 
			];
		protected $casts = [
			];
		public static function getLast30Day($id){
			return Purchase::where('user_id','=',$id)->where('created_at','>',date('Y-m-d',strtotime('-1 month')))->where('handled','=',1)->sum('price');
		}
		public static function calculateNextLevel($id){

			$sum = Purchase::where('user_id','=',$id)->where('created_at','>',date('Y-m-d',strtotime('-1 month')))->where('handled','=',1)->sum('price');
			$range =PriceRange::where('price','>',$sum)->orderBy('price','asc')->first();
			if(is_null($range)){
				return 0;
			}
			else{
				return $range->price - $sum;
			}

		}
		public static function getAllPurchase($id){
			return Purchase::where('user_id','=',$id)->where('handled','=',1)->sum('price');
		}
		public static function getAllDiscount30Day($id){
			return Purchase::where('user_id','=',$id)->where('created_at','>',date('Y-m-d',strtotime('-1 month')))->where('handled','=',1)->sum(DB::raw('price - discount'));
		}
		public static function calculatePriceRange($user_id){
			$sum = self::getLast30Day($user_id);
			$range =PriceRange::where('price','<=',$sum)->orderBy('price','desc')->first();
			if(is_null($range)){
				return 0;
			}
			return $range->percent;
		}
		public static function getPriceByDate($from,$to){
			$shops = Shop::getShopsShopId();
			$data=[];
			$purchase = Purchase::select(DB::raw('sum(price) as sm, shop_id, DATE(created_at) as date'))->where('created_at','>=',$from.' 00:00:00')->where('created_at','<=',$to.' 23:59:59')->groupBy(DB::raw("DATE(created_at)"))->groupBy('shop_id')->get();
			foreach ($purchase as $p) {

				$data[array_get($shops, $p->shop_id)][$p->date]=$p->sm;
			}
		//dd($data);
			return $data;

		}
		public static function getUsersByDate($from,$to){
			$shops = Shop::getShopsShopId();
			$data =[];
			$purchase = Purchase::select(DB::raw('count(user_id) as cn, shop_id, DATE(created_at) as date'))->where('created_at','>=',$from.' 00:00:00')->where('created_at','<=',$to.' 23:59:59')->groupBy(DB::raw("DATE(created_at)"))->groupBy('shop_id');
			if(Auth::user()->shop_id >0){
				$purchase= $purchase->where('shop_id','=',Auth::user()->shop_id);
			}
			$purchase = $purchase->get();
			foreach ($purchase as $p) {
				$data[array_get($shops, $p->shop_id)][$p->date]=$p->cn;
			}
			//dd($data);
			return $data;
		}
		public static function getNewUsers($from,$to){
			$shops = Shop::getShopsShopId();
			$data =[];
			$query = "select date(purchases.created_at) as date, count(*)as cnt, purchases.user_id as user_id,purchases.shop_id from purchases
			join(
				SELECT min(created_at) as min_date,user_id,shop_id FROM `purchases` group by user_id,shop_id
			) as t
			on purchases.created_at = t.min_date and t.user_id = purchases.user_id and t.shop_id = purchases.shop_id where purchases.created_at >= '".$from."' and purchases.created_at <= '".$to."' group by user_id,shop_id ";
			if(Auth::user()->shop_id >0){
				$query = "select date(purchases.created_at) as date, count(*)as cnt, purchases.user_id as user_id,purchases.shop_id from purchases
			join(
				SELECT min(created_at) as min_date,user_id,shop_id FROM `purchases` group by user_id,shop_id
			) as t
			on purchases.created_at = t.min_date and t.user_id = purchases.user_id and t.shop_id = purchases.shop_id where purchases.shop_id= '".Auth::user()->shop_id."' and purchases.created_at >= '".$from."' and purchases.created_at <= '".$to."' group by user_id,shop_id ";
			}
			$purchase = DB::select($query);
			foreach ($purchase as $p) {
				$data[array_get($shops, $p->shop_id)][$p->date]=$p->cnt;
			}

			return $data;
		}
		public static function getTopUsers($from,$to){
			$shops = Shop::getShopsShopId();
			$data =[];
			$purchase = Purchase::select(DB::raw('sum(price) as sm, shop_id, user_name'))->where('created_at','>=',$from.' 00:00:00')->where('created_at','<=',$to.' 23:59:59')->groupBy(DB::raw("user_id"))->groupBy('shop_id');
			if(Auth::user()->shop_id >0){
					$purchase= $purchase->where('shop_id','=',Auth::user()->shop_id);
			}
			$purchase = $purchase->orderBy('sm','desc')->limit(30)->get();
			foreach ($purchase as $p) {
				$data[array_get($shops, $p->shop_id)][$p->user_name]=$p->sm;
			}
			//dd($data);
			return $data;
		}
		public static function getUserByHour($from,$to){
			$shops = Shop::getShopsShopId();
			$data =[];
			$purchase = Purchase::select(DB::raw('count(user_id) as cn, shop_id, HOUR(created_at) as date'))->where('created_at','>=',$from.' 00:00:00')->where('created_at','<=',$to.' 23:59:59')->groupBy(DB::raw("HOUR(created_at)"))->orderBy(DB::raw("HOUR(created_at)"),'asc')->groupBy('shop_id');
			if(Auth::user()->shop_id >0){
				$purchase= $purchase->where('shop_id','=',Auth::user()->shop_id);
			}
			$purchase = $purchase->get();
			foreach ($purchase as $p) {
				$data[array_get($shops, $p->shop_id)][$p->date.':00']=$p->cn;
			}

			return $data;
		}
		public static function getUserByShop($from,$to){
			$shops = Shop::getShopsShopId();
			$data =[];
			$purchase = Purchase::select(DB::raw('count(user_id) as cn, shop_id'))->where('created_at','>=',$from.' 00:00:00')->where('created_at','<=',$to.' 23:59:59')->groupBy('shop_id');
			if(Auth::user()->shop_id >0){
				$purchase= $purchase->where('shop_id','=',Auth::user()->shop_id);
			}
			$purchase = $purchase->get();
			foreach ($purchase as $p) {
				$data[array_get($shops, $p->shop_id)]=$p->cn;
			}
			//dd($data);
			return $data;
		}


	}
?>
