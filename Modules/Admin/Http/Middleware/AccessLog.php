<?php

namespace Modules\Admin\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AccessLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $routeName = $request->route()->getName();
        $route = $request->path();
        if ($routeName == 'admin_dashboard')
        {
            return $next($request);
        }
		if($request->isMethod('POST')) {
			$aclog = new \Modules\Admin\Entities\Accesslog();
			$aclog->user_id = \Auth::id();
			$aclog->route = $route;
			$aclog->action = $request->method();
			$aclog->extra = $request->all();
			$aclog->save();

		}
        return $next($request);
    }
}
