<ul class="nav nav-pills nav-fill" id="customerPurchase"role="tablist">
											@if(isset($customers['data']))
												@foreach($customers['data'] as $c)
													@if(\Illuminate\Support\Facades\Auth::user()->shop_id == $c['shop_id'])
												<li class="nav-item">
													<a class="nav-link @if($loop->iteration==1) active show @endif" data-toggle="tab" data-id="{{$c['id']}}" href="#">
														<img class="thumbnail" height="30" src="{{$c['avatar']}}">{{$c['user_name']}}
													</a>
												</li>
												@endif
												@endforeach

											@endif
										</ul>