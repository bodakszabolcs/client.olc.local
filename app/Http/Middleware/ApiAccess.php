<?php

namespace App\Http\Middleware;

use Closure;

class ApiAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $date = new \DateTime();
        $timestamp = $date->getTimestamp();

        $response = $next($request);
        $response->headers->set('Access-Control-Allow-Headers', 'Origin, Content-Type, Content-Range, Content-Disposition, Content-Description, X-Auth-Token');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Pragma', 'no-cache');
        $response->headers->set('Content-type', 'application/json; charset=utf-8');
        $response->headers->set('Expires', 'Tue, 01 Jan 1980 1:00:00 GMT');
        $response->headers->set('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0');
        $response->headers->set('Last-Modified',gmdate('D, d M Y H:i:s ', $timestamp) . 'GMT');
        //add more headers here
        return $response;
    }
}
