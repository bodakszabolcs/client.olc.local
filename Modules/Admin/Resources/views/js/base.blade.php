<script src="/alert.js"></script>
<script type="text/javascript">

(function( $ ){

    $.fn.filemanager = function(type, options) {
        type = type || 'file';

        this.on('click', function(e) {
            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
            localStorage.setItem('target_input', $(this).data('input'));
            localStorage.setItem('target_preview', $(this).data('preview'));
            window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
            window.SetUrl = function (url, file_path) {
                //set the value of the desired input to image url
                var target_input = $('#' + localStorage.getItem('target_input'));
                target_input.val(file_path).trigger('change');

                //set or change the preview image src
                var target_preview = $('#' + localStorage.getItem('target_preview'));
                target_preview.attr('src', url).trigger('change');
            };
            return false;
        });
    }


    @if(\Illuminate\Support\Facades\Auth::user()->shop_id >0)
        $(document).ready(function(){
    var lastUpdateAdmin = 0;
    localStorage.setItem('lastNoti',0);
    setInterval(getHasUpdate,1000);
    function getHasUpdate(){

        var time=new Date().getTime();
        $.get('/app/updated{{\Illuminate\Support\Facades\Auth::user()->shop_id}}.json?v='+time,function(data){
            console.log(lastUpdateAdmin);
            if(data.updated > lastUpdateAdmin){
                refreshData();
                lastUpdateAdmin = data.updated;
            }
        })
    }
    function refreshData(){
        var time=new Date().getTime();
        $.get('/app/purchase{{\Illuminate\Support\Facades\Auth::user()->shop_id}}.json?v='+time,function(data){
           let lastNoti = localStorage.getItem('lastNoti');
             $.each(data,function(item){

                 if(lastNoti < data[item].created_at){
                     playSound();
                     localStorage.setItem('lastNoti',data[item].created_at);
                 }
             });


        });
    }

    function playSound(){

        playAlert('funk');
// play sound

    }
    });
    @endif
})(jQuery);
$(document).ready(function(){
    if($('.has-danger').length) {
        $('html, body').animate({
            scrollTop: $(".has-danger").offset().top - 70
        }, 1000)
    }
})
	  var initAjax = function()
	  {
	      $(document).on('change','.ajax-select',function(){
	         var url = $(this).data('url');
	         var value = $(this).val();
	         var target = $(this).data('target');
              $.ajax({
                  type: 'get',
                  url: url+'?term='+value,
                  success: function (data) {
					$(target).html(data);
                  }
              });
		  });
	  }
	  var initFormSubmit =function()
      {
          $('form').submit(function(){
              $('#loading-wrapper').show();
          })
      }
    // Initialize datatable showing a search box at the top right corner


        Dropzone.autoDiscover = false;
    var initFormSubmit =function()
    {
        $('form').submit(function(){

            $('#loading-wrapper').show();
        })
        $(document).on('blur',' input, select, textarea, file',function(){
            if($(this).val().length >0 )
            {
                $(this).closest('.form-group').addClass('has-success');
            }
            else
            {
                $(this).closest('.form-group').removeClass('has-success');
            }
        });
        $(document).on('change',' input, select, textarea, file',function(){
            if($(this).val().length >0 )
            {
                $(this).closest('.form-group').addClass('has-success');
            }
            else
            {
                $(this).closest('.form-group').removeClass('has-success');
            }
        });
    }

    var initTableSorting =function()
    {
        if($('.filterTable').length >0) {
            if ($('.filterTable input[name="orderBy"]').val().length > 0) {

                var item = $('table ').find('[data-column="' + $('.filterTable input[name="orderBy"]').val() + '"]');


                if ($('.filterTable input[name="direction"]').val() == 'asc') {
                    $(item).data('sort', 'desc');
                }
                else if ($('.filterTable input[name="direction"]').val() == 'desc') {
                    $(item).data('sort', 'asc');
                }
                else {
                    $(item).data('sort', 'asc');
                }


            }
            $.each($('th.sorting'), function () {
                if ($(this).data('sort') == 'asc') {
                    $(this).append('<i class="fa fa-sort-desc pull-right"></i>');
                }
                else if ($(this).data('sort') == 'desc') {
                    $(this).append('<i class="fa fa-sort-asc pull-right"></i>');
                }
                else {
                    $(this).append('<i class="fa fa-sort pull-right"></i>');
                }
            });
            $(document).on('click', 'th.sorting', function () {

                $('.filterTable input[name="orderBy"]').val($(this).data('column'));

                if ($(this).data('sort') != undefined) {
                    $('.filterTable input[name="direction"]').val($(this).data('sort'));
                }
                else {
                    $('.filterTableinput[name="direction"]').val('asc');
                }
                $('.filterTable').closest('form').submit();
            });
        }
    }

    var initWyswygEditor = function() {

        $(".wyswyg").each(function(index,item) {
            CKEDITOR.replace( $(item).attr("id"), {
                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token=',
                contentsCss: ['/assets/vendors/base/vendors.bundle.css', '/assets/demo/default/base/style.bundle.css'],
                //extraAllowedContent: 'section',
                allowedContent: true,
                removeFormatAttributes: '',
                height: 600,
            });
        });
        CKEDITOR.config.autoParagraph = true;

        CKEDITOR.config.format_qouteRegular = { name: 'qouteRegular', element: 'p', attributes: { 'class': 'qouteRegular' } };
		CKEDITOR.config.format_qouteBold = { name: 'qouteBold', element: 'p', attributes: { 'class': 'qouteBold' } };
        CKEDITOR.config.format_separator1 = { name: 'separator1', element: 'div', attributes: { 'class': 'separator1' } };
        CKEDITOR.config.format_separator2 = { name: 'separator2', element: 'div', attributes: { 'class': 'separator2' } };
        CKEDITOR.config.format_separator3 = { name: 'separator3', element: 'div', attributes: { 'class': 'separator3' } };
        CKEDITOR.config.format_separator4 = { name: 'separator4', element: 'div', attributes: { 'class': 'separator4' } };
        CKEDITOR.config.format_highlight1 = { name: 'highlight1', element: 'span', attributes: { 'class': 'highlight1' } };
        CKEDITOR.config.format_highlight2 = { name: 'highlight2', element: 'span', attributes: { 'class': 'highlight2' } };
        CKEDITOR.config.format_dropCaps1 = { name: 'dropCaps1', element: 'span', attributes: { 'class': 'dropCaps1' } };
        CKEDITOR.config.format_dropCaps2 = { name: 'dropCaps2', element: 'span', attributes: { 'class': 'dropCaps2' } };
        CKEDITOR.config.allowedContent = true;
        CKEDITOR.config.forcePasteAsPlainText = true;
        CKEDITOR.config.format_tags = 'p;h1;h2;h3;h4;h5;h6;qouteRegular;qouteBold;separator1;separator2;separator3;separator4;highlight1;highlight2;dropCaps1;dropCaps2';
        CKEDITOR.config.removePlugins = 'about,a11yhelp,dialogadvtab,bidi,blockquote,templates,copyformatting,div,find,flash,floatingspace,forms,iframe,smiley,language,magicline,newpage,pagebreak,preview,print,save,selectall,showblocks,showborders,scayt,stylescombo,wsc,embed';
        CKEDITOR.config.removeButtons = 'Font';
        CKEDITOR.config.language = 'hu'

        CKEDITOR.config.pasteFromWordPromptCleanup = true;
    };
    var initRecomended = function(){
        $('.recomended_select').change(function(){
            var val =$(this).find('option:selected').val()
            if(val == 1)
			{
				$('.tags_r').removeClass('hide');
				$('.category_r').addClass('hide');
			}
			else if(val==2)
			{
                $('.tags_r').addClass('hide');
                $('.category_r').removeClass('hide');
			}
			else
			{
                $('.tags_r').addClass('hide');
                $('.category_r').addClass('hide');
			}
		});
        $('.recomended_select').trigger('change');

	}


    var initWidgetEmbed = function()
    {
        $('#form-widget_embed').change(function(){
            if($(this).find('option:selected').val() !='') {
                $('#txtEmbed').val($(this).find('option:selected').text());
                $('.editWidget').removeAttr('disabled');
            }
            else
            {
                $('#txtEmbed').val('');
                $('.editWidget').attr('disabled','disabled');
            }
        });
        $('#form-widget_embed').trigger('change');
        $(document).on('change','input[name="size"]',function(){

            var cls = '.l'+$('input[name="size"]:checked').val();
            $('.layout').attr('disabled','disabled');
            $(cls).removeAttr('disabled');


        });
        $(document).on('click', '#widgetSave', function () {

            var url="";
            if($('#addWidget').data('model').length)
            {
                    url="/"+$('#addWidget').data('model');
            }

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/admin/widget/edit'+url ,
                data: $('#addWidget form').serializeArray(),
                success: function (data) {
                    if (data.status === "ok") {

                        $('#widgetEmbed').val('##widget|'+data.slug+'##');
                        var arr = data.slug.split('-');

                        $('#addWidget').modal('hide');
                        $('.widget_embed_list').append('<input type="hidden" name="widget_embed['+arr[arr.length-1]+']" value="'+'##widget|'+data.slug+'##'+'">');
                        $('#form-widget_embed').append('<option value="'+arr[arr.length-1]+'" selected="selected">##widget|'+data.slug+'##</option>');
                        $('#form-widget_embed').change();

                    }
                    else {
                        $('#addWidget form').html(data.html);
                        $('.cont [data-draggable="item"]').draggable({
                            cursor: 'move',
                            helper: "clone"
                        });
                        $('#addWidget form .has-error input').focus();
                        $('#addWidget ').animate({
                            scrollTop: $("#addWidget form .has-error input").offset().top
                        }, 1000);
                        $('input[name="size"]').trigger('change');

                        $(".show_manual .double-target").droppable({
                            drop: function (event, ui) {
                                if ($(event.target).hasClass('double-target')) {
                                    var first = true;
                                    var itemid = $(event.originalEvent.toElement).attr("itemid");

                                    $('.cont [data-draggable="item"]').each(function () {

                                        if (($(this).attr("itemid") === itemid) && first) {
                                            var item = $(this).clone();
                                            var elem = '<div class="form-inline btn btn-default">' +
                                                '<input type="hidden" name="article_id[]" value="' + $(this).data('id') + '">' +
                                                '<input type="hidden" name="type[]" value="' + $(this).data('type') + '">' +
                                                $(item).removeClass('btn btn-default').prop('outerHTML') +
                                                '<a class="btn btn-xs btn-danger trash"><i class="fa fa-trash"></i></a>' +
                                                '</div>';

                                            //elem.appendTo(".double-target");
                                            $('.show_manual .double-target').append(elem);
                                            first = false;

                                        }
                                    });
                                }
                            }
                        });
                    }


                },

            });
        });
        $(document).on('click', '.addWidget, .editWidget', function () {



            var edit =$(this).hasClass('editWidget');
            if( edit  )
            {
                $('#addWidget').data('model',$('#form-widget_embed').find('option:selected').val());
                var model="/"+$('#form-widget_embed').find('option:selected').val();
                $('add')
            }
            else
            {
                $('#addWidget').data('model','');
                var model="";
            }

            console.log(model);
            $.ajax({
                type: 'GET',
                dataType: 'html',
                url: '/admin/widget/edit'+model,

                success: function (data) {
                    console.log("ok");


                    $('#addWidget form').html(data);
                    $('.cont [data-draggable="item"]').draggable({
                        cursor: 'move',
                        helper: "clone"
                    });
                    $('input[name="size"]').trigger('change');
                    $(".double-target").droppable({
                        drop: function (event, ui) {
                            if ($(event.target).hasClass('double-target')) {
                                var first = true;
                                var itemid = $(event.originalEvent.toElement).attr("itemid");

                                $('.cont [data-draggable="item"]').each(function () {

                                    if (($(this).attr("itemid") === itemid) && first) {
                                        var item = $(this).clone();
                                        var elem = '<div class="form-inline btn btn-default">' +
                                            '<input type="hidden" name="article_id[]" value="' + $(this).data('id') + '">' +
                                            '<input type="hidden" name="type[]" value="' + $(this).data('type') + '">' +
                                            $(item).removeClass('btn btn-default').prop('outerHTML') +
                                            '<a class="btn btn-xs btn-danger trash"><i class="fa fa-trash"></i></a>' +
                                            '</div>';

                                        //elem.appendTo(".double-target");
                                        $('.double-target').append(elem);
                                        first = false;

                                    }
                                });
                            }
                        }
                    });

                },

            });
        });

    }
    var initConfirmModal = function() {
        $('.confirm').on('click', function () {

            var href = $(this).attr('href');
            var title = $(this).text();
            var title2 = $(this).attr('title');
            if (typeof title2 != "undefined" && title2.length > 0) {
                title += " (" + title2 + ")";
            }


            var ret = '';
            ret += '<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-hidden="false"> \
                    <div class="modal-dialog"> \
                        <div class="modal-content"> \
                            <div class="modal-header"> \
                                <h4 class="modal-title">{{__('A művelet jóváhagyást igényel!')}} ' + title + '</h4> \
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button> \
                            </div> \
                            <div class="modal-body"> \
{{__('Kérem erősítse meg, hogy a műveletet valóban el kívánja végezni. A művelet nem visszavonható.')}} \
                            </div> \
                            <div class="modal-footer"> \
                                <button type="button" class="btn default" data-dismiss="modal">{{__('Mégsem')}}</button> \
                                <a href="' + href + '" class="btn btn-danger">{{__('Igen, a műveletet elvégzem')}}</a> \
                            </div> \
                        </div> \
                    </div> \
                </div>';
            $('body').append(ret);
            $('#confirm').modal();

            $('#confirm').on('hidden.bs.modal', function () {
                $("#confirm").remove();
            });
            return false;
        });
    };
    var initCategory= function()
    {
        $(document).on('change','.category-block li input[type="checkbox"]',function() {
            var child =$(this).closest('li').next('li');
            console.log($(this).is(':checked'));
            if($(this).is(':checked'))
            {
                $(child).find('ul li').each(function(){

               $(this).find('input[type="checkbox"]').attr('checked', true);;
               $(this).find('input[type="checkbox"]').prop('checked', true);

            });
            }
            else
            {
                $(child).find('ul li').each(function(){

                    $(this).find('input[type="checkbox"]').attr('checked', false);
                    $(this).find('input[type="checkbox"]').prop('checked', false);
                });
            }

        });
    }
    var initSelect2 = function() {
        $('.select2').select2();

        $(".tags").select2({
            tags: true,
            tokenSeparators: [',',';']
        });
    };

    var initDateTimePicker = function() {
        $.each($(".datepicker"),function(){
           	 if($(this).val()=='0000-00-00'){
           	     $(this).val('');
           	 }
		});
        $('.table-responsive').doubleScroll({
            resetOnWindowResize: true
        });
        $(".datepicker").datepicker({
			language: "hu",
            locale: "hu",
			format:"yyyy-mm-dd",
            autoclose: true
        });

        $(".timepicker").timepicker({
            showMeridian: false
        });

        $(".datetimepicker").datetimepicker({
            language: "hu",
            locale: "hu",
            autoclose: true
        });


        $(".daterangepicker-liquid").daterangepicker({
            buttonClasses:"m-btn btn",
            applyClass:"btn-primary",
            cancelClass:"btn-secondary",
            showButtonPanel: true,
            language: "hu",
            locale: {
                format: "YYYY-MM-DD"
            }
        });


    };

    var initSupport = function() {
        $(".error-reporting").on('click', function(evt) {
            $("#modalSlideUp").modal('show');
        });

        $("#form-support-modal").on('submit', function(event) {
            event.preventDefault();

            if ($("#error-type").val() == "")
            {
                alert("{{__('Hiba leírása mező kitöltése kötelező!')}}");
            }

            $.ajax({
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{route('admin_support_email')}}',
                dataType: 'json',
                data: $("#form-support-modal").serialize(),
                success: function(data) {
                    $("#notification-wrapper").prepend('<div class="m-alert m-alert--icon alert alert-success" role="alert">\n' +
                        '        <div class="m-alert__icon">\n' +
                        '            <i class="flaticon-danger"></i>\n' +
                        '        </div>\n' +
                        '        <div class="m-alert__text">\n' +
                        '            <strong>\n' +
                        '                {{__('Sikeres!')}}\n' +
                        '            </strong>\n' +
                        '             '+ data.message+
                        '        </div>\n' +
                        '        <div class="m-alert__actions" style="width: 220px;">\n' +
                        '            <button type="button" class="btn btn-outline-light btn-sm m-btn m-btn--hover-brand"\n' +
                        '                    data-dismiss="alert" aria-label="Close">\n' +
                        '                {{__('Bezárás')}}\n' +
                        '            </button>\n' +
                        '        </div>\n' +
                        '    </div>');
                    $("#form-support-modal").trigger("reset");
                    $("#modalSlideUp").modal('hide');
                    $('#loading-wrapper').hide();
                },
                error: function (data) {
                    $("#notification-wrapper").prepend('<div class="m-alert m-alert--icon alert alert-danger" role="alert">\n' +
                        '        <div class="m-alert__icon">\n' +
                        '            <i class="flaticon-danger"></i>\n' +
                        '        </div>\n' +
                        '        <div class="m-alert__text">\n' +
                        '            <strong>\n' +
                        '                {{__('Hiba történt!')}}\n' +
                        '            </strong>\n' +
                        '             {{__('Hiba történt a küldés során!')}}'+
                        '        </div>\n' +
                        '        <div class="m-alert__actions" style="width: 220px;">\n' +
                        '            <button type="button" class="btn btn-outline-light btn-sm m-btn m-btn--hover-brand"\n' +
                        '                    data-dismiss="alert" aria-label="Close">\n' +
                        '                {{__('Bezárás')}}\n' +
                        '            </button>\n' +
                        '        </div>\n' +
                        '    </div>');
                    $("#form-support-modal").trigger("reset");
                    $("#modalSlideUp").modal('hide');
                    $('#loading-wrapper').hide();
                }
            })
        });
    };

    var modalInputFocus = function() {
        $('.modal').on('shown.bs.modal', function () {
            $(this).find('input[type="text"]').focus();
        });
    };

    var initDropZone = function(dropzone_element, url) {
        if (dzelem == null) {
            dzelem = new Dropzone(dropzone_element, {
                url: url,
                paramName: "file", // The name that will be used to transfer the file
                addRemoveLinks: false,
                parallelUploads: 1,
                dictRemoveFile: "Törlés",
                dictDefaultMessage: 'A feltöltéshez húzza ide a fájlokat, vagy kattintson a mezőbe.',
                dictCancelUpload: 'Mégse',
                //dictMaxFilesExceeded: 'Maximum 5 fájl feltöltése lehetséges',
                dictInvalidFileType: 'Ilyen típusú fájl feltöltése nem engedélyezett',
                createImageThumbnails: true,
                init: function () {
                    this.on("complete", function (file) {
                        this.removeFile(file);
                        refreshFileGrid();
                    });
                },
                sending: function (file, xhr, formData) {
                    // Pass token. You can use the same method to pass any other values as well such as a id to associate the image with for example.
                    formData.append("_token", $('meta[name="csrf-token"]').attr('content')); // Laravel expect the token post value to be named _token by default
                    formData.append("level", $("#level").val()); // Laravel expect the token post value to be named _token by default
                    formData.append("parent_path", $("#parent_path").val()); // Laravel expect the token post value to be named _token by default
                },

                removedfile: function (file) {
                    var _ref;
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                }
            });
        }
    };

    var initImageBrowser = function()
    {
        $(".browse-image").filemanager('image');
    };

    var initFileBrowser = function()
    {
        $(".browse-file").filemanager('file');
    };

    var $image = $("#image-for-crop");
    var model_id, image, type, x, y, width, height, rotate, scaleX, scaleY;

    var initCropperModal = function (class_name) {

        $("."+class_name).on('click', function() {

            model_id = $(this).data('id');
            image = $(this).data('image');
            type = $(this).data('type');

            $("#image-for-crop").attr('src',$(this).data('image'));

            $("#cropperModal").modal('show');

            $("#cropperModal").on('shown.bs.modal', function() {
                if (type == 16) {
                    $image.cropper({
                        aspectRatio: 16 / 9,
                        center: true,
                        highlight: true,
                        background: true,
                        modal:true,
                        viewMode:0,
                        //minContainerWidth:984,
                        //minContainerHeight:500,
                        getCroppedCanvas:{fillcolor: "#FFFFFF"}
                    }).on('crop', function(e) {
                        width = e.width;
                        height = e.height;
                        x = e.x;
                        y = e.y;
                        rotate = e.rotate;
                        scaleX = e.scaleX;
                        scaleY = e.scaleY;
                    });
                } else if (type == '3')
                {
                    $image.cropper({
                        aspectRatio: 3 / 2,
                        center: true,
                        highlight: true,
                        background: true,
                        modal:true,
                        viewMode:0,
                        //minContainerWidth:984,
                        //minContainerHeight:500,
                        getCroppedCanvas:{fillcolor: "#FFFFFF"}
                    }).on('crop', function(e) {
                        width = e.width;
                        height = e.height;
                        x = e.x;
                        y = e.y;
                        rotate = e.rotate;
                        scaleX = e.scaleX;
                        scaleY = e.scaleY;
                    });
                } else if (type == '4')
                {
                    $image.cropper({
                        aspectRatio: 4 / 3,
                        center: true,
                        highlight: true,
                        background: true,
                        modal:true,
                        viewMode:0,
                        minContainerWidth:984,
                        minContainerHeight:500,
                        getCroppedCanvas:{fillcolor: "#FFFFFF"}
                    }).on('crop', function(e) {
                        width = e.width;
                        height = e.height;
                        x = e.x;
                        y = e.y;
                        rotate = e.rotate;
                        scaleX = e.scaleX;
                        scaleY = e.scaleY;
                    });
                }
            });

            $("#cropperModal").on('hidden.bs.modal', function () {
                $image.cropper('destroy');
            });
        });

    };

    var initSendCrop = function () {
        $(".send-new-image-size").on('click', function() {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: {
                    image: image,
                    type: type,
                    x: Math.floor(x),
                    y: Math.floor(y),
                    width: Math.floor(width),
                    height: Math.floor(height),
                    rotate: rotate,
                    scaleX: scaleX,
                    scaleY: scaleY
                },
                url: '{{route('admin_crop_image')}}',
                success: function(data) {
                    if (data.status == 'OK')
                    {
                        if(type==1) {
                            $('#image-logo').val(data.file);
                            $('.sablon-notification-inputs input').first().change();
                        }
                        if(type==2)
                        {
                            $('#image').val(data.file);
                            $('.sablon-inputs input, .sablon-inputs textarea').first().change();
                        }
                        if(type==3)
                        {
                            $('#image2').val(data.file);
                            $('.sablon-inputs input, .sablon-inputs textarea').first().change();
                        }

                        $("#cropperModal").modal('hide');


                    }
                }
            });
        });
    };


    var initEndDate = function() {
        $("#watch_end").on('change', function() {
            if ($(this).prop('checked'))
            {
                $("#form-end_date").removeAttr('disabled');
                $("#form-end_time").removeAttr('disabled');
            } else {
                $("#form-end_date").attr('disabled','disabled');
                $("#form-end_time").attr('disabled','disabled');
            }
        }).trigger('change');
    };

    var maintanceSwitcher = function() {
        $(".maintenance-switcher").on('change', function (event) {
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '{{route('admin_maintenance_mode')}}'
            });
        });
    };

    var compactSwitcher = function() {
        $(".compact-switcher").on('change', function (event) {
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '{{route('admin_compact_mode')}}',
                success: function(data) {
                    console.log(data);
                    if (data.status == "1")
                    {
                        $("body").addClass('m-brand--minimize');
                        $("body").addClass('m-aside-left--minimize');
                    } else {
                        $("body").removeClass('m-brand--minimize');
                        $("body").removeClass('m-aside-left--minimize');
                    }
                }
            });
        });
    };

    var languageSwitcher = function() {
        $("#form-language").on('change', function (event) {
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '{{route('admin_language_change')}}',
                data: {
                    locale: $(this).val()
                },
                success: function(data) {
                    document.location.reload();
                }
            });
        });
    };
    var searchWorker = function() {

        $(document).on('keyup','#searchWorker',function(){
            var val = $(this).val();
            var project = $('#workerSearchModal .project_id').val();
            $.ajax({
                type: 'GET',
                url: '{{route('admin_dashboard')}}?search-fillable='+val+'&project_id='+project,

                success: function(data) {
                    console.log(data);
                    console.log(data);
                    $('#searchWorkerTable > tbody').html(data);
                },

                error: function(data) {
                    console.log(data);
                    $('#searchWorkerTable > tbody').html(data);
                },
            });
		});
        $(document).on('click','.addWorker',function(){
            var position =$(this).closest('tr').find('select').val();
            $(this).attr('href',$(this).attr('href')+'?position='+position);
		})
	}

$(document).ready(function(event) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

	searchWorker();
    initConfirmModal();
    initDateTimePicker();
    initSelect2();
    initSupport();
    initWyswygEditor();
    modalInputFocus();
    initImageBrowser();
    initFileBrowser();
    initCropperModal('edit-169');
    initCropperModal('edit-43');
    initCropperModal('edit-32');
    initSendCrop();
    initWidgetEmbed();

    initEndDate();

    initCategory();
	initAjax();
    initFormSubmit();
    maintanceSwitcher();
    compactSwitcher();
    languageSwitcher();
    initTableSorting();
    initFormSubmit();

    window.addEventListener("click", function (ev) { zenscroll.stop(); }, false);

});
</script>
