@extends('frontend.layout')

@section('content')




	<section class="munkakategoria margin60" >
	<div class="container">

		<div class="row">
   			@include('frontend.work.left',['categories'=>$categories,'headCat'=>$headCat,'subCat'=>$subCat,])
	   		@include('frontend.work.right',['headCat'=>$headCat,'subCat'=>$subCat,'works'=>$works,'city'=>$city,'hasWork'=>$hasWork])
		</div>

	</div>

</section>
	<style>
	li.btn.btn-outline.btn-lg.col-xs-12 {
		margin-bottom: 6px;
	}
	li.btn.btn-outline.btn-lg.col-xs-12 a{
		color:white;
	}
	li.btn.btn-outline.btn-lg.col-xs-12 a:hover{
		color:black;
		text-decoration: none;
	}
	li:hover
	{
		color:black;
	}
	.btn-outline.active, .btn-outline:active, .btn-outline:focus, .btn-outline:hover {

		background: #fff;
		border: 2px solid #fff;
		color:black;
	}
	.btn-outline.active, .btn-outline:active, .btn-outline:focus, .btn-outline:hover a{


		color:black !important;
	}
	.panel h3
	{
		overflow: inherit !important;
	}
	.panel-body.hatodik
	{

		background-color: #164e6b;
		color:white;

	}
	.panel-default>.panel-heading.hetedik {
		color: white;
		background-color: #71b2d2;
		border-color: #ddd;
		font-size: 18px;
		font-weight: 600;
	}
	.navbar-default .navbar-offcanvas {

	}
	.panel-heading.hetedik h3
	{
		font-size: 17px;;
	}
	.btn.btn-outline.active a
	{
		color:black !important;
	}
	a, a.active, a:active, a:focus, a:hover {
		color: #000000;
		outline: 0;
	}
	span.breadcrumb-item.active {
		color: grey;
	}
	.panel h3,.panel h4{
		text-transform: none;
	}

</style>

@endsection