<?php
	echo "
@extends('admin::layouts.layout')
@section('content')
   <div class='row'>
        <div class='col-12'>
            <div class='m-portlet'>
                <div class='m-portlet__head'>
                    <div class='head-btn'>
            {!! \\App\\Http\\Helper::text_button(__('Új Hozzáadás'),array('route_name' => 'admin_".$root."_edit', 'params' => array()),'fa-plus', array('class'=>'btn m-btn--square btn-primary')) !!}
          </div>
                </div>
                <div class='m-portlet__body'>
                {!! \$filterHeader !!}
               <div class='table-responsive'>
                        <table class='table table-bordered table-hover mb-150'>
                                <thead>
                                <tr>
                                    <th class='sorting' data-column='id'>{{__('ID')}}</th>\n";
	foreach ($data['column_fillable'] as $k=>$bool)
	{
		if($bool)
		{
			echo "                                    		<th class='sorting' data-column='".$k."'>{{__('".$data['column_label'][$k]."')}}</th>\n";
		}
	}

	echo "                              <th width='15%'>{{__('Művelet')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(\$model as \$m)

                                    <tr>
                                        <td>{{\$m->id}}</td>\n";
	foreach ($data['column_fillable'] as $k=>$bool)
	{
		if($bool)
		{
			echo "                                        <td>{{\$m->".$data['column_name'][$k]."}}</td>\n";
		}
	}
	echo "                                        <td>";

	if($data['edit'])
	{
		echo "                                                    {!! \App\Http\Helper::text_button(__('Szerkesztés'),array('route_name'=>'admin_".$root."_edit', 'params' => array(\$m->id)),'fa-edit', array('class'=>'dropdown-item')) !!}\n";
	}
	if($data['delete'])
	{
		echo "                                                    {!! \App\Http\Helper::text_button(__('Törlés'),array('route_name'=>'admin_".$root."_delete', 'params' => array(\$m->id)),'fa-trash', array('class'=>'dropdown-item confirm')) !!}\n";
	}

	echo "
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                             {!! \$model->appends(\$_GET)->links() !!}
                        </div>
                </div>
            </div>
        
    </div>
@endsection ";

?>
