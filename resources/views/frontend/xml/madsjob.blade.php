<jobs>
<?php
	$server = 'http://'.$_SERVER['HTTP_HOST'];
	$data = \Modules\Work\Entities\Work::where('is_full','=',0)->where('archive','=',0)->get()->toArray();
		
		foreach($data as $row):

			$c=\Modules\Workcategory\Entities\Workcategory::find($row['workcategory_id']);
			$head_category = \Modules\Workcategory\Entities\Workcategory::find($c->parent_id);
			$url= $server.route('frontend_work',[$head_category->slug,$c->slug,$row['slug']]);
			$projekt =\Modules\User\Entities\User::find($row['responsible']);
			$projekt_text = $projekt->name."<br/>".
				"Email: <a href='mailto:".$projekt->email."'><b>".$projekt->email."</b></a><br/>".
				"Telefon: <b>".$projekt->phone."</b>";
			?>

			<job>
				<link><?=$url?></link>
				<name><![CDATA[<?=$row['name']?>]]></name>
				<?php $city=\Modules\City\Entities\City::find($row['city']); ?>
				<region><?= $city->name ?></region>

				<solary><?=$row['wage']?></solary>
				<company>MADS Work</company>
				<company_url>http://www.madswork.hu</company_url>
				<description><![CDATA[<h3> <?=$row['name']?></h3></br><?=$row['lead']?></br> Feladat: <?=$row['task']?></br>Feltétel: <?=$row['conditions']?> </br> Kapcsolattartó: <?=$projekt_text?> ]]></description>
				<categories>

				<category><?= $head_category->name ?>/<?= $c->name ?></category>

				<category>Munka</category>

				</categories>
				<?php $endOfCycle = date("d.m.y", strtotime("+1 month")); ?>
				<expire><?=$endOfCycle?></expire>
				<update><?=date("d.m.y")?></update>
			</job>
			
	<?php endforeach; ?>
</jobs>