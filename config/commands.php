<?php
return [
	'notification:generate'=>'Értesítések generálása',
	'send:news'=>'Napi újdonságok küldése',
	'modelCache:clear' => 'Model cache űrítése',
    'generate:coupon' => 'Automatikus kuponok generálása'
];
