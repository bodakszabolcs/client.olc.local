<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Modules\Acl\Entities\Role;
use Modules\Admin\Http\Controllers\AdminController;
use Modules\User\Entities\User;
use Modules\User\Entities\UserBilling;
use Modules\User\Entities\UserShipping;
use Rap2hpoutre\FastExcel\FastExcel;

class UserController extends AdminController
{
    public $niceNames =[
        'lastname' => 'Vezetéknév',
        'firstname' => 'Keresztnév',
        'email' => 'E-mail',
        'password' => 'Jelszó',
        'password_again' => 'Jelszó újra'
    ];
    public function __construct()
    {
        View::share('sub_title',__('Felhasználók'));
        View::share('subtitle_link',route('admin_user_list'));
    }
	public function Avatar(Request $request){
    	$user = User::find(Auth::user()->id);
    	$user->avatar= '/avatars/'.$request->input('filename');
    	$user->save();
    	$request->session()->flash('success_message','Az avatar váltás sikeres');
    	return redirect()->back();
	}
    public function UserList(Request $request)
    {
        $model = new User();
        $list=$model->searchInModel($request->input());
        $list = $list->where('id','<>',1)->paginate(20)->withPath($request->path());
        View::share('title',__('Felhasználó Lista'));
        $params = explode("list?", url()->full());
        if (isset($params[1]))
        {
            $params = $params[1];
        } else {
            $params = '';
        }

        return view('user::list', ['model' => $list, 'filterHeader'=>$model->drawSearchTableHeader($request), 'params' => $params]);
    }

    public function Export(Request $request)
    {
        return (new FastExcel(User::all()))->download('file.csv');
    }
	public function GetSigno(Request $request,$id){

    	$user =User::where('email','=',$id)->first();
    	return response()->json($user->signature);
	}
    public function Profile(Request $request, $tab = 'general')
    {
        View::share('title',__('Profil'));

        $model = User::find(Auth::user()->id);
        $id = Auth::user()->id;
        if ($request->isMethod('post')) {
            $tab = $request->input('tab','general');
            if ($tab == 'general')
            {
                if ($id > 0) {

                    $rules = [
                        'lastname' => 'required',
                        'firstname' => 'required',
                        'email' => 'required|email|unique:users,email,'.$model->email.',email'
                    ];

                    if ($request->has('password') && !empty($request->input('password')))
                    {
                        $rules['password'] = 'min:8|max:16';
                        $rules['password_again'] = 'same:password';
                    }


                    $validator = Validator::make($request->all(), $rules);
                } else {
                	$rules =[
		                'lastname' => 'required',
		                'firstname' => 'required',
		                'email' => 'required|email||unique:users,email',
		                'password' => 'required|min:8|max:16',
		                'password_again' => 'required|same:password'

	                ];
                    $validator = Validator::make($request->all(), $rules);
                }

                $validator->setAttributeNames($this->niceNames);
                if ($id > 0 && $request->has('password') && $request->input('password') != $request->input('password_again'))
                {
                    $validator->after(function($validator) {
                        $validator->errors()->add('password', __('A jelszó és jelszó újra mezők nem egyeznek!'));
                    });
                }

                if ($validator->fails()) {
                    return redirect(route('admin_profile', [$id, $tab]))
                        ->withErrors($validator)
                        ->withInput();
                }

                $model->fill($request->all());
                if ($request->has('password') && !empty($request->input('password')))
                {
                    $model->password = Hash::make($request->password);
                }
				if($request->input('role') == 4)
                $model->save();


            }

            $request->session()->flash('success_message',__('success_save'));

            return redirect()->intended(route('admin_profile', [$tab]));
        }

        return view('admin::index.profile', ['model'=>$model, 'id'=>$id, 'tab'=>$tab, 'shipping' => $model->shipping, 'billing' => $model->billing]);
    }

    public function Edit(Request $request, $id = 0, $tab = 'general')
    {
        View::share('title',__('Felhasználó Szerkesztés'));
		if($id == 1){
			return redirect(route('admin_user_list'));

		}
        $model = User::find($id);

        $tab = $request->input('tab','general');
        if (is_null($model))
        {
            if ($id > 0 || $id == 1)
            {
                return redirect(route('admin_user_list'));
            }
            $model = new User;
        }

        if ($request->isMethod('post')) {
            if ($tab == 'general')
            {
                if ($id > 0) {
	                $rules = [
		                'lastname' => 'required',
		                'firstname' => 'required',
		                'role'=> 'required',
		                'email' => 'required|email|unique:users,email,'.$model->email.',email'
	                ];

	                if ($request->has('password') && !empty($request->input('password')))
	                {
		                $rules['password'] = 'min:8|max:16';
		                $rules['password_again'] = 'same:password';
	                }
	                if ($request->has('role') && $request->input('role') == 4){
		                $rules['shop_id'] = 'required';
	                }

	                $validator = Validator::make($request->all(), $rules);
                } else {
	                $rules =[
		                'lastname' => 'required',
		                'firstname' => 'required',
		                'role'=> 'required',
		                'email' => 'required|email||unique:users,email',
		                'password' => 'required|min:8|max:16',
		                'password_again' => 'required|same:password'

	                ];
	                if ($request->has('role') && $request->input('role') == 4){
		                $rules['shop_id'] = 'required';
	                }
	                $validator = Validator::make($request->all(), $rules);
                }
                $validator->setAttributeNames($this->niceNames);
                if ($id > 0 && $request->has('password') && $request->input('password') != $request->input('password_again'))
                {
                    $validator->after(function($validator) {
                        $validator->errors()->add('password', __('A jelszó és jelszó újra mezők nem egyeznek!'));
                    });
                }

                if ($validator->fails()) {
                    return redirect(route('admin_user_edit', [$id, $tab]))
                        ->withErrors($validator)
                        ->withInput();
                }

                $model->fill($request->all());
                if ($request->has('password') && !empty($request->input('password')))
                {
                    $model->password = Hash::make($request->password);
                }

                $model->save();
	            $model->roles()->sync($request->input('role'),2);


            }

            $request->session()->flash('success_message',__('success_save'));
            if ($request->has('save_and_exit')) {
                return redirect()->intended(session()->get('redirect_url', route('admin_user_list')));
            }

            return redirect()->intended(route('admin_user_edit', [$model->id, $tab]));
        }

        return view('user::edit', ['model'=>$model, 'id'=>$id, 'tab'=>$tab]);
    }

    public function ForceLogin(Request $request, $id)
    {
        try {
            $user = User::findOrFail($id);

            $request->session()->forget('is_superadmin');
            Auth::login($user);

            return redirect(route('admin_dashboard'));
        } catch (ModelNotFoundException $e)
        {
            abort(400);
        }
    }

    public function ResetLoginLog(Request $request, $id)
    {
        $user = User::find($id);

        DB::table('_logins')->where('email', $user->email)->delete();

        $request->session()->flash('success_message',__('A felhasználó tiltása feloldva!'));

        return redirect()->back();
    }

    public function Delete(Request $request, $id)
    {
        try {
            if ($id == 1)
            {
                throw new ModelNotFoundException();
            }
            $model = User::findOrFail($id);
            $model->email = "DELETED_".date("Y-m-d H:i:s")."_".$model->email;
            $model->delete();

            $request->session()->flash('success_message',__('success_delete'));
        } catch (ModelNotFoundException $e)
        {
            $request->session()->flash('error_message',__('Az elem nem törölhető'));
        }

        return redirect()->back();
    }
}
