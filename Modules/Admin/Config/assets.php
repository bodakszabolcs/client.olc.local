<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Javascript
    |--------------------------------------------------------------------------
    |
    */
    'javascript' => [
        '/assets/vendors/base/vendors.bundle.js',
        '/assets/demo/default/base/scripts.bundle.js',
        '/vendor/ckeditor/ckeditor.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.6.4/jquery.colorbox-min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.3/cropper.min.js',
        '/vendor/laravel-filemanager/js/lfm.js',
        '/assets/js/jquery.nestable.js',

        '/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js',
        '/assets/app/js/dashboard.js',
        '/assets/demo/default/custom/crud/forms/widgets/bootstrap-datepicker.js',
        '/assets/demo/default/custom/crud/forms/widgets/bootstrap-datetimepicker.js'
    ],
    'stylesheet' => [
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css',
        'https://use.fontawesome.com/releases/v5.0.11/css/all.css',
        'https://cdnjs.cloudflare.com/ajax/libs/cropper/3.1.3/cropper.min.css',
        '/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css',
        '/assets/vendors/base/vendors.bundle.css',
        '/assets/demo/default/base/style.bundle.css',
        '/assets/css/custom.css'
    ]
];
