<?php

namespace App\Console\Commands;

use App\Mail\NewsletterEmail;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Message\Topics;
use Modules\Company\Entities\Shop;
use Modules\News\Entities\News;
use Modules\Newsletter\Entities\Newsletter;
use Modules\Newsletter\Entities\NewsletterList;
use Modules\Newsletter\Entities\NewsletterStatistic;
use Modules\Page\Entities\SiteBuilder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class SendNews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Cron - News start '.date('Y-m-d H:i:s'));
        foreach (Shop::all() as $s) {
            $news = News::where('day', '=', date('N'))
                ->where('shop_id','=',$s->shop_id)
                ->where('active','=',1)
                ->first();

            if(!is_null($news)){
                $this->topicNotification($news->title,$news->description,$news->link,$news->image,'shop'.$s->shop_id,$s->name,optional($news)->color);
            }
        }
        Log::info('Cron - News end '.date('Y-m-d H:i:s'));
    }
    protected function topicNotification($title,$body,$link,$image,$topicId,$shop,$color){

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);
        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setSound('default')->setBadge('1');
        $notificationBuilder->setClickAction('FCM_PLUGIN_ACTIVITY');
        $notificationBuilder->setIcon($image);
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData([
            "click_action"=>"FCM_PLUGIN_ACTIVITY",
            'type' => 'news',
            'shop'=> $shop,
            'title'=>$title,
            'link'=>$link,
            'image'=>$image,
            'content' => $body,
            'color' => $color,
        ]);
        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $topic = new Topics();
        $topic->topic($topicId);
        $topicResponse = FCM::sendToTopic($topic, $option, $notification, $data);
       echo $topicResponse->isSuccess();
       echo $topicResponse->shouldRetry();
       echo  $topicResponse->error();
    }
	
}
