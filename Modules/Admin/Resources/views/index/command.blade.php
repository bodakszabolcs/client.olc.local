@extends('admin::layouts.layout')
@section('content')
    <?php
    $errors = Session::get('field_errors');
    Session::forget('field_errors');
    ?>
    <div class="row">
        <div class="col-12">
            <div class="m-portlet">
                <div class="m-portlet__body">
                    {!!  \App\Http\Helper::formOpen('CommissionForm','post',NULL)  !!}
                    <div class="m-form__group form-group">
                        <div class="m-radio-list">
                            @foreach(config('commands') as $k =>$v)
                                <label class="m-radio">
                                    <input type="radio" name="command" value="{{$k}}">
                                    {{$v}}
                                    <span></span>
                                </label>
                            @endforeach
                        </div>
                    </div>


                    <button class='btn btn-success btn-cons' name='save_and_exit' type='submit'><i
                                class='fas fa-terminal'></i> {{__('Futtatás')}}</button>

                    {!! \App\Http\Helper::formClose() !!}
                </div>
            </div>
        </div>
    </div>
@endsection