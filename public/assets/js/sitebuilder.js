$(function () {
    savePreview();
    refreshWidget();


    var h = $('#builder .builder-container #header').outerHeight();
    h += $('#builder .builder-container #content').outerHeight();
    h += $('#builder .builder-container #footer').outerHeight();
    h += 45;
    $('#builder .widget-list').css({
        'height': h + 'px',
        'overflowY': 'scroll'
    });

    function removeBtn() {
        $('.builder-container [data-slug]').hover(function () {

            if ($(this).find('.layer').length === 0) {
                var str= $(this).data('slug');

                var arr = str.split('-');
                // console.log($(this).data('href'));
                if($(this).data('href') != undefined)
                {
                    $("<div class='layer'><button class='btn btn-danger remove'>TĂśrlĂŠs</button><a class='btn btn-success edit' target='_blank' href='"+$(this).data('href')+"'>SzerkesztĂŠs</a></div>").css({
                        position: "absolute",
                        width: "100%",
                        height: "100%",
                        top: 0,
                        left: 0,
                        'z-index': 10000,
                        background: 'rgba(35, 35, 35, .6)'
                    }).appendTo($(this).find(">:first-child").css("position", "relative"));
                }
                else {
                    $("<div class='layer'><button class='btn btn-danger remove'>TĂśrlĂŠs</button><button class='btn btn-success edit addWidget' data-edit='edit' data-toggle='modal' data-target='#addWidget' data-model-id='" + arr[arr.length - 1] + "'>SzerkesztĂŠs</button></div>").css({
                        position: "absolute",
                        width: "100%",
                        height: "100%",
                        top: 0,
                        left: 0,
                        'z-index': 10000,
                        background: 'rgba(35, 35, 35, .6)'
                    }).appendTo($(this).find(">:first-child").css("position", "relative"));
                }
                $('.remove').click(function (event) {
                    // console.log("dsfdsa");
                    event.preventDefault();
                    $(this).closest('[data-slug]').remove();

                    savePreview();
                    refreshWidget();
                });
            }
        }, function () {
            $(this).find(('.layer')).remove();
        });
    }

    function dragItem() {
        $('.col-sm-3 [data-draggable="item"]').draggable({
            cursor: 'move',
            helper: "clone",
            revert: "",

            drag: function (event, ui) {
                ui.helper.addClass("draggable");
            },
            stop: function (event, ui) {

            }
        });


    }

    dragItem();


    removeBtn();

    $('#tartalmi-zona[data-draggable="target"]').droppable({
        activeClass: "ui-state-hover",
        hoverClass: "ui-state-active",
        axis: "x",
        drop: function (event, ui) {
            if(!ui.draggable.is('li')) {
                if (ui.draggable.hasClass('dropped-down')) {
                    item = ui.draggable.appendTo($(this));
                }
                else {
                    var item = ui.draggable.clone().addClass('dropped-down').appendTo($(this));
                }
                savePreview();
                refreshWidget();
                removeBtn();

            }

        },
        over: function (event, ui) {
            // console.log(event.target);

        }


    });
    $('.builder-container .widget-list-item').addClass('dropped-down');
    /* $('#fejlec-zona,#hirdetes-zona,#hirdetes-zona-2,#tartalmi-zona,#lablec-zona').sortable({
         revert:true,
     });*/
    $('.builder-container .row.ui-droppable').sortable({
        cursor: 'move',

        containment: '.builder-container',
        placeholder: 'highlight',
        // dropOnEmpty: true,
        tolerance: "pointer",
        forcePlaceholderSize: true,
        connectWith: '.builder-container .row.ui-droppable',
    });


    $('.addBlock').click(function () {

        $('#tartalmi-zona').append('<div class="recze col-md-12 ui-draggable widget-content ui-draggable-handle dropped-down panel panel-default" style="min-height: 50px;padding:5px">' +
            '<div> ' +
            '<button type="button" class="btn btn-warning btn-md addWidget " data-toggle="modal" data-target="#addWidget" >Ăj widget hozzĂĄadĂĄsa</button>' +
            '<button type="button" class="btn btn-danger btn-md deleteBlock " >Blokk tĂśrlĂŠse</button>' +
            '</div>' +

            '<div class="widget-container"></div>' +
            '</div>');


        $('html, body').animate({
            scrollTop: $(document).height()
        }, 2000);

    });
    $(document).on('click', '.deleteBlock ', function () {
        $(this).parent().parent().remove();
        savePreview();
        saveTemplate();

    });
    $(document).on('click', '.addWidget', function () {
        var index = $(this).parent().parent().index();
        var edit = null;

        if($(this).attr('data-edit') )
        {
            edit = 'edit';
        }
        $('#addWidget').data('index',index);
        $('#addWidget').data('edit',edit);
        //  console.log(edit);
        var model=$(this).data('model-id');
        if( model >0 )
        {
            var model="/"+model;
        }
        else
        {
            model="";
        }
        $.ajax({
            type: 'GET',
            dataType: 'html',
            url: '/admin/widget/edit'+model,

            success: function (data) {
                // console.log("ok");


                $('#addWidget form').html(data);
                $('.cont [data-draggable="item"]').draggable({
                    cursor: 'move',
                    helper: "clone"
                });
                $('input[name="size"]').trigger('change');
                $(".double-target").droppable({
                    drop: function (event, ui) {
                        if ($(event.target).hasClass('double-target')) {
                            var first = true;
                            var itemid = $(event.originalEvent.toElement).attr("itemid");

                            $('.cont [data-draggable="item"]').each(function () {

                                if (($(this).attr("itemid") === itemid) && first) {
                                    var item = $(this).clone();
                                    var elem = '<div class="form-inline btn btn-default">' +
                                        '<input type="hidden" name="article_id[]" value="' + $(this).data('id') + '">' +
                                        '<input type="hidden" name="type[]" value="' + $(this).data('type') + '">' +
                                        $(item).removeClass('btn btn-default').prop('outerHTML') +
                                        '<a class="btn btn-xs btn-danger trash"><i class="fa fa-trash"></i></a>' +
                                        '</div>';

                                    //elem.appendTo(".double-target");
                                    $('.double-target').append(elem);
                                    first = false;

                                }
                            });
                        }
                    }
                });

            },

        });
    });
    $('#form-configuration').on('submit',function(event){

        saveTemplate();
        savePreview();


    });
    var currentItem = null
    if($('.successLine').length > 0) {
        setTimeout(function(){
            $('.successLine').addClass('in');
        }, 500);
        setTimeout(function(){
            $('.successLine').removeClass('in');
            $(".successLine").remove();
        }, 5000);
    }
    $(document).on('change','.category-block li input[type="checkbox"]',function() {
        var child =$(this).closest('li').next('li');
        ///console.log($(this).is(':checked'));
        if($(this).is(':checked'))
        {
            $(child).find('ul li').each(function(){

                $(this).find('input[type="checkbox"]').attr('checked', true);;
                $(this).find('input[type="checkbox"]').prop('checked', true);

            });
        }
        else
        {
            $(child).find('ul li').each(function(){

                $(this).find('input[type="checkbox"]').attr('checked', false);
                $(this).find('input[type="checkbox"]').prop('checked', false);
            });
        }

    });
    $(document).on('click', '#widgetSave', function () {

        var id = $('#addWidget form').find('input[name="id"]').val();
        if (id.length) {
            id = '/' + id;
        }
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/admin/widget/edit' + id,
            data: $('#addWidget form').serializeArray(),
            success: function (data) {
                if (data.status === "ok") {
                    var index = $('#addWidget').data('index');


                    var item = $('#tartalmi-zona .widget-content:nth-child(' + (index + 1) + ')');
                    var html = "<div class=''  style='min-height:50px;' data-slug='" + data.slug + "'></div>";

                    //  console.log($('#addWidget').data('edit'));
                    if($('#addWidget').data('edit') != 'edit')
                    {
                        item.find('.widget-container').append(html);
                        // console.log('append');
                    }

                    savePreview();
                    refreshWidget();
                    $('#addWidget').modal('hide');



                }
                else {
                    $('#addWidget form').html(data.html);
                    $('.cont [data-draggable="item"]').draggable({
                        cursor: 'move',
                        helper: "clone"
                    });
                    $('#addWidget form .has-error input').focus();
                    $('#addWidget ').animate({
                        scrollTop: $("#addWidget form .has-error input").offset().top
                    }, 1000);
                    $('input[name="size"]').trigger('change');
                    console.log('change');
                    $(".show_manual .double-target").droppable({
                        drop: function (event, ui) {
                            if ($(event.target).hasClass('double-target')) {
                                var first = true;
                                var itemid = $(event.originalEvent.toElement).attr("itemid");

                                $('.cont [data-draggable="item"]').each(function () {

                                    if (($(this).attr("itemid") === itemid) && first) {
                                        var item = $(this).clone();
                                        var elem = '<div class="form-inline btn btn-default">' +
                                            '<input type="hidden" name="article_id[]" value="' + $(this).data('id') + '">' +
                                            '<input type="hidden" name="type[]" value="' + $(this).data('type') + '">' +
                                            $(item).removeClass('btn btn-default').prop('outerHTML') +
                                            '<a class="btn btn-xs btn-danger trash"><i class="fa fa-trash"></i></a>' +
                                            '</div>';

                                        //elem.appendTo(".double-target");
                                        $('.show_manual .double-target').append(elem);
                                        first = false;

                                    }
                                });
                            }
                        }
                    });
                }


            },

        });
    });

    function refreshWidget()
    {
        $('#tartalmi-zona [data-slug]').each(function()
        {
            //   console.log($(this).data('slug'));
            var _slug = $(this).data('slug');
            var item = $(this);
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: "/admin/sitebuilder/get-widget",

                data: {
                    slug: _slug
                },
                success: function (data) {
                    // console.log(data);
                    html = $.parseHTML( data ),
                        item.removeClass('bg-success card card-default');
                    item.attr('style','');
                    item.html(html);

                },
                error: function (data) {
                    //  console.log(data);
                }
            });
        });
        saveTemplate();
        removeBtn();

    }
    function saveTemplate()
    {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "/admin/sitebuilder/save-template",
            data: {

                content: $('#tartalmi-zona').html(),

                url: $('input[name="url"]').val()
            },
            success: function (data) {


            },
        });
    }

    function savePreview() {
        var response = [];
        response['content'] = [];
        var index = 0;
        var content = $('.builder-container #content [data-slug]').each(function () {
            var parent_index = $('div').index($(this).closest('.widget-content'));
            if( index < parent_index )
            {
                response['content'].push('clearfix');
            }
            index = parent_index;


            var data_slug = $(this).data('slug');
            // console.log(data_slug);
            response['content'].push(data_slug);

        });
        //console.log(response);

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "/admin/sitebuilder/save-preview",
            data: {

                content: JSON.stringify(response['content']),

                url: $('input[name="url"]').val()
            },
            success: function (data) {

                refreshWidget();
            },
            error: function (data) {
                // console.log(data);
            }
        });
    }


});


$(document).ready(function() {
    $(document).on('change','input[name="size"]',function(){

        var cls = '.l'+$('input[name="size"]:checked').val();
        $('.layout').attr('disabled','disabled');
        $(cls).removeAttr('disabled');


    });

    $(document).on('click','.trash',function(e){
        e.preventDefault();
        $(this).parent().remove();
    });

    $(document).on('click','#getWidgetContent',function(event){
        event.preventDefault();
        var _model_id= $(this).data('model_id');
        var selectedCategories=[];
        $(".category-block input:checkbox:checked").map(function(){
            selectedCategories.push($(this).attr('name'));
        });
        var _type=$('select[name="manual"] option:selected').val();

        $.ajax({
            type:'post',
            url:'/admin/widget/get-widget-content',
            data:{model_id:_model_id,type:_type,category:selectedCategories},
            dataType:'html',
            success:function(data)
            {
                $('#widget_content').html(data);
                $('.cont [data-draggable="item"]').draggable({
                    cursor: 'move',
                    helper: "clone"
                });

                $(".double-target").droppable({
                    drop: function(event, ui) {
                        //  console.log(event.target);
                        //  console.log($(event.target).hasClass('double-target'));
                        if($(event.target).hasClass('double-target')) {
                            var first = true;
                            var itemid = $(event.originalEvent.toElement).attr("itemid");
                            $('.cont [data-draggable="item"]').each(function () {

                                if (($(this).attr("itemid") === itemid) && first) {
                                    var item = $(this).clone();
                                    var elem = '<div class="form-inline btn btn-default">' +
                                        '<input type="hidden" name="article_id[]" value="' + $(this).data('id') + '">' +
                                        '<input type="hidden" name="type[]" value="' + $(this).data('type') + '">' +
                                        $(item).removeClass('btn btn-default').prop('outerHTML') +
                                        '<a class="btn btn-xs btn-danger trash"><i class="fa fa-trash"></i></a>' +
                                        '</div>';

                                    //elem.appendTo(".double-target");
                                    $('.double-target').append(elem);
                                    first = false;

                                }
                            });
                        }
                    }
                });
            }
        });
    });
    $(document).on('keyup','#filter',function(){
        var value = $(this).val();

        $('[data-draggable="target"] > li').each(function() {

            if ($(this).text().search(value) > -1) {
                $(this).show();
            }
            else {
                $(this).hide();
            }
        });
    });

});
