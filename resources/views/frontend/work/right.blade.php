<div class="col-sm-9 padding-left">
   @php
		   @endphp
	<hr>
   <div class="row">
      <div class="col-md-8">
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a class="breadcrumb-item" href="/munkak">Összes kategória </a></li>
			 @if(!is_null($headCat))
				 <li class="breadcrumb-item"><a class="breadcrumb-item" href="/munkak/{{$headCat->slug}}">{{$headCat->name}}</a></li>
			 @endif
			 @if(!is_null($subCat) && !is_null($headCat))
				 <li class="breadcrumb-item"><a class="breadcrumb-item" href="/munkak/{{$headCat->slug}}/{{$subCat->slug}}">{{$subCat->name}}</a></li>
			 @endif
         </ol>
      </div>
      <div class="col-md-4">
         <form action="/munkak" method="get" accept-charset="utf-8" class="form form-horizontal">
            <div class="form-group">
               <label class="control-label col-md-4">Város</label>
               <div class="col-md-8">
                  <select id="form-city" name="city" class="form-control" onchange="this.form.submit()" data-generated="true">
					  <option value="">Kérem válasszon</option>
                     @foreach($city as $c)
						  <option value="{{$c->city_id}}" @if($c->city_id== array_get($_GET,'city')) selected="selected" @endif>{{$c->city_name}}</option>
					 @endforeach
                  </select>
               </div>
            </div>
         </form>
      </div>
   </div>
	@if(!is_null($hasWork))
		<div class="panel panel-default">
      <div class="panel-heading kilencedik">
         <h3>{{$hasWork->name}}</h3>
      </div>
      <div class="panel-body">
         <h4 class="text-left">{{$hasWork->lead}} </h4>
         <hr>
         <div class="col-sm-8">
			 <div style="background: #fff   url('{{$hasWork->image}}')   center center no-repeat; background-size: cover;height:300px"></div>

         </div>
         <div class="col-sm-4">
			 @if($hasWork->is_full == 0 )
            	<button class="btn btn-warning btn-lg form-control" style="padding: 6px 12px;" data-toggle="modal" data-target="#workModal">Jelentkezem a munkára</button>
            	<br>
            	<br>
				 <div style="background: #fff   url('https://www.iconexperience.com/_img/g_collection_png/standard/512x512/hand_touch.png')   center center no-repeat; background-size: cover;height:300px"></div>

			@endif
         </div>
         <div class="clearfix"></div>
         <hr>
         <div class="text-center">
            <div class="fb-like pull-right fb_iframe_widget" data-url="{{url()->current()}}" data-share="true" data-layout="button_count" data-size="small" data-mobile-iframe="true" data-show-faces="false" ></div>
            <div class="clearfix"></div>
         </div>
         <div>
			 @if($hasWork->task)
				<h5>Feladatok</h5>
				<div class="col-md-11 col-md-offset-1">
				   {!! $hasWork->task !!}
				</div>

				<div class="clearfix"></div>
				<hr>
			 @endif
				 @if($hasWork->conditions)
					 <h5>Feltételek</h5>
					 <div class="col-md-11 col-md-offset-1">
				   {!! $hasWork->conditions !!}
				</div>

					 <div class="clearfix"></div>
					 <hr>
				 @endif
				 @if($hasWork->preference)
					 <h5>Feltételek/előnyök</h5>
					 <div class="col-md-11 col-md-offset-1">
				   {!! $hasWork->preference !!}
				</div>

					 <div class="clearfix"></div>
					 <hr>
				 @endif
				 @if($hasWork->offer)
					 <h5>Amit kínálunk</h5>
					 <div class="col-md-11 col-md-offset-1">
				   {!! $hasWork->offer !!}
				</div>

					 <div class="clearfix"></div>
					 <hr>
				 @endif
				 @if($hasWork->start)
					 <h5>Kezdés</h5>
					 <div class="col-md-11 col-md-offset-1">
				   {!! $hasWork->start !!}
				</div>

					 <div class="clearfix"></div>
					 <hr>
				 @endif
				 @if($hasWork->time)
					 <h5>Munkaidő</h5>
					 <div class="col-md-11 col-md-offset-1">
				   {!! $hasWork->time !!}
				</div>

					 <div class="clearfix"></div>
					 <hr>
				 @endif
				 @if($hasWork->work_time)
					 <h5>Munka időtartama</h5>
					 <div class="col-md-11 col-md-offset-1">
				   {!! $hasWork->work_time !!}
				</div>

					 <div class="clearfix"></div>
					 <hr>
				 @endif
				 @if($hasWork->interview)
					 <h5>Interjú</h5>
					 <div class="col-md-11 col-md-offset-1">
				   {!! $hasWork->interview !!}
				</div>

					 <div class="clearfix"></div>
					 <hr>
				 @endif
				 @if($hasWork->wage)
					 <h5>Bruttó bér</h5>
					 <div class="col-md-11 col-md-offset-1">
				   {!! $hasWork->wage !!}
				</div>

					 <div class="clearfix"></div>
					 <hr>
				 @endif
				 @if($hasWork->work_place)
					 <h5>Munkavégzés helye</h5>
					 <div class="col-md-11 col-md-offset-1">
				   {!! optional(\Modules\City\Entities\City::find($hasWork->city))->name !!}
				</div>

					 <div class="clearfix"></div>
					 <hr>
				 @endif

				 @if($hasWork->comment)
					 <h5>Megjegyzés</h5>
					 <div class="col-md-11 col-md-offset-1">
				   {!! $hasWork->comment !!}
				</div>

					 <div class="clearfix"></div>
					 <hr>
				 @endif



            <h5>Projekt menedzser</h5>
				 @php
				  $user= \Modules\User\Entities\User::find($hasWork->responsible);
				@endphp
				 <div class="col-md-11 col-md-offset-1">{{$user->lastname}} {{$user->firstname}}<br>Email: <a href="mailto:{{str_replace('@','##at##',$user->email)}}" class="gaShowE"><span>{{str_replace('@','##at##',$user->email)}}</span> <i class="fa fa-phone"></i></a><br>Telefon: <b><a href="tel:{{$user->phone}}" class="gaShow">{{$user->phone}} <i class="fa fa-phone"></i></span></a></b></div>
            <div class="clearfix"></div>
            <hr>
				 @if($hasWork->apply)
					 <h5>Jelentkezés</h5>
					 <div class="col-md-11 col-md-offset-1">
				   {!! $hasWork->apply !!}
				</div>

					 <div class="clearfix"></div>
					 <hr>
				 @endif
				 <div id="workModal" class="modal fade" role="dialog">
					  <div class="modal-dialog">
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Munkajelentkezés</h4>
						  </div>
						  <div class="modal-body">
							   <h4 class="text-left">{{$hasWork->lead}} </h4>
							   <b>Projekt menedzser</b>
							  @php
								  $user= \Modules\User\Entities\User::find($hasWork->responsible);
									$model=[];

							  	if(array_get($_COOKIE,'worker')){
							  		$model= json_decode($_COOKIE['worker']);
							  	}

							  @endphp
							  <div id="form-content">
							  <div class="col-md-11 col-md-offset-1">{{$user->name}}<br>Email: <a href="mailto:{{str_replace('@','##at##',$user->email)}}" class="gaShowE"><span>{{str_replace('@','##at##',$user->email)}}</span> <i class="fa fa-phone"></i></a><br>Telefon: <b><a href="tel:{{$user->phone}}" class="gaShow">{{$user->phone}} <i class="fa fa-phone"></i></span></a></b></div>
								<div class="clearfix"></div>
								<hr>
								@include('frontend.work.work_modal',['id'=>$hasWork->id,'error'=>array(),'model'=>$model])
							  </div>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-success apply" style="padding: 6px 12px;height: auto;font-size: 12pt" >Jelentkezés</button>
							<button type="button" class="btn btn-danger" style="padding: 6px 12px;height: auto;" data-dismiss="modal">Bezár</button>
						  </div>
						</div>

					  </div>
					</div>
         </div>
      </div>
   </div>
	@else
		@foreach($works as $w)
			<div class="panel panel-default ">
      <div class="panel-heading kilencedik-2 work-back">{{$w->name}}</div>
      <div class="panel-body ">
         <div class="col-sm-3">
            {!! view('frontend.partials.img',['alt'=>$w->name,'src'=>$w->image]) !!}
         </div>
         <div class="col-sm-9">{{$w->lead}}<br></div>
         <a class="btn btn-success" href="/munkak/{{$w->full_url}}">Részletek</a>
      </div>
   </div>
		@endforeach
	@endif
</div>