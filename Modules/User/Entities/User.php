<?php

namespace Modules\User\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
class User extends Authenticatable
{

    use Notifiable, SoftDeletes,Cachable,HasApiTokens;

    protected $dates = ['deleted_at'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lastname',
        'firstname',
        'name',
        'email',
        'active',
        'avatar',
        'signature',
        'role',
        'shop_id',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	public static function getSelectList()
	{
		return User::select(DB::raw("concat(lastname,' ',firstname) as name,id"))->pluck('name','id')->toArray();
	}
	public static function getEmailList()
	{
		return User::select(DB::raw("concat(lastname,' ',firstname) as name,email"))->pluck('name','email')->toArray();
	}
    public function roles()
    {
        return $this->belongsToMany('Modules\Acl\Entities\Role');
    }


    protected $searchColumns = [
        'id' => 'text',
        'created_at' => 'date_interval',
        'name' => 'text',
        'email' => 'text',
    ];

    protected $searchColumnLabels = [
        'id' => 'ID',
        'created_at' => 'Regisztráció dátuma',
        'name' => 'Név',
        'email' => 'Email',
    ];

    public $extraColumns =[
        'name'=>"Concat(lastname,' ','firstname')",
    ];

    public function getName()
    {
        return $this->lastname.' '.$this->firstname;
    }

    public function getAvatar()
    {
        if (!is_null($this->avatar))
        {
            return url($this->avatar);
        }

        return url('/assets/app/media/img/users/default-user.png');
    }

    public function getUserArray($model)
    {
        $arr = [];
        foreach ($model as $elem) {
            $arr[$elem->id] = $elem->last_name . ' ' . $elem->firstname;
        }

        return $arr;
    }

    public static function getSuperAdmins()
    {
        return User::with('roles')->whereHas('roles', function ($query) {
            $query->where('id', '=', 1);
        })->get();
    }

    public function drawSearchTableHeader($filter)
    {

        $tableHeaderRow = "
        <div class=\"m-portlet m-portlet--primary m-portlet--head-solid-bg m-portlet--head-sm m-portlet--collapse\" m-portlet=\"true\" >
        <form method='get' class='filterTable'>
        <input type='hidden' name='direction' value='" . old('direction', $filter['direction']) . "'>
		<input type='hidden' name='orderBy' value='" . old('orderBy', $filter['orderBy']) . "'>
        <div class=\"m-portlet__head\">
        		<div class=\"m-portlet__head-caption\">
					<div class=\"m-portlet__head-title\">
						<span class=\"m-portlet__head-icon\">
								<i class=\"flaticon-search-2\"></i>
						</span>
						<h3 class=\"m-portlet__head-text d-none d-md-table-cell\">
						" . __('Keresés') . "
						</h3>
					
						
					</div>
				</div>
				<div class=\"m-portlet__head-tools\">
					<ul class=\"m-portlet__nav\">
												<li class=\"m-portlet__nav-item\">
													<input class=\"form-control m-input\" placeholder='" . __('Keresés') . "' type=\"text\" value=\"" . old('search-fillable', $filter['search-fillable']) . "\" name=\"search-fillable\" id=\"example-text-input\">
												</li>
												
					<li  class=\"m-portlet__nav-item\">
					<div class='btn-group' role='group' aria-label='" . __('Keresés') . "'>
						<a href='?' class='m-btn btn btn-danger'>
							<i class='fa fa-history'></i>
						</a>
						<button type='submit' class='m-btn btn btn-success'>
							<i class='fa fa-filter'></i>
						</button>
						<a class='m-btn btn btn-warning'  data-toggle=\"collapse\" href=\"#search-header\" role=\"button\" aria-expanded=\"false\" aria-controls=\"search-header\">
														<i class=\"la la-angle-down\"></i>
													</a>
					</div>
					</li>
					</ul>
					
				</div>
		</div>
		<div  class=\"m-portlet__body collapse\" id=\"search-header\">
        <div class='row'>
        ";
        foreach ($this->searchColumns as $key => $col) {
            switch ($col) {
                case 'text' :
                    $tableHeaderRow .= '<div class="col-md-6 col-lg-4 col-xl-2">' . \App\Http\Helper::inputInlineLabel($this->searchColumnLabels[$key], $key, old($key, $filter[$key]), array('class' => 'form-control input-sm')) . '</div>';
                    break;
                case 'interval':
                    $tableHeaderRow .= '<div class="col-md-6 col-lg-4 col-xl-2">' . \App\Http\Helper::inputInlineLabel($this->searchColumnLabels[$key] . ' -tól', $key . '_from', old($key . '_from', $filter[$key . '_from']), array('class' => 'form-control input-sm')) . '</div><div class="col-md-6 col-lg-4 col-xl-2">'
                        . \App\Http\Helper::inputInlineLabel($this->searchColumnLabels[$key] . ' -ig', $key . '_to',
                            old($key . '_to', $filter[$key . '_to']), array('class' => 'form-control input-sm')) . '</div>';
                    break;
                case 'date_interval':
                    $tableHeaderRow .= '<div class="col-md-6 col-lg-4 col-xl-2">' . \App\Http\Helper::inputInlineLabel($this->searchColumnLabels[$key], $key . '_from_to', old($key . '_from_to', array_get($filter, $key . '_from_to', null)), array('class' => 'form-control  daterangepicker-liquid')) . '</div>';
                    break;
                default:
                    if (is_array($col)) {
                        $tableHeaderRow .= '<div class="col-md-6 col-lg-4 col-xl-2">' . \App\Http\Helper::selectInlineLabel($this->searchColumnLabels[$key], $key, $col['select'], old($key, $filter[$key]), array()) . '</div>';
                    }
                    break;


            }
        }

        return $tableHeaderRow . "</div></div></form></div>";
    }

    public function searchInModel($filter, $query = false)
    {

        if ($query == false) {
            $class = get_class($this);
            $list = $class::where($class::getTable() . '.id', '>', 0);
        } else {

            $list = $query;
        }
        $array_search = $this->searchColumns;
        if (isset($filter['search-fillable']) && !empty($filter['search-fillable'])) {
            $where = "";
            foreach ($this->searchColumns as $k => $v) {
                if (isset($this->extraColumns[$k])) {
                    $where .= $this->extraColumns[$k] . " LIKE '%" . $filter['search-fillable'] . "%' or ";
                } else {
                    $where .= $k . " LIKE '%" . $filter['search-fillable'] . "%' or ";
                }

            }
            if (!empty($where)) {
                $list = $list->whereRaw(DB::raw('(' . substr($where, 0, strlen($where) - 3)) . ')');
            }
        }
        foreach ($filter as $key => $value) {

            $is_interval = false;

            if (strpos($key, '_from_to') !== false) {

                $is_interval = true;

            }
            if ($is_interval) {
                $actual_key = str_replace('_from_to', '', $key);


                $from_to = explode(" - ", $filter[$actual_key . '_from_to']);
                $from = $from_to[0];
                if (isset($from_to[1])) {
                    $to = $from_to[1];
                }


                if (isset($from) && !empty($from) && $from != $to) {
                    if (isset($this->extraColumns[$actual_key])) {
                        $list = $list->where(DB::raw($this->extraColumns[$actual_key]), '>=', $from);
                    } else {
                        $list = $list->where($actual_key, '>=', $from);
                    }

                }
                if (isset($to) && !empty($to) && $from != $to) {
                    if (isset($this->extraColumns[$actual_key])) {
                        $list = $list->where(DB::raw($this->extraColumns[$actual_key]), '<=', $to);
                    } else {


                        $list = $list->where($actual_key, '<=', $to);
                    }

                }
                unset($array_search[$actual_key]);

            } else {

                if (!empty($value) && $value !== '-' && array_key_exists($key, $this->searchColumns)) {
                    if (isset($this->extraColumns[$key])) {
                        $list = $list->where(DB::raw($this->extraColumns[$key]), 'LIKE', '%' . $value . '%');
                    } else {
                        $list = $list->where($key, 'LIKE', '%' . $value . '%');
                    }
                }
            }


        }
        if (!empty($filter['direction']) && !empty($filter['orderBy'])) {
            if (isset($this->extraColumns[$filter['orderBy']])) {
                $list = $list->orderBy(DB::raw($this->extraColumns[$filter['orderBy']]), $filter['direction']);
            } else {
                $list = $list->orderBy($filter['orderBy'], $filter['direction']);
            }
        }

        return $list;

    }
}
