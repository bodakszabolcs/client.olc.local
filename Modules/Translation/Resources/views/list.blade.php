@extends('admin::layouts.layout')
@section('content')
    @php
        $langs =config('app.languages');

    @endphp
    <div class="row">
        <div class="col-12">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="head-btn">
                            <h3 class="m-portlet__head-text">{{__('Fordítások')}}</h3>




                    </div>
                    <div class="m-portlet__head-tools" >
                        <div class="form-group pull-right">

                            <input type="text" id="filter" placeholder="Keresés" value="" class="form-control m-input">
                        </div>
                    </div>
                </div>

            <div class="m-portlet__body">


                        <ul class="nav nav-tabs nav-fill"
                            role="tablist">
                            @foreach($langs as $k=> $lang)

                                @if(file_exists(base_path('resources/lang/'.$k.'.json')))

                                    <li class="nav-item">
                                        <a class="nav-link  @if($loop->iteration ==1) active @endif" data-toggle="tab" href="#{{$k}}"
                                           role="tab">
                                            <i class="flaticon-share m--hide"></i>
                                            {{$lang}}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>




                <div class="tab-content">
                   @foreach($langs as $k=> $lang)

                    @if(file_exists(base_path('resources/lang/'.$k.'.json')))
                        @php
                            $data = file_get_contents(base_path('resources/lang/'.$k.'.json'));

                            $avilable= \json_decode($data,true);


                        @endphp
                    @if(is_array($avilable))

                                <div class="tab-pane @if($loop->iteration ==1) active @endif " id="{{$k}}">
                                    <table class='table table-hover table-condensed table-bordered tab-table' data-lang="{{$k}}">
                            <thead>
                            <tr>
                                <th>{{__('Kulcs')}}</th>
                                <th>{{__('Érték')}}</th>
                                <th width="10%">{{__('Művelet')}}</th>

                            </tr>
                            </thead>
                            <tbody>
                        @foreach($avilable as $k=>$v)
                            <tr data-key="{{$k}}">
                            <td >{{$k}}</td>
                            <td >{{$v}}</td>
                                <td>
                                    <button type="button"  class="editButton btn btn-info btn-sm">{{  __('Szerkeszt') }} </button>
                                    <button type="button"  class="saveButton btn btn-success btn-sm" >{{  __('Mentés') }}</button>
                                </td>
                            </tr>
                        @endforeach
                            </tbody>
                        </table>
                                </div>
                     @endif
                     @endif




                @endforeach
                </div>

            </div>
        </div>
        </div>
    </div>
    <style>
        .form-group {
            display: table-cell;
            width: auto;
        }

        .saveButton,
        td[contenteditable]+ td .editButton{
            display: none;
        }

        .editButton,
        td[contenteditable] + td .saveButton {
            display: inline; /* For IE */
            display: inline-block;
        }
        .btn-group-sm>.btn, .btn-sm {
            padding: .35rem .75rem !important;

        }
        td[contenteditable] {
            /* background: #ddddff; */
            border: dotted 2px red;
        }
    </style>
@endsection
