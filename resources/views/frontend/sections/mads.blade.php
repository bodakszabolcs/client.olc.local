@php
	$diakmunka = \Modules\Content\Entities\Content::find(5);

@endphp
@if(!is_null($diakmunka))
<section class="otodik" id="mads">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-3 text-center">
				<img style="" src="/assets/frontend//img/madslogo_allo_feher.png" title="diákmunka" alt="diákmunka">
				<h3>{{$diakmunka->title}}</h3>
			</div>
		</div>
		<div class="row">
			{!! $diakmunka->content !!}
		</div>
		<div class="col-lg-8 col-lg-offset-2 text-center">
			<a href="https://www.mads.hu" class="btn btn-lg btn-outline">
				Ajánlatkérés
			</a>
		</div>
	</div>

</section>
@endif