@extends('admin::layouts.layout')
@section('content')
	<?php
		$errors= Session::get('field_errors');
		Session::forget('field_errors');
	?>
<div class='row'>
		<div class='col-md-12'>
			<div class='m-portlet'>
				<div class='m-portlet__body'>
					<input type='hidden' name='tab' value='data'>
					<div class = 'row'>
						<div class = 'col-md-12'>	
		{!!  \App\Http\Helper::formOpen('PurchaseForm','post',NULL,['class'=>'form '])  !!}<div class = 'm-portlet__head'>
									<div class = 'm-portlet__head-caption'>
										<div class = 'm-portlet__head-title'>
											<h3 class = 'm-portlet__head-text'>
												{{__('Tartalom adatok')}}
											</h3>
										</div>
									</div>
								</div>
								<div class = 'm-portlet__body'>
									<!--begin::Widget 29-->
									<div class = 'm-widget29'>
										<div class = 'm-widget_content'>
											<div class = 'm-widget_content-items'>
												<div class='row'>
													<div class='col-md-6'>	<div class="col-md-6">					{!! \App\Http\Helper::input(trans('Felhasználó'),'user_id',old('user_id',$model->user_id),array('class'=>'form-control'),$errors) !!}		
</div>
<div class="col-md-6">					{!! \App\Http\Helper::input(trans('Felhasználónév'),'user_name',old('user_name',$model->user_name),array('class'=>'form-control'),$errors) !!}		
</div>
<div class="col-md-6">					{!! \App\Http\Helper::input(trans('Vásárlás összege'),'price',old('price',$model->price),array('class'=>'form-control'),$errors) !!}		
</div>
<div class="col-md-6">					{!! \App\Http\Helper::input(trans('Kedvezmény'),'discount',old('discount',$model->discount),array('class'=>'form-control'),$errors) !!}		
</div>
										</div>

										</div>
									</div>
								</div>
		 {!! \App\Http\Helper::formButtons('admin_purchase_list') !!}
		 </div>
		 {!! \App\Http\Helper::formClose() !!}

			</div>
					</div>
				</div>
			</div>
		</div>
	</div>



@endsection 