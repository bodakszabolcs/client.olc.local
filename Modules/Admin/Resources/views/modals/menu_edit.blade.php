<style>
    .select2-dropdown {
        z-index: 9001;
    }
</style>
<div class="modal fade slide-up disable-scroll" id="menuEdit" tabindex="-1" role="dialog"
     aria-hidden="false">
    <div class="modal-dialog ">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5 class="modal-title">
                        {{__('Menü szerkesztése')}}
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">
												&times;
											</span>
                    </button>
                </div>
                {!! \App\Http\Helper::formOpen('menu-modal','POST',route('admin_menu_edit', [$model->id]),array('class'=>'form-horizontal', 'role' => 'form','id'=>'menu-form', 'autocomplete' => 'off')) !!}
                <input type="hidden" name="menu_item_id" id="menu_item_id"/>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default form-group-default-select2">
                                <label class="">{{__('Oldalak')}}</label>
                                <select class="full-width menuEditSelect"
                                        data-placeholder="{{__('Kérem válasszon')}}"
                                         name="pages">
                                    <option value="">{{__('Kérem válasszon')}}</option>
                                    @foreach($pages as $key => $val)
                                        <option value="{{$key}}">{{$val}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-group-default" style="padding: 7px">
                                <label class="label-sm">{{__('Egyedi link')}}</label>
                                <input type="text" name="link_url" id="link_url" placeholder="www.google.hu"
                                       class="form-control input-sm">
                            </div>
                            <div class="clearfix m-t-10"></div>
                            <div class="form-group form-group-default" style="padding: 7px">
                                <label class="label-sm">{{__('Link felirat')}}</label>
                                <input type="text" name="link_title" id="link_title" placeholder="Google"
                                       class="form-control input-sm">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="p-t-20 clearfix p-l-10 p-r-10">

                            </div>
                        </div>
                        <div class="col-sm-4 m-t-10 sm-m-t-10">
                            <button type="submit"
                                    class="btn m-btn--square btn-primary btn-block m-t-5 menuFormSubmit">{{__('Mentés')}}</button>
                        </div>
                    </div>
                    {!! \App\Http\Helper::formClose() !!}
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>