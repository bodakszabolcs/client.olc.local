<?php

namespace Tests;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Modules\User\Entities\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase as BaseTestCase;

class AdminHttpTest extends TestCase
{
    protected $user = "";
    protected $password = "";

    public function setUp()
    {
        parent::setUp();
        $this->user = User::find(1);
    }

    public function testTicketHttp()
    {

        $response = $this->actingAs($this->user)
            ->get(route('admin_ticket_list'))
            ->assertStatus(200);


    }

    public function testDashboardHttp()
    {

        $response = $this->actingAs($this->user)
            ->get(route('admin_dashboard'))
            ->assertStatus(200);


    }

    public function testModulesHttp()
    {

        $response = $this->actingAs($this->user)
            ->get(route('admin_modules'))
            ->assertStatus(200);


    }

    public function testCommandsHttp()
    {

        $response = $this->actingAs($this->user)
            ->get(route('admin_run_command'))
            ->assertStatus(200);


    }

    public function testFilesHttp()
    {

        $response = $this->actingAs($this->user)
            ->get(route('admin_files_view'))
            ->assertStatus(200);


    }

    public function testThemesHttp()
    {

        $response = $this->actingAs($this->user)
            ->get(route('admin_theme_edit'))
            ->assertStatus(200);

        $response = $this->actingAs($this->user)
            ->post(route('admin_theme_edit'),
                [
                    'page_js' => '<script type="text/javascript"></script>',
                    'socials' => [
                        5 => "#",
                        6 => "#",
                        7 => "#",
                        8 => "#"
                        ]
                ])
            ->assertStatus(302);

    }

    public function testSlowLogHttp()
    {

        $response = $this->actingAs($this->user)
            ->get(route('admin_slowlog_list'))
            ->assertStatus(200);


    }

    public function testProfileHttp()
    {

        $response = $this->actingAs($this->user)
            ->get(route('admin_profile'))
            ->assertStatus(200);


    }

    public function testAdminLogoutHttp()
    {
        $response = $this->actingAs($this->user)
            ->get(route('admin_logout'))
            ->assertStatus(302);
    }

    public function testAdminLoginHttp()
    {

        $new_user = new User();
        $new_user->lastname = Str::random(5);
        $new_user->firstname = Str::random(5);
        $new_user->email = Str::random(16).'@example.com';

        $this->password = Str::random(10);
        $new_user->password = Hash::make($this->password);
        $new_user->save();

        $response = $this->post(route('admin_login'),
            [
                'email' => $new_user->email,
                'password' => $this->password
            ])
            ->assertStatus(302);


    }

}
