
	<script>

	if ($("#{{$name}}").length) {
        var pieChart{{$name}} = $("#{{$name}}");

        // line chart data
        var pieData = {
            labels: [
				@foreach($labels as $l)
                    '{{$l}}' @if(!$loop->last) , @endif
				@endforeach
			],
            datasets: [

                {

                    borderWidth: 0,
                    data: [
						@foreach($data as $k=> $v)
							  {{$v}} @if(!$loop->last) , @endif
						@endforeach
                    ],
                    backgroundColor: [
						@foreach($data as $k=> $v)
							'{{\Modules\Admin\Entities\Chart::rand_color($loop->iteration)}}' @if(!$loop->last) , @endif
						@endforeach
					],

                    spanGaps: true
                }

                ]
        };

        // line chart init
         new Chart(pieChart{{$name}}, {
            type: 'pie',
            data: pieData,
             options: {
                 showAllTooltips: true,
                 legend: {
                     position: 'bottom',
                     labels: {
                         boxWidth: 15,
                         fontColor: '#3e4b5b'
                     }
                 },
                 animation: {
                     animateScale: true,
                     onAnimationComplete: function () {

                         var ctx = this.chart.ctx;
                         ctx.font = this.scale.font;
                         ctx.fillStyle = this.scale.textColor
                         ctx.textAlign = "center";
                         ctx.textBaseline = "bottom";

                         this.datasets.forEach(function (dataset) {
                             dataset.points.forEach(function (points) {
                                 ctx.fillText(points.value, points.x, points.y - 10);
                             });
                         })
                     }
                 },

             },

        });
    }

	</script>
