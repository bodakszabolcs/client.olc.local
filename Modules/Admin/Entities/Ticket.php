<?php

namespace Modules\Admin\Entities;

use App\LiquidModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends LiquidModel
{
    use SoftDeletes;

    protected $table = 'tickets';

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function user()
    {
        return $this->belongsTo('Modules\User\Entities\User');
    }

    protected $fillable = [
        'user_id', //Megnevezés
        'user_name',
        'type',
        'page',
        'content',
        'status'
    ]
    ;
    public $searchColumns = [
        'content'	=>	'text',
        'type' => 'select',
        'page' => 'text',
        'user_id' => 'select'
    ];

    public $searchColumnLabels = [
        'content'	=>	'Hiba leírás',
        'type' => 'Típus',
        'page' => 'Oldal',
        'user_id' => 'Riporter'
    ];

    protected $casts = [
    ];

    public function __construct($attributes = [])
    {
        config(['laravel-model-caching.cache-prefix' => str_slug(config('app.name'))]);

        parent::__construct($attributes);
    }
}
